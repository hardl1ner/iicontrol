package ua.ii.uvpcontrol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import ua.ii.uvpcontrol.app.security.SecurityConfiguration;
import ua.ii.uvpcontrol.backend.data.content.Ether;
import ua.ii.uvpcontrol.backend.data.content.FileInfo;
import ua.ii.uvpcontrol.backend.data.content.Folder;
import ua.ii.uvpcontrol.backend.data.layer.*;
import ua.ii.uvpcontrol.backend.data.station.Station;
import ua.ii.uvpcontrol.backend.data.station.StationSetting;
import ua.ii.uvpcontrol.backend.data.users.User;
import ua.ii.uvpcontrol.backend.repositories.content.EtherRepository;
import ua.ii.uvpcontrol.backend.repositories.content.FileInfoRepository;
import ua.ii.uvpcontrol.backend.repositories.content.FolderRepository;
import ua.ii.uvpcontrol.backend.repositories.layer.*;
import ua.ii.uvpcontrol.backend.repositories.station.StationRepository;
import ua.ii.uvpcontrol.backend.repositories.user.UserRepository;
import ua.ii.uvpcontrol.backend.service.*;
import ua.ii.uvpcontrol.backend.service.content.EtherService;
import ua.ii.uvpcontrol.backend.service.content.FileInfoService;
import ua.ii.uvpcontrol.backend.service.content.FolderService;
import ua.ii.uvpcontrol.backend.service.layer.*;
import ua.ii.uvpcontrol.backend.service.station.StationSettingService;
import ua.ii.uvpcontrol.backend.service.station.StationService;
import ua.ii.uvpcontrol.ui.MainView;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

@SpringBootApplication(scanBasePackageClasses = { SecurityConfiguration.class, MainView.class, IiControlApplication.class,
		UserService.class, LayerGroupImageService.class, LayerGroupPanelService.class,LayerGroupImageService.class, LayerGroupPanelService.class,
		LayerGroupTextService.class, LayerGroupVideoService.class,
		LayerGroupTextService.class, LayerGroupVideoService.class, LayerImageService.class, LayerPanelService.class,
		LayerTextService.class, LayerVideoService.class, StationService.class, EtherService.class},
		exclude = ErrorMvcAutoConfiguration.class)
@EnableJpaRepositories(basePackageClasses = {LayerGroupImageRepository.class, LayerGroupPanelRepository.class,
		LayerGroupTextRepository.class, LayerGroupVideoRepository.class, LayerImageRepository.class,
		LayerPanelRepository.class, LayerTextRepository.class, LayerVideoRepository.class, UserRepository.class,
		 StationRepository.class, FolderRepository.class, FileInfoRepository.class, EtherRepository.class})
@EntityScan(basePackageClasses = { User.class,  Station.class, Folder.class, FileInfo.class,
		LayerGroupImage.class, LayerGroupPanel.class, LayerGroupText.class, LayerGroupVideo.class,
		LayerImage.class, LayerPanel.class, LayerText.class, LayerVideo.class, Ether.class})
public class IiControlApplication extends SpringBootServletInitializer {

	@Bean
	public ApplicationListener<ContextRefreshedEvent> initDatabase(StationService stationService,
																   StationSettingService stationSettingService,
		FolderService folderService, FileInfoService fileInfoService, LayerGroupImageService layerGroupImageService,
		LayerGroupPanelService layerGroupPanelService, LayerGroupTextService layerGroupTextService,
		LayerGroupVideoService layerGroupVideoService, LayerImageService layerImageService,
		LayerPanelService layerPanelService, LayerTextService layerTextService,
		LayerVideoService layerVideoService, EtherService etherService) {
		return event -> {
			if (stationService.findAll().size() == 0) {
				createDemoData(stationService, stationSettingService, folderService,
						fileInfoService, layerGroupImageService, layerGroupPanelService, layerGroupTextService,
						layerGroupVideoService, layerImageService, layerPanelService, layerTextService,
						layerVideoService, etherService);
			}
		};
	}


	private void createDemoData(StationService stationService,
								StationSettingService stationSettingService, FolderService folderService,
								FileInfoService fileInfoService, LayerGroupImageService layerGroupImageService,
								LayerGroupPanelService layerGroupPanelService, LayerGroupTextService layerGroupTextService,
								LayerGroupVideoService layerGroupVideoService, LayerImageService layerImageService,
								LayerPanelService layerPanelService, LayerTextService layerTextService,
								LayerVideoService layerVideoService, EtherService etherService) {

		List<Folder> folders = new ArrayList<>();

		FileInfo fileInfo = new FileInfo("test",
				5L,
				100L,
				1920,
				1080,
				"video",
				"wog",
				LocalDate.now());

		fileInfoService.save(fileInfo);

		folders.add(new Folder("video", null, new LinkedHashSet<>()));
		folders.add(new Folder("image", null, new LinkedHashSet<>()));
		folders.add(new Folder("panel", null, new LinkedHashSet<>(fileInfoService.findAll())));

		folders.forEach(folderService::save);

		LayerImage layerImage = new LayerImage("weather", fileInfoService.findAll().get(0), 190,
				200, 0, 0, true);
		layerImageService.save(layerImage);
		LayerPanel layerPanel = new LayerPanel("weather", folderService.findAll().get(0), 190, 200,
				0, 0, "klip", true);
		layerPanelService.save(layerPanel);
		LayerText layerText = new LayerText("weather", 190, 200, 0, 0,
				32, "wwww", "test", LayerText.StyleFont.bold,
				LayerText.Orientation.horizontal, "522", LayerText.TextAlignment.center,
				LayerText.LocaleLanguage.UA, true, LayerText.Type.text);
		layerTextService.save(layerText);
		LayerVideo layerVideo = new LayerVideo("weather", folderService.findAll().get(0),
				190, 200, 0, 0, true);
		layerVideoService.save(layerVideo);

		LayerGroupImage layerGroupImage = new LayerGroupImage("WOG", null,
				new LinkedHashSet<>(layerImageService.findAll()));
		layerGroupImageService.save(layerGroupImage);
		LayerGroupPanel layerGroupPanel = new LayerGroupPanel("WOG", null,
				new HashSet<>(layerPanelService.findAll()));
		layerGroupPanelService.save(layerGroupPanel);
		LayerGroupText layerGroupText = new LayerGroupText("WOG", null,
				new HashSet<>(layerTextService.findAll()));
		layerGroupTextService.save(layerGroupText);
		LayerGroupVideo layerGroupVideo = new LayerGroupVideo("WOG", null,
				new HashSet<>(layerVideoService.findAll()));
		layerGroupVideoService.save(layerGroupVideo);


		StationSetting stationSetting = new StationSetting("Setting_WOG",
				layerVideoService.findAll().get(0),
				layerGroupImageService.findAll().get(0),
				layerGroupPanelService.findAll().get(0),
				layerGroupTextService.findAll().get(0),
				1920,
				1080,
				0,
				0,
				5, 5, LocalTime.now(), LocalTime.now(), LocalTime.now(), LocalTime.now(),
				LocalTime.now(), LocalTime.now(), LocalTime.now(), true, StationSetting.TypePlayList.indortv,
				folderService.findAll().get(0));

		StationSetting stationSetting2 = new StationSetting("Setting_OKKO",
				layerVideoService.findAll().get(0),
				layerGroupImageService.findAll().get(0),
				layerGroupPanelService.findAll().get(0),
				layerGroupTextService.findAll().get(0), 1920, 1080,0,0,
				5, 5, LocalTime.now(), LocalTime.now(), LocalTime.now(), LocalTime.now(),
				LocalTime.now(), LocalTime.now(), LocalTime.now(), true, StationSetting.TypePlayList.standard,
				null);

		stationSettingService.save(stationSetting);
		stationSettingService.save(stationSetting2);

		Ether ether = new Ether("", fileInfoService.findAll().get(0),Ether.Type.file,LocalDateTime.now(),
				LocalDateTime.now().plusDays(2),
				false, false, false, false,false, false, false);

		Ether ether2 = new Ether("", fileInfoService.findAll().get(0),Ether.Type.file,LocalDateTime.now(),
				LocalDateTime.now().plusDays(2),
				false, false, false, false,false, false, false);

		etherService.save(ether);
		etherService.save(ether2);

		List<Station> stations= new ArrayList<>();
		stations.add(new Station("WOG", null, null, null, null, true,
				null));
		stations.add(new Station("WOG-TV-K-01", stations.get(0), "Наддніпрянське шосе, 8 (ДакКар)",
				stationSettingService.findAll().get(0), new ArrayList<>(etherService.findAll()), true, Station.generateKeyStation()));
		stations.add(new Station("WOG-TV-K-02", stations.get(0), "BOB",
				stationSettingService.findAll().get(0), new ArrayList<>(etherService.findAll()),true, Station.generateKeyStation()));

		stations.forEach(stationService::save);


	}

	public static void main(String[] args) {
		SpringApplication.run(IiControlApplication.class, args);
	}

}
