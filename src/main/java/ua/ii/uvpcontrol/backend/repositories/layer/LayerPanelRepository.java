package ua.ii.uvpcontrol.backend.repositories.layer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.ii.uvpcontrol.backend.data.layer.LayerPanel;

@Repository
public interface LayerPanelRepository extends JpaRepository<LayerPanel, Long>, LayerGroupPanelRepositoryCustom {

}
