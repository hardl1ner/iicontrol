package ua.ii.uvpcontrol.backend.repositories.layer;

import ua.ii.uvpcontrol.backend.data.layer.LayerGroupVideo;

import java.util.List;

public interface LayerGroupVideoRepositoryCustom {
    long getChildCount(LayerGroupVideo parent);
    Boolean hasChildren(LayerGroupVideo parent);
    List<LayerGroupVideo> getChildren(LayerGroupVideo parent);

    void saveAll(List<LayerGroupVideo> list);
}
