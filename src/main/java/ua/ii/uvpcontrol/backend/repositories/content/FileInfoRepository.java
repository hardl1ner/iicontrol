package ua.ii.uvpcontrol.backend.repositories.content;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.ii.uvpcontrol.backend.data.content.Ether;
import ua.ii.uvpcontrol.backend.data.content.FileInfo;

@Repository
public interface FileInfoRepository extends JpaRepository<FileInfo, Long>, FolderRepositoryCustom{
    FileInfo findByid(Long id);
}
