package ua.ii.uvpcontrol.backend.repositories.layer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.ii.uvpcontrol.backend.data.layer.LayerText;

@Repository
public interface LayerTextRepository extends JpaRepository<LayerText, Long>, LayerGroupTextRepositoryCustom {
    LayerText findByid(Long id);
}
