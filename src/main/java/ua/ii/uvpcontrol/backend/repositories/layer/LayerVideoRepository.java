package ua.ii.uvpcontrol.backend.repositories.layer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ua.ii.uvpcontrol.backend.data.layer.LayerVideo;


@Repository
public interface LayerVideoRepository extends JpaRepository<LayerVideo, Long>, LayerGroupVideoRepositoryCustom {
    @Query("select distinct lv from LayerVideo lv left join fetch lv.folder where lv.id=:id")
    LayerVideo findByid(Long id);
}
