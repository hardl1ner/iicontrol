package ua.ii.uvpcontrol.backend.repositories.layer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ua.ii.uvpcontrol.backend.data.layer.LayerGroupPanel;

import java.util.List;

@Repository
public interface LayerGroupPanelRepository extends JpaRepository<LayerGroupPanel, Long>, LayerGroupPanelRepositoryCustom{
    long getChildCount(LayerGroupPanel parent);
    Boolean hasChildren(LayerGroupPanel parent);
    @Query("SELECT DISTINCT l FROM LayerGroupPanel l left join fetch l.parent left join fetch l.layerPanels lp left join fetch lp.folder")
    List<LayerGroupPanel> getChildren(LayerGroupPanel parent);
    @Query("SELECT DISTINCT l FROM LayerGroupPanel l left join fetch l.parent left join fetch l.layerPanels lp left join fetch lp.folder")
    List<LayerGroupPanel> findAll();
    void saveAll(List<LayerGroupPanel> list);
}
