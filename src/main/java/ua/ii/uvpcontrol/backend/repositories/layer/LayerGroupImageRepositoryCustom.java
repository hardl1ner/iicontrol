package ua.ii.uvpcontrol.backend.repositories.layer;

import ua.ii.uvpcontrol.backend.data.layer.LayerGroupImage;

import java.util.List;

public interface LayerGroupImageRepositoryCustom {
    long getChildCount(LayerGroupImage parent);
    Boolean hasChildren(LayerGroupImage parent);
    List<LayerGroupImage> getChildren(LayerGroupImage parent);

    void saveAll(List<LayerGroupImage> list);
}
