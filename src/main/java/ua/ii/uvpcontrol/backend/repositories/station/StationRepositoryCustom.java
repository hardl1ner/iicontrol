package ua.ii.uvpcontrol.backend.repositories.station;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.ii.uvpcontrol.backend.data.station.Station;

import java.util.List;

public interface StationRepositoryCustom{
    long getChildCount(Station parent);
    Boolean hasChildren(Station parent);
    List<Station> getChildren(Station parent);

    void saveAll(List<Station> list);
}
