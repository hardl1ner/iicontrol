package ua.ii.uvpcontrol.backend.repositories.layer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ua.ii.uvpcontrol.backend.data.layer.LayerGroupText;

import java.util.List;

@Repository
public interface LayerGroupTextRepository extends JpaRepository<LayerGroupText, Long>, LayerGroupTextRepositoryCustom{
    long getChildCount(LayerGroupText parent);
    Boolean hasChildren(LayerGroupText parent);
    @Query("SELECT DISTINCT l FROM LayerGroupText l left join fetch l.parent left join fetch l.layerTexts ")
    List<LayerGroupText> getChildren(LayerGroupText parent);
    @Query("SELECT DISTINCT l FROM LayerGroupText l left join fetch l.parent left join fetch l.layerTexts ")
    List<LayerGroupText> findAll();
    void saveAll(List<LayerGroupText> list);
}
