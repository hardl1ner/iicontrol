package ua.ii.uvpcontrol.backend.repositories.station;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.ii.uvpcontrol.backend.data.station.Station;

import java.util.List;
@Repository
public interface StationRepository extends JpaRepository<Station, Long>, StationRepositoryCustom{
    @Query("select distinct s from Station s left join fetch s.parent left join fetch s.stationSetting ss left join fetch " +
            "ss.layerVideo lv left join fetch lv.folder left join fetch " +
            "ss.layerGroupImage lgi left join fetch lgi.layerImages li left join fetch li.fileInfo left join fetch" +
            " ss.layerGroupPanel lgp left join fetch " +
            "lgp.layerPanels lp left join fetch lp.folder left join fetch ss.layerGroupText lt left join fetch " +
            "lt.layerTexts where s.keyStation=:keyStation")
    Station findByKeyStation(@Param("keyStation") String keyStation);
    @Query("select distinct s from Station s where s.address=:address")
    List<Station> findByAddress(@Param("address") String address);
    @Query("select distinct s from Station s left join fetch s.parent left join fetch s.stationSetting left join fetch  s.ethers e left join fetch e.file where s.id=:id")
    Station findByid(@Param("id")Long id);
    List<Station> findAll();
    @Query("select s from Station s join s.ethers e where e.id=:idEther")
    List<Station> searchEther(@Param("idEther") Long idEther);
    @Query("select distinct s from Station s left join fetch s.parent left join fetch s.stationSetting left join fetch  s.ethers e left join fetch e.file")
    long getChildCount(Station parent);
    @Query("select distinct s from Station s left join fetch s.parent left join fetch s.stationSetting left join fetch  s.ethers e left join fetch e.file")
    Boolean hasChildren(Station parent);
    @Query("select distinct s from Station s left join fetch s.parent left join fetch s.stationSetting left join fetch  s.ethers e left join fetch e.file")
    List<Station> getChildren(Station parent);
    void saveAll(List<Station> list);
    @Query("select c from Station c where lower(c.name) like lower(concat('%', :searchTerm, '%')) or lower(c.address) like lower(concat('%', :searchTerm, '%'))")
    List<Station> search(@Param("searchTerm") String searchTerm);
}
