package ua.ii.uvpcontrol.backend.repositories.content;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.ii.uvpcontrol.backend.data.content.Ether;

import java.util.List;

@Repository
public interface EtherRepository extends JpaRepository<Ether, Long>{
    Ether findByid(Long id);
    @EntityGraph(attributePaths = {"file"}, type= EntityGraph.EntityGraphType.FETCH)
    List<Ether> findAll();
}
