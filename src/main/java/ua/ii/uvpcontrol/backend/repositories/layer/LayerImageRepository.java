package ua.ii.uvpcontrol.backend.repositories.layer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.ii.uvpcontrol.backend.data.layer.LayerImage;
import ua.ii.uvpcontrol.backend.data.layer.LayerText;
import ua.ii.uvpcontrol.backend.data.station.Station;

import java.util.List;

@Repository
public interface LayerImageRepository extends JpaRepository<LayerImage, Long>, LayerGroupImageRepositoryCustom {
    @Query("select li from LayerImage li join li.fileInfo f where f.id=:idFile")
    List<LayerImage> searchFile(@Param("idFile") Long idFile);
    LayerImage findByid(Long id);
}
