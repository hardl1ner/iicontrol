package ua.ii.uvpcontrol.backend.repositories.layer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ua.ii.uvpcontrol.backend.data.layer.LayerGroupImage;

import java.util.List;

@Repository
public interface LayerGroupImageRepository extends JpaRepository<LayerGroupImage, Long>, LayerGroupImageRepositoryCustom {
    long getChildCount(LayerGroupImage parent);
    Boolean hasChildren(LayerGroupImage parent);
    @Query("SELECT DISTINCT l FROM LayerGroupImage l left join fetch l.parent left join fetch l.layerImages li left join fetch li.fileInfo")
    List<LayerGroupImage> getChildren(LayerGroupImage parent);
    @Query("SELECT DISTINCT l FROM LayerGroupImage l left join fetch l.parent left join fetch l.layerImages li left join fetch li.fileInfo")
    List<LayerGroupImage> findAll();
    void saveAll(List<LayerGroupImage> list);
}
