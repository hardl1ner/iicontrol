package ua.ii.uvpcontrol.backend.repositories.content;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ua.ii.uvpcontrol.backend.data.content.Folder;

import java.util.List;

@Repository
public interface FolderRepository extends JpaRepository<Folder, Long>, FolderRepositoryCustom{
    long getChildCount(Folder parent);
    Boolean hasChildren(Folder parent);
    @Query("SELECT DISTINCT f FROM Folder f left join fetch f.parent left join fetch f.fileInfos ")
    List<Folder> getChildren(Folder parent);
    Folder findByid(Long id);
    void saveAll(List<Folder> list);
    @Query("SELECT DISTINCT f FROM Folder f left join fetch f.parent left join fetch f.fileInfos ")
    List<Folder> findAll();
}
