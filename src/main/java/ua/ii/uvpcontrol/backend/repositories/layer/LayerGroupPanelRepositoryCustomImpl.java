package ua.ii.uvpcontrol.backend.repositories.layer;

import ua.ii.uvpcontrol.backend.data.layer.LayerGroupPanel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class LayerGroupPanelRepositoryCustomImpl implements LayerGroupPanelRepositoryCustom{
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public long getChildCount(LayerGroupPanel parent) {
        List<LayerGroupPanel> list = entityManager
                .createQuery("select t from LayerGroupPanel t where t.parent = :parent", LayerGroupPanel.class)
                .setParameter("parent", parent).getResultList();
        return list.size();
    }

    @Override
    public Boolean hasChildren(LayerGroupPanel parent) {
        List<LayerGroupPanel> list = entityManager
                .createQuery("select t from LayerGroupPanel t where t.parent = :parent", LayerGroupPanel.class)
                .setParameter("parent", parent).getResultList();
        return !list.isEmpty();
    }

    @Override
    public List<LayerGroupPanel> getChildren(LayerGroupPanel parent) {
        List<LayerGroupPanel> list = entityManager
                .createQuery("select t from LayerGroupPanel t where t.parent = :parent", LayerGroupPanel.class)
                .setParameter("parent", parent).getResultList();
        return list;
    }

    public void saveAll(List<LayerGroupPanel> list) {
        list.forEach(e -> entityManager.persist(e));
    }
}
