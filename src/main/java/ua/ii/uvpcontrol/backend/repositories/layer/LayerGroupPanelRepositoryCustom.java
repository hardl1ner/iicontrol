package ua.ii.uvpcontrol.backend.repositories.layer;

import ua.ii.uvpcontrol.backend.data.layer.LayerGroupPanel;

import java.util.List;

public interface LayerGroupPanelRepositoryCustom {
    long getChildCount(LayerGroupPanel parent);
    Boolean hasChildren(LayerGroupPanel parent);
    List<LayerGroupPanel> getChildren(LayerGroupPanel parent);

    void saveAll(List<LayerGroupPanel> list);
}
