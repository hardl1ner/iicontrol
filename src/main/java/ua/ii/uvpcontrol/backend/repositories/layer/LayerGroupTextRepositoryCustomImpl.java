package ua.ii.uvpcontrol.backend.repositories.layer;

import ua.ii.uvpcontrol.backend.data.layer.LayerGroupText;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class LayerGroupTextRepositoryCustomImpl implements LayerGroupTextRepositoryCustom{
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public long getChildCount(LayerGroupText parent) {
        List<LayerGroupText> list = entityManager
                .createQuery("select t from LayerGroupText t where t.parent = :parent", LayerGroupText.class)
                .setParameter("parent", parent).getResultList();
        return list.size();
    }

    @Override
    public Boolean hasChildren(LayerGroupText parent) {
        List<LayerGroupText> list = entityManager
                .createQuery("select t from LayerGroupText t where t.parent = :parent", LayerGroupText.class)
                .setParameter("parent", parent).getResultList();
        return !list.isEmpty();
    }

    @Override
    public List<LayerGroupText> getChildren(LayerGroupText parent) {
        List<LayerGroupText> list = entityManager
                .createQuery("select t from LayerGroupText t where t.parent = :parent", LayerGroupText.class)
                .setParameter("parent", parent).getResultList();
        return list;
    }

    public void saveAll(List<LayerGroupText> list) {
        list.forEach(e -> entityManager.persist(e));
    }
}
