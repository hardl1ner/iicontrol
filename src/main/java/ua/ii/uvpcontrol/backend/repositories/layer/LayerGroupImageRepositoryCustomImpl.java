package ua.ii.uvpcontrol.backend.repositories.layer;

import ua.ii.uvpcontrol.backend.data.layer.LayerGroupImage;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class LayerGroupImageRepositoryCustomImpl implements LayerGroupImageRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public long getChildCount(LayerGroupImage parent) {
        List<LayerGroupImage> list = entityManager
                .createQuery("select t from LayerGroupImage t where t.parent = :parent", LayerGroupImage.class)
                .setParameter("parent", parent).getResultList();
        return list.size();
    }

    @Override
    public Boolean hasChildren(LayerGroupImage parent) {
        List<LayerGroupImage> list = entityManager
                .createQuery("select t from LayerGroupImage t where t.parent = :parent", LayerGroupImage.class)
                .setParameter("parent", parent).getResultList();
        return !list.isEmpty();
    }

    @Override
    public List<LayerGroupImage> getChildren(LayerGroupImage parent) {
        List<LayerGroupImage> list = entityManager
                .createQuery("select t from LayerGroupImage t where t.parent = :parent", LayerGroupImage.class)
                .setParameter("parent", parent).getResultList();
        return list;
    }

    public void saveAll(List<LayerGroupImage> list) {
        list.forEach(e -> entityManager.persist(e));
    }
}
