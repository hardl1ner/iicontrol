package ua.ii.uvpcontrol.backend.repositories.layer;

import ua.ii.uvpcontrol.backend.data.layer.LayerGroupText;

import java.util.List;

public interface LayerGroupTextRepositoryCustom {
    long getChildCount(LayerGroupText parent);
    Boolean hasChildren(LayerGroupText parent);
    List<LayerGroupText> getChildren(LayerGroupText parent);

    void saveAll(List<LayerGroupText> list);
}
