package ua.ii.uvpcontrol.backend.repositories.layer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ua.ii.uvpcontrol.backend.data.layer.LayerGroupVideo;

import java.util.List;

@Repository
public interface LayerGroupVideoRepository extends JpaRepository<LayerGroupVideo, Long>, LayerGroupVideoRepositoryCustom{
    long getChildCount(LayerGroupVideo parent);
    Boolean hasChildren(LayerGroupVideo parent);
    @Query("SELECT DISTINCT l FROM LayerGroupVideo l left join fetch l.parent left join fetch l.layerVideos lv left join fetch lv.folder")
    List<LayerGroupVideo> getChildren(LayerGroupVideo parent);
    @Query("SELECT DISTINCT l FROM LayerGroupVideo l left join fetch l.parent left join fetch l.layerVideos lv left join fetch lv.folder")
    List<LayerGroupVideo> findAll();
    void saveAll(List<LayerGroupVideo> list);
}
