package ua.ii.uvpcontrol.backend.repositories.layer;

import ua.ii.uvpcontrol.backend.data.layer.LayerGroupVideo;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class LayerGroupVideoRepositoryCustomImpl implements LayerGroupVideoRepositoryCustom{
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public long getChildCount(LayerGroupVideo parent) {
        List<LayerGroupVideo> list = entityManager
                .createQuery("select t from LayerGroupVideo t where t.parent = :parent", LayerGroupVideo.class)
                .setParameter("parent", parent).getResultList();
        return list.size();
    }

    @Override
    public Boolean hasChildren(LayerGroupVideo parent) {
        List<LayerGroupVideo> list = entityManager
                .createQuery("select t from LayerGroupVideo t where t.parent = :parent", LayerGroupVideo.class)
                .setParameter("parent", parent).getResultList();
        return !list.isEmpty();
    }

    @Override
    public List<LayerGroupVideo> getChildren(LayerGroupVideo parent) {
        List<LayerGroupVideo> list = entityManager
                .createQuery("select t from LayerGroupVideo t where t.parent = :parent", LayerGroupVideo.class)
                .setParameter("parent", parent).getResultList();
        return list;
    }

    public void saveAll(List<LayerGroupVideo> list) {
        list.forEach(e -> entityManager.persist(e));
    }
}
