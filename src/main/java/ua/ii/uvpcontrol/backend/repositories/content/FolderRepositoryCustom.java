package ua.ii.uvpcontrol.backend.repositories.content;

import ua.ii.uvpcontrol.backend.data.content.Folder;

import java.util.List;

public interface FolderRepositoryCustom {
    long getChildCount(Folder parent);
    Boolean hasChildren(Folder parent);
    List<Folder> getChildren(Folder parent);

    void saveAll(List<Folder> list);
}
