package ua.ii.uvpcontrol.backend.repositories.station;

import ua.ii.uvpcontrol.backend.data.station.Station;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class StationRepositoryCustomImpl implements StationRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public long getChildCount(Station parent) {
        List<Station> list = entityManager
                .createQuery("select s from Station s join s.parent p where p.id=:parent", Station.class)
                .setParameter("parent", parent.getId()).getResultList();
        return list.size();
    }

    @Override
    public Boolean hasChildren(Station parent) {
        List<Station> list = entityManager
                .createQuery("select s from Station s join s.parent p where p.id=:parent", Station.class)
                .setParameter("parent", parent.getId()).getResultList();
        return !list.isEmpty();
    }

    @Override
    public List<Station> getChildren(Station parent) {
        List<Station> list = entityManager
                .createQuery("select s from Station s join s.parent p where p.id=:parent", Station.class)
                .setParameter("parent", parent.getId()).getResultList();
        return list;
    }

    public void saveAll(List<Station> list) {
        list.forEach(e -> entityManager.persist(e));
    }
}
