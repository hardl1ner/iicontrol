package ua.ii.uvpcontrol.backend.repositories.content;

import ua.ii.uvpcontrol.backend.data.content.Folder;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class FolderRepositoryCustomImpl implements FolderRepositoryCustom{
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public long getChildCount(Folder parent) {
        List<Folder> list = entityManager
                .createQuery("select t from Folder t where t.parent = :parent", Folder.class)
                .setParameter("parent", parent).getResultList();
        return list.size();
    }

    @Override
    public Boolean hasChildren(Folder parent) {
        List<Folder> list = entityManager
                .createQuery("select t from Folder t where t.parent = :parent", Folder.class)
                .setParameter("parent", parent).getResultList();
        return !list.isEmpty();
    }

    @Override
    public List<Folder> getChildren(Folder parent) {
        List<Folder> list = entityManager
                .createQuery("select t from Folder t where t.parent = :parent", Folder.class)
                .setParameter("parent", parent).getResultList();
        return list;
    }

    public void saveAll(List<Folder> list) {
        list.forEach(e -> entityManager.persist(e));
    }

}
