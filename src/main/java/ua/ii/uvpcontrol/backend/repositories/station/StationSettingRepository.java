package ua.ii.uvpcontrol.backend.repositories.station;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.ii.uvpcontrol.backend.data.station.Station;
import ua.ii.uvpcontrol.backend.data.station.StationSetting;

import java.util.List;

@Repository
public interface StationSettingRepository extends JpaRepository<StationSetting, Long> {
    @EntityGraph(attributePaths = {"layerVideo", "layerGroupImage", "layerGroupPanel", "layerGroupText", "folderPlayList"}, type= EntityGraph.EntityGraphType.FETCH)
    @Query(value = "select c from StationSetting c where lower(c.name) like lower(concat('%', :searchTerm, '%')) ")
    List<StationSetting> search(@Param("searchTerm") String searchTerm);
    @Query("select s from StationSetting s left join fetch s.layerVideo left join fetch s.layerGroupImage left join fetch s.layerGroupPanel left join fetch s.layerGroupText left join fetch s.folderPlayList")
    List<StationSetting> findAll();
    @EntityGraph(attributePaths = {"layerVideo", "layerGroupImage", "layerGroupPanel", "layerGroupText", "folderPlayList"})
    StationSetting findByid(Long id);

}
