package ua.ii.uvpcontrol.backend.service.content;

import org.springframework.stereotype.Service;
import ua.ii.uvpcontrol.backend.data.content.Ether;
import ua.ii.uvpcontrol.backend.data.content.FileInfo;
import ua.ii.uvpcontrol.backend.repositories.content.FileInfoRepository;

import java.util.List;

@Service
public class FileInfoService {
    private FileInfoRepository fileInfoRepository;

    public FileInfoService(FileInfoRepository fileInfoRepository) {
        this.fileInfoRepository = fileInfoRepository;
    }

    public List<FileInfo> findAll() {
        return fileInfoRepository.findAll();
    }
    public FileInfo findByid(Long id){
        return fileInfoRepository.findByid(id);
    }

    public int count() {
        return (int) fileInfoRepository.count();
    }

    public FileInfo save(FileInfo fileInfo) {
            return fileInfoRepository.save(fileInfo);
        }

    public void delete(FileInfo fileInfo) {
            fileInfoRepository.delete(fileInfo);
    }





}
