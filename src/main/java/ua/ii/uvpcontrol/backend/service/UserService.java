package ua.ii.uvpcontrol.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ua.ii.uvpcontrol.backend.data.users.User;
import ua.ii.uvpcontrol.backend.repositories.user.UserRepository;
import ua.ii.uvpcontrol.backend.service.exception.UniqueDataException;

import java.util.List;
import java.util.Optional;

@Service
public class UserService implements FilterableCrudService<User>{

    public static final String MODIFY_LOCKED_USER_NOT_PERMITTED = "Пользователь заблокирован, его нельзя изменить или удалить";
    private static final String DELETING_SELF_NOT_PERMITTED = "Вы не можете удалить свою учетную запись";
    private final UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    public static final int USERS_COUNT_LIMIT = 1000;

    public static class LimitReached extends RuntimeException {
    }


    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }
    public int countAll() {
        return (int) userRepository.count();
    }

    @Override
    public Page<User> findAnyMatching(Optional<String> filter, Pageable pageable) {
        if (filter.isPresent()){
            String repositoryFilter = "%" + filter.get() + "%";
            return getRepository()
                    .findByEmailLikeIgnoreCaseOrNameLikeIgnoreCaseOrRoleLikeIgnoreCase(
                            repositoryFilter, repositoryFilter, repositoryFilter, pageable);
        } else {
            return find(pageable);
        }

    }

    @Override
    public long countAnyMatching(Optional<String> filter) {
        if (filter.isPresent()){
            String repositoryFilter = "%" + filter.get() + "%";
            return userRepository.countByEmailLikeIgnoreCaseOrNameLikeIgnoreCaseOrRoleLikeIgnoreCase(
                    repositoryFilter, repositoryFilter, repositoryFilter);
        } else {
            return count();
        }
    }

    public User findByEmailIgnoreCase(String email){
        return userRepository.findByEmailIgnoreCase(email);
    }

    @Override
    public UserRepository getRepository() {
        return userRepository;
    }

    public Page<User> find(Pageable pageable){
        return getRepository().findBy(pageable);
    }

    public User add(User entity) {
        User user = new User();
        user.setEmail(entity.getEmail());
        user.setName(entity.getName());
        user.setPasswordHash(passwordEncoder.encode(entity.getPasswordHash()));
        user.setRole(entity.getRole());
        user.setLocked(entity.isLocked());
        return getRepository().save(user);
    }
    @Override
    public User save(User currentUser, User entity) {
        throwIfUserLocked(entity);
        return getRepository().saveAndFlush(entity);
    }

    public User edit(User entity){
        if (!entity.getPasswordHash().equals(userRepository.findById(entity.getId()).get().getPasswordHash())) {
            entity.setPasswordHash(passwordEncoder.encode(entity.getPasswordHash()));
        }
        return userRepository.save(entity);

    }

    @Override
    public void delete(User currentUser, User userToDelete) {
        throwIfDeletingSelf(currentUser, userToDelete);
        throwIfUserLocked(userToDelete);
        FilterableCrudService.super.delete(currentUser, userToDelete);

    }

    private void throwIfDeletingSelf(User currentUser, User user) {
        if (currentUser.equals(user)) {
            throw new UniqueDataException(DELETING_SELF_NOT_PERMITTED);
        }
    }

    private void throwIfUserLocked(User entity) {
        if (entity != null && entity.isLocked()) {
            throw new UniqueDataException(MODIFY_LOCKED_USER_NOT_PERMITTED);
        }
    }


    @Override
    public User createNew(User currentUser) {
        return new User();
    }

}
