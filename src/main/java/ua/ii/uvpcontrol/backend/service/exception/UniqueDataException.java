package ua.ii.uvpcontrol.backend.service.exception;

import org.springframework.dao.DataIntegrityViolationException;

public class UniqueDataException extends DataIntegrityViolationException {
    public UniqueDataException(String message) {
        super(message);
    }
}
