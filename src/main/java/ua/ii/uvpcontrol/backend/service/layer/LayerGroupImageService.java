package ua.ii.uvpcontrol.backend.service.layer;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ii.uvpcontrol.backend.data.layer.LayerGroupImage;
import ua.ii.uvpcontrol.backend.repositories.layer.LayerGroupImageRepository;
import ua.ii.uvpcontrol.backend.service.exception.UniqueDataException;

import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class LayerGroupImageService {
    private LayerGroupImageRepository layerGroupImageRepository;

    public LayerGroupImageService(LayerGroupImageRepository layerGroupImageRepository) {
        this.layerGroupImageRepository = layerGroupImageRepository;
    }

    public List<LayerGroupImage> findAll() {
        return layerGroupImageRepository.findAll();
    }

    public int count() {
        return (int) layerGroupImageRepository.count();
    }

    public LayerGroupImage save(LayerGroupImage layerGroupImage) {
        throwIfStationUniqueName(layerGroupImage);
            return layerGroupImageRepository.save(layerGroupImage);
        }

    public LayerGroupImage edit(LayerGroupImage layerGroupImage) {
        return layerGroupImageRepository.save(layerGroupImage);
    }

    public long getChildCount(LayerGroupImage layerGroupImage) {
        return layerGroupImageRepository.findAll().stream()
                .filter(account -> Objects.equals(layerGroupImage, account.getParent()))
                .count();
    }

    public List<LayerGroupImage> parent() {

        return layerGroupImageRepository.findAll().stream()
                .filter(folder -> folder.getParent() == null).collect(Collectors.toList());
    }

    public Boolean hasChildren(LayerGroupImage parent) {

        return layerGroupImageRepository.findAll().stream()
                .anyMatch(account -> Objects.equals(parent, account.getParent()));
    }

    public List<LayerGroupImage> fetchChildren(LayerGroupImage parent) {

        return layerGroupImageRepository.findAll().stream()
                .filter(account -> Objects.equals(parent, account.getParent())).collect(Collectors.toList());
    }

    public void delete(LayerGroupImage group) {
        layerGroupImageRepository.delete(group);
    }

    private void throwIfStationUniqueName(LayerGroupImage layerGroupImage){
        findAll().forEach(layerGroupImage1 -> {
            if (layerGroupImage1.getName().toLowerCase(Locale.ROOT).equals(layerGroupImage.getName().toLowerCase(Locale.ROOT))){
                throw new UniqueDataException("Группа с именем '" + layerGroupImage.getName() + "' существуе.\n" +
                        "Дубликат имен запрещен.");
            }
        });
    }



}
