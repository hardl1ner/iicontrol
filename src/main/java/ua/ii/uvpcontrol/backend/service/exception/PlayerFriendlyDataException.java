package ua.ii.uvpcontrol.backend.service.exception;

import org.springframework.dao.DataIntegrityViolationException;

public class PlayerFriendlyDataException extends DataIntegrityViolationException {
    public PlayerFriendlyDataException(String message) {
        super(message);
    }
}
