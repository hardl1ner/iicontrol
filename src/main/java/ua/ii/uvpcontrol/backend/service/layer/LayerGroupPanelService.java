package ua.ii.uvpcontrol.backend.service.layer;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ii.uvpcontrol.backend.data.layer.LayerGroupPanel;
import ua.ii.uvpcontrol.backend.repositories.layer.LayerGroupPanelRepository;
import ua.ii.uvpcontrol.backend.service.exception.UniqueDataException;

import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class LayerGroupPanelService {
    private LayerGroupPanelRepository layerGroupPanelRepository;

    public LayerGroupPanelService(LayerGroupPanelRepository layerGroupPanelRepository) {
        this.layerGroupPanelRepository = layerGroupPanelRepository;
    }

    public List<LayerGroupPanel> findAll() {
        return layerGroupPanelRepository.findAll();
    }

    public int count() {
        return (int) layerGroupPanelRepository.count();
    }

    public LayerGroupPanel save(LayerGroupPanel layerGroupPanel) {
        throwIfStationUniqueName(layerGroupPanel);
            return layerGroupPanelRepository.save(layerGroupPanel);
        }

    public LayerGroupPanel edit(LayerGroupPanel layerGroupPanel) {
        return layerGroupPanelRepository.save(layerGroupPanel);
    }

    public long getChildCount(LayerGroupPanel layerGroupPanel) {
        return layerGroupPanelRepository.findAll().stream()
                .filter(account -> Objects.equals(layerGroupPanel, account.getParent()))
                .count();
    }

    public Boolean hasChildren(LayerGroupPanel parent) {

        return layerGroupPanelRepository.findAll().stream()
                .anyMatch(account -> Objects.equals(parent, account.getParent()));
    }

    public List<LayerGroupPanel> parent() {

        return layerGroupPanelRepository.findAll().stream()
                .filter(folder -> folder.getParent() == null).collect(Collectors.toList());
    }

    public List<LayerGroupPanel> fetchChildren(LayerGroupPanel parent) {

        return layerGroupPanelRepository.findAll().stream()
                .filter(account -> Objects.equals(parent, account.getParent())).collect(Collectors.toList());
    }

    public void delete(LayerGroupPanel group) {
        layerGroupPanelRepository.delete(group);
    }

    private void throwIfStationUniqueName(LayerGroupPanel layerGroupPanel){
        findAll().forEach(layerGroupPanel1 -> {
            if (layerGroupPanel1.getName().toLowerCase(Locale.ROOT).equals(layerGroupPanel.getName().toLowerCase(Locale.ROOT))){
                throw new UniqueDataException("Группа с именем '" + layerGroupPanel.getName() + "' существуе.\n" +
                        "Дубликат имен запрещен.");
            }
        });
    }



}
