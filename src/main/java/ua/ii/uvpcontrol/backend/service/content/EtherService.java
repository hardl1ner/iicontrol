package ua.ii.uvpcontrol.backend.service.content;

import org.springframework.stereotype.Service;
import ua.ii.uvpcontrol.backend.data.content.Ether;
import ua.ii.uvpcontrol.backend.data.content.FileInfo;
import ua.ii.uvpcontrol.backend.data.station.Station;
import ua.ii.uvpcontrol.backend.repositories.content.EtherRepository;
import ua.ii.uvpcontrol.backend.repositories.content.FileInfoRepository;

import java.util.List;

@Service
public class EtherService {
    private EtherRepository etherRepository;

    public EtherService(EtherRepository etherRepository) {
        this.etherRepository = etherRepository;
    }

    public List<Ether> findAll() {
        return etherRepository.findAll();
    }

    public Ether findByid(Long id){
        return etherRepository.findByid(id);
    }


    public Ether save(Ether ether) {
        return etherRepository.save(ether);
    }

    public void delete(Ether ether) {
            etherRepository.delete(ether);
    }





}
