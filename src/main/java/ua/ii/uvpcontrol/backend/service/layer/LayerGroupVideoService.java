package ua.ii.uvpcontrol.backend.service.layer;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ii.uvpcontrol.backend.data.layer.LayerGroupVideo;
import ua.ii.uvpcontrol.backend.repositories.layer.LayerGroupVideoRepository;
import ua.ii.uvpcontrol.backend.service.exception.UniqueDataException;

import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class LayerGroupVideoService {
    private LayerGroupVideoRepository layerGroupVideoRepository;

    public LayerGroupVideoService(LayerGroupVideoRepository layerGroupVideoRepository) {
        this.layerGroupVideoRepository = layerGroupVideoRepository;
    }

    public List<LayerGroupVideo> findAll() {
        return layerGroupVideoRepository.findAll();
    }

    public int count() {
        return (int) layerGroupVideoRepository.count();
    }

    public LayerGroupVideo save(LayerGroupVideo layerGroupVideo) {
        throwIfStationUniqueName(layerGroupVideo);
            return layerGroupVideoRepository.save(layerGroupVideo);
        }
    public List<LayerGroupVideo> parent() {

        return layerGroupVideoRepository.findAll().stream()
                .filter(folder -> folder.getParent() == null).collect(Collectors.toList());
    }

    public LayerGroupVideo edit(LayerGroupVideo layerGroupVideo) {
        return layerGroupVideoRepository.save(layerGroupVideo);
    }

    public long getChildCount(LayerGroupVideo layerGroupVideo) {
        return layerGroupVideoRepository.findAll().stream()
                .filter(account -> Objects.equals(layerGroupVideo, account.getParent()))
                .count();
    }

    public Boolean hasChildren(LayerGroupVideo parent) {

        return layerGroupVideoRepository.findAll().stream()
                .anyMatch(account -> Objects.equals(parent, account.getParent()));
    }

    public List<LayerGroupVideo> fetchChildren(LayerGroupVideo parent) {

        return layerGroupVideoRepository.findAll().stream()
                .filter(account -> Objects.equals(parent, account.getParent())).collect(Collectors.toList());
    }

    public void delete(LayerGroupVideo group) {
        layerGroupVideoRepository.delete(group);
    }

    private void throwIfStationUniqueName(LayerGroupVideo layerGroupVideo){
        findAll().forEach(layerGroupVideo1 -> {
            if (layerGroupVideo1.getName().toLowerCase(Locale.ROOT).equals(layerGroupVideo.getName().toLowerCase(Locale.ROOT))){
                throw new UniqueDataException("Группа с именем '" + layerGroupVideo.getName() + "' существуе.\n" +
                        "Дубликат имен запрещен.");
            }
        });
    }



}
