package ua.ii.uvpcontrol.backend.service.layer;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ii.uvpcontrol.backend.data.layer.LayerGroupText;
import ua.ii.uvpcontrol.backend.repositories.layer.LayerGroupTextRepository;
import ua.ii.uvpcontrol.backend.service.exception.UniqueDataException;

import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class LayerGroupTextService {
    private LayerGroupTextRepository layerGroupTextRepository;

    public LayerGroupTextService(LayerGroupTextRepository layerGroupTextRepository) {
        this.layerGroupTextRepository = layerGroupTextRepository;
    }

    public List<LayerGroupText> findAll() {
        return layerGroupTextRepository.findAll();
    }

    public int count() {
        return (int) layerGroupTextRepository.count();
    }

    public LayerGroupText save(LayerGroupText layerGroupText) {
        throwIfStationUniqueName(layerGroupText);
            return layerGroupTextRepository.save(layerGroupText);
        }

    public List<LayerGroupText> parent() {

        return layerGroupTextRepository.findAll().stream()
                .filter(folder -> folder.getParent() == null).collect(Collectors.toList());
    }


    public LayerGroupText edit(LayerGroupText layerGroupText) {
        return layerGroupTextRepository.save(layerGroupText);
    }

    public long getChildCount(LayerGroupText layerGroupText) {
        return layerGroupTextRepository.findAll().stream()
                .filter(account -> Objects.equals(layerGroupText, account.getParent()))
                .count();
    }

    public Boolean hasChildren(LayerGroupText parent) {

        return layerGroupTextRepository.findAll().stream()
                .anyMatch(account -> Objects.equals(parent, account.getParent()));
    }

    public List<LayerGroupText> fetchChildren(LayerGroupText parent) {

        return layerGroupTextRepository.findAll().stream()
                .filter(account -> Objects.equals(parent, account.getParent())).collect(Collectors.toList());
    }

    public void delete(LayerGroupText group) {
        layerGroupTextRepository.delete(group);
    }

    private void throwIfStationUniqueName(LayerGroupText layerGroupText){
        findAll().forEach(layerGroupText1 -> {
            if (layerGroupText1.getName().toLowerCase(Locale.ROOT).equals(layerGroupText.getName().toLowerCase(Locale.ROOT))){
                throw new UniqueDataException("Группа с именем '" + layerGroupText.getName() + "' существуе.\n" +
                        "Дубликат имен запрещен.");
            }
        });
    }



}
