package ua.ii.uvpcontrol.backend.service.layer;

import org.springframework.stereotype.Service;
import ua.ii.uvpcontrol.app.HasLogger;
import ua.ii.uvpcontrol.backend.data.layer.LayerPanel;
import ua.ii.uvpcontrol.backend.repositories.layer.LayerPanelRepository;
import ua.ii.uvpcontrol.backend.service.exception.UniqueDataException;

import java.util.List;

@Service
public class LayerPanelService implements HasLogger {

    public static final String MODIFY_LOCKED_PLAYER = "Ошибка: Слой используется его нельза изменить или удалить";

    private final LayerPanelRepository layerPanelRepository;

    public LayerPanelService(LayerPanelRepository layerPanelRepository) {
        this.layerPanelRepository = layerPanelRepository;
    }

    public List<LayerPanel> findAll() {
        return layerPanelRepository.findAll();
    }

    public int countAll() {
        return (int) layerPanelRepository.count();
    }

    public LayerPanel save(LayerPanel layerPanel) {
        return layerPanelRepository.save(layerPanel);
    }

    public void delete(LayerPanel layerPanel) {
        throwIfLayerPanelLocked(layerPanel);
        layerPanelRepository.delete(layerPanel);
    }

    private void throwIfLayerPanelLocked(LayerPanel layerPanel) {
        if (layerPanel != null && layerPanel.isActive()) {
            throw new UniqueDataException(MODIFY_LOCKED_PLAYER);
        }
    }
}
