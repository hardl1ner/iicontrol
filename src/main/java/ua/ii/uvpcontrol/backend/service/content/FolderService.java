package ua.ii.uvpcontrol.backend.service.content;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ii.uvpcontrol.app.HasLogger;
import ua.ii.uvpcontrol.backend.data.content.Folder;
import ua.ii.uvpcontrol.backend.repositories.content.FolderRepository;

import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Transactional
public class FolderService implements HasLogger {
    private final FolderRepository folderRepository;
    private final String storage;

    @Autowired
    public FolderService(FolderRepository folderRepository, @Value("${storage}") String storage){
        this.folderRepository = folderRepository;
        this.storage = storage;
    }

    public Folder findById(Long id){
        return folderRepository.findByid(id);
    }

    public long getChildCount(Folder parent) {
        return folderRepository.findAll().stream()
                .filter(account -> Objects.equals(parent, account.getParent()))
                .count();
    }

    public List<Folder> findAll() {
        return folderRepository.findAll();
    }

    public Boolean hasChildren(Folder parent) {

        return folderRepository.findAll().stream()
                .anyMatch(account -> Objects.equals(parent, account.getParent()));
    }

    public Folder save(Folder folder) {
            if (folder.getParent() != null) {
                Folder folder2 = folder.getParent();
                String path = folder.getParent().getName() + "/" + folder.getName() + "/";
                while (true){
                    if (folder2.getParent() != null){
                        path = folder2.getParent().getName() + "/" + path;
                        folder2 = folder2.getParent();
                    } else {
                        break;
                    }
                }
                getLogger().info("" + new File(storage + path).mkdir());

            } else {
                getLogger().info("" + new File(storage + folder.getName() + "/").mkdir());
            }

        return folderRepository.save(folder);
    }

    public List<Folder> fetchChildren(Folder parent) {

        return folderRepository.findAll().stream()
                .filter(account -> Objects.equals(parent, account.getParent())).collect(Collectors.toList());
    }

    public List<Folder> parent() {

        return folderRepository.findAll().stream()
                .filter(folder -> folder.getParent() == null).collect(Collectors.toList());
    }

    public void saveAll(List<Folder> list) {
        folderRepository.saveAll(list);
    }



    public void delete(Folder folder) {
        if (folder.getParent() != null) {
            Folder folder2 = folder.getParent();
            String path = folder.getParent().getName() + "/" + folder.getName() + "/";
            while (true){
                if (folder2.getParent() != null){
                    path = folder2.getParent().getName() + "/" + path;
                    folder2 = folder2.getParent();
                } else {
                    break;
                }
            }
            getLogger().info("" + new File(storage + path).delete());

        } else {
            getLogger().info("" + new File(storage + folder.getName() + "/").delete());
        }
        folderRepository.delete(folder);
    }
}
