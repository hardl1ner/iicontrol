package ua.ii.uvpcontrol.backend.service.layer;

import org.springframework.stereotype.Service;
import ua.ii.uvpcontrol.app.HasLogger;
import ua.ii.uvpcontrol.backend.data.layer.LayerImage;
import ua.ii.uvpcontrol.backend.data.station.Station;
import ua.ii.uvpcontrol.backend.repositories.layer.LayerImageRepository;
import ua.ii.uvpcontrol.backend.service.exception.UniqueDataException;

import java.util.List;

@Service
public class LayerImageService implements HasLogger {

    public static final String MODIFY_LOCKED_PLAYER = "Ошибка: Слой используется его нельза изменить или удалить";

    private final LayerImageRepository layerImageRepository;

    public LayerImageService(LayerImageRepository layerImageRepository) {
        this.layerImageRepository = layerImageRepository;
    }

    public List<LayerImage> searchFile(Long idFile){
        return layerImageRepository.searchFile(idFile);
    }

    public List<LayerImage> findAll() {
        return layerImageRepository.findAll();
    }

    public int countAll() {
        return (int) layerImageRepository.count();
    }

    public LayerImage save(LayerImage layerImage) {
        return layerImageRepository.save(layerImage);
    }

    public void delete(LayerImage layerImage) {
        throwIfLayerImageLocked(layerImage);
        layerImageRepository.delete(layerImage);
    }

    private void throwIfLayerImageLocked(LayerImage layerImage) {
        if (layerImage != null && layerImage.isActive()) {
            throw new UniqueDataException(MODIFY_LOCKED_PLAYER);
        }
    }
}
