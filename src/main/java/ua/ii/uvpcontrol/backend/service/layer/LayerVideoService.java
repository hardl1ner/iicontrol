package ua.ii.uvpcontrol.backend.service.layer;

import org.springframework.stereotype.Service;
import ua.ii.uvpcontrol.app.HasLogger;
import ua.ii.uvpcontrol.backend.data.layer.LayerVideo;
import ua.ii.uvpcontrol.backend.repositories.layer.LayerVideoRepository;
import ua.ii.uvpcontrol.backend.service.exception.UniqueDataException;

import java.util.List;

@Service
public class LayerVideoService implements HasLogger {

    public static final String MODIFY_LOCKED_PLAYER = "Ошибка: Слой используется его нельза изменить или удалить";

    private final LayerVideoRepository layerVideoRepository;

    public LayerVideoService(LayerVideoRepository layerVideoRepository) {
        this.layerVideoRepository = layerVideoRepository;
    }

    public LayerVideo findById(Long id){
        return layerVideoRepository.findByid(id);
    }

    public List<LayerVideo> findAll() {
        return layerVideoRepository.findAll();
    }

    public int countAll() {
        return (int) layerVideoRepository.count();
    }

    public LayerVideo save(LayerVideo layerVideo) {
        return layerVideoRepository.save(layerVideo);
    }

    public void delete(LayerVideo layerVideo) {
        throwIfLayerVideoLocked(layerVideo);
        layerVideoRepository.delete(layerVideo);
    }

    private void throwIfLayerVideoLocked(LayerVideo layerVideo) {
        if (layerVideo != null && layerVideo.isActive()) {
            throw new UniqueDataException(MODIFY_LOCKED_PLAYER);
        }
    }
}
