package ua.ii.uvpcontrol.backend.service.layer;

import org.springframework.stereotype.Service;
import ua.ii.uvpcontrol.app.HasLogger;
import ua.ii.uvpcontrol.backend.data.layer.LayerText;
import ua.ii.uvpcontrol.backend.data.station.Station;
import ua.ii.uvpcontrol.backend.repositories.layer.LayerTextRepository;
import ua.ii.uvpcontrol.backend.service.exception.UniqueDataException;

import java.util.List;

@Service
public class LayerTextService implements HasLogger {

    public static final String MODIFY_LOCKED_PLAYER = "Ошибка: Слой используется его нельза изменить или удалить";

    private final LayerTextRepository layerTextRepository;

    public LayerTextService(LayerTextRepository layerTextRepository) {
        this.layerTextRepository = layerTextRepository;
    }

    public List<LayerText> findAll() {
        return layerTextRepository.findAll();
    }

    public int countAll() {
        return (int) layerTextRepository.count();
    }

    public LayerText save(LayerText layerText) {
        return layerTextRepository.save(layerText);
    }
    public LayerText findById(Long id){
        return layerTextRepository.findByid(id);
    }

    public void delete(LayerText layerText) {
        throwIfLayerTextLocked(layerText);
        layerTextRepository.delete(layerText);
    }

    private void throwIfLayerTextLocked(LayerText layerText) {
        if (layerText != null && layerText.isActive()) {
            throw new UniqueDataException(MODIFY_LOCKED_PLAYER);
        }
    }
}
