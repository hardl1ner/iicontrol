package ua.ii.uvpcontrol.backend.service.station;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ii.uvpcontrol.backend.data.station.Station;
import ua.ii.uvpcontrol.backend.data.station.StationSetting;
import ua.ii.uvpcontrol.backend.repositories.station.StationSettingRepository;

import java.util.List;

@Service
public class StationSettingService {

    private StationSettingRepository stationSettingRepository;

    public StationSettingService(StationSettingRepository stationSettingRepository) {
        this.stationSettingRepository = stationSettingRepository;
    }

    public List<StationSetting> findAll() {
        return stationSettingRepository.findAll();
    }

    public StationSetting findById(Long id){
        return stationSettingRepository.findByid(id);
    }

    public List<StationSetting> findAll(String stringFilter) {
        if (stringFilter == null || stringFilter.isEmpty()) {
            return stationSettingRepository.findAll();
        } else {
            return stationSettingRepository.search(stringFilter);
        }
    }

    public int count() {
        return (int) stationSettingRepository.count();
    }

    public StationSetting save(StationSetting stationSetting) {
            return stationSettingRepository.save(stationSetting);
        }

    public void delete(StationSetting stationSetting) {
        stationSettingRepository.delete(stationSetting);
    }



}
