package ua.ii.uvpcontrol.backend.service.station;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ii.uvpcontrol.app.HasLogger;
import ua.ii.uvpcontrol.backend.data.station.Station;
import ua.ii.uvpcontrol.backend.repositories.station.StationRepository;
import ua.ii.uvpcontrol.backend.service.exception.UniqueDataException;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class StationService implements HasLogger {

    public static final String MODIFY_LOCKED_PLAYER = "Ошибка: Станция заблокирован ее нельза изменить или удалить";

    private final StationRepository stationRepository;

    public StationService(StationRepository stationRepository) {
        this.stationRepository = stationRepository;
    }

    public List<Station> findAll() {
        return stationRepository.findAll();
    }

    public List<Station> findAll(String stringFilter) {
        if (stringFilter == null || stringFilter.isEmpty()) {
            return stationRepository.findAll();
        } else {
            return stationRepository.search(stringFilter);
        }
    }

    public List<Station> searchEther(Long idEther){
        return stationRepository.searchEther(idEther);
    }

    public Station findById(Long id){
        return stationRepository.findByid(id);
    }

    public Station findByAddress(String address){
        return stationRepository.findByAddress(address).get(0);
    }

    public List<Station> parent() {

        return stationRepository.findAll().stream()
                .filter(folder -> folder.getParent() == null).collect(Collectors.toList());
    }

    public long getChildCount(Station parent) {
        return stationRepository.findAll().stream()
                .filter(account -> Objects.equals(parent, account.getParent()))
                .count();
    }

    public Boolean hasChildren(Station parent) {

        return stationRepository.findAll().stream()
                .anyMatch(account -> Objects.equals(parent, account.getParent()));
    }

    public List<Station> fetchChildren(Station parent) {

        return stationRepository.findAll().stream()
                .filter(account -> Objects.equals(parent, account.getParent())).collect(Collectors.toList());
    }

    public int countAll() {
        return (int) stationRepository.count();
    }

    public Station save(Station station) {
        return stationRepository.save(station);
    }

    public void delete(Station station) {
        throwIfStationLocked(station);
        stationRepository.delete(station);
    }

    private void throwIfStationLocked(Station station) {
        if (station != null && station.isActive()) {
            throw new UniqueDataException(MODIFY_LOCKED_PLAYER);
        }
    }
}
