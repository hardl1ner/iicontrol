package ua.ii.uvpcontrol.backend.conroller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.ii.uvpcontrol.backend.data.station.Station;
import ua.ii.uvpcontrol.backend.data.users.Role;
import ua.ii.uvpcontrol.backend.dto.StationDto;
import ua.ii.uvpcontrol.backend.dto.StationSettingDto;
import ua.ii.uvpcontrol.backend.repositories.content.FolderRepository;
import ua.ii.uvpcontrol.backend.repositories.station.StationRepository;

@RestController
@RequestMapping("/api/station/")
@Secured({Role.ADMIN, Role.API})
public class StationController {
    StationRepository stationRepository;
    FolderRepository folderRepository;

    public StationController(StationRepository stationRepository, FolderRepository folderRepository) {
        this.stationRepository = stationRepository;
        this.folderRepository = folderRepository;
    }
    @GetMapping(value = "{keyStation}")
    public ResponseEntity<StationDto> getKeyStation(@PathVariable String keyStation) {
        Station station = stationRepository.findByKeyStation(keyStation);
        StationDto stationDto = new StationDto();
        if (station != null) {
            stationDto.setId(station.getId());
            stationDto.setName(station.getName());
            stationDto.setAddress(station.getAddress());
            stationDto.setParent(station.getParent());
            stationDto.setActive(station.isActive());
            stationDto.setKeyStation(station.getKeyStation());
            stationDto.setEthers(stationRepository.findByid(station.getId()).getEthers());
            StationSettingDto stationSettingDto = new StationSettingDto();
            if (station.getStationSetting().getFolderPlayList() != null){
                station.getStationSetting().setFolderPlayList(folderRepository.findByid(station.getStationSetting().getFolderPlayList().getId()));
            }
            stationSettingDto.setSetting(station.getStationSetting());
            stationDto.setStationSetting(stationSettingDto);

        } else {
            stationDto = null;
        }

        return new ResponseEntity<>(stationDto, HttpStatus.OK) ;
    }

}