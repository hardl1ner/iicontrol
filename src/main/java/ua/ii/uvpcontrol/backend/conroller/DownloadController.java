package ua.ii.uvpcontrol.backend.conroller;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ua.ii.uvpcontrol.app.HasLogger;
import ua.ii.uvpcontrol.backend.data.content.FileInfo;
import ua.ii.uvpcontrol.backend.data.users.Role;
import ua.ii.uvpcontrol.backend.repositories.content.FileInfoRepository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@RequestMapping("/file")
@Secured({Role.ADMIN, Role.MANAGER, Role.API})
public class DownloadController implements HasLogger {
    private final FileInfoRepository fileInfoRepository;
    private final String STORAGE;

    public DownloadController(FileInfoRepository fileInfoRepository, @Value("${storage}") String storage) {
        this.STORAGE = storage;
        this.fileInfoRepository = fileInfoRepository;
    }

    @SneakyThrows
    @GetMapping(value= "/download/{id}")
    public ResponseEntity<ByteArrayResource> download(@PathVariable Long id) {
        FileInfo fileInfo = fileInfoRepository.findByid(id);
        File file = new File(STORAGE + fileInfo.getFile_path() + fileInfo.getFile_name());
        Path path = Paths.get(file.getAbsolutePath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
        return ResponseEntity
                .ok()
                .contentLength(file.length())
                .header("Content-type", "application/octet-stream")
                .header("Content-disposition", "attachment; filename=\"" + fileInfo.getFile_name() + "\"")
                .body(resource);
    }

    @PostMapping("/upload") // //new annotation since 4.3
    public void singleFileUpload(@RequestParam("file") MultipartFile file, @RequestParam("path") String path) {
        try {

            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();
            Path pathUpload = Paths.get(STORAGE + path + file.getOriginalFilename());
            File file1 = new File(STORAGE + path);
            if (!file1.isDirectory()){
                getLogger().info("create dir " + file1.getAbsolutePath() + file1.mkdirs());
            }
            File fileLocal = new File(STORAGE + path + file.getOriginalFilename());
            if (fileLocal.isFile()){
                if (file.getSize() != fileLocal.length()){
                    Files.write(pathUpload, bytes);
                }
            } else {
                Files.write(pathUpload, bytes);
            }
        } catch (IOException e) {
            getLogger().error("" + e);
        } finally {
            try {
                file.getInputStream().close();
            } catch (IOException e) {
                getLogger().error("" + e);
            }
        }
    }
}