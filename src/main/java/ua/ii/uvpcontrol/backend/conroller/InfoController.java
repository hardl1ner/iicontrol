package ua.ii.uvpcontrol.backend.conroller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ua.ii.uvpcontrol.backend.data.content.FileInfo;
import ua.ii.uvpcontrol.backend.data.layer.LayerImage;
import ua.ii.uvpcontrol.backend.data.layer.LayerText;
import ua.ii.uvpcontrol.backend.data.users.Role;
import ua.ii.uvpcontrol.backend.repositories.content.FileInfoRepository;
import ua.ii.uvpcontrol.backend.repositories.layer.LayerImageRepository;
import ua.ii.uvpcontrol.backend.repositories.layer.LayerTextRepository;

@RestController
@RequestMapping("/api/info/")
@Secured({Role.ADMIN, Role.API})
public class InfoController {
    LayerImageRepository layerImageRepository;
    LayerTextRepository layerTextRepository;
    FileInfoRepository fileInfoRepository;

    public InfoController(LayerImageRepository layerImageRepository, LayerTextRepository layerTextRepository,
                          FileInfoRepository fileInfoRepository) {
        this.layerImageRepository = layerImageRepository;
        this.layerTextRepository = layerTextRepository;
        this.fileInfoRepository = fileInfoRepository;
    }
    @PutMapping("/text/{id}")
    ResponseEntity<LayerText> replaceText(@RequestBody LayerText updateText, @PathVariable Long id) {
        LayerText layerText = layerTextRepository.findByid(id);

        if (layerText != null){
            layerText.setText(updateText.getText());
            layerText.setColor(updateText.getColor());
            layerTextRepository.save(layerText);
            return new ResponseEntity<>(layerText, HttpStatus.OK) ;
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/text/{id}")
    public ResponseEntity<LayerText> getLayerText(@PathVariable Long id) {
        LayerText layerText = layerTextRepository.findByid(id);

        if (layerText != null){
            return new ResponseEntity<>(layerText, HttpStatus.OK) ;
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }


    }

    @PutMapping("/image/{id}")
    ResponseEntity<LayerImage> replaceImage(@RequestBody LayerImage updateImage, @PathVariable Long id) {

        LayerImage layerImage = layerImageRepository.findByid(id);

        if (layerImage != null){
            if (updateImage.getFileInfo() != null){
                layerImage.setFileInfo(fileInfoRepository.findByid(updateImage.getFileInfo().getId()));
                layerImageRepository.save(layerImage);
                return new ResponseEntity<>(layerImage, HttpStatus.OK);
            }
            return new ResponseEntity<>(layerImage, HttpStatus.NOT_MODIFIED);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/image/{id}")
    public ResponseEntity<LayerImage> getLayerImage(@PathVariable Long id) {
        LayerImage layerImage = layerImageRepository.findByid(id);

        if (layerImage != null){
            if (layerImage.getFileInfo() != null){
                layerImage.setFileInfo(fileInfoRepository.findByid(layerImage.getFileInfo().getId()));
            }
            return new ResponseEntity<>(layerImage, HttpStatus.OK) ;
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }


    }


}