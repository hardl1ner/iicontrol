package ua.ii.uvpcontrol.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.ii.uvpcontrol.backend.data.layer.LayerVideo;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LayerVideoDto {
    private long id;
    private String name;
    private FolderDto folder;
    private Integer width;
    private Integer height;
    private Integer x;
    private Integer y;
    private boolean active;

    public void setLayer(LayerVideo layerVideo){
        setId(layerVideo.getId());
        setName(layerVideo.getName());
        if (layerVideo.getFolder() != null) {
            FolderDto folderDto = new FolderDto();
            folderDto.setFolder(layerVideo.getFolder());
            setFolder(folderDto);
        } else {
            setFolder(null);
        }
        setWidth(layerVideo.getWidth());
        setHeight(layerVideo.getHeight());
        setX(layerVideo.getX());
        setY(layerVideo.getY());
        setActive(layerVideo.isActive());
    }
}
