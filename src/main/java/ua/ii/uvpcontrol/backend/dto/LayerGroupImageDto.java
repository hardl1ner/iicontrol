package ua.ii.uvpcontrol.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.ii.uvpcontrol.backend.data.layer.LayerGroupImage;
import ua.ii.uvpcontrol.backend.data.layer.LayerImage;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LayerGroupImageDto {
    private long id;
    private String name;
    private Set<LayerImage> layerImages;

    public void setLayer(LayerGroupImage layerGroupImage){
        setId(layerGroupImage.getId());
        setName(layerGroupImage.getName());
        if (layerGroupImage.getLayerImages() != null) {
            if (layerGroupImage.getLayerImages().size() > 0) {
                setLayerImages(layerGroupImage.getLayerImages());
            } else {
                setLayerImages(null);
            }
        } else {
            setLayerImages(null);
        }
    }
}
