package ua.ii.uvpcontrol.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.ii.uvpcontrol.backend.data.layer.LayerGroupPanel;
import ua.ii.uvpcontrol.backend.data.layer.LayerPanel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LayerGroupPanelDto {
    private long id;
    private String name;
    private Set<LayerPanelDto> layerPanels;

    public void setLayer(LayerGroupPanel layerGroupPanel){
        setId(layerGroupPanel.getId());
        setName(layerGroupPanel.getName());
        if (layerGroupPanel.getLayerPanels() != null) {
            if(layerGroupPanel.getLayerPanels().size() > 0) {
                Set<LayerPanelDto> layerPanelDtos = new HashSet<>();
                layerGroupPanel.getLayerPanels().forEach(layerPanel -> {
                    LayerPanelDto layerPanelDto = new LayerPanelDto();
                    layerPanelDto.setLayerPanel(layerPanel);
                    layerPanelDtos.add(layerPanelDto);
                });
                setLayerPanels(layerPanelDtos);
            } else {
                setLayerPanels(null);
            }
        } else {
            setLayerPanels(null);
        }
    }
}
