package ua.ii.uvpcontrol.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.ii.uvpcontrol.backend.data.content.FileInfo;
import ua.ii.uvpcontrol.backend.data.content.Folder;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FolderDto {
    private long id;
    private String name;
    private Set<FileInfo> fileInfos;

    public void setFolder(Folder folder){
        setId(folder.getId());
        setName(folder.getName());
        if (folder.getFileInfos() != null) {
            if (folder.getFileInfos().size() > 0) {
                setFileInfos(folder.getFileInfos());
            } else {
                setFileInfos(null);
            }
        } else {
            setFileInfos(null);
        }
    }
}
