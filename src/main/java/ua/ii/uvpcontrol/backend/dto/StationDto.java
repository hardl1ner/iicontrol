package ua.ii.uvpcontrol.backend.dto;

import ua.ii.uvpcontrol.backend.data.content.Ether;
import ua.ii.uvpcontrol.backend.data.station.Station;

import java.util.ArrayList;
import java.util.List;

public class StationDto {
    private long id;
    private String name;
    private Station parent;
    private String address;
    private boolean active;
    private String keyStation;
    private List<Ether> ethers;
    private StationSettingDto stationSetting;

    public void setParent(Station parent) {
        if (parent != null) {
            if (parent.getParent() != null) {
                Station parent2 = parent.getParent();
                String parentPath = parent.getParent().getName() + "/" + parent.getName() + "/";
                while (true) {
                    if (parent2.getParent() != null) {
                        parentPath = parent2.getParent().getName() + "/" + parentPath;
                        parent2 = parent2.getParent();
                    } else {
                        break;
                    }
                }
                parent.setParent(null);
                parent.setName(parentPath);
                parent.setEthers(new ArrayList<>());
                parent.setStationSetting(null);

            }
            parent.setEthers(new ArrayList<>());
            parent.setStationSetting(null);
        }
        this.parent = parent;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Station getParent() {
        return parent;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getKeyStation() {
        return keyStation;
    }

    public void setKeyStation(String keyStation) {
        this.keyStation = keyStation;
    }

    public List<Ether> getEthers() {
        return ethers;
    }

    public void setEthers(List<Ether> ethers) {
        this.ethers = ethers;
    }

    public StationSettingDto getStationSetting() {
        return stationSetting;
    }

    public void setStationSetting(StationSettingDto stationSetting) {
        this.stationSetting = stationSetting;
    }
}
