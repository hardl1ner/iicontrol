package ua.ii.uvpcontrol.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.ii.uvpcontrol.backend.data.layer.LayerGroupText;
import ua.ii.uvpcontrol.backend.data.layer.LayerText;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LayerGroupTextDto {
    private long id;
    private String name;
    private Set<LayerText> layerTexts;

    public void setLayer(LayerGroupText layerGroupText){
        setId(layerGroupText.getId());
        setName(layerGroupText.getName());
        if (layerGroupText.getLayerTexts() != null) {
            if (layerGroupText.getLayerTexts().size() > 0) {
                setLayerTexts(layerGroupText.getLayerTexts());
            } else {
                setLayerTexts(null);
            }
        } else {
            setLayerTexts(null);
        }
    }
}
