package ua.ii.uvpcontrol.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.ii.uvpcontrol.backend.data.layer.LayerPanel;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LayerPanelDto {
    private long id;
    private String name;
    private FolderDto folder;
    private Integer width;
    private Integer height;
    private Integer x;
    private Integer y;
    private String trigger_panel;
    private boolean active;

    public void setLayerPanel (LayerPanel layerPanel){
        setId(layerPanel.getId());
        setName(layerPanel.getName());

        if (layerPanel.getFolder() != null){
            FolderDto folderDto = new FolderDto();
            folderDto.setFolder(layerPanel.getFolder());
            setFolder(folderDto);
        } else {
            setFolder(null);
        }

        setWidth(layerPanel.getWidth());
        setHeight(layerPanel.getHeight());
        setX(layerPanel.getX());
        setY(layerPanel.getY());
        setTrigger_panel(layerPanel.getTrigger_panel());
        setActive(layerPanel.isActive());

    }
}
