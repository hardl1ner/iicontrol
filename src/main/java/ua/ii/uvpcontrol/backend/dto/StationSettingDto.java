package ua.ii.uvpcontrol.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.ii.uvpcontrol.backend.data.station.StationSetting;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StationSettingDto {
    private long id;
    private String name;

    private LayerVideoDto layerVideo;
    private LayerGroupImageDto layerGroupImage;
    private LayerGroupPanelDto layerGroupPanel;
    private LayerGroupTextDto layerGroupText;

    private Integer widthScreen;
    private Integer heightScreen;

    private Integer x;
    private Integer y;

    private Integer downloadInterval;
    private Integer uploadInterval;

    private LocalTime monday;
    private LocalTime tuesday;
    private LocalTime wednesday;
    private LocalTime thursday;
    private LocalTime friday;
    private LocalTime saturday;
    private LocalTime sunday;

    private boolean shutdownOn;
    private StationSetting.TypePlayList typePlayList;
    private FolderDto folderPlayList;

    public void setSetting(StationSetting stationSetting){
        setId(stationSetting.getId());
        setName(stationSetting.getName());

        if (stationSetting.getLayerVideo() != null) {
            LayerVideoDto layerVideoDto = new LayerVideoDto();
            layerVideoDto.setLayer(stationSetting.getLayerVideo());
            setLayerVideo(layerVideoDto);
        } else {
            setLayerVideo(null);
        }

        if (stationSetting.getLayerGroupImage() != null) {
            LayerGroupImageDto layerGroupImageDto = new LayerGroupImageDto();
            layerGroupImageDto.setLayer(stationSetting.getLayerGroupImage());
            setLayerGroupImage(layerGroupImageDto);
        } else {
            setLayerGroupImage(null);
        }

        if (stationSetting.getLayerGroupPanel() != null) {
            LayerGroupPanelDto layerGroupPanelDto = new LayerGroupPanelDto();
            layerGroupPanelDto.setLayer(stationSetting.getLayerGroupPanel());
            setLayerGroupPanel(layerGroupPanelDto);
        } else {
            setLayerGroupPanel(null);
        }

        if (stationSetting.getLayerGroupText() != null) {
            LayerGroupTextDto layerGroupTextDto = new LayerGroupTextDto();
            layerGroupTextDto.setLayer(stationSetting.getLayerGroupText());
            setLayerGroupText(layerGroupTextDto);
        } else {
            setLayerGroupText(null);
        }

        setWidthScreen(stationSetting.getWidthScreen());
        setHeightScreen(stationSetting.getHeightScreen());

        setX(stationSetting.getX());
        setY(stationSetting.getY());

        setDownloadInterval(stationSetting.getDownloadInterval());
        setUploadInterval(stationSetting.getUploadInterval());

        setMonday(stationSetting.getMonday());
        setTuesday(stationSetting.getTuesday());
        setWednesday(stationSetting.getWednesday());
        setThursday(stationSetting.getThursday());
        setFriday(stationSetting.getFriday());
        setSaturday(stationSetting.getSaturday());
        setSunday(stationSetting.getSunday());

        setShutdownOn(stationSetting.isShutdownOn());
        setTypePlayList(stationSetting.getTypePlayList());

        if (stationSetting.getFolderPlayList() !=null) {
            FolderDto folderDto = new FolderDto();
            folderDto.setFolder(stationSetting.getFolderPlayList());
            setFolderPlayList(folderDto);
        } else {
            setFolderPlayList(null);
        }

    }

}
