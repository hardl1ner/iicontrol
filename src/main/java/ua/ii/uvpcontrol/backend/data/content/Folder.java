package ua.ii.uvpcontrol.backend.data.content;

import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import ua.ii.uvpcontrol.backend.data.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Data
public class Folder extends AbstractEntity {
    @NotEmpty
    private String name;
    @ManyToOne(fetch = FetchType.LAZY)
    @Fetch(FetchMode.JOIN)
    private Folder parent;

    @OneToMany(fetch = FetchType.LAZY)
    @Fetch(FetchMode.JOIN)
    @NotNull
    private Set<FileInfo> fileInfos = new LinkedHashSet<>();

    public Folder() {
    }

    public Folder(String name, Folder parent, LinkedHashSet<FileInfo> fileInfos) {
        this.name = name;
        this.parent = parent;
        this.fileInfos = fileInfos;
    }

    public long getFileNumber(){
        return fileInfos.size();
    }

    @Override
    public String toString() {
        return getName();
    }
}
