package ua.ii.uvpcontrol.backend.data.layer;

import ua.ii.uvpcontrol.backend.data.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
public class LayerGroupImage extends AbstractEntity {
    @NotEmpty
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    private LayerGroupImage parent;

    @ManyToMany(fetch = FetchType.LAZY)
    @NotNull
    private Set<LayerImage> layerImages = new HashSet<>();

    public LayerGroupImage() {
    }

    public LayerGroupImage(String name, LayerGroupImage parent, Set<LayerImage> layerImages) {
        this.name = name;
        this.parent = parent;
        this.layerImages = layerImages;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LayerGroupImage getParent() {
        return parent;
    }

    public void setParent(LayerGroupImage parent) {
        this.parent = parent;
    }

    public Set<LayerImage> getLayerImages() {
        return layerImages;
    }

    public void setLayerImages(Set<LayerImage> layerImages) {
        this.layerImages = layerImages;
    }
}
