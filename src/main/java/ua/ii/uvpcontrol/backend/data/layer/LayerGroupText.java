package ua.ii.uvpcontrol.backend.data.layer;

import ua.ii.uvpcontrol.backend.data.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
public class LayerGroupText extends AbstractEntity {
    @NotEmpty
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    private LayerGroupText parent;

    @ManyToMany(fetch = FetchType.LAZY)
    @NotNull
    private Set<LayerText> layerTexts = new HashSet<>();

    public LayerGroupText() {
    }

    public LayerGroupText(String name, LayerGroupText parent, Set<LayerText> layerTexts) {
        this.name = name;
        this.parent = parent;
        this.layerTexts = layerTexts;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LayerGroupText getParent() {
        return parent;
    }

    public void setParent(LayerGroupText parent) {
        this.parent = parent;
    }

    public Set<LayerText> getLayerTexts() {
        return layerTexts;
    }

    public void setLayerTexts(Set<LayerText> layerTexts) {
        this.layerTexts = layerTexts;
    }
}
