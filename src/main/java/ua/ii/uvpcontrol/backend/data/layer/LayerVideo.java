package ua.ii.uvpcontrol.backend.data.layer;

import ua.ii.uvpcontrol.backend.data.AbstractEntity;
import ua.ii.uvpcontrol.backend.data.content.Folder;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class LayerVideo extends AbstractEntity {
    @NotEmpty
    @Size(max = 255)
    private String name;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Folder folder;

    @NotNull
    private Integer width;

    @NotNull
    private Integer height;

    @NotNull
    private Integer x;

    @NotNull
    private Integer y;

    private boolean active = true;

    public LayerVideo() {
    }

    public LayerVideo(String name, Folder folder, Integer width, Integer height, Integer x, Integer y, boolean active) {
        this.name = name;
        this.folder = folder;
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Folder getFolder() {
        return folder;
    }

    public void setFolder(Folder folder) {
        this.folder = folder;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return getName();
    }
}
