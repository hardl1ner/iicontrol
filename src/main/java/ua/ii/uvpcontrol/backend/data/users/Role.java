package ua.ii.uvpcontrol.backend.data.users;

public class Role {
    public static final String MANAGER = "manager";
    public static final String ADMIN = "admin";
    public static final String API = "api";

    private Role(){

    }

    public static String[] getAllRoles(){
        return new String[]{MANAGER, ADMIN, API};
    }

}
