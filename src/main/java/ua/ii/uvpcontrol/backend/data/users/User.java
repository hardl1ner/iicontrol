package ua.ii.uvpcontrol.backend.data.users;

import ua.ii.uvpcontrol.backend.data.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Objects;

@Entity(name = "UserInfo")
public class User extends AbstractEntity {

    @Column(unique=true)
    @NotEmpty(message = "Введите корректную почту.")
    @Email
    @Size(max = 255)
    private String email;

    @NotNull
    @Size(min = 4, max = 255)
    private String passwordHash;

    @Column(unique=true)
    @NotEmpty(message = "Введите имя.")
    @Size(max = 255)
    private String name;


    @NotBlank
    @Size(max = 255)
    private String role;

    private boolean locked = false;

    @PrePersist
    @PreUpdate
    private void prepareData(){
        this.email = email == null ? null : email.toLowerCase();
    }

    public User(){

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), email, name, role, locked);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }

        if (o == null || getClass() != o.getClass()){
            return false;
        }

        if (!super.equals(o)){
            return false;
        }

        User that = (User) o;
        return locked == that.locked &&
                Objects.equals(email, that.email) &&
                Objects.equals(name, that.name) &&
                Objects.equals(role, that.role);
    }
}
