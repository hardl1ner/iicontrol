package ua.ii.uvpcontrol.backend.data.layer;

import ua.ii.uvpcontrol.backend.data.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class LayerText extends AbstractEntity {
    public enum Type{
        text, clock, calendar
    }
    public enum StyleFont{
        bold, italic, plain
    }

    public enum Orientation{
        horizontal, vertical
    }

    public enum TextAlignment{
        center, left, right
    }

    public enum LocaleLanguage{
        UA, RU, US
    }

    @NotEmpty
    @Size(max = 255)
    private String name;
    @NotNull
    private Integer width;
    @NotNull
    private Integer height;
    @NotNull
    private Integer x;
    @NotNull
    private Integer y;
    @NotNull
    private Integer sizeFont;
    @NotEmpty
    private String text;
    @NotEmpty
    private String nameFont;
    @Enumerated(EnumType.STRING)
    @NotNull
    private LayerText.StyleFont styleFont;
    @Enumerated(EnumType.STRING)
    @NotNull
    private LayerText.Orientation orientation;
    @NotEmpty
    private String color = "522";
    @Enumerated(EnumType.STRING)
    @NotNull
    private LayerText.TextAlignment textAlignment;

    @Enumerated(EnumType.STRING)
    @NotNull
    private LayerText.LocaleLanguage localeLanguage;

    private boolean active = true;
    @Enumerated(EnumType.STRING)
    @NotNull
    private LayerText.Type type;

    public LayerText() {
    }

    public LayerText(String name, Integer width, Integer height, Integer x, Integer y, Integer sizeFont, String text, String nameFont, StyleFont styleFont, Orientation orientation, String color, TextAlignment textAlignment, LocaleLanguage localeLanguage, boolean active, Type type) {
        this.name = name;
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
        this.sizeFont = sizeFont;
        this.text = text;
        this.nameFont = nameFont;
        this.styleFont = styleFont;
        this.orientation = orientation;
        this.color = color;
        this.textAlignment = textAlignment;
        this.localeLanguage = localeLanguage;
        this.active = active;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public Integer getSizeFont() {
        return sizeFont;
    }

    public void setSizeFont(Integer sizeFont) {
        this.sizeFont = sizeFont;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getNameFont() {
        return nameFont;
    }

    public void setNameFont(String nameFont) {
        this.nameFont = nameFont;
    }

    public StyleFont getStyleFont() {
        return styleFont;
    }

    public void setStyleFont(StyleFont styleFont) {
        this.styleFont = styleFont;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public TextAlignment getTextAlignment() {
        return textAlignment;
    }

    public void setTextAlignment(TextAlignment textAlignment) {
        this.textAlignment = textAlignment;
    }

    public LocaleLanguage getLocaleLanguage() {
        return localeLanguage;
    }

    public void setLocaleLanguage(LocaleLanguage localeLanguage) {
        this.localeLanguage = localeLanguage;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
