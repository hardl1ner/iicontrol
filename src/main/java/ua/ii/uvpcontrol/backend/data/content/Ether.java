package ua.ii.uvpcontrol.backend.data.content;

import lombok.Data;
import ua.ii.uvpcontrol.backend.data.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Data
public class Ether extends AbstractEntity implements Cloneable{
    public enum Type{
        file, admixer, stream
    }
    private String url;
    @ManyToOne(fetch = FetchType.EAGER)
    private FileInfo file;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Ether.Type type;

    private LocalDateTime start;
    private LocalDateTime end;

    private boolean monday;
    private boolean tuesday;
    private boolean wednesday;
    private boolean thursday;
    private boolean friday;
    private boolean saturday;
    private boolean sunday;

    public Ether() {
    }

    public Ether(String url, FileInfo file, Type type, LocalDateTime start, LocalDateTime end, boolean monday, boolean tuesday, boolean wednesday, boolean thursday, boolean friday, boolean saturday, boolean sunday) {
        this.url = url;
        this.file = file;
        this.type = type;
        this.start = start;
        this.end = end;
        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
        this.saturday = saturday;
        this.sunday = sunday;
    }
}
