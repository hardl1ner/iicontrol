package ua.ii.uvpcontrol.backend.data.station;

import ua.ii.uvpcontrol.backend.data.AbstractEntity;
import ua.ii.uvpcontrol.backend.data.content.Ether;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.validation.constraints.*;
import java.util.*;

@Entity
public class Station extends AbstractEntity {
    @NotEmpty
    @Size(max = 255)
    private String name = "";

    @ManyToOne(fetch = FetchType.EAGER)
    private Station parent;

    @Size(max = 255)
    private String address = "";

    @ManyToOne(fetch = FetchType.LAZY)
    private StationSetting stationSetting;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<Ether> ethers;

    private boolean active = true;

    @Size(max = 255)
    private String keyStation;

    public Station(){

    }

    public Station(String name, Station parent, String address, StationSetting stationSetting, List<Ether> ethers, boolean active, String keyStation) {
        this.name = name;
        this.parent = parent;
        this.address = address;
        this.stationSetting = stationSetting;
        this.ethers = ethers;
        this.active = active;
        this.keyStation = keyStation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Station getParent() {
        return parent;
    }

    public void setParent(Station parent) {
        this.parent = parent;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public StationSetting getStationSetting() {
        return stationSetting;
    }

    public void setStationSetting(StationSetting stationSetting) {
        this.stationSetting = stationSetting;
    }

    public List<Ether> getEthers() {
        return ethers;
    }

    public void setEthers(List<Ether> ethers) {
        this.ethers = ethers;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getKeyStation() {
        return keyStation;
    }

    public void setKeyStation(String keyStation) {
        this.keyStation = keyStation;
    }

    //автогенерация ключа регистраци
    public static String generateKeyStation(){
        char[] chars = "abcdefghijklmnopqrstuvwxyzABSDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();
        Random r = new Random(System.currentTimeMillis());
        char[] id = new char[8];
        for (int i = 0;  i < 8;  i++) {
            id[i] = chars[r.nextInt(chars.length)];
        }
        return new String(id);
    }
}
