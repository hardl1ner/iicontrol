package ua.ii.uvpcontrol.backend.data.layer;

import ua.ii.uvpcontrol.backend.data.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
public class LayerGroupVideo extends AbstractEntity {
    @NotEmpty
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    private LayerGroupVideo parent;

    @ManyToMany(fetch = FetchType.LAZY)
    @NotNull
    private Set<LayerVideo> layerVideos = new HashSet<>();

    public LayerGroupVideo() {
    }

    public LayerGroupVideo(String name, LayerGroupVideo parent, Set<LayerVideo> layerVideos) {
        this.name = name;
        this.parent = parent;
        this.layerVideos = layerVideos;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LayerGroupVideo getParent() {
        return parent;
    }

    public void setParent(LayerGroupVideo parent) {
        this.parent = parent;
    }

    public Set<LayerVideo> getLayerVideos() {
        return layerVideos;
    }

    public void setLayerVideos(Set<LayerVideo> layerVideos) {
        this.layerVideos = layerVideos;
    }
}
