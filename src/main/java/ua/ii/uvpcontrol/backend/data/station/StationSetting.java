package ua.ii.uvpcontrol.backend.data.station;

import ua.ii.uvpcontrol.backend.data.AbstractEntity;
import ua.ii.uvpcontrol.backend.data.content.Folder;
import ua.ii.uvpcontrol.backend.data.layer.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalTime;

@Entity
public class StationSetting extends AbstractEntity {

    public enum TypePlayList{
        standard, indortv
    }
    @NotEmpty
    @NotNull
    @Column(unique=true)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @NotNull
    private LayerVideo layerVideo;
    @ManyToOne(fetch = FetchType.LAZY)
    private LayerGroupImage layerGroupImage;

    @ManyToOne(fetch = FetchType.LAZY)
    private LayerGroupPanel layerGroupPanel;
    @ManyToOne(fetch = FetchType.LAZY)
    private LayerGroupText layerGroupText;

    @NotNull
    private Integer widthScreen;

    @NotNull
    private Integer heightScreen;

    @NotNull
    private Integer x;

    @NotNull
    private Integer y;

    @NotNull
    @Min(1)
    @Max(1440)
    private Integer downloadInterval;

    @NotNull
    @Min(1)
    @Max(1440)
    private Integer uploadInterval;

    @Column
    @NotNull
    private LocalTime monday;
    @Column
    @NotNull
    private LocalTime tuesday;
    @Column
    @NotNull
    private LocalTime wednesday;
    @Column
    @NotNull
    private LocalTime thursday;
    @Column
    @NotNull
    private LocalTime friday;
    @Column
    @NotNull
    private LocalTime saturday;
    @Column
    @NotNull
    private LocalTime sunday;

    private boolean shutdownOn = true;

    @Enumerated(EnumType.STRING)
    @NotNull
    private StationSetting.TypePlayList typePlayList;

    @ManyToOne(fetch = FetchType.LAZY)
    private Folder folderPlayList;

    public StationSetting() {

    }

    public StationSetting(String name, LayerVideo layerVideo, LayerGroupImage layerGroupImage, LayerGroupPanel layerGroupPanel, LayerGroupText layerGroupText, Integer widthScreen, Integer heightScreen, Integer x, Integer y, Integer downloadInterval, Integer uploadInterval, LocalTime monday, LocalTime tuesday, LocalTime wednesday, LocalTime thursday, LocalTime friday, LocalTime saturday, LocalTime sunday, boolean shutdownOn, TypePlayList typePlayList, Folder folderPlayList) {
        this.name = name;
        this.layerVideo = layerVideo;
        this.layerGroupImage = layerGroupImage;
        this.layerGroupPanel = layerGroupPanel;
        this.layerGroupText = layerGroupText;
        this.widthScreen = widthScreen;
        this.heightScreen = heightScreen;
        this.x = x;
        this.y = y;
        this.downloadInterval = downloadInterval;
        this.uploadInterval = uploadInterval;
        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
        this.saturday = saturday;
        this.sunday = sunday;
        this.shutdownOn = shutdownOn;
        this.typePlayList = typePlayList;
        this.folderPlayList = folderPlayList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LayerVideo getLayerVideo() {
        return layerVideo;
    }

    public void setLayerVideo(LayerVideo layerVideo) {
        this.layerVideo = layerVideo;
    }

    public LayerGroupImage getLayerGroupImage() {
        return layerGroupImage;
    }

    public void setLayerGroupImage(LayerGroupImage layerGroupImage) {
        this.layerGroupImage = layerGroupImage;
    }

    public LayerGroupPanel getLayerGroupPanel() {
        return layerGroupPanel;
    }

    public void setLayerGroupPanel(LayerGroupPanel layerGroupPanel) {
        this.layerGroupPanel = layerGroupPanel;
    }

    public LayerGroupText getLayerGroupText() {
        return layerGroupText;
    }

    public void setLayerGroupText(LayerGroupText layerGroupText) {
        this.layerGroupText = layerGroupText;
    }

    public Integer getWidthScreen() {
        return widthScreen;
    }

    public void setWidthScreen(Integer widthScreen) {
        this.widthScreen = widthScreen;
    }

    public Integer getHeightScreen() {
        return heightScreen;
    }

    public void setHeightScreen(Integer heightScreen) {
        this.heightScreen = heightScreen;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public Integer getDownloadInterval() {
        return downloadInterval;
    }

    public void setDownloadInterval(Integer downloadInterval) {
        this.downloadInterval = downloadInterval;
    }

    public Integer getUploadInterval() {
        return uploadInterval;
    }

    public void setUploadInterval(Integer uploadInterval) {
        this.uploadInterval = uploadInterval;
    }

    public LocalTime getMonday() {
        return monday;
    }

    public void setMonday(LocalTime monday) {
        this.monday = monday;
    }

    public LocalTime getTuesday() {
        return tuesday;
    }

    public void setTuesday(LocalTime tuesday) {
        this.tuesday = tuesday;
    }

    public LocalTime getWednesday() {
        return wednesday;
    }

    public void setWednesday(LocalTime wednesday) {
        this.wednesday = wednesday;
    }

    public LocalTime getThursday() {
        return thursday;
    }

    public void setThursday(LocalTime thursday) {
        this.thursday = thursday;
    }

    public LocalTime getFriday() {
        return friday;
    }

    public void setFriday(LocalTime friday) {
        this.friday = friday;
    }

    public LocalTime getSaturday() {
        return saturday;
    }

    public void setSaturday(LocalTime saturday) {
        this.saturday = saturday;
    }

    public LocalTime getSunday() {
        return sunday;
    }

    public void setSunday(LocalTime sunday) {
        this.sunday = sunday;
    }

    public boolean isShutdownOn() {
        return shutdownOn;
    }

    public void setShutdownOn(boolean shutdownOn) {
        this.shutdownOn = shutdownOn;
    }

    public TypePlayList getTypePlayList() {
        return typePlayList;
    }

    public void setTypePlayList(TypePlayList typePlayList) {
        this.typePlayList = typePlayList;
    }

    public Folder getFolderPlayList() {
        return folderPlayList;
    }

    public void setFolderPlayList(Folder folderPlayList) {
        this.folderPlayList = folderPlayList;
    }
}
