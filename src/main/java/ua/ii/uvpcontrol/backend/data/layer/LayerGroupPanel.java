package ua.ii.uvpcontrol.backend.data.layer;

import ua.ii.uvpcontrol.backend.data.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
public class LayerGroupPanel extends AbstractEntity {
    @NotEmpty
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    private LayerGroupPanel parent;

    @ManyToMany(fetch = FetchType.LAZY)
    @NotNull
    private Set<LayerPanel> layerPanels = new HashSet<>();

    public LayerGroupPanel() {
    }

    public LayerGroupPanel(String name, LayerGroupPanel parent, Set<LayerPanel> layerPanels) {
        this.name = name;
        this.parent = parent;
        this.layerPanels = layerPanels;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LayerGroupPanel getParent() {
        return parent;
    }

    public void setParent(LayerGroupPanel parent) {
        this.parent = parent;
    }

    public Set<LayerPanel> getLayerPanels() {
        return layerPanels;
    }

    public void setLayerPanels(Set<LayerPanel> layerPanels) {
        this.layerPanels = layerPanels;
    }
}
