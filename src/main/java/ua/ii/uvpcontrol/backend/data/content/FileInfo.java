package ua.ii.uvpcontrol.backend.data.content;


import lombok.Data;
import ua.ii.uvpcontrol.backend.data.AbstractEntity;

import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
@Data
@Entity
public class FileInfo extends AbstractEntity {
    @NotEmpty
    private String file_name;
    @NotNull
    private Long file_time;
    @NotNull
    private Long file_size;
    @NotNull
    private Integer file_width;
    @NotNull
    private Integer file_height;
    @NotEmpty
    private String file_type;
    private String file_path;
    private LocalDate uploadDate;

    @Override
    public String toString() {
        return getFile_name();
    }

    public FileInfo() {
    }

    public FileInfo(String file_name, Long file_time, Long file_size, Integer file_width, Integer file_height, String file_type, String file_path, LocalDate uploadDate) {
        this.file_name = file_name;
        this.file_time = file_time;
        this.file_size = file_size;
        this.file_width = file_width;
        this.file_height = file_height;
        this.file_type = file_type;
        this.file_path = file_path;
        this.uploadDate = uploadDate;
    }
}
