package ua.ii.uvpcontrol.app.security;

import ua.ii.uvpcontrol.backend.data.users.User;

@FunctionalInterface
public interface CurrentUser {

    User getUser();
}
