package ua.ii.uvpcontrol.app;

import com.vaadin.flow.spring.annotation.SpringComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import ua.ii.uvpcontrol.backend.data.users.Role;
import ua.ii.uvpcontrol.backend.data.users.User;
import ua.ii.uvpcontrol.backend.repositories.user.UserRepository;

import javax.annotation.PostConstruct;

@SpringComponent
public class DateGenerator implements HasLogger {
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public DateGenerator(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @PostConstruct
    public void loadData(){
        if (userRepository.count() != 0L){
           getLogger().info("Использование существующей базы данных");
           return;
        }

        getLogger().info("«Генерация демонстрационных данных»");

        getLogger().info("... создание пользователей");
        createAdmin(userRepository, passwordEncoder);
        createManager(userRepository, passwordEncoder);
    }

    private User createAdmin(UserRepository userRepository, PasswordEncoder passwordEncoder){
        return userRepository.save(
                createUser("it.spec2.ii@gmail.com", "admin",  passwordEncoder.encode("admin"), Role.ADMIN, true)
        );
    }

    private User createManager(UserRepository userRepository, PasswordEncoder passwordEncoder){
        return userRepository.save(
                createUser("it@gmail.com", "it",  passwordEncoder.encode("1234"), Role.MANAGER, true)
        );
    }

    private User createUser(String email, String name, String passwordHash, String role,
                            boolean locked){
        User user = new User();
        user.setEmail(email);
        user.setName(name);
        user.setPasswordHash(passwordHash);
        user.setRole(role);
        user.setLocked(locked);
        return user;

    }
}
