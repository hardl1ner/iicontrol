package ua.ii.uvpcontrol.ui.component.station;

import com.flowingcode.vaadin.addons.ironicons.IronIcons;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import org.claspina.confirmdialog.ButtonOption;
import org.claspina.confirmdialog.ConfirmDialog;
import org.springframework.dao.DataIntegrityViolationException;
import ua.ii.uvpcontrol.backend.data.station.StationSetting;
import ua.ii.uvpcontrol.backend.service.content.FolderService;
import ua.ii.uvpcontrol.backend.service.exception.UniqueDataException;
import ua.ii.uvpcontrol.backend.service.layer.LayerGroupImageService;
import ua.ii.uvpcontrol.backend.service.layer.LayerGroupPanelService;
import ua.ii.uvpcontrol.backend.service.layer.LayerGroupTextService;
import ua.ii.uvpcontrol.backend.service.layer.LayerGroupVideoService;
import ua.ii.uvpcontrol.backend.service.station.StationSettingService;

import java.time.LocalTime;
@CssImport(value = "./styles/station_settings.css", themeFor = "vaadin-grid")
public class StationSettingGrid extends VerticalLayout {
    private final StationSettingService stationSettingService;

    private final Grid<StationSetting> grid = new Grid<>(StationSetting.class);
    private TextField filterText = new TextField();

    private StationSettingForm form;

    public StationSettingGrid(StationSettingService stationSettingService, LayerGroupImageService layerGroupImageService,
                              LayerGroupPanelService layerGroupPanelService, LayerGroupVideoService layerGroupVideoService,
                              LayerGroupTextService layerGroupTextService, FolderService folderService){

        this.stationSettingService = stationSettingService;

        addClassName("list-view");
        setSizeFull();

        configureGrid();

        form = new StationSettingForm(layerGroupImageService, layerGroupPanelService,
                layerGroupVideoService, layerGroupTextService, folderService
        );
        form.addListener(StationSettingForm.SaveEvent.class, this::saveStationSetting);
        form.addListener(StationSettingForm.DeleteEvent.class, this::deleteStationSetting);
        form.addListener(StationSettingForm.CloseEvent.class, e -> closeEditor());
        form.addListener(StationSettingForm.CopyEvent.class, this::copyStationSetting);

        Div content = new Div(grid, form);
        content.addClassName("content");
        content.setSizeFull();

        add(content);
        updateList();

        closeEditor();
    }

    private void copyStationSetting(StationSettingForm.CopyEvent event){
        ConfirmDialog
                .createQuestion()
                .withCaption("Сохранение")
                .withMessage("Сохранить изменения?")
                .withOkButton(() -> {
                    try {
                            StationSetting clone = new StationSetting(event.getSettingStation().getName() + " - " + LocalTime.now(),
                                    event.getSettingStation().getLayerVideo(), event.getSettingStation().getLayerGroupImage(),
                                    event.getSettingStation().getLayerGroupPanel(), event.getSettingStation().getLayerGroupText(),
                                    event.getSettingStation().getWidthScreen(), event.getSettingStation().getHeightScreen(),
                                    event.getSettingStation().getX(), event.getSettingStation().getY(),
                                    event.getSettingStation().getDownloadInterval(), event.getSettingStation().getUploadInterval(),
                                    event.getSettingStation().getMonday(), event.getSettingStation().getTuesday(),
                                    event.getSettingStation().getWednesday(), event.getSettingStation().getThursday(),
                                    event.getSettingStation().getFriday(), event.getSettingStation().getSaturday(),
                                    event.getSettingStation().getSunday(), event.getSettingStation().isShutdownOn(),
                                    event.getSettingStation().getTypePlayList(), event.getSettingStation().getFolderPlayList());
                        stationSettingService.save(clone);
                        Notification.show("Сохранено!", 2000,
                                Notification.Position.MIDDLE);
                        updateList();
                        closeEditor();
                    }  catch (UniqueDataException uniqueDataException){
                        new Notification(
                                uniqueDataException.getMessage(), 3000).open();
                    }
                }, ButtonOption.focus(), ButtonOption.caption("Да"))
                .withCancelButton(ButtonOption.caption("Нет"))
                .open();

    }

    private void saveStationSetting(StationSettingForm.SaveEvent event){
        ConfirmDialog
                .createQuestion()
                .withCaption("Сохранение")
                .withMessage("Сохранить изменения?")
                .withOkButton(() -> {
                    try {
                        stationSettingService.save(event.getSettingStation());
                        Notification.show("Сохранено!", 2000,
                                Notification.Position.MIDDLE);
                        updateList();
                        closeEditor();
                    }  catch (UniqueDataException uniqueDataException){
                        new Notification(
                                uniqueDataException.getMessage(), 3000).open();
                    }
                }, ButtonOption.focus(), ButtonOption.caption("Да"))
                .withCancelButton(ButtonOption.caption("Нет"))
                .open();

    }

    private void deleteStationSetting(StationSettingForm.DeleteEvent event) {
        ConfirmDialog
                .createQuestion()
                .withCaption("Удаление")
                .withMessage("Вы уверенные что хотите удалить?\nОтменить действие будет не возможно")
                .withOkButton(() -> {
                    try {
                        stationSettingService.delete(event.getSettingStation());
                        Notification.show("Удалено!", 2000,
                                Notification.Position.MIDDLE);
                        updateList();
                        closeEditor();
                    } catch (DataIntegrityViolationException dataIntegrityViolationException){
                        new Notification(
                                "Ошибка: Не возможно удалить. Выбранная настройка используется.", 3000).open();
                        updateList();
                        closeEditor();
                    }
                }, ButtonOption.focus(), ButtonOption.caption("Да"))
                .withCancelButton(ButtonOption.caption("Нет"))
                .open();

    }

    private void configureGrid(){
        grid.addClassName("contact-grid");
        grid.setSizeFull();
        grid.setColumns("name", "widthScreen", "heightScreen", "x", "y", "typePlayList");
        grid.getColumnByKey("name").setHeader("Имя настроек").setResizable(true);
        grid.getColumnByKey("widthScreen").setHeader("Ширина экрана");
        grid.getColumnByKey("heightScreen").setHeader("Высота экрана");
        grid.getColumnByKey("x").setHeader("X");
        grid.getColumnByKey("y").setHeader("Y");
        grid.getColumnByKey("typePlayList").setHeader("Тип плейлиста");
        grid.getElement().getThemeList().add("grid-no-padding");
        grid.addColumn(item -> "").setKey("rowIndex").setFlexGrow(0);


        HeaderRow filterRow = grid.prependHeaderRow();

        // First filter
        filterText.setPlaceholder("Поиск...");
        filterText.setClearButtonVisible(true);
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.addValueChangeListener(e -> updateList());
        Button addStationSettingButton = new Button(IronIcons.ADD.create());
        addStationSettingButton.addClickListener(click -> addStationSetting());


        filterRow.getCell(grid.getColumnByKey("name")).setComponent(filterText);
        filterRow.getCell(grid.getColumnByKey("rowIndex")).setComponent(addStationSettingButton);
        filterText.setSizeFull();
        addStationSettingButton.setSizeFull();
        filterText.getElement().setAttribute("focus-target", "");

        grid.addAttachListener(event -> grid.getColumnByKey("rowIndex").getElement().executeJs(
                "this.renderer = function(root, column, rowData) {root.textContent = rowData.index + 1}"
        ));

        grid.setColumnOrder(
                grid.getColumnByKey("rowIndex"),
                grid.getColumnByKey("name"),
                grid.getColumnByKey("widthScreen"),
                grid.getColumnByKey("heightScreen"),
                grid.getColumnByKey("x"),
                grid.getColumnByKey("y"),
                grid.getColumnByKey("typePlayList"));


        grid.asSingleSelect().addValueChangeListener(event ->
                editStationSetting(event.getValue()));
    }

    public void editStationSetting(StationSetting stationSetting){
        if (stationSetting == null) {
            closeEditor();
        } else {
            form.setSettingStation(stationSetting);
            form.setVisible(true);
            addClassName("editing");
        }
    }

    private void closeEditor(){
        form.setSettingStation(null);
        form.setVisible(false);
        removeClassName("editing");
    }

    void addStationSetting(){
        grid.asSingleSelect().clear();
        editStationSetting(new StationSetting());
    }

    private void updateList(){
        grid.setItems(stationSettingService.findAll(filterText.getValue()));
    }

}
