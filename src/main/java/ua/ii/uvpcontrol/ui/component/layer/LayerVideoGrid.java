package ua.ii.uvpcontrol.ui.component.layer;

import com.flowingcode.vaadin.addons.ironicons.IronIcons;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.grid.GridSortOrderBuilder;
import com.vaadin.flow.component.grid.contextmenu.GridContextMenu;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.hierarchy.AbstractBackEndHierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalQuery;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import org.claspina.confirmdialog.ButtonOption;
import org.claspina.confirmdialog.ConfirmDialog;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionSystemException;
import ua.ii.uvpcontrol.backend.data.layer.LayerGroupPanel;
import ua.ii.uvpcontrol.backend.data.layer.LayerGroupVideo;
import ua.ii.uvpcontrol.backend.data.layer.LayerPanel;
import ua.ii.uvpcontrol.backend.data.layer.LayerVideo;
import ua.ii.uvpcontrol.backend.data.station.Station;
import ua.ii.uvpcontrol.backend.service.content.FolderService;
import ua.ii.uvpcontrol.backend.service.exception.UniqueDataException;
import ua.ii.uvpcontrol.backend.service.layer.LayerGroupVideoService;
import ua.ii.uvpcontrol.backend.service.layer.LayerVideoService;

import java.time.LocalTime;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;

public class LayerVideoGrid extends VerticalLayout {
    private final LayerVideoService layerVideoService;
    private final LayerGroupVideoService layerGroupVideoService;

    private final Grid<LayerVideo> grid = new Grid<>(LayerVideo.class);

    private final TreeGrid<LayerGroupVideo> gridTree = new TreeGrid<>();
    private final TextField filterText = new TextField();

    private final LayerVideoForm form;

    public LayerVideoGrid(LayerVideoService layerVideoService, LayerGroupVideoService layerGroupVideoService,
                          FolderService folderService){
        this.layerVideoService = layerVideoService;
        this.layerGroupVideoService = layerGroupVideoService;

        addClassName("list-view");
        setSizeFull();
        Div div = new Div(gridTree);
        div.addClassName("content");
        div.setSizeFull();
        div.setWidth("400px");
        configureGrid();

        setPadding(true);
        setSpacing(false);

        form = new LayerVideoForm(folderService);
        form.addListener(LayerVideoForm.SaveEvent.class, this::saveLayerVideo);
        form.addListener(LayerVideoForm.DeleteEvent.class, this::deleteLayerVideo);
        form.addListener(LayerVideoForm.CloseEvent.class, e -> closeEditor());
        form.addListener(LayerVideoForm.CopyEvent.class, this::copyLayerVideo);

        Div content = new Div(div, grid, form);
        content.addClassName("content");
        content.setSizeFull();

        add(getToolbar(), content);

        closeEditor();
    }

    private void copyLayerVideo(LayerVideoForm.CopyEvent event){
        Dialog dialog = new Dialog();


        TreeGrid<LayerGroupVideo> tempTreeGrid = new TreeGrid<>();
        tempTreeGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        tempTreeGrid.setWidth("500px");
        tempTreeGrid.setHeight("400px");

        tempTreeGrid.addHierarchyColumn(LayerGroupVideo::getName).setHeader("Группы");

        HierarchicalDataProvider<LayerGroupVideo, Void> dataProvider =
                new AbstractBackEndHierarchicalDataProvider<>() {

                    @Override
                    public int getChildCount(HierarchicalQuery<LayerGroupVideo, Void> query) {
                        return (int) layerGroupVideoService.getChildCount(query.getParent());
                    }

                    @Override
                    public boolean hasChildren(LayerGroupVideo item) {
                        return layerGroupVideoService.hasChildren(item);
                    }

                    @Override
                    protected Stream<LayerGroupVideo> fetchChildrenFromBackEnd(
                            HierarchicalQuery<LayerGroupVideo, Void> query) {
                        return layerGroupVideoService.fetchChildren(query.getParent()).stream();
                    }
                };

        tempTreeGrid.setDataProvider(dataProvider);

        Button save = new Button(IronIcons.SAVE.create());
        Button cancel = new Button(IronIcons.CANCEL.create());

        cancel.addClickListener(eventClick -> dialog.close());
        save.addClickListener(buttonClickEvent -> ConfirmDialog
                .createQuestion()
                .withCaption("Сохранение")
                .withMessage("Сохранить изменения?")
                .withOkButton(() -> {
                    if (tempTreeGrid.asMultiSelect().getSelectedItems().size() > 0) {
                        tempTreeGrid.asMultiSelect().getSelectedItems().forEach(layerGroupVideo -> {
                            LayerVideo clone = new LayerVideo(event.getLayerVideo().getName() + " - " + (LocalTime.now()), event.getLayerVideo().getFolder(),
                                    event.getLayerVideo().getWidth(), event.getLayerVideo().getHeight(), event.getLayerVideo().getX(),
                                    event.getLayerVideo().getY(), false);

                            layerVideoService.save(clone);
                            layerGroupVideo.getLayerVideos().add(clone);
                            layerGroupVideoService.edit(layerGroupVideo);
                        });
                        gridTree.getDataProvider().refreshAll();
                        gridTree.select(tempTreeGrid.getSelectedItems().iterator().next());
                        grid.setItems(tempTreeGrid.getSelectedItems().iterator().next().getLayerVideos());
                        Notification.show("Сохранено!", 2000,
                                Notification.Position.MIDDLE);
                        dialog.close();
                    } else {
                        Notification.show("Выберить группу в которой нужно создать копию!", 2000,
                                Notification.Position.MIDDLE);
                    }
                }, ButtonOption.focus(), ButtonOption.caption("Да"))
                .withCancelButton(ButtonOption.caption("Нет"))
                .open());

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setWidth("550px");
        verticalLayout.setHeight("550px");
        verticalLayout.add(new H2("Клонирование"),tempTreeGrid, new HorizontalLayout(save, cancel));

        dialog.setWidth("600px");
        dialog.setHeight("600px");

        dialog.add(verticalLayout);
        dialog.open();
    }

    private void saveLayerVideo(LayerVideoForm.SaveEvent event){
        ConfirmDialog
                .createQuestion()
                .withCaption("Сохранение")
                .withMessage("Сохранить изменения?")
                .withOkButton(() -> {
                    try {
                        layerVideoService.save(event.getLayerVideo());
                        layerGroupVideoService.findAll().forEach(layerGroupVideo -> {
                            if (layerGroupVideo.getName().equals(gridTree.asSingleSelect().getValue().getName())){
                                layerGroupVideo.getLayerVideos().add(event.getLayerVideo());
                                layerGroupVideoService.edit(layerGroupVideo);
                                gridTree.getDataProvider().refreshAll();
                                gridTree.deselect(layerGroupVideo);
                                gridTree.select(layerGroupVideo);
                                grid.setItems(gridTree.asSingleSelect().getValue().getLayerVideos());
                            }
                        });
                        Notification.show("Сохранено!", 2000,
                                Notification.Position.MIDDLE);
                        closeEditor();
                    }  catch (DataIntegrityViolationException dataIntegrityViolationException){
                        new Notification(
                                "Ошибка: Слой с именем '" + event.getLayerVideo().getName() + "' существуе.\n" +
                                        "Дубликат имен запрещен.", 3000).open();
                        closeEditor();
                    }
                }, ButtonOption.focus(), ButtonOption.caption("Да"))
                .withCancelButton(ButtonOption.caption("Нет"))
                .open();

    }

    private void deleteLayerVideo(LayerVideoForm.DeleteEvent event) {
        ConfirmDialog
                .createQuestion()
                .withCaption("Удаление")
                .withMessage("Вы уверенные что хотите удалить?\nОтменить действие будет не возможно")
                .withOkButton(() -> layerGroupVideoService.findAll().forEach(layerGroupVideo -> {
                    if (layerGroupVideo.getName().equals(gridTree.asSingleSelect().getValue().getName())) {
                        try {
                            layerVideoService.delete(grid.asSingleSelect().getValue());
                            layerGroupVideo.getLayerVideos().remove(grid.asSingleSelect().getValue());
                            layerGroupVideoService.edit(layerGroupVideo);
                            gridTree.getDataProvider().refreshAll();
                            gridTree.deselect(layerGroupVideo);
                            gridTree.select(layerGroupVideo);
                            grid.setItems(gridTree.asSingleSelect().getValue().getLayerVideos());
                            new Notification(
                                    "Слой удален", 3000).open();
                        } catch (DataIntegrityViolationException d) {
                            if (d.getRootCause().getMessage().contains("layer_group_video_layer_videos")) {
                                layerGroupVideo.getLayerVideos().remove(grid.asSingleSelect().getValue());
                                layerGroupVideoService.edit(layerGroupVideo);
                                layerVideoService.delete(grid.asSingleSelect().getValue());
                                gridTree.getDataProvider().refreshAll();
                                gridTree.deselect(layerGroupVideo);
                                gridTree.select(layerGroupVideo);
                                grid.setItems(gridTree.asSingleSelect().getValue().getLayerVideos());

                                new Notification(
                                        "Слой удален", 3000).open();
                            } else {
                                new Notification(
                                        "Слой - " + grid.asSingleSelect().getValue().getName() + " используется, удалить не возможно",
                                        3000).open();
                            }
                        }
                    }
                }), ButtonOption.focus(), ButtonOption.caption("Да"))
                .withCancelButton(ButtonOption.caption("Нет"))
                .open();

    }

    private void configureGrid(){
        GridContextMenu<LayerGroupVideo> contextMenu = new GridContextMenu<>(gridTree);

        gridTree.setSizeFull();
        gridTree.setWidth("200px");
        gridTree.addHierarchyColumn(LayerGroupVideo::getName).setHeader("Группы");


        grid.addClassName("contact-grid");
        grid.setSizeFull();
        grid.setColumns("name", "folder", "width", "height", "x", "y", "active");
        grid.getColumnByKey("name").setHeader("Имя").setResizable(true);
        grid.getColumnByKey("folder").setHeader("Папка").setResizable(true);
        grid.getColumnByKey("width").setHeader("Ширина");
        grid.getColumnByKey("height").setHeader("Высота");
        grid.getColumnByKey("x").setHeader("X");
        grid.getColumnByKey("y").setHeader("Y");
        grid.removeColumnByKey("active");
        grid.addColumn(new ComponentRenderer<>(layerVideo -> {
            Checkbox checkbox = new Checkbox();
            checkbox.setValue(layerVideo.isActive());
            checkbox.addClickListener(checkboxClickEvent -> ConfirmDialog
                    .createQuestion()
                    .withCaption("Изменения статуса")
                    .withMessage("Применить изменение?")
                    .withOkButton(() -> {
                        layerVideo.setActive(checkbox.getValue());
                        layerVideoService.save(layerVideo);
                        updateList();
                    }, ButtonOption.focus(), ButtonOption.caption("Да"))
                    .withCancelButton(() -> checkbox.setValue(layerVideo.isActive()), ButtonOption.caption("Нет"))
                    .open());
            return checkbox;
        })).setHeader("Активирован").setKey("active");

        grid.getColumns().forEach(col -> col.setAutoWidth(true));

        grid.addColumn(item -> "").setKey("rowIndex").setFlexGrow(0).setWidth("75px");

        grid.addAttachListener(event -> grid.getColumnByKey("rowIndex").getElement().executeJs(
                "this.renderer = function(root, column, rowData) {root.textContent = rowData.index + 1}"
        ));

        grid.setColumnOrder(
                grid.getColumnByKey("rowIndex"),
                grid.getColumnByKey("name"),
                grid.getColumnByKey("folder"),
                grid.getColumnByKey("width"),
                grid.getColumnByKey("height"),
                grid.getColumnByKey("x"),
                grid.getColumnByKey("y"),
                grid.getColumnByKey("active"));

        List<GridSortOrder<LayerVideo>> sortByNumber = new GridSortOrderBuilder<LayerVideo>()
                .thenAsc(grid.getColumnByKey("name")).build();
        grid.sort(sortByNumber);

        grid.asSingleSelect().addValueChangeListener(event ->
                editLayerVideo(event.getValue()));

        gridTree.asSingleSelect().addValueChangeListener(gridFolderComponentValueChangeEvent -> {
            if(gridFolderComponentValueChangeEvent.getValue() != null) {
                grid.setItems(gridFolderComponentValueChangeEvent.getValue().getLayerVideos());
            }
        });



        HierarchicalDataProvider<LayerGroupVideo, Void> dataProvider =
                new AbstractBackEndHierarchicalDataProvider<>() {

                    @Override
                    public int getChildCount(HierarchicalQuery<LayerGroupVideo, Void> query) {
                        return (int) layerGroupVideoService.getChildCount(query.getParent());
                    }

                    @Override
                    public boolean hasChildren(LayerGroupVideo item) {
                        return layerGroupVideoService.hasChildren(item);
                    }

                    @Override
                    protected Stream<LayerGroupVideo> fetchChildrenFromBackEnd(
                            HierarchicalQuery<LayerGroupVideo, Void> query) {
                        return layerGroupVideoService.fetchChildren(query.getParent()).stream().
                                sorted(Comparator.comparing(LayerGroupVideo::getName));
                    }
                };

        gridTree.setDataProvider(dataProvider);

        contextMenu.addItem(IronIcons.ADD.create(), event -> {
            if (event.getItem().isPresent()) {
                event.getItem().ifPresent(layerGroupVideo -> createAddLayerGroupVideoForm(layerGroupVideoService,
                        layerGroupVideo, dataProvider).open());
            } else {
                createAddLayerGroupVideoForm(layerGroupVideoService, null, dataProvider).open();
            }
        });

        contextMenu.addItem(IronIcons.DELETE.create(), event -> event.getItem().ifPresent(layerGroupVideo -> {
            try {

                if (layerGroupVideo.getLayerVideos().size() == 0) {
                    layerGroupVideoService.delete(layerGroupVideo);
                    dataProvider.refreshAll();
                } else {
                    new Notification(
                            "Ошибка: Не возможно удалить группу '" + layerGroupVideo.getName() + "'.\n" +
                                    "В группе есть слои.", 3000).open();
                }
            } catch (DataIntegrityViolationException dataIntegrityViolationException){
                new Notification(
                        "Ошибка: Не возможно удалить группу '" + layerGroupVideo.getName() + "'.\n" +
                                "В группе есть вложенные группы.", 3000).open();
            }
        }));
    }

    public void editLayerVideo(LayerVideo layerVideo){
        if (layerVideo == null) {
            closeEditor();
        } else {
            form.setLayerVideo(layerVideo);
            form.setVisible(true);
            addClassName("editing");
        }
    }

    private void closeEditor(){
        form.setLayerVideo(null);
        form.setVisible(false);
    }

    private HorizontalLayout getToolbar(){
        filterText.setPlaceholder("Поиск...");
        filterText.setClearButtonVisible(true);
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.addValueChangeListener(e -> updateList());

        Button addLayerVideoButton = new Button(IronIcons.ADD.create());
        addLayerVideoButton.setEnabled(false);
        gridTree.addSelectionListener(selectionEvent -> addLayerVideoButton
                .setEnabled(selectionEvent.getFirstSelectedItem().isPresent()));

        addLayerVideoButton.addClickListener(click -> addLayerVideo());

        HorizontalLayout toolbar = new HorizontalLayout(filterText, addLayerVideoButton);
        toolbar.addClassName("toolbar");

        return toolbar;
    }

    void addLayerVideo(){
        grid.asSingleSelect().clear();
        editLayerVideo(new LayerVideo());
    }

    private void updateList(){
        grid.setItems(gridTree.asSingleSelect()
                .getValue()
                .getLayerVideos()
                .stream()
                .filter(layerVideo  -> layerVideo.getName()
                        .toLowerCase(Locale.ROOT)
                        .contains(filterText.getValue())));
    }

    private Dialog createAddLayerGroupVideoForm(LayerGroupVideoService layerGroupVideoService, LayerGroupVideo parent,
                                             HierarchicalDataProvider<LayerGroupVideo, Void> dataProvider){
        Dialog dialog = new Dialog();
        dialog.setCloseOnEsc(false);
        dialog.setCloseOnOutsideClick(false);

        TextField textField = new TextField("Имя группы");

        Binder<LayerGroupVideo> binder = new Binder<>(LayerGroupVideo.class);

        binder.forField(textField)
                .withValidator(min -> min.length() >= 2, "Минимум 2 символа")
                .bind(LayerGroupVideo::getName, LayerGroupVideo::setName);

        dialog.add(textField);

        Button confirmButton = new Button("Добавить", event -> {
            try {
                LayerGroupVideo layerGroupVideo;
                if (parent != null) {
                    layerGroupVideo = new LayerGroupVideo(textField.getValue(), parent, new HashSet<>());
                } else {
                    layerGroupVideo = new LayerGroupVideo(textField.getValue(), null, new HashSet<>());
                }
                layerGroupVideoService.save(layerGroupVideo);
                dataProvider.refreshAll();
                new Notification(
                        "Сохранено.", 3000).open();
                dialog.close();
            } catch (TransactionSystemException transactionSystemException){
                new Notification(
                        "Ошибка: Имя не может быть пустым!!", 3000).open();
            }  catch (UniqueDataException uniqueDataException){
                new Notification(
                        uniqueDataException.getMessage(), 3000).open();
            }

        });
        confirmButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        Button cancelButton = new Button("Отмена", event -> dialog.close());

        cancelButton.addThemeVariants(ButtonVariant.LUMO_ERROR);

        dialog.add(new Div( confirmButton, new Text("    "), cancelButton));

        return dialog;
    }

}
