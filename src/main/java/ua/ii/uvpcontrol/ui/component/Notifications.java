package ua.ii.uvpcontrol.ui.component;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

import java.util.ArrayList;
import java.util.List;

public interface Notifications {
   List<Notification> allNotifications = new ArrayList<>();

    default void error(String text) {
        Notification notification = new Notification();
        notification.addThemeVariants(NotificationVariant.LUMO_ERROR);

        Div statusText = new Div(new Text(text));
        Button closeButton = new Button(new Icon("lumo", "cross"));
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
        closeButton.getElement().
                setAttribute("aria-label","Close");
        closeButton.addClickListener(event -> notification.close());

        Button closeAllButton = new Button("Закрыть все");
        closeAllButton.setMaxWidth("130px");
        closeAllButton.setMinWidth("130px");
        closeAllButton.addThemeVariants(ButtonVariant.LUMO_SMALL);
        closeAllButton.getElement().
                setAttribute("aria-label","Close");
        closeAllButton.addClickListener(event -> {
            if (allNotifications.size() > 0) {
                allNotifications.forEach(Notification::close);
            }
        });

        HorizontalLayout layout = new HorizontalLayout(statusText, closeAllButton, closeButton);
        layout.setAlignItems(FlexComponent.Alignment.CENTER);

        allNotifications.add(notification);
        notification.add(layout);
        notification.open();
    }

    default void success(String text) {
        Notification notification = new Notification();
        notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);
        notification.setDuration(5000);

        Div statusText = new Div(new Text(text));
        Button closeButton = new Button(new Icon("lumo", "cross"));
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
        closeButton.getElement().
                setAttribute("aria-label","Close");
        closeButton.addClickListener(event -> notification.close());

        Button closeAllButton = new Button("Закрыть все");
        closeAllButton.setMaxWidth("130px");
        closeAllButton.setMinWidth("130px");
        closeAllButton.addThemeVariants(ButtonVariant.LUMO_SMALL);
        closeAllButton.getElement().
                setAttribute("aria-label","Close");
        closeAllButton.addClickListener(event -> {
            if (allNotifications.size() > 0) {
                allNotifications.forEach(Notification::close);
            }
        });

        HorizontalLayout layout = new HorizontalLayout(statusText, closeAllButton, closeButton);
        layout.setAlignItems(FlexComponent.Alignment.CENTER);

        allNotifications.add(notification);
        notification.add(layout);
        notification.open();
    }

}
