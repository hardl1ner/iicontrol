package ua.ii.uvpcontrol.ui.component.station;

import com.flowingcode.vaadin.addons.ironicons.EditorIcons;
import com.flowingcode.vaadin.addons.ironicons.IronIcons;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.IronIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.provider.hierarchy.AbstractBackEndHierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalQuery;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import org.claspina.confirmdialog.ButtonOption;
import org.claspina.confirmdialog.ConfirmDialog;
import org.springframework.dao.DataIntegrityViolationException;
import ua.ii.uvpcontrol.backend.data.station.Station;
import ua.ii.uvpcontrol.backend.service.exception.UniqueDataException;
import ua.ii.uvpcontrol.backend.service.station.StationSettingService;
import ua.ii.uvpcontrol.backend.service.station.StationService;

import java.time.LocalTime;
import java.util.Comparator;
import java.util.stream.Stream;

public class StationGrid extends VerticalLayout {
    private final StationService stationService;
    private final StationSettingService stationSettingService;
    private final TreeGrid<Station> gridTree = new TreeGrid<>();
    private final TextField filterText = new TextField();

    private final StationForm form;

    public StationGrid(StationService stationService,
                        StationSettingService stationSettingService){
        this.stationService = stationService;
        this.stationSettingService = stationSettingService;

        addClassName("list-view");
        setSizeFull();
        configureGrid();

        setPadding(true);
        setSpacing(false);

        form = new StationForm(stationSettingService, stationService);
        form.addListener(StationForm.SaveEvent.class, this::saveStation);
        form.addListener(StationForm.DeleteEvent.class, this::deleteStation);
        form.addListener(StationForm.CloseEvent.class, e -> closeEditor());

        Div content = new Div(gridTree, form);
        content.addClassName("content");
        content.setSizeFull();

        add(content);

        closeEditor();
    }

    private void saveStation(StationForm.SaveEvent event){
        ConfirmDialog
                .createQuestion()
                .withCaption("Сохранение")
                .withMessage("Сохранить изменения?")
                .withOkButton(() -> {
                    try {
                        String message = "";
                        if (event.getStation().getParent() != null) {
                                if (event.getStation().getKeyStation() != null) {
                                    stationService.save(event.getStation());
                                    message = "Сохранено!";
                                    closeEditor();
                                    updateList();
                                } else {
                                    message = "Родителя нельзя присвоить к другой группе!";
                                }

                        } else {
                            stationService.save(event.getStation());
                            message = "Сохранено!";
                            closeEditor();
                            updateList();
                        }

                        Notification.show(message, 2000,
                                Notification.Position.MIDDLE);

                    }  catch (DataIntegrityViolationException dataIntegrityViolationException){
                        new Notification(
                                "Ошибка: " + dataIntegrityViolationException, 3000).open();
                    }
                }, ButtonOption.focus(), ButtonOption.caption("Да"))
                .withCancelButton(ButtonOption.caption("Нет"))
                .open();

    }

    private void deleteStation(StationForm.DeleteEvent event) {

            ConfirmDialog
                    .createQuestion()
                    .withCaption("Удаление")
                    .withMessage("Вы уверенные что хотите удалить?\nОтменить действие будет не возможно")
                    .withOkButton(() -> {
                        try {
                        stationService.delete(event.getStation());
                        Notification.show("Удалено!", 2000,
                                Notification.Position.MIDDLE);
                        closeEditor();
                        updateList();
                        } catch (UniqueDataException uniqueDataException){
                            new Notification(
                                    uniqueDataException.getMessage(), 3000).open();
                            closeEditor();
                        }
                    }, ButtonOption.focus(), ButtonOption.caption("Да"))
                    .withCancelButton(ButtonOption.caption("Нет"))
                    .open();


    }

    private void configureGrid(){
        gridTree.setSizeFull();
        gridTree.addColumn(item -> "").setKey("temp").setFlexGrow(0).setWidth("80px");
        gridTree.addHierarchyColumn(Station::getName).setHeader("Станции").setResizable(true).setKey("name").setSortable(true);
        gridTree.addColumn(Station::getAddress).setHeader("Адресс").setResizable(true);
        gridTree.addColumn(Station::getKeyStation).setHeader("Ключ активации");
        gridTree.setSelectionMode(Grid.SelectionMode.NONE);
        gridTree.addColumn(new ComponentRenderer<>(station -> {
            Checkbox checkbox = new Checkbox();
            checkbox.setValue(station.isActive());
            checkbox.addClickListener(checkboxClickEvent -> checkbox.setValue(station.isActive()));
            return checkbox;
        })).setHeader("Заблокирован").setKey("active");
        gridTree.addColumn(new ComponentRenderer<>(this::createButtonsLayout))
                .setHeader("Действия").setKey("action").setWidth("100px");

        HierarchicalDataProvider<Station, Void> dataProvider =
                new AbstractBackEndHierarchicalDataProvider<>() {

                    @Override
                    public int getChildCount(HierarchicalQuery<Station, Void> query) {
                        return (int) stationService.getChildCount(query.getParent());
                    }

                    @Override
                    public boolean hasChildren(Station item) {
                        return stationService.hasChildren(item);
                    }

                    @Override
                    protected Stream<Station> fetchChildrenFromBackEnd(
                            HierarchicalQuery<Station, Void> query) {
                        return stationService.fetchChildren(query.getParent()).stream().
                                sorted(Comparator.comparing(Station::getName));
                    }
                };

        filterText.setPlaceholder("Поиск...");
        filterText.setClearButtonVisible(true);
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.addValueChangeListener(e -> updateList());

        Button addStationButton = new Button(IronIcons.ADD.create());
        addStationButton.addClickListener(click -> addStation());

        HeaderRow filterRow = gridTree.prependHeaderRow();
        filterRow.getCell(gridTree.getColumnByKey("name")).setComponent(filterText);
        filterRow.getCell(gridTree.getColumnByKey("temp")).setComponent(addStationButton);
        filterText.setSizeFull();
        filterText.getElement().setAttribute("focus-target", "");

        gridTree.setDataProvider(dataProvider);
    }

    public void editStation(Station station){
        if (station == null) {
            closeEditor();
        } else {
            if (station.getStationSetting() != null) {
                station.setStationSetting(stationSettingService.findById(station.getStationSetting().getId()));
            }
            form.setStation(station);
            form.setVisible(true);
            addClassName("editing");
        }
    }

    private void closeEditor(){
        form.setStation(null);
        form.setVisible(false);
        removeClassName("editing");
    }

    void addStation(){
        editStation(new Station());
    }

    private void updateList(){
        gridTree.getDataProvider().refreshAll();
    }

    private HorizontalLayout createButtonsLayout(Station station){
        HorizontalLayout horizontalLayout = new HorizontalLayout();

        IronIcon iconDelete = IronIcons.DELETE.create();
        iconDelete.setColor("red");

        Button edit = new Button(EditorIcons.MODE_EDIT.create());
        Button copy = new Button(IronIcons.CONTENT_COPY.create());
        Button delete = new Button(iconDelete);
        edit.addClickListener( e -> editStation(station));

        delete.addClickListener(event -> deleteStation(new StationForm.DeleteEvent(form, station)));

        copy.addClickListener(clickEvent -> {
            Station stationClone = new Station(station.getName() + "-copy" +"(" + LocalTime.now() + ")",
                    station.getParent(), station.getAddress(), station.getStationSetting(), null, station.isActive(),
                    Station.generateKeyStation());

            try {
                stationService.save(stationClone);

                Notification.show("Сохранено!", 2000,
                        Notification.Position.MIDDLE);
                updateList();
            }  catch (DataIntegrityViolationException dataIntegrityViolationException){
                new Notification(
                        "Ошибка: Станция с именем '" + stationClone.getName() + "' существуе.\n" +
                                "Дубликат имен запрещен.", 3000).open();
            }


        });

        horizontalLayout.add(edit, copy, delete);

        return horizontalLayout;
    }


}
