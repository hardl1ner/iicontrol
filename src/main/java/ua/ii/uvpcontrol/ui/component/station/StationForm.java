package ua.ii.uvpcontrol.ui.component.station;

import com.vaadin.componentfactory.TreeComboBox;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.provider.hierarchy.TreeData;
import com.vaadin.flow.shared.Registration;
import ua.ii.uvpcontrol.backend.data.station.Station;
import ua.ii.uvpcontrol.backend.data.station.StationSetting;
import ua.ii.uvpcontrol.backend.service.station.StationService;
import ua.ii.uvpcontrol.backend.service.station.StationSettingService;

public class StationForm extends FormLayout {
    private TextField name = new TextField("Имя станции");
    private TextField address = new TextField("Адресс станции");
    private TreeComboBox<Station> parent = new TreeComboBox<>(Station::getName);
    private ComboBox<StationSetting> stationSetting = new ComboBox<>("Настройки");
    private TextField keyStation = new TextField("Ключ активации");
    private Checkbox active = new Checkbox("Заблокировать");

    Binder<Station> binder = new BeanValidationBinder<>(Station.class);

    private Station station;

    Button save = new Button("Сохранить");
    Button delete = new Button("Удалить");
    Button close = new Button("Отменить");

    private StationService stationService;

    public StationForm(StationSettingService stationSettingService, StationService stationService){
        this.stationService = stationService;
        setWidth("400px");
        name.setWidth("300px");
        address.setWidth("300px");
        stationSetting.setWidth("300px");
        keyStation.setWidth("300px");
        active.setWidth("300px");

        parent.setLabel("Группа");

        parent.setTreeData(new TreeData<Station>().addItems(stationService.parent(),
                stationService::fetchChildren));


        stationSetting.setItems(stationSettingService.findAll());
        stationSetting.setItemLabelGenerator(StationSetting::getName);

        binder.bindInstanceFields(this);

        Button generateKeys = new Button("Сгенерировать ключ");
        generateKeys.addClickListener(click -> keyStation.setValue(Station.generateKeyStation()));

        add(new VerticalLayout(name,
            address,
            parent,
            stationSetting,
            generateKeys,
            keyStation,
            active,
            createButtonsLayout()));

    }

    private HorizontalLayout createButtonsLayout(){
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        delete.addThemeVariants(ButtonVariant.LUMO_ERROR);
        close.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

        save.addClickShortcut(Key.ENTER);
        close.addClickShortcut(Key.ESCAPE);

        save.addClickListener(event -> validateAndSave());
        delete.addClickListener(event -> fireEvent(new DeleteEvent(this, station)));
        close.addClickListener(event -> fireEvent(new CloseEvent(this)));


        binder.addStatusChangeListener(e -> save.setEnabled(binder.isValid()));
        return new HorizontalLayout(save, delete, close);
    }

    private void validateAndSave(){
        try {
            binder.writeBean(station);
            fireEvent(new SaveEvent(this, station));
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }


    public void setStation(Station station) {
        this.station = station;
        parent.setTreeData(new TreeData<Station>().addItems(stationService.parent(),
                stationService::fetchChildren));
        binder.readBean(station);
    }

    // Events
    public static abstract class StationFormEvent extends ComponentEvent<StationForm> {
        private Station station;

        protected StationFormEvent(StationForm source, Station station) {
            super(source, false);
            this.station = station;
        }

        public Station getStation() {
            return station;
        }
    }

    public static class SaveEvent extends StationFormEvent {
        SaveEvent(StationForm source, Station station) {
            super(source, station);
        }
    }

    public static class DeleteEvent extends StationFormEvent {
        DeleteEvent(StationForm source, Station station) {
            super(source, station);
        }

    }

    public static class CloseEvent extends StationFormEvent {
        CloseEvent(StationForm source) {
            super(source, null);
        }
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType,
                                                                  ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }
}


