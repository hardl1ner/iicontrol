package ua.ii.uvpcontrol.ui.component.content;

import com.flowingcode.vaadin.addons.ironicons.AvIcons;
import com.flowingcode.vaadin.addons.ironicons.IronIcons;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.grid.GridSortOrderBuilder;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.provider.hierarchy.AbstractBackEndHierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalQuery;
import com.vaadin.flow.data.value.ValueChangeMode;
import org.claspina.confirmdialog.ButtonOption;
import org.claspina.confirmdialog.ConfirmDialog;
import org.springframework.dao.DataIntegrityViolationException;
import ua.ii.uvpcontrol.backend.data.content.Ether;
import ua.ii.uvpcontrol.backend.data.content.FileInfo;
import ua.ii.uvpcontrol.backend.data.station.Station;
import ua.ii.uvpcontrol.backend.service.content.EtherService;
import ua.ii.uvpcontrol.backend.service.layer.LayerVideoService;
import ua.ii.uvpcontrol.backend.service.station.StationService;
import ua.ii.uvpcontrol.ui.component.HasTooltip;
import ua.ii.uvpcontrol.ui.component.RecursiveSelectTreeGrid;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Stream;
@CssImport(value = "./styles/ether.css", themeFor = "vaadin-grid")
public class EtherGrid extends VerticalLayout implements HasTooltip {
    private final EtherService etherService;
    private final StationService stationService;
    private final LayerVideoService layerVideoService;
    private final TextField filterText = new TextField();

    private final Grid<Ether> gridEther = new Grid<>(Ether.class);
    private final TreeGrid<Station> gridTreeStation = new TreeGrid<>();

    public EtherGrid(EtherService etherService, StationService stationService, LayerVideoService layerVideoService){
        this.etherService = etherService;
        this.stationService = stationService;
        this.layerVideoService = layerVideoService;

        addClassName("list-view");
        setSizeFull();
        Div div = new Div(gridTreeStation);
        div.addClassName("content");
        div.setSizeFull();
        div.setWidth("600px");
        configureGrid();

        setPadding(true);
        setSpacing(false);

        Div content = new Div(div, gridEther);
        content.addClassName("content");
        content.setSizeFull();

        add(content);
    }

    private void multiAdd(){
        // STATION GRID
        RecursiveSelectTreeGrid<Station> stationTreeGrid = new RecursiveSelectTreeGrid<>();
        stationTreeGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        stationTreeGrid.setHeight("550px");
        stationTreeGrid.setWidth("400px");
        stationTreeGrid.addHierarchyColumn(Station::getName).setHeader("Станции");

        HierarchicalDataProvider<Station, Void> dataProvider =
                new AbstractBackEndHierarchicalDataProvider<>() {

                    @Override
                    public int getChildCount(HierarchicalQuery<Station, Void> query) {
                        return (int) stationService.getChildCount(query.getParent());
                    }

                    @Override
                    public boolean hasChildren(Station item) {
                        return stationService.hasChildren(item);
                    }

                    @Override
                    protected Stream<Station> fetchChildrenFromBackEnd(
                            HierarchicalQuery<Station, Void> query) {
                        return stationService.fetchChildren(query.getParent()).stream().
                                sorted(Comparator.comparing(Station::getName));
                    }
                };

        stationTreeGrid.setDataProvider(dataProvider);

        //FILEINFO GRID
        Grid<FileInfo> gridFileInfo = new Grid<>(FileInfo.class);
        gridFileInfo.setHeight("550px");
        gridFileInfo.setWidth("400px");
        gridFileInfo.setColumns("file_name");
        gridFileInfo.addColumn(playList -> "№").setKey("rowIndex").setFlexGrow(0).setWidth("75px");

        gridFileInfo.getColumnByKey("file_name").setHeader("Файлы").setWidth("300px").setResizable(true);
        gridFileInfo.addAttachListener(event -> gridFileInfo.getColumnByKey("rowIndex").getElement().executeJs(
                "this.renderer = function(root, column, rowData) {root.textContent = rowData.index + 1}"
        ));

        gridFileInfo.setColumnOrder(
                gridFileInfo.getColumnByKey("rowIndex"),
                gridFileInfo.getColumnByKey("file_name"));

        List<GridSortOrder<FileInfo>> sortByNameFile = new GridSortOrderBuilder<FileInfo>()
                .thenAsc(gridFileInfo.getColumnByKey("file_name")).build();
        gridFileInfo.sort(sortByNameFile);
        gridFileInfo.setSelectionMode(Grid.SelectionMode.MULTI);
        Set<FileInfo> fileInfos = new HashSet<>();
        stationTreeGrid.asMultiSelect().addSelectionListener(value -> {
            fileInfos.clear();
            if(value.getValue() != null) {
                List<Station> stationArrayList = new ArrayList<>();
                for(Station station : value.getValue()) {
                    stationArrayList.addAll(stationService.fetchChildren(station));
                }

                if (stationArrayList.size() > 0) {
                    stationArrayList.forEach(station -> {
                        if (station.getStationSetting() != null) {
                            fileInfos.addAll(layerVideoService.findById(stationService.findById(station.getId()).getStationSetting().getLayerVideo().getId()).getFolder().getFileInfos());
                        }
                    });
                } else {
                    value.getValue().forEach(station -> fileInfos.addAll(layerVideoService.findById(stationService.findById(station.getId()).getStationSetting().getLayerVideo().getId()).getFolder().getFileInfos()));

                }
                gridFileInfo.setItems(fileInfos);

            }
        });

        Dialog dialog = new Dialog();

        RadioButtonGroup<String> typeFileEtherRadioBtn = new RadioButtonGroup<>();
        typeFileEtherRadioBtn.setItems("Добавить файлы", "Добавить ссылку");
        typeFileEtherRadioBtn.setValue("Добавить файлы");

        Button save = new Button(IronIcons.SAVE.create());
        Button cancel = new Button(IronIcons.CLOSE.create());
        cancel.addThemeVariants(ButtonVariant.LUMO_CONTRAST);

        DateTimePicker start = new DateTimePicker("Начало трансляции");
        start.setValue(LocalDateTime.now().withHour(0).withMinute(0));
        DateTimePicker end = new DateTimePicker("Завершение трансляции");
        end.setValue(LocalDateTime.now().plusYears(10).withHour(0).withMinute(0));
        Locale rus = new Locale("ru", "RU");
        start.setLocale(rus);
        end.setLocale(rus);

        Checkbox monday = new Checkbox("ПН");
        Checkbox tuesday = new Checkbox("ВТ");
        Checkbox wednesday = new Checkbox("СР");
        Checkbox thursday = new Checkbox("ЧТ");
        Checkbox friday = new Checkbox("ПТ");
        Checkbox saturday = new Checkbox("СБ");
        Checkbox sunday = new Checkbox("ВС");
        monday.setWidth("50px");
        tuesday.setWidth("50px");
        wednesday.setWidth("50px");
        thursday.setWidth("50px");
        friday.setWidth("50px");
        saturday.setWidth("50px");
        sunday.setWidth("50px");

        TextField searchTextField = new TextField();
        searchTextField.setPlaceholder("Поиск...");
        searchTextField.setValueChangeMode(ValueChangeMode.LAZY);
        searchTextField.addValueChangeListener(e ->{
            gridFileInfo.setItems(fileInfos.stream()
                    .filter(fileInfo  -> fileInfo.getFile_name()
                            .toLowerCase(Locale.ROOT)
                            .contains(searchTextField.getValue())));
        });

        HeaderRow headerRow = gridFileInfo.prependHeaderRow();
        headerRow.getCell(gridFileInfo.getColumnByKey("file_name")).setComponent(searchTextField);

        FormLayout formLayout = new FormLayout();
        formLayout.setWidth("400px");
        formLayout.add(new VerticalLayout(start, end,
                new HorizontalLayout(monday, tuesday, wednesday, thursday, friday),
                new HorizontalLayout(saturday, sunday)));
        gridFileInfo.setVisible(typeFileEtherRadioBtn.getValue().equals("Добавить файлы"));

        ComboBox<Ether.Type> typeComboBox = new ComboBox<>("Тип ссылки");
        typeComboBox.setItems(Ether.Type.stream, Ether.Type.admixer);
        typeComboBox.setWidth("350px");

        TextField urlTextField = new TextField("Ссылка");
        urlTextField.setWidth("350px");

        VerticalLayout urlLayout = new VerticalLayout(urlTextField, typeComboBox);
        urlLayout.setDefaultHorizontalComponentAlignment(Alignment.CENTER);
        urlLayout.setWidth("400px");
        urlLayout.setVisible(false);

        typeFileEtherRadioBtn.addValueChangeListener(value -> {
           if (value.getValue().equals("Добавить файлы")){
               gridFileInfo.setVisible(true);
               urlLayout.setVisible(false);
           } else {
               gridFileInfo.setVisible(false);
               urlLayout.setVisible(true);
           }
        });

        cancel.addClickListener(eventClick -> dialog.close());

        save.addClickListener(eventClick -> {
            if (typeFileEtherRadioBtn.getValue().equals("Добавить файлы")) {
                if (stationTreeGrid.asMultiSelect().getValue().size() > 0) {
                    if (gridFileInfo.asMultiSelect().getValue().size() > 0) {
                        if (start.getValue() != null) {
                            if (end.getValue() != null) {
                                ConfirmDialog
                                        .createQuestion()
                                        .withCaption("Добавление")
                                        .withMessage("Вы уверенные что хотите добавить у выбранные станции?")
                                        .withOkButton(() -> {
                                            ArrayList<Ether> ethers = new ArrayList<>();
                                            gridFileInfo.asMultiSelect().getValue().forEach(fileInfo -> {
                                                Ether ether = new Ether("", fileInfo, Ether.Type.file, start.getValue(), end.getValue(),
                                                        monday.getValue(), tuesday.getValue(), wednesday.getValue(),
                                                        thursday.getValue(), friday.getValue(), saturday.getValue(),
                                                        sunday.getValue());
                                                etherService.save(ether);

                                                ethers.add(ether);
                                            });
                                            stationTreeGrid.asMultiSelect().getValue().forEach(station -> {
                                                if (station.getStationSetting() != null) {
                                                    station = stationService.findById(station.getId());
                                                    station.getEthers().addAll(ethers);
                                                    stationService.save(station);
                                                }
                                            });
                                            stationTreeGrid.deselectAll();
                                            stationTreeGrid.getDataProvider().refreshAll();
                                            fileInfos.clear();
                                            gridFileInfo.setItems(fileInfos);
                                            gridTreeStation.deselectAll();
                                            gridTreeStation.getDataProvider().refreshAll();
                                            gridEther.setItems(new ArrayList<>());
                                            Notification.show("Сохранено!", 2000,
                                                    Notification.Position.MIDDLE);

                                        }, ButtonOption.focus(), ButtonOption.caption("Да"))
                                        .withCancelButton(ButtonOption.caption("Нет"))
                                        .open();

                            } else {
                                Notification.show("Ошибка: Выберить дату и время завершения!", 2000,
                                        Notification.Position.MIDDLE);
                            }
                        } else {
                            Notification.show("Ошибка: Выберить дату и время начала!", 2000,
                                    Notification.Position.MIDDLE);
                        }
                    } else {
                        Notification.show("Ошибка: Выберить файл!", 2000,
                                Notification.Position.MIDDLE);
                    }
                } else {
                    Notification.show("Ошибка: Выберить станцию!", 2000,
                            Notification.Position.MIDDLE);
                }
            } else {
                    if (stationTreeGrid.asMultiSelect().getValue().size() > 0) {
                        if (!urlTextField.isEmpty()) {
                        if (typeComboBox.getValue() != null) {
                            if (start.getValue() != null) {
                                if (end.getValue() != null) {
                                    ConfirmDialog
                                            .createQuestion()
                                            .withCaption("Добавление")
                                            .withMessage("Вы уверенные что хотите добавить у выбранные станции?")
                                            .withOkButton(() -> {
                                                Ether ether = new Ether(urlTextField.getValue(), null, typeComboBox.getValue(),
                                                        start.getValue(), end.getValue(),
                                                        monday.getValue(), tuesday.getValue(), wednesday.getValue(),
                                                        thursday.getValue(), friday.getValue(), saturday.getValue(),
                                                        sunday.getValue());
                                                etherService.save(ether);
                                                stationTreeGrid.asMultiSelect().getValue().forEach(station -> {
                                                    if (station.getStationSetting() != null) {
                                                        station = stationService.findById(station.getId());
                                                        station.getEthers().add(ether);
                                                        stationService.save(station);
                                                    }
                                                });
                                                stationTreeGrid.deselectAll();
                                                stationTreeGrid.getDataProvider().refreshAll();
                                                fileInfos.clear();
                                                gridFileInfo.setItems(fileInfos);
                                                gridTreeStation.deselectAll();
                                                gridTreeStation.getDataProvider().refreshAll();
                                                gridEther.setItems(new ArrayList<>());
                                                Notification.show("Сохранено!", 2000,
                                                        Notification.Position.MIDDLE);

                                            }, ButtonOption.focus(), ButtonOption.caption("Да"))
                                            .withCancelButton(ButtonOption.caption("Нет"))
                                            .open();
                                } else {
                                    Notification.show("Ошибка: Выберить дату и время завершения!", 2000,
                                            Notification.Position.MIDDLE);
                                }
                            } else {
                                Notification.show("Ошибка: Выберить дату и время начала!", 2000,
                                        Notification.Position.MIDDLE);
                            }

                        } else {
                            Notification.show("Ошибка: Выберите тип ссылки!", 2000,
                                    Notification.Position.MIDDLE);
                        }

                    } else {
                        Notification.show("Ошибка: Введите ссылку!", 2000,
                                Notification.Position.MIDDLE);
                    }
                } else {
                    Notification.show("Ошибка: Выберить станцию!", 2000,
                            Notification.Position.MIDDLE);
                }
            }

        });

        dialog.setWidth("1300px");
        dialog.setHeight("800px");
        H3 h3 = new H3("Добавление в эфир");
        h3.setWidth("300px");
        VerticalLayout verticalLayout = new VerticalLayout(cancel);
        verticalLayout.setDefaultHorizontalComponentAlignment(Alignment.END);
        HorizontalLayout horizontalLayout = new HorizontalLayout(h3, verticalLayout);
        horizontalLayout.setDefaultVerticalComponentAlignment(Alignment.CENTER);
        Span span = new Span();
        span.setWidth("150px");
        HorizontalLayout horizontalLayout1 = new HorizontalLayout(save, span, typeFileEtherRadioBtn);
        horizontalLayout1.setDefaultVerticalComponentAlignment(Alignment.CENTER);

        dialog.add(horizontalLayout, new VerticalLayout(new HorizontalLayout(stationTreeGrid, gridFileInfo, urlLayout,
                formLayout), horizontalLayout1));
        dialog.open();
    }

    private void multiDel(){
        // STATION GRID
        Set<Ether> ethers = new HashSet<>();
        RecursiveSelectTreeGrid<Station> stationTreeGrid = new RecursiveSelectTreeGrid<>();
        stationTreeGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        stationTreeGrid.setHeight("550px");
        stationTreeGrid.setWidth("400px");
        stationTreeGrid.addHierarchyColumn(Station::getName).setHeader("Станции");

        HierarchicalDataProvider<Station, Void> dataProvider =
                new AbstractBackEndHierarchicalDataProvider<>() {

                    @Override
                    public int getChildCount(HierarchicalQuery<Station, Void> query) {
                        return (int) stationService.getChildCount(query.getParent());
                    }

                    @Override
                    public boolean hasChildren(Station item) {
                        return stationService.hasChildren(item);
                    }

                    @Override
                    protected Stream<Station> fetchChildrenFromBackEnd(
                            HierarchicalQuery<Station, Void> query) {
                        return stationService.fetchChildren(query.getParent()).stream().
                                sorted(Comparator.comparing(Station::getName));
                    }
                };

        stationTreeGrid.setDataProvider(dataProvider);

        //FILEINFO GRID
        Grid<Ether> etherGrid = new Grid<>();
        etherGrid.setHeight("550px");
        etherGrid.setWidth("400px");
        etherGrid.addColumn(playList -> "№").setKey("rowIndex").setFlexGrow(0).setWidth("75px");
        etherGrid.addColumn(ether -> {
            String name = "";
            if (ether.getFile() != null){
                name = ether.getFile().getFile_name();
            } else {
                name = ether.getUrl();
            }

            return name;
        }).setKey("file_name").setHeader("Файлы").setWidth("300px").setResizable(true);

        etherGrid.addAttachListener(event -> etherGrid.getColumnByKey("rowIndex").getElement().executeJs(
                "this.renderer = function(root, column, rowData) {root.textContent = rowData.index + 1}"
        ));

        etherGrid.setColumnOrder(
                etherGrid.getColumnByKey("rowIndex"),
                etherGrid.getColumnByKey("file_name"));

        List<GridSortOrder<Ether>> sortByNameFile = new GridSortOrderBuilder<Ether>()
                .thenAsc(etherGrid.getColumnByKey("file_name")).build();
        etherGrid.sort(sortByNameFile);
        etherGrid.setSelectionMode(Grid.SelectionMode.MULTI);

        stationTreeGrid.asMultiSelect().addSelectionListener(value -> {
            ethers.clear();
            if(value.getValue() != null) {
                List<Station> stationArrayList = new ArrayList<>();
                for(Station station : value.getValue()) {
                    stationArrayList.addAll(stationService.fetchChildren(station));
                }

                if (stationArrayList.size() > 0) {
                    stationArrayList.forEach(station -> ethers.addAll(stationService.findById(station.getId()).getEthers()));
                } else {
                    value.getValue().forEach(station -> ethers.addAll(stationService.findById(station.getId()).getEthers()));

                }
                etherGrid.setItems(ethers);

            }
        });
        Dialog dialog = new Dialog();

        TextField searchTextField = new TextField();
        searchTextField.setPlaceholder("Поиск...");
        searchTextField.setValueChangeMode(ValueChangeMode.LAZY);
        searchTextField.addValueChangeListener(e ->{
            etherGrid.setItems(ethers.stream()
                    .filter(ether -> ether.getFile().getFile_name()
                            .toLowerCase(Locale.ROOT)
                            .contains(searchTextField.getValue())));
        });

        HeaderRow headerRow = etherGrid.prependHeaderRow();
        headerRow.getCell(etherGrid.getColumnByKey("file_name")).setComponent(searchTextField);

        Button delete = new Button(IronIcons.DELETE.create());
        delete.addThemeVariants(ButtonVariant.LUMO_ERROR);
        Button cancel = new Button(IronIcons.CLOSE.create());
        cancel.addThemeVariants(ButtonVariant.LUMO_CONTRAST);

        cancel.addClickListener(eventClick -> dialog.close());
        delete.addClickListener(eventClick -> {
            if (stationTreeGrid.asMultiSelect().getValue().size() > 0) {
                if (etherGrid.asMultiSelect().getValue().size() > 0) {
                            ConfirmDialog
                                    .createQuestion()
                                    .withCaption("Удаление")
                                    .withMessage("Вы уверенные что хотите удалить?")
                                    .withOkButton(() -> {
                                        stationTreeGrid.asMultiSelect().getValue().forEach(station -> {
                                            station = stationService.findById(station.getId());
                                            station.getEthers().removeAll(etherGrid.asMultiSelect().getValue());
                                            stationService.save(station);
                                        });
                                        try {
                                            etherGrid.asMultiSelect().getValue().forEach(etherService::delete);
                                        } catch (DataIntegrityViolationException ignored){

                                        }
                                        Notification.show("Удалено!", 2000,
                                                Notification.Position.MIDDLE);
                                        etherGrid.setItems(new ArrayList<>());
                                        stationTreeGrid.deselectAll();
                                        stationTreeGrid.getDataProvider().refreshAll();
                                        ethers.clear();
                                        gridTreeStation.deselectAll();
                                        gridTreeStation.getDataProvider().refreshAll();
                                        gridEther.setItems(new ArrayList<>());

                                    }, ButtonOption.focus(), ButtonOption.caption("Да"))
                                    .withCancelButton(ButtonOption.caption("Нет"))
                                    .open();


                } else {
                    Notification.show("Ошибка: Выберить файл!", 2000,
                            Notification.Position.MIDDLE);
                }
            } else {
                Notification.show("Ошибка: Выберить станцию!", 2000,
                        Notification.Position.MIDDLE);
            }

        });

        dialog.setWidth("900px");
        dialog.setHeight("800px");
        H3 h3 = new H3("Удаление с эфира");
        h3.setWidth("300px");
        VerticalLayout verticalLayout = new VerticalLayout(cancel);
        verticalLayout.setDefaultHorizontalComponentAlignment(Alignment.END);
        HorizontalLayout horizontalLayout = new HorizontalLayout(h3, verticalLayout);
        horizontalLayout.setDefaultVerticalComponentAlignment(Alignment.CENTER);

        dialog.add(horizontalLayout, new VerticalLayout(new HorizontalLayout(stationTreeGrid, etherGrid), delete));
        dialog.open();
    }

    private void configureGrid(){
        gridTreeStation.setSizeFull();
        gridTreeStation.setWidth("600px");
        gridTreeStation.addHierarchyColumn(Station::getName).setWidth("300px").setHeader("Станции").setResizable(true);
        gridTreeStation.addColumn(playList -> "").setKey("action");

        Button multiAddButton = new Button(AvIcons.LIBRARY_ADD.create());
        Button multiDelButton = new Button(IronIcons.DELETE.create());
        multiDelButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
        multiAddButton.addClickListener(buttonClickEvent -> multiAdd());
        multiDelButton.addClickListener(buttonClickEvent -> multiDel());
        HeaderRow headerRow = gridTreeStation.prependHeaderRow();
        headerRow.getCell(gridTreeStation.getColumnByKey("action")).setComponent(new HorizontalLayout(
                multiAddButton, multiDelButton));

        gridEther.addClassName("contact-grid");
        gridEther.setSizeFull();
        gridEther.setSelectionMode(Grid.SelectionMode.NONE);
        gridEther.setColumns("start", "end");
        gridEther.addColumn(ether -> {
            String name = "";
            if (ether.getFile() != null){
                name = ether.getFile().getFile_name();
            } else {
                name = ether.getUrl();
            }

            return name;
        }).setKey("file_name");
        gridEther.addColumn(ether -> {
            String s = "";
            if (ether.isMonday()){
                s = s + "ПН/";
            }
            if (ether.isTuesday()){
                s = s + "ВТ/";
            }
            if (ether.isWednesday()){
                s = s + "СР/";
            }
            if (ether.isThursday()){
                s = s + "ЧТ/";
            }
            if (ether.isFriday()){
                s = s + "ПТ/";
            }
            if (ether.isSaturday()){
                s = s + "СБ/";
            }
            if (ether.isSunday()){
                s = s + "ВС/";
            }
            if (s.equals("ПН/ВТ/СР/ЧТ/ПТ/СБ/ВС/")){
                s = "Все дни";
            } else {
                if (s.isEmpty()){
                    s = "Все дни";
                }
            }
            return s;

        }).setKey("days").setHeader("Дни недели").setWidth("140px");
        gridEther.getColumnByKey("file_name").setHeader("Имя файла").setWidth("350px").setResizable(true);
        gridEther.getColumnByKey("start").setHeader("Начало трансляции").setWidth("150px");
        gridEther.getColumnByKey("end").setHeader("Завершение трансляции").setWidth("150px");

        gridEther.addColumn(playList -> "№").setKey("rowIndex").setFlexGrow(0).setWidth("75px");

        gridEther.addAttachListener(event -> gridEther.getColumnByKey("rowIndex").getElement().executeJs(
                "this.renderer = function(root, column, rowData) {root.textContent = rowData.index + 1}"
        ));

        gridEther.setColumnOrder(
                gridEther.getColumnByKey("rowIndex"),
                gridEther.getColumnByKey("file_name"),
                gridEther.getColumnByKey("start"),
                gridEther.getColumnByKey("end"),
                gridEther.getColumnByKey("days"));

        List<GridSortOrder<Ether>> sortByName = new GridSortOrderBuilder<Ether>()
                .thenAsc(gridEther.getColumnByKey("file_name")).build();
        gridEther.sort(sortByName);
        gridEther.getElement().getThemeList().add("grid-no-padding");

        HeaderRow filterRow = gridEther.prependHeaderRow();

        // First filter
        filterText.setPlaceholder("Поиск...");
        filterText.setClearButtonVisible(true);
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.addValueChangeListener(e -> search());

        filterRow.getCell(gridEther.getColumnByKey("file_name")).setComponent(filterText);
        filterText.setWidth("300px");
        filterText.getElement().setAttribute("focus-target", "");



        HierarchicalDataProvider<Station, Void> dataProvider =
                new AbstractBackEndHierarchicalDataProvider<>() {

                    @Override
                    public int getChildCount(HierarchicalQuery<Station, Void> query) {
                        return (int) stationService.getChildCount(query.getParent());
                    }

                    @Override
                    public boolean hasChildren(Station item) {
                        return stationService.hasChildren(item);
                    }

                    @Override
                    protected Stream<Station> fetchChildrenFromBackEnd(
                            HierarchicalQuery<Station, Void> query) {
                        return stationService.fetchChildren(query.getParent()).stream().
                                sorted(Comparator.comparing(Station::getName));
                    }
                };

        gridTreeStation.setDataProvider(dataProvider);

        gridTreeStation.asSingleSelect().addValueChangeListener(valueChangeEvent -> {
            if(valueChangeEvent.getValue() != null) {
                List<Ether> ethers = new ArrayList<>();
                Station station = stationService.findById(valueChangeEvent.getValue().getId());
                station.getEthers().forEach(ether -> ethers.add(etherService.findByid(ether.getId())));
                gridEther.setItems(ethers);
            }
        });
    }

    private void search(){
        List<Ether> ethers = new ArrayList();
        gridTreeStation.asSingleSelect()
                .getValue()
                .getEthers().forEach(ether -> ethers.add(etherService.findByid(ether.getId())));

        gridEther.setItems(ethers.stream()
                .filter(ether  -> {
                    if (ether.getFile() != null) {
                        return ether.getFile().getFile_name()
                                .toLowerCase(Locale.ROOT)
                                .contains(filterText.getValue());
                    } else {
                        return ether.getUrl()
                                .toLowerCase(Locale.ROOT)
                                .contains(filterText.getValue());
                    }
                }));
    }

}
