package ua.ii.uvpcontrol.ui.component.station;

import com.flowingcode.vaadin.addons.ironicons.IronIcons;
import com.vaadin.componentfactory.Tooltip;
import com.vaadin.componentfactory.TreeComboBox;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.IronIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.timepicker.TimePicker;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.provider.hierarchy.*;
import com.vaadin.flow.shared.Registration;
import ua.ii.uvpcontrol.backend.data.content.Folder;
import ua.ii.uvpcontrol.backend.data.layer.*;
import ua.ii.uvpcontrol.backend.data.station.StationSetting;
import ua.ii.uvpcontrol.backend.service.content.FolderService;
import ua.ii.uvpcontrol.backend.service.layer.LayerGroupImageService;
import ua.ii.uvpcontrol.backend.service.layer.LayerGroupPanelService;
import ua.ii.uvpcontrol.backend.service.layer.LayerGroupTextService;
import ua.ii.uvpcontrol.backend.service.layer.LayerGroupVideoService;
import ua.ii.uvpcontrol.ui.component.HasTooltip;

import java.util.Locale;

public class StationSettingForm extends VerticalLayout implements HasTooltip {
    private IntegerField widthScreen = new IntegerField("Ширина");
    private IntegerField heightScreen = new IntegerField("Высота");
    private IntegerField x = new IntegerField("X");
    private IntegerField y = new IntegerField("Y");
    private IntegerField downloadInterval = new IntegerField("Интервал загрузки");
    private IntegerField uploadInterval = new IntegerField("Интервал выгрузки");

    private TreeComboBox<LayerGroupImage> layerGroupImage = new TreeComboBox<>(LayerGroupImage::getName);
    private TreeComboBox<LayerGroupPanel> layerGroupPanel = new TreeComboBox<>(LayerGroupPanel::getName);
    private TreeComboBox<LayerGroupText> layerGroupText = new TreeComboBox<>(LayerGroupText::getName);
    private ComboBox<LayerVideo> layerVideo = new ComboBox<>("Слой видео");
    private ComboBox<StationSetting.TypePlayList> typePlayList = new ComboBox<>("Тип плейлиста");
    private TreeComboBox<Folder> folderPlayList = new TreeComboBox<>(Folder::getName);

    private TextField name = new TextField();

    private TimePicker monday = new TimePicker("Понедельник(ЧЧ:ММ)");
    private TimePicker tuesday = new TimePicker("Вторник(ЧЧ:ММ)");
    private TimePicker wednesday = new TimePicker("Среда(ЧЧ:ММ)");
    private TimePicker thursday = new TimePicker("Четверг(ЧЧ:ММ)");
    private TimePicker friday = new TimePicker("Пятница(ЧЧ:ММ)");
    private TimePicker saturday = new TimePicker("Суббота(ЧЧ:ММ)");
    private TimePicker sunday = new TimePicker("Воскресенье(ЧЧ:ММ)");
    private Checkbox shutdownOn = new Checkbox("Выключать/Не выключать");

    private Accordion accordion = new Accordion();

    Binder<StationSetting> binder = new BeanValidationBinder<>(StationSetting.class);

    private StationSetting stationSetting;

    private LayerGroupVideoService layerGroupVideoService;

    Button save = new Button();
    Button delete = new Button();
    Button close = new Button();
    Button copy = new Button();

    public StationSettingForm(LayerGroupImageService layerGroupImageService, LayerGroupPanelService layerGroupPanelService,
                              LayerGroupVideoService layerGroupVideoService, LayerGroupTextService layerGroupTextService,
                              FolderService folderService){

        monday.setLocale(new Locale("ru"));
        tuesday.setLocale(new Locale("ru"));
        wednesday.setLocale(new Locale("ru"));
        thursday.setLocale(new Locale("ru"));
        friday.setLocale(new Locale("ru"));
        saturday.setLocale(new Locale("ru"));
        sunday.setLocale(new Locale("ru"));

        setWidth("500px");
        binder.bindInstanceFields(this);
        typePlayList.setItems(StationSetting.TypePlayList.values());

        this.layerGroupVideoService = layerGroupVideoService;

        layerGroupImage.setLabel("Группа слоя изображения");
        layerGroupPanel.setLabel("Группа слоя панели");
        layerGroupText.setLabel("Группа слоя текс");
        folderPlayList.setLabel("Папка с плейлистами");

        layerGroupImage.setTreeData(new TreeData<LayerGroupImage>().addItems(layerGroupImageService.parent(),
                layerGroupImageService::fetchChildren));
        layerGroupText.setTreeData(new TreeData<LayerGroupText>().addItems(layerGroupTextService.parent(),
                layerGroupTextService::fetchChildren));
        layerGroupPanel.setTreeData(new TreeData<LayerGroupPanel>().addItems(layerGroupPanelService.parent(),
                layerGroupPanelService::fetchChildren));
        folderPlayList.setTreeData(new TreeData<Folder>().addItems(folderService.parent(),
                folderService::fetchChildren));
        folderPlayList.setVisible(false);

        createAccordion();

        add(accordion, createButtonsLayout());

    }

    private HorizontalLayout createButtonsLayout(){
        IronIcon iconDelete = IronIcons.DELETE.create();
        iconDelete.setColor("red");
        delete.setIcon(iconDelete);
        save.setIcon(IronIcons.SAVE.create());
        close.setIcon(IronIcons.CLOSE.create());
        copy.setIcon(IronIcons.CONTENT_COPY.create());

        save.addClickShortcut(Key.ENTER);
        close.addClickShortcut(Key.ESCAPE);

        save.addClickListener(event -> validateAndSave());
        delete.addClickListener(event -> fireEvent(new DeleteEvent(this, stationSetting)));
        close.addClickListener(event -> fireEvent(new CloseEvent(this)));
        copy.addClickListener(event -> fireEvent(new CopyEvent(this, stationSetting)));


        binder.addStatusChangeListener(e -> save.setEnabled(binder.isValid()));
        return new HorizontalLayout(save, copy, delete, close);
    }

    private void validateAndSave(){
        try {
            binder.writeBean(stationSetting);
            fireEvent(new SaveEvent(this, stationSetting));
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    private void createAccordion(){
        layerVideo.setEnabled(false);
        TreeComboBox<LayerGroupVideo> layerGroupVideo = new TreeComboBox<>(LayerGroupVideo::getName);
        layerGroupVideo.setLabel("Группа слоя видео");

        layerGroupVideo.setTreeData(new TreeData<LayerGroupVideo>().addItems(layerGroupVideoService.parent(),
                layerGroupVideoService::fetchChildren));

        layerGroupVideo.addValueChangeListener(event -> {
           layerVideo.setEnabled(true);
           layerVideo.setItems(event.getValue().getLayerVideos());
        });
        accordion.add("Имя настройки", name);

        // Настройка экрана
        FormLayout screenForm = new FormLayout();
        screenForm.add(widthScreen);
        screenForm.add(heightScreen);
        screenForm.add(x);
        screenForm.add(y);

        accordion.add("Разрешение экрана", screenForm);

// Время обновления контента (В минутах)
        FormLayout timeIntervalForm = new FormLayout();
        timeIntervalForm.add(downloadInterval);
        timeIntervalForm.add(uploadInterval);

        accordion.add("Интервалы (В минутах)", timeIntervalForm);

// Время выключения станции (По дням недели)
        FormLayout timeShutdownForm = new FormLayout();
        timeShutdownForm.add(monday);
        timeShutdownForm.add(tuesday);
        timeShutdownForm.add(wednesday);
        timeShutdownForm.add(thursday);
        timeShutdownForm.add(friday);
        timeShutdownForm.add(saturday);
        timeShutdownForm.add(sunday);
        timeShutdownForm.add(shutdownOn);

        accordion.add("Время выключения станции (По дням недели)", timeShutdownForm);

// layer
        FormLayout layerForm = new FormLayout();
        layerForm.add(layerGroupVideo);
        layerForm.add(layerVideo);
        layerForm.add(layerGroupPanel);
        layerForm.add(layerGroupImage);
        layerForm.add(layerGroupText);

        accordion.add("Слои", layerForm);

        // playlist
        FormLayout playListForm = new FormLayout();
        playListForm.add(typePlayList);
        playListForm.add(folderPlayList);

        accordion.add("Тип плейлиста", playListForm);

        typePlayList.addValueChangeListener(comboBoxTypePlayListComponentValueChangeEvent -> {
            if (comboBoxTypePlayListComponentValueChangeEvent.getValue() != null) {
                if (comboBoxTypePlayListComponentValueChangeEvent.getValue().equals(StationSetting.TypePlayList.indortv)) {
                    folderPlayList.setVisible(true);
                } else {
                    folderPlayList.setVisible(false);
                    folderPlayList.setValue(null);
                }
            }
        });

    }

    public void setSettingStation(StationSetting stationSetting) {
        this.stationSetting = stationSetting;
        binder.readBean(stationSetting);
    }

    // Events
    public static abstract class StationSettingFormEvent extends ComponentEvent<StationSettingForm> {
        private StationSetting stationSetting;

        protected StationSettingFormEvent(StationSettingForm source, StationSetting stationSetting) {
            super(source, false);
            this.stationSetting = stationSetting;
        }

        public StationSetting getSettingStation() {
            return stationSetting;
        }
    }

    public static class SaveEvent extends StationSettingFormEvent {
        SaveEvent(StationSettingForm source, StationSetting stationSetting) {
            super(source, stationSetting);
        }
    }

    public static class CopyEvent extends StationSettingFormEvent {
        CopyEvent(StationSettingForm source, StationSetting stationSetting) {
            super(source, stationSetting);
        }
    }

    public static class DeleteEvent extends StationSettingFormEvent {
        DeleteEvent(StationSettingForm source, StationSetting stationSetting) {
            super(source, stationSetting);
        }

    }

    public static class CloseEvent extends StationSettingFormEvent {
        CloseEvent(StationSettingForm source) {
            super(source, null);
        }
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType,
                                                                  ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }
}


