package ua.ii.uvpcontrol.ui.component.layer;

import com.flowingcode.vaadin.addons.ironicons.IronIcons;
import com.github.juchar.colorpicker.ColorPickerFieldRaw;
import com.vaadin.componentfactory.Tooltip;
import com.vaadin.componentfactory.TooltipAlignment;
import com.vaadin.componentfactory.TooltipPosition;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.icon.IronIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.shared.Registration;
import ua.ii.uvpcontrol.backend.data.layer.LayerPanel;
import ua.ii.uvpcontrol.backend.data.layer.LayerText;
import ua.ii.uvpcontrol.ui.component.HasTooltip;


public class LayerTextForm extends FormLayout implements HasTooltip {
    private TextField name = new TextField("Имя Слоя");
    private IntegerField width = new IntegerField("Ширина");
    private IntegerField height= new IntegerField("Высота");
    private IntegerField x = new IntegerField("X");
    private IntegerField y = new IntegerField("Y");
    private IntegerField sizeFont = new IntegerField("Размер шрифта");
    private TextField text = new TextField("Текст");
    private TextField nameFont = new TextField("Имя шрифта");
    private ComboBox<LayerText.StyleFont> styleFont = new ComboBox<>("Стиль шрифта");
    private ComboBox<LayerText.Orientation> orientation = new ComboBox<>("Ориентация");
    private final ColorPickerFieldRaw color = new ColorPickerFieldRaw("Цвет");
    private ComboBox<LayerText.TextAlignment> textAlignment = new ComboBox<>("Положение");
    private ComboBox<LayerText.LocaleLanguage> localeLanguage = new ComboBox<>("Язык");
    private ComboBox<LayerText.Type> type = new ComboBox<>("Тип");
    private Checkbox active = new Checkbox("Активирован");

    Binder<LayerText> binder = new BeanValidationBinder<>(LayerText.class);

    private LayerText layerText;

    Button save = new Button();
    Button delete = new Button();
    Button close = new Button();
    Button copy = new Button();

    public LayerTextForm(){
        setWidth("400px");
        name.setWidth("150px");
        width.setWidth("70px");
        height.setWidth("70px");
        x.setWidth("70px");
        y.setWidth("70px");
        sizeFont.setWidth("150px");
        text.setWidth("150px");
        nameFont.setWidth("150px");
        styleFont.setWidth("150px");
        orientation.setWidth("150px");
        color.setWidth("200px");
        textAlignment.setWidth("150px");
        localeLanguage.setWidth("150px");
        type.setWidth("150px");
        active.setWidth("300px");

        binder.bindInstanceFields(this);

        styleFont.setItems(LayerText.StyleFont.values());
        orientation.setItems(LayerText.Orientation.values());
        textAlignment.setItems(LayerText.TextAlignment.values());
        localeLanguage.setItems(LayerText.LocaleLanguage.values());
        type.setItems(LayerText.Type.values());

        add(new VerticalLayout(new HorizontalLayout(name, text),
                new HorizontalLayout(width, height, x, y),
                new HorizontalLayout(sizeFont, nameFont),
                new HorizontalLayout(styleFont, textAlignment),
                new HorizontalLayout(orientation, localeLanguage),
                new HorizontalLayout(color, type),
                active, createButtonsLayout()));

    }

    private HorizontalLayout createButtonsLayout(){
        IronIcon iconDelete = IronIcons.DELETE.create();
        iconDelete.setColor("red");
        delete.setIcon(iconDelete);
        save.setIcon(IronIcons.SAVE.create());
        close.setIcon(IronIcons.CANCEL.create());
        copy.setIcon(IronIcons.CONTENT_COPY.create());

        Tooltip saveToolTip = getTooltip(save, "Сохранить");
        Tooltip deleteToolTip = getTooltip(delete, "Удалить");
        Tooltip closeToolTip = getTooltip(close, "Отменить");
        Tooltip copyToolTip = getTooltip(copy, "Копировать");

        save.addClickShortcut(Key.ENTER);
        close.addClickShortcut(Key.ESCAPE);

        save.addClickListener(event -> validateAndSave());
        delete.addClickListener(event -> fireEvent(new DeleteEvent(this, layerText)));
        close.addClickListener(event -> fireEvent(new CloseEvent(this)));
        copy.addClickListener(event -> fireEvent(new CopyEvent(this, layerText)));


        binder.addStatusChangeListener(e -> save.setEnabled(binder.isValid()));
        return new HorizontalLayout(saveToolTip, deleteToolTip, closeToolTip, copyToolTip, save, copy, close, delete);
    }

    private void validateAndSave(){
        try {
            binder.writeBean(layerText);
            fireEvent(new SaveEvent(this, layerText));
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }


    public void setLayerText(LayerText layerText) {
        this.layerText = layerText;
        binder.readBean(layerText);
    }

    // Events
    public static abstract class LayerTextFormEvent extends ComponentEvent<LayerTextForm> {
        private LayerText layerText;

        protected LayerTextFormEvent(LayerTextForm source, LayerText layerText) {
            super(source, false);
            this.layerText = layerText;
        }

        public LayerText getLayerText() {
            return layerText;
        }
    }

    public static class SaveEvent extends LayerTextFormEvent {
        SaveEvent(LayerTextForm source, LayerText layerText) {
            super(source, layerText);
        }
    }

    public static class DeleteEvent extends LayerTextFormEvent {
        DeleteEvent(LayerTextForm source, LayerText layerText) {
            super(source, layerText);
        }

    }

    public static class CloseEvent extends LayerTextFormEvent {
        CloseEvent(LayerTextForm source) {
            super(source, null);
        }
    }

    public static class CopyEvent extends LayerTextFormEvent {
        CopyEvent(LayerTextForm source, LayerText layerText) {
            super(source, layerText);
        }
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType,
                                                                  ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }
}


