package ua.ii.uvpcontrol.ui.component.user;

import com.flowingcode.vaadin.addons.ironicons.IronIcons;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import ua.ii.uvpcontrol.app.security.CurrentUser;
import ua.ii.uvpcontrol.backend.data.users.User;
import ua.ii.uvpcontrol.backend.service.UserService;
import ua.ii.uvpcontrol.backend.service.exception.UniqueDataException;

public class UserGrid extends VerticalLayout {
    private final UserService userService;
    private final Grid<User> grid = new Grid<>(User.class);

    private final UserForm form;
    private final CurrentUser currentUser;
    private PasswordEncoder passwordEncoder;
    private boolean isEdit;

    public UserGrid(UserService userService, CurrentUser currentUser, PasswordEncoder passwordEncoder){
        this.currentUser = currentUser;
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;

        addClassName("list-view");
        setSizeFull();
        configureGrid();

        setPadding(true);
        setSpacing(false);

        form = new UserForm();
        form.addListener(UserForm.SaveEvent.class, this::saveUser);
        form.addListener(UserForm.DeleteEvent.class, this::deleteUser);
        form.addListener(UserForm.CloseEvent.class, e -> closeEditor());

        Div content = new Div(grid, form);
        content.addClassName("content");
        content.setSizeFull();

        add(getToolbar(), content);

        closeEditor();
    }

    private void saveUser(UserForm.SaveEvent event){
        try{
            if (isEdit){
                userService.edit(event.getUser());
                closeEditor();
                updateList();
            } else {
                userService.add(event.getUser());
            }
        Notification.show("Сохранено!", 2000,
                        Notification.Position.MIDDLE);

        }  catch (DataIntegrityViolationException dataIntegrityViolationException){
            new Notification(
                    "Ошибка: Пользователь с именем '" + event.getUser().getName() + "' существуе.\n" +
                            "Дубликат имен запрещен.", 3000).open();
        }
    }

    private void deleteUser(UserForm.DeleteEvent event) {
        try {
            userService.delete(currentUser.getUser(), event.getUser());


            Notification.show("Удалено!", 2000,
                    Notification.Position.MIDDLE);
            closeEditor();
            updateList();
        } catch (UniqueDataException uniqueDataException){
            new Notification(
                    uniqueDataException.getMessage(), 3000).open();
            closeEditor();
        }
    }

    private void configureGrid(){
        grid.setSizeFull();
        grid.setColumns("name", "email", "role");
        grid.getColumnByKey("name").setHeader("Имя");
        grid.getColumnByKey("email").setHeader("email");
        grid.getColumnByKey("role").setHeader("Роль");
        grid.addColumn(new ComponentRenderer<>(this::createButtonsLayout))
                .setHeader("Действия").setKey("action").setWidth("100px");
        grid.setItems(userService.findAll());
    }

    public void editUser(User user){
        if (user == null) {
            closeEditor();
        } else {
            form.setUser(user);
            form.setVisible(true);
            addClassName("editing");
        }
    }

    private void closeEditor(){
        form.setUser(null);
        form.setVisible(false);
        removeClassName("editing");
    }

    private HorizontalLayout getToolbar(){
        Button addStationButton = new Button(IronIcons.ADD.create());
        addStationButton.addClickListener(click -> {
            isEdit = false;
            addUser();
        });

        HorizontalLayout toolbar = new HorizontalLayout(addStationButton);
        toolbar.addClassName("toolbar");

        return toolbar;
    }

    void addUser(){
       grid.asSingleSelect().clear();
        editUser(new User());
    }

    private void updateList(){
        grid.setItems(userService.findAll());
    }

    private HorizontalLayout createButtonsLayout(User user){
        HorizontalLayout horizontalLayout = new HorizontalLayout();

        Button edit = new Button(new Icon(VaadinIcon.EDIT));
        Icon icon = (new Icon(VaadinIcon.TRASH));
        icon.setColor("red");
        Button delete = new Button(icon);
        edit.addClickListener( e -> {
            isEdit = true;
            editUser(user);
        });

        delete.addClickListener(event -> deleteUser(new UserForm.DeleteEvent(form, user)));

        horizontalLayout.add(edit, delete);

        return horizontalLayout;
    }


}
