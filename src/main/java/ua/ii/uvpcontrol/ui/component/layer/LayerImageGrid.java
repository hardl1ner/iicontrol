package ua.ii.uvpcontrol.ui.component.layer;

import com.flowingcode.vaadin.addons.ironicons.IronIcons;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.grid.GridSortOrderBuilder;
import com.vaadin.flow.component.grid.contextmenu.GridContextMenu;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.hierarchy.AbstractBackEndHierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalQuery;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import org.claspina.confirmdialog.ButtonOption;
import org.claspina.confirmdialog.ConfirmDialog;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionSystemException;
import ua.ii.uvpcontrol.backend.data.layer.LayerGroupImage;
import ua.ii.uvpcontrol.backend.data.layer.LayerImage;
import ua.ii.uvpcontrol.backend.data.station.Station;
import ua.ii.uvpcontrol.backend.service.content.FolderService;
import ua.ii.uvpcontrol.backend.service.exception.UniqueDataException;
import ua.ii.uvpcontrol.backend.service.layer.LayerGroupImageService;
import ua.ii.uvpcontrol.backend.service.layer.LayerImageService;

import java.time.LocalTime;
import java.util.*;
import java.util.stream.Stream;

public class LayerImageGrid extends VerticalLayout {
    private final LayerImageService layerImageService;
    private final LayerGroupImageService layerGroupImageService;

    private final Grid<LayerImage> grid = new Grid<>(LayerImage.class);
    private final TreeGrid<LayerGroupImage> gridTree = new TreeGrid<>();
    private final TextField filterText = new TextField();

    private final LayerImageForm form;

    public LayerImageGrid(LayerImageService layerImageService, LayerGroupImageService layerGroupImageService,
                          FolderService folderService){
        this.layerImageService = layerImageService;
        this.layerGroupImageService = layerGroupImageService;

        addClassName("list-view");
        setSizeFull();
        Div div = new Div(gridTree);
        div.addClassName("content");
        div.setSizeFull();
        div.setWidth("400px");
        configureGrid();

        setPadding(true);
        setSpacing(false);

        form = new LayerImageForm(folderService);
        form.addListener(LayerImageForm.SaveEvent.class, this::saveLayerImage);
        form.addListener(LayerImageForm.DeleteEvent.class, this::deleteLayerImage);
        form.addListener(LayerImageForm.CloseEvent.class, closeEvent -> closeEditor());
        form.addListener(LayerImageForm.CopyEvent.class, this::copyLayerImage);

        Div content = new Div(div, grid, form);
        content.addClassName("content");
        content.setSizeFull();

        add(getToolbar(), content);

        closeEditor();
    }

    private void copyLayerImage(LayerImageForm.CopyEvent event){
        Dialog dialog = new Dialog();

        TreeGrid<LayerGroupImage> tempTreeGrid = new TreeGrid<>();
        tempTreeGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        tempTreeGrid.setWidth("500px");
        tempTreeGrid.setHeight("400px");

        tempTreeGrid.addHierarchyColumn(LayerGroupImage::getName).setHeader("Группы");

        HierarchicalDataProvider<LayerGroupImage, Void> dataProvider =
                new AbstractBackEndHierarchicalDataProvider<>() {

                    @Override
                    public int getChildCount(HierarchicalQuery<LayerGroupImage, Void> query) {
                        return (int) layerGroupImageService.getChildCount(query.getParent());
                    }

                    @Override
                    public boolean hasChildren(LayerGroupImage item) {
                        return layerGroupImageService.hasChildren(item);
                    }

                    @Override
                    protected Stream<LayerGroupImage> fetchChildrenFromBackEnd(
                            HierarchicalQuery<LayerGroupImage, Void> query) {
                        return layerGroupImageService.fetchChildren(query.getParent()).stream();
                    }
                };

        tempTreeGrid.setDataProvider(dataProvider);

        Button save = new Button(IronIcons.SAVE.create());
        Button cancel = new Button(IronIcons.CANCEL.create());

        cancel.addClickListener(eventClick -> dialog.close());
        save.addClickListener(buttonClickEvent -> ConfirmDialog
                .createQuestion()
                .withCaption("Сохранение")
                .withMessage("Сохранить изменения?")
                .withOkButton(() -> {

                    if (tempTreeGrid.asMultiSelect().getSelectedItems().size() > 0) {
                        tempTreeGrid.asMultiSelect().getSelectedItems().forEach(layerGroupImage -> {
                            LayerImage layerImageClone = new LayerImage(event.getLayerImage().getName() + " - " + (LocalTime.now()), event.getLayerImage().getFileInfo(),
                                    event.getLayerImage().getWidth(), event.getLayerImage().getHeight(), event.getLayerImage().getX(),
                                    event.getLayerImage().getY(), false);

                            layerImageService.save(layerImageClone);
                            layerGroupImage.getLayerImages().add(layerImageClone);
                            layerGroupImageService.edit(layerGroupImage);
                        });
                        gridTree.getDataProvider().refreshAll();
                        gridTree.select(tempTreeGrid.getSelectedItems().iterator().next());
                        grid.setItems(tempTreeGrid.getSelectedItems().iterator().next().getLayerImages());
                        Notification.show("Сохранено!", 2000,
                                Notification.Position.MIDDLE);
                        dialog.close();
                    } else {
                        Notification.show("Выберить группу в которой нужно создать копию!", 2000,
                                Notification.Position.MIDDLE);
                    }
                }, ButtonOption.focus(), ButtonOption.caption("Да"))
                .withCancelButton(ButtonOption.caption("Нет"))
                .open());

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setWidth("550px");
        verticalLayout.setHeight("550px");
        verticalLayout.add(new H2("Клонирование"),tempTreeGrid, new HorizontalLayout(save, cancel));

        dialog.setWidth("600px");
        dialog.setHeight("600px");

        dialog.add(verticalLayout);
        dialog.open();
    }

    private void saveLayerImage(LayerImageForm.SaveEvent event){
        ConfirmDialog
                .createQuestion()
                .withCaption("Сохранение")
                .withMessage("Сохранить изменения?")
                .withOkButton(() -> {
                    try {
                        layerImageService.save(event.getLayerImage());
                        layerGroupImageService.findAll().forEach(layerGroupImage -> {
                            if (layerGroupImage.getName().equals(gridTree.asSingleSelect().getValue().getName())){
                                layerGroupImage.getLayerImages().add(event.getLayerImage());
                                layerGroupImageService.edit(layerGroupImage);
                                gridTree.getDataProvider().refreshAll();
                                gridTree.deselect(layerGroupImage);
                                gridTree.select(layerGroupImage);
                                grid.setItems(gridTree.asSingleSelect().getValue().getLayerImages());
                            }
                        });
                        Notification.show("Сохранено!", 2000,
                                Notification.Position.MIDDLE);
                        closeEditor();
                    }  catch (DataIntegrityViolationException dataIntegrityViolationException){
                        new Notification(
                                "Ошибка: Слой с именем '" + event.getLayerImage().getName() + "' существуе.\n" +
                                        "Дубликат имен запрещен.", 3000).open();
                        closeEditor();
                    }
                }, ButtonOption.focus(), ButtonOption.caption("Да"))
                .withCancelButton(ButtonOption.caption("Нет"))
                .open();

    }

    private void deleteLayerImage(LayerImageForm.DeleteEvent event) {
        ConfirmDialog
                .createQuestion()
                .withCaption("Удаление")
                .withMessage("Вы уверенные что хотите удалить?\nОтменить действие будет не возможно")
                .withOkButton(() -> layerGroupImageService.findAll().forEach(layerGroupImage -> {
                    if (!grid.asSingleSelect().getValue().isActive()) {
                        if (layerGroupImage.getName().equals(gridTree.asSingleSelect().getValue().getName())) {
                            try {
                                layerImageService.delete(grid.asSingleSelect().getValue());
                                layerGroupImage.getLayerImages().remove(grid.asSingleSelect().getValue());
                                layerGroupImageService.edit(layerGroupImage);
                                gridTree.getDataProvider().refreshAll();
                                gridTree.deselect(layerGroupImage);
                                gridTree.select(layerGroupImage);
                                grid.setItems(gridTree.asSingleSelect().getValue().getLayerImages());
                                new Notification(
                                        "Удалено", 3000).open();
                            } catch (DataIntegrityViolationException d) {
                                if (d.getRootCause().getMessage().contains("layer_group_image_layer_images")) {
                                    layerGroupImage.getLayerImages().remove(grid.asSingleSelect().getValue());
                                    layerGroupImageService.edit(layerGroupImage);
                                    layerImageService.delete(grid.asSingleSelect().getValue());
                                    gridTree.getDataProvider().refreshAll();
                                    gridTree.deselect(layerGroupImage);
                                    gridTree.select(layerGroupImage);
                                    grid.setItems(gridTree.asSingleSelect().getValue().getLayerImages());

                                    new Notification(
                                            "Слой удален", 3000).open();
                                } else {
                                    new Notification(
                                            "Слой - " + grid.asSingleSelect().getValue().getName() + " используется, удалить не возможно",
                                            3000).open();
                                }
                            }
                        }
                    } else {
                        new Notification(
                                "Слой - " + grid.asSingleSelect().getValue().getName() + " активный, удалить не возможно",
                                3000).open();
                    }
                }), ButtonOption.focus(), ButtonOption.caption("Да"))
                .withCancelButton(ButtonOption.caption("Нет"))
                .open();

    }

    private void configureGrid(){
        GridContextMenu<LayerGroupImage> contextMenu = new GridContextMenu<>(gridTree);

        gridTree.setSizeFull();
        gridTree.setWidth("200px");
        gridTree.addHierarchyColumn(LayerGroupImage::getName).setHeader("Группы");


        grid.addClassName("contact-grid");
        grid.setSizeFull();
        grid.setColumns("name", "fileInfo", "width", "height", "x", "y", "active");
        grid.getColumnByKey("name").setHeader("Имя").setResizable(true);
        grid.getColumnByKey("fileInfo").setHeader("Файл").setWidth("300px").setResizable(true);
        grid.getColumnByKey("width").setHeader("Ширина");
        grid.getColumnByKey("height").setHeader("Высота");
        grid.getColumnByKey("x").setHeader("X");
        grid.getColumnByKey("y").setHeader("Y");
        grid.removeColumnByKey("active");
        grid.addColumn(new ComponentRenderer<>(layerImage -> {
            Checkbox checkbox = new Checkbox();
            checkbox.setValue(layerImage.isActive());
            checkbox.addClickListener(checkboxClickEvent -> ConfirmDialog
                    .createQuestion()
                    .withCaption("Изменения статуса")
                    .withMessage("Применить изменение?")
                    .withOkButton(() -> {
                        layerImage.setActive(checkbox.getValue());
                        layerImageService.save(layerImage);
                        updateList();
                    }, ButtonOption.focus(), ButtonOption.caption("Да"))
                    .withCancelButton(() -> checkbox.setValue(layerImage.isActive()), ButtonOption.caption("Нет"))
                    .open());
            return checkbox;
        })).setHeader("Активирован").setKey("active");

        grid.addColumn(item -> "").setKey("rowIndex").setFlexGrow(0).setWidth("75px");

        grid.addAttachListener(event -> grid.getColumnByKey("rowIndex").getElement().executeJs(
                "this.renderer = function(root, column, rowData) {root.textContent = rowData.index + 1}"
        ));

        grid.setColumnOrder(
                grid.getColumnByKey("rowIndex"),
                grid.getColumnByKey("name"),
                grid.getColumnByKey("fileInfo"),
                grid.getColumnByKey("width"),
                grid.getColumnByKey("height"),
                grid.getColumnByKey("x"),
                grid.getColumnByKey("y"),
                grid.getColumnByKey("active"));

        List<GridSortOrder<LayerImage>> sortByNumber = new GridSortOrderBuilder<LayerImage>()
                .thenAsc(grid.getColumnByKey("name")).build();
        grid.sort(sortByNumber);

        grid.asSingleSelect().addValueChangeListener(event ->
                editLayerImage(event.getValue()));

        gridTree.asSingleSelect().addValueChangeListener(gridFolderComponentValueChangeEvent -> {
            if(gridFolderComponentValueChangeEvent.getValue() != null) {
                grid.setItems(gridFolderComponentValueChangeEvent.getValue().getLayerImages());
            }
        });



        HierarchicalDataProvider<LayerGroupImage, Void> dataProvider =
                new AbstractBackEndHierarchicalDataProvider<>() {

                    @Override
                    public int getChildCount(HierarchicalQuery<LayerGroupImage, Void> query) {
                        return (int) layerGroupImageService.getChildCount(query.getParent());
                    }

                    @Override
                    public boolean hasChildren(LayerGroupImage item) {
                        return layerGroupImageService.hasChildren(item);
                    }

                    @Override
                    protected Stream<LayerGroupImage> fetchChildrenFromBackEnd(
                            HierarchicalQuery<LayerGroupImage, Void> query) {
                        return layerGroupImageService.fetchChildren(query.getParent()).stream().
                                sorted(Comparator.comparing(LayerGroupImage::getName));
                    }
                };

        gridTree.setDataProvider(dataProvider);

        contextMenu.addItem("Добавить", event -> {
            if (event.getItem().isPresent()) {
                event.getItem().ifPresent(layerGroupImage -> createAddLayerGroupImageForm(layerGroupImageService,
                        layerGroupImage, dataProvider).open());
            } else {
                createAddLayerGroupImageForm(layerGroupImageService, null, dataProvider).open();
            }
        });

        contextMenu.addItem("Удалить", event -> event.getItem().ifPresent(layerGroupImage -> {
            try {

                if (layerGroupImage.getLayerImages().size() == 0) {
                    layerGroupImageService.delete(layerGroupImage);
                    dataProvider.refreshAll();
                } else {
                    new Notification(
                            "Ошибка: Не возможно удалить группу '" + layerGroupImage.getName() + "'.\n" +
                                    "В группе есть слои.", 3000).open();
                }
            } catch (DataIntegrityViolationException dataIntegrityViolationException){
                new Notification(
                        "Ошибка: Не возможно удалить группу '" + layerGroupImage.getName() + "'.\n" +
                                "В группе есть вложенные группы.", 3000).open();
            }
        }));
    }

    public void editLayerImage(LayerImage layerImage){
        if (layerImage == null) {
            closeEditor();
        } else {
            form.setLayerImage(layerImage);
            form.setVisible(true);
            addClassName("editing");
        }
    }

    private void closeEditor(){
        form.setLayerImage(null);
        form.setVisible(false);
    }

    private HorizontalLayout getToolbar(){
        filterText.setPlaceholder("Поиск...");
        filterText.setClearButtonVisible(true);
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.addValueChangeListener(e -> updateList());

        Button addLayerImageButton = new Button(IronIcons.ADD.create());
        addLayerImageButton.setEnabled(false);
        gridTree.addSelectionListener(selectionEvent -> {
            if (selectionEvent.getFirstSelectedItem().isPresent()) {
                addLayerImageButton.setEnabled(true);
            } else {
                addLayerImageButton.setEnabled(false);
            }
        });

        addLayerImageButton.addClickListener(click -> addLayerImage());

        HorizontalLayout toolbar = new HorizontalLayout(filterText, addLayerImageButton);
        toolbar.addClassName("toolbar");

        return toolbar;
    }

    void addLayerImage(){
        grid.asSingleSelect().clear();
        editLayerImage(new LayerImage());
    }

    private void updateList(){
        grid.setItems(gridTree.asSingleSelect()
                .getValue()
                .getLayerImages()
                .stream()
                .filter(layerImage  -> layerImage.getName()
                        .toLowerCase(Locale.ROOT)
                        .contains(filterText.getValue())));
    }

    private Dialog createAddLayerGroupImageForm(LayerGroupImageService layerGroupImageService, LayerGroupImage parent,
                                             HierarchicalDataProvider<LayerGroupImage, Void> dataProvider){
        Dialog dialog = new Dialog();
        dialog.setCloseOnEsc(false);
        dialog.setCloseOnOutsideClick(false);

        TextField textField = new TextField("Имя группы");

        Binder<LayerGroupImage> binder = new Binder<>(LayerGroupImage.class);

        binder.forField(textField)
                .withValidator(min -> min.length() >= 2, "Минимум 2 символа")
                .bind(LayerGroupImage::getName, LayerGroupImage::setName);

        dialog.add(textField);

        Button confirmButton = new Button("Добавить", event -> {
            try {
                LayerGroupImage layerGroupImage;
                if (parent != null) {
                    layerGroupImage = new LayerGroupImage(textField.getValue(), parent, new LinkedHashSet<>());
                } else {
                    layerGroupImage = new LayerGroupImage(textField.getValue(), null, new LinkedHashSet<>());
                }
                layerGroupImageService.save(layerGroupImage);
                dataProvider.refreshAll();
                new Notification(
                        "Сохранено.", 3000).open();
                dialog.close();
            } catch (TransactionSystemException transactionSystemException){
                new Notification(
                        "Ошибка: Имя не может быть пустым!!", 3000).open();
            }  catch (UniqueDataException uniqueDataException){
                new Notification(
                        uniqueDataException.getMessage(), 3000).open();
            }

        });
        confirmButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        Button cancelButton = new Button("Отмена", event -> dialog.close());

        cancelButton.addThemeVariants(ButtonVariant.LUMO_ERROR);

        dialog.add(new Div( confirmButton, new Text("    "), cancelButton));

        return dialog;
    }

}
