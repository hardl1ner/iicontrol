package ua.ii.uvpcontrol.ui.component;

import com.vaadin.componentfactory.Tooltip;
import com.vaadin.componentfactory.TooltipAlignment;
import com.vaadin.componentfactory.TooltipPosition;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.H5;

public interface HasTooltip {
    default Tooltip getTooltip(Component component, String text){
        Tooltip tooltip = new Tooltip();

        tooltip.attachToComponent(component);

        tooltip.setPosition(TooltipPosition.TOP);
        tooltip.setAlignment(TooltipAlignment.CENTER);

        tooltip.add(new H5(text));

        return tooltip;
    }
}
