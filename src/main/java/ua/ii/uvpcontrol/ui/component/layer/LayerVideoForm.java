package ua.ii.uvpcontrol.ui.component.layer;

import com.flowingcode.vaadin.addons.ironicons.IronIcons;
import com.vaadin.componentfactory.Tooltip;
import com.vaadin.componentfactory.TreeComboBox;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.IronIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.provider.hierarchy.TreeData;
import com.vaadin.flow.shared.Registration;
import ua.ii.uvpcontrol.backend.data.content.Folder;
import ua.ii.uvpcontrol.backend.data.layer.LayerText;
import ua.ii.uvpcontrol.backend.data.layer.LayerVideo;
import ua.ii.uvpcontrol.backend.service.content.FolderService;
import ua.ii.uvpcontrol.ui.component.HasTooltip;


public class LayerVideoForm extends FormLayout implements HasTooltip {
    private TextField name = new TextField("Имя Слоя");
    private TreeComboBox<Folder> folder = new TreeComboBox<>(Folder::getName);
    private IntegerField width = new IntegerField("Ширина");
    private IntegerField height= new IntegerField("Высота");
    private IntegerField x = new IntegerField("X");
    private IntegerField y = new IntegerField("Y");
    private Checkbox active = new Checkbox("Активирован");

    Binder<LayerVideo> binder = new BeanValidationBinder<>(LayerVideo.class);

    private LayerVideo layerVideo;

    Button save = new Button();
    Button delete = new Button();
    Button close = new Button();
    Button copy = new Button();

    public LayerVideoForm(FolderService folderService){
        setWidth("400px");
        name.setWidth("300px");
        folder.setWidth("300px");
        width.setWidth("150px");
        height.setWidth("150px");
        x.setWidth("150px");
        y.setWidth("150px");
        active.setWidth("300px");
        binder.bindInstanceFields(this);

        folder.setLabel("Папка с видео");

        folder.setTreeData(new TreeData<Folder>().addItems(folderService.parent(), folderService::fetchChildren));

        add(new VerticalLayout(name, folder,
            new HorizontalLayout(width, height),
            new HorizontalLayout(x, y),
            active,
                createButtonsLayout()));

    }

    private HorizontalLayout createButtonsLayout(){
        IronIcon iconDelete = IronIcons.DELETE.create();
        iconDelete.setColor("red");
        delete.setIcon(iconDelete);
        save.setIcon(IronIcons.SAVE.create());
        close.setIcon(IronIcons.CANCEL.create());
        copy.setIcon(IronIcons.CONTENT_COPY.create());

        Tooltip saveToolTip = getTooltip(save, "Сохранить");
        Tooltip deleteToolTip = getTooltip(delete, "Удалить");
        Tooltip closeToolTip = getTooltip(close, "Отменить");
        Tooltip copyToolTip = getTooltip(copy, "Копировать");

        save.addClickShortcut(Key.ENTER);
        close.addClickShortcut(Key.ESCAPE);

        save.addClickListener(event -> validateAndSave());
        delete.addClickListener(event -> fireEvent(new DeleteEvent(this, layerVideo)));
        close.addClickListener(event -> fireEvent(new CloseEvent(this)));
        copy.addClickListener(event -> fireEvent(new CopyEvent(this, layerVideo)));


        binder.addStatusChangeListener(e -> save.setEnabled(binder.isValid()));
        return new HorizontalLayout(saveToolTip, deleteToolTip, closeToolTip, copyToolTip, save, copy, close, delete);
    }

    private void validateAndSave(){
        try {
            binder.writeBean(layerVideo);
            fireEvent(new SaveEvent(this, layerVideo));
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }


    public void setLayerVideo(LayerVideo layerVideo) {
        this.layerVideo = layerVideo;
        binder.readBean(layerVideo);
    }

    // Events
    public static abstract class LayerVideoFormEvent extends ComponentEvent<LayerVideoForm> {
        private LayerVideo layerVideo;

        protected LayerVideoFormEvent(LayerVideoForm source, LayerVideo layerVideo) {
            super(source, false);
            this.layerVideo = layerVideo;
        }

        public LayerVideo getLayerVideo() {
            return layerVideo;
        }
    }

    public static class SaveEvent extends LayerVideoFormEvent {
        SaveEvent(LayerVideoForm source, LayerVideo layerVideo) {
            super(source, layerVideo);
        }
    }

    public static class DeleteEvent extends LayerVideoFormEvent {
        DeleteEvent(LayerVideoForm source, LayerVideo layerVideo) {
            super(source, layerVideo);
        }

    }

    public static class CloseEvent extends LayerVideoFormEvent {
        CloseEvent(LayerVideoForm source) {
            super(source, null);
        }
    }

    public static class CopyEvent extends LayerVideoFormEvent {
        CopyEvent(LayerVideoForm source, LayerVideo layerVideo) {
            super(source, layerVideo);
        }
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType,
                                                                  ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }
}


