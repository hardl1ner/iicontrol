package ua.ii.uvpcontrol.ui.component.layer;

import com.flowingcode.vaadin.addons.ironicons.IronIcons;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.grid.GridSortOrderBuilder;
import com.vaadin.flow.component.grid.contextmenu.GridContextMenu;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.hierarchy.AbstractBackEndHierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalQuery;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import org.claspina.confirmdialog.ButtonOption;
import org.claspina.confirmdialog.ConfirmDialog;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionSystemException;
import ua.ii.uvpcontrol.backend.data.layer.LayerGroupImage;
import ua.ii.uvpcontrol.backend.data.layer.LayerGroupPanel;
import ua.ii.uvpcontrol.backend.data.layer.LayerImage;
import ua.ii.uvpcontrol.backend.data.layer.LayerPanel;
import ua.ii.uvpcontrol.backend.data.station.Station;
import ua.ii.uvpcontrol.backend.service.content.FolderService;
import ua.ii.uvpcontrol.backend.service.exception.UniqueDataException;
import ua.ii.uvpcontrol.backend.service.layer.LayerGroupPanelService;
import ua.ii.uvpcontrol.backend.service.layer.LayerPanelService;

import java.time.LocalTime;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;

public class LayerPanelGrid extends VerticalLayout {
    private final LayerPanelService layerPanelService;
    private final LayerGroupPanelService layerGroupPanelService;

    private final Grid<LayerPanel> grid = new Grid<>(LayerPanel.class);
    private final TreeGrid<LayerGroupPanel> gridTree = new TreeGrid<>();
    private final TextField filterText = new TextField();

    private final LayerPanelForm form;

    public LayerPanelGrid(LayerPanelService layerPanelService, LayerGroupPanelService layerGroupPanelService,
                          FolderService folderService){
        this.layerPanelService = layerPanelService;
        this.layerGroupPanelService = layerGroupPanelService;

        addClassName("list-view");
        setSizeFull();
        Div div = new Div(gridTree);
        div.addClassName("content");
        div.setSizeFull();
        div.setWidth("400px");
        configureGrid();

        setPadding(true);
        setSpacing(false);

        form = new LayerPanelForm(folderService);
        form.addListener(LayerPanelForm.SaveEvent.class, this::saveLayerPanel);
        form.addListener(LayerPanelForm.DeleteEvent.class, this::deleteLayerPanel);
        form.addListener(LayerPanelForm.CloseEvent.class, e -> closeEditor());
        form.addListener(LayerPanelForm.CopyEvent.class, this::copyLayerPanel);

        Div content = new Div(div, grid, form);
        content.addClassName("content");
        content.setSizeFull();

        add(getToolbar(), content);

        closeEditor();
    }

    private void copyLayerPanel(LayerPanelForm.CopyEvent event){
        Dialog dialog = new Dialog();


        TreeGrid<LayerGroupPanel> tempTreeGrid = new TreeGrid<>();
        tempTreeGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        tempTreeGrid.setWidth("500px");
        tempTreeGrid.setHeight("400px");

        tempTreeGrid.addHierarchyColumn(LayerGroupPanel::getName).setHeader("Группы");

        HierarchicalDataProvider<LayerGroupPanel, Void> dataProvider =
                new AbstractBackEndHierarchicalDataProvider<>() {

                    @Override
                    public int getChildCount(HierarchicalQuery<LayerGroupPanel, Void> query) {
                        return (int) layerGroupPanelService.getChildCount(query.getParent());
                    }

                    @Override
                    public boolean hasChildren(LayerGroupPanel item) {
                        return layerGroupPanelService.hasChildren(item);
                    }

                    @Override
                    protected Stream<LayerGroupPanel> fetchChildrenFromBackEnd(
                            HierarchicalQuery<LayerGroupPanel, Void> query) {
                        return layerGroupPanelService.fetchChildren(query.getParent()).stream().
                                sorted(Comparator.comparing(LayerGroupPanel::getName));
                    }
                };

        tempTreeGrid.setDataProvider(dataProvider);

        Button save = new Button(IronIcons.SAVE.create());
        Button cancel = new Button(IronIcons.CANCEL.create());

        cancel.addClickListener(eventClick -> dialog.close());
        save.addClickListener(buttonClickEvent -> ConfirmDialog
                .createQuestion()
                .withCaption("Сохранение")
                .withMessage("Сохранить изменения?")
                .withOkButton(() -> {
                    if (tempTreeGrid.asMultiSelect().getSelectedItems().size() > 0) {
                        tempTreeGrid.asMultiSelect().getSelectedItems().forEach(layerGroupPanel -> {
                            LayerPanel clone = new LayerPanel(event.getLayerPanel().getName() + " - " + (LocalTime.now()), event.getLayerPanel().getFolder(),
                                    event.getLayerPanel().getWidth(), event.getLayerPanel().getHeight(), event.getLayerPanel().getX(),
                                    event.getLayerPanel().getY(), event.getLayerPanel().getTrigger_panel(), false);

                            layerPanelService.save(clone);
                            layerGroupPanel.getLayerPanels().add(clone);
                            layerGroupPanelService.edit(layerGroupPanel);
                        });
                        gridTree.getDataProvider().refreshAll();
                        gridTree.select(tempTreeGrid.getSelectedItems().iterator().next());
                        grid.setItems(tempTreeGrid.getSelectedItems().iterator().next().getLayerPanels());
                        Notification.show("Сохранено!", 2000,
                                Notification.Position.MIDDLE);
                        dialog.close();
                    } else {
                        Notification.show("Выберить группу в которой нужно создать копию!", 2000,
                                Notification.Position.MIDDLE);
                    }
                }, ButtonOption.focus(), ButtonOption.caption("Да"))
                .withCancelButton(ButtonOption.caption("Нет"))
                .open());

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setWidth("550px");
        verticalLayout.setHeight("550px");
        verticalLayout.add(new H2("Клонирование"),tempTreeGrid, new HorizontalLayout(save, cancel));

        dialog.setWidth("600px");
        dialog.setHeight("600px");

        dialog.add(verticalLayout);
        dialog.open();
    }

    private void saveLayerPanel(LayerPanelForm.SaveEvent event){
        ConfirmDialog
                .createQuestion()
                .withCaption("Сохранение")
                .withMessage("Сохранить изменения?")
                .withOkButton(() -> {
                    try {
                        layerPanelService.save(event.getLayerPanel());
                        layerGroupPanelService.findAll().forEach(layerGroupPanel -> {
                            if (layerGroupPanel.getName().equals(gridTree.asSingleSelect().getValue().getName())){
                                layerGroupPanel.getLayerPanels().add(event.getLayerPanel());
                                layerGroupPanelService.edit(layerGroupPanel);
                                gridTree.getDataProvider().refreshAll();
                                gridTree.deselect(layerGroupPanel);
                                gridTree.select(layerGroupPanel);
                                grid.setItems(gridTree.asSingleSelect().getValue().getLayerPanels());
                            }
                        });
                        Notification.show("Сохранено!", 2000,
                                Notification.Position.MIDDLE);
                        closeEditor();
                    }  catch (DataIntegrityViolationException dataIntegrityViolationException){
                        new Notification(
                                "Ошибка: Слой с именем '" + event.getLayerPanel().getName() + "' существуе.\n" +
                                        "Дубликат имен запрещен.", 3000).open();
                        closeEditor();
                    }
                }, ButtonOption.focus(), ButtonOption.caption("Да"))
                .withCancelButton(ButtonOption.caption("Нет"))
                .open();

    }

    private void deleteLayerPanel(LayerPanelForm.DeleteEvent event) {
        ConfirmDialog
                .createQuestion()
                .withCaption("Удаление")
                .withMessage("Вы уверенные что хотите удалить?\nОтменить действие будет не возможно")
                .withOkButton(() -> {
                    try {
                        layerGroupPanelService.findAll().forEach(layerGroupPanel -> {
                            if (layerGroupPanel.getName().equals(gridTree.asSingleSelect().getValue().getName())){
                                layerGroupPanel.getLayerPanels().remove(event.getLayerPanel());
                                layerGroupPanelService.edit(layerGroupPanel);
                                layerPanelService.delete(event.getLayerPanel());
                                gridTree.getDataProvider().refreshAll();
                                gridTree.deselect(layerGroupPanel);
                                gridTree.select(layerGroupPanel);
                                grid.setItems(gridTree.asSingleSelect().getValue().getLayerPanels());
                            }
                        });


                        Notification.show("Удалено!", 2000,
                                Notification.Position.MIDDLE);
                        closeEditor();
                    } catch (UniqueDataException uniqueDataException){
                        new Notification(
                                uniqueDataException.getMessage(), 3000).open();
                        grid.setItems(gridTree.asSingleSelect().getValue().getLayerPanels());
                        closeEditor();
                    }
                }, ButtonOption.focus(), ButtonOption.caption("Да"))
                .withCancelButton(ButtonOption.caption("Нет"))
                .open();

    }

    private void configureGrid(){
        GridContextMenu<LayerGroupPanel> contextMenu = new GridContextMenu<>(gridTree);

        gridTree.setSizeFull();
        gridTree.setWidth("200px");
        gridTree.addHierarchyColumn(LayerGroupPanel::getName).setHeader("Группы");


        grid.addClassName("contact-grid");
        grid.setSizeFull();
        grid.setColumns("name", "folder", "trigger_panel", "width", "height", "x", "y", "active");
        grid.getColumnByKey("name").setHeader("Имя").setResizable(true);
        grid.getColumnByKey("folder").setHeader("Папка").setResizable(true);
        grid.getColumnByKey("trigger_panel").setHeader("Триггер").setResizable(true);
        grid.getColumnByKey("width").setHeader("Ширина");
        grid.getColumnByKey("height").setHeader("Высота");
        grid.getColumnByKey("x").setHeader("X");
        grid.getColumnByKey("y").setHeader("Y");
        grid.removeColumnByKey("active");
        grid.addColumn(new ComponentRenderer<>(layerPanel -> {
            Checkbox checkbox = new Checkbox();
            checkbox.setValue(layerPanel.isActive());
            checkbox.addClickListener(checkboxClickEvent -> ConfirmDialog
                    .createQuestion()
                    .withCaption("Изменения статуса")
                    .withMessage("Применить изменение?")
                    .withOkButton(() -> {
                        layerPanel.setActive(checkbox.getValue());
                        layerPanelService.save(layerPanel);
                        updateList();
                    }, ButtonOption.focus(), ButtonOption.caption("Да"))
                    .withCancelButton(() -> checkbox.setValue(layerPanel.isActive()), ButtonOption.caption("Нет"))
                    .open());
            return checkbox;
        })).setHeader("Активирован").setKey("active");

        grid.getColumns().forEach(col -> col.setAutoWidth(true));

        grid.addColumn(item -> "").setKey("rowIndex").setFlexGrow(0).setWidth("75px");

        grid.addAttachListener(event -> grid.getColumnByKey("rowIndex").getElement().executeJs(
                "this.renderer = function(root, column, rowData) {root.textContent = rowData.index + 1}"
        ));

        grid.setColumnOrder(
                grid.getColumnByKey("rowIndex"),
                grid.getColumnByKey("name"),
                grid.getColumnByKey("folder"),
                grid.getColumnByKey("trigger_panel"),
                grid.getColumnByKey("width"),
                grid.getColumnByKey("height"),
                grid.getColumnByKey("x"),
                grid.getColumnByKey("y"),
                grid.getColumnByKey("active"));

        List<GridSortOrder<LayerPanel>> sortByNumber = new GridSortOrderBuilder<LayerPanel>()
                .thenAsc(grid.getColumnByKey("name")).build();
        grid.sort(sortByNumber);

        grid.asSingleSelect().addValueChangeListener(event ->
                editLayerPanel(event.getValue()));

        gridTree.asSingleSelect().addValueChangeListener(gridFolderComponentValueChangeEvent -> {
            if(gridFolderComponentValueChangeEvent.getValue() != null) {
                grid.setItems(gridFolderComponentValueChangeEvent.getValue().getLayerPanels());
            }
        });



        HierarchicalDataProvider<LayerGroupPanel, Void> dataProvider =
                new AbstractBackEndHierarchicalDataProvider<>() {

                    @Override
                    public int getChildCount(HierarchicalQuery<LayerGroupPanel, Void> query) {
                        return (int) layerGroupPanelService.getChildCount(query.getParent());
                    }

                    @Override
                    public boolean hasChildren(LayerGroupPanel item) {
                        return layerGroupPanelService.hasChildren(item);
                    }

                    @Override
                    protected Stream<LayerGroupPanel> fetchChildrenFromBackEnd(
                            HierarchicalQuery<LayerGroupPanel, Void> query) {
                        return layerGroupPanelService.fetchChildren(query.getParent()).stream();
                    }
                };

        gridTree.setDataProvider(dataProvider);


        contextMenu.addItem("Добавить", event -> {
            if (event.getItem().isPresent()) {
                event.getItem().ifPresent(layerGroupPanel -> createAddLayerGroupPanelForm(layerGroupPanelService,
                        layerGroupPanel, dataProvider).open());
            } else {
                createAddLayerGroupPanelForm(layerGroupPanelService, null, dataProvider).open();
            }
        });

        contextMenu.addItem("Удалить", event -> event.getItem().ifPresent(layerGroupPanel -> {
            try {

                if (layerGroupPanel.getLayerPanels().size() == 0) {
                    layerGroupPanelService.delete(layerGroupPanel);
                    dataProvider.refreshAll();
                } else {
                    new Notification(
                            "Ошибка: Не возможно удалить группу '" + layerGroupPanel.getName() + "'.\n" +
                                    "В группе есть слои.", 3000).open();
                }
            } catch (DataIntegrityViolationException dataIntegrityViolationException){
                new Notification(
                        "Ошибка: Не возможно удалить группу '" + layerGroupPanel.getName() + "'.\n" +
                                "В группе есть вложенные группы.", 3000).open();
            }
        }));
    }

    public void editLayerPanel(LayerPanel layerPanel){
        if (layerPanel == null) {
            closeEditor();
        } else {
            form.setLayerPanel(layerPanel);
            form.setVisible(true);
            addClassName("editing");
        }
    }

    private void closeEditor(){
        form.setLayerPanel(null);
        form.setVisible(false);
        removeClassName("editing");
    }

    private HorizontalLayout getToolbar(){
        filterText.setPlaceholder("Поиск...");
        filterText.setClearButtonVisible(true);
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.addValueChangeListener(e -> updateList());

        Button addLayerPanelButton = new Button(IronIcons.ADD.create());
        addLayerPanelButton.setEnabled(false);
        gridTree.addSelectionListener(selectionEvent -> {
            if (selectionEvent.getFirstSelectedItem().isPresent()) {
                addLayerPanelButton.setEnabled(true);
            } else {
                addLayerPanelButton.setEnabled(false);
            }
        });

        addLayerPanelButton.addClickListener(click -> addLayerPanel());

        HorizontalLayout toolbar = new HorizontalLayout(filterText, addLayerPanelButton);
        toolbar.addClassName("toolbar");

        return toolbar;
    }

    void addLayerPanel(){
        grid.asSingleSelect().clear();
        editLayerPanel(new LayerPanel());
    }

    private void updateList(){
        grid.setItems(gridTree.asSingleSelect()
                .getValue()
                .getLayerPanels()
                .stream()
                .filter(layerPanel  -> layerPanel.getName()
                        .toLowerCase(Locale.ROOT)
                        .contains(filterText.getValue())));
    }

    private Dialog createAddLayerGroupPanelForm(LayerGroupPanelService layerGroupPanelService, LayerGroupPanel parent,
                                             HierarchicalDataProvider<LayerGroupPanel, Void> dataProvider){
        Dialog dialog = new Dialog();
        dialog.setCloseOnEsc(false);
        dialog.setCloseOnOutsideClick(false);

        TextField textField = new TextField("Имя группы");

        Binder<LayerGroupPanel> binder = new Binder<>(LayerGroupPanel.class);

        binder.forField(textField)
                .withValidator(min -> min.length() >= 2, "Минимум 2 символа")
                .bind(LayerGroupPanel::getName, LayerGroupPanel::setName);

        dialog.add(textField);

        Button confirmButton = new Button("Добавить", event -> {
            try {
                LayerGroupPanel layerGroupPanel;
                if (parent != null) {
                    layerGroupPanel = new LayerGroupPanel(textField.getValue(), parent, new HashSet<>());
                } else {
                    layerGroupPanel = new LayerGroupPanel(textField.getValue(), null, new HashSet<>());
                }
                layerGroupPanelService.save(layerGroupPanel);
                dataProvider.refreshAll();
                new Notification(
                        "Сохранено.", 3000).open();
                dialog.close();
            } catch (TransactionSystemException transactionSystemException){
                new Notification(
                        "Ошибка: Имя не может быть пустым!!", 3000).open();
            }  catch (UniqueDataException uniqueDataException){
                new Notification(
                        uniqueDataException.getMessage(), 3000).open();
            }

        });
        confirmButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        Button cancelButton = new Button("Отмена", event -> dialog.close());

        cancelButton.addThemeVariants(ButtonVariant.LUMO_ERROR);

        dialog.add(new Div( confirmButton, new Text("    "), cancelButton));

        return dialog;
    }

}
