package ua.ii.uvpcontrol.ui.component.layer;

import com.flowingcode.vaadin.addons.ironicons.IronIcons;
import com.vaadin.componentfactory.Tooltip;
import com.vaadin.componentfactory.TreeComboBox;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.IronIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.provider.hierarchy.TreeData;
import com.vaadin.flow.shared.Registration;
import ua.ii.uvpcontrol.backend.data.content.FileInfo;
import ua.ii.uvpcontrol.backend.data.content.Folder;
import ua.ii.uvpcontrol.backend.data.layer.LayerImage;
import ua.ii.uvpcontrol.backend.service.content.FolderService;
import ua.ii.uvpcontrol.ui.component.HasTooltip;

import java.util.ArrayList;


public class LayerImageForm extends FormLayout implements HasTooltip {
    private TextField name = new TextField("Имя Слоя");
    private ComboBox<FileInfo> fileInfo = new ComboBox<>("Файл");
    private IntegerField width = new IntegerField("Ширина");
    private IntegerField height= new IntegerField("Высота");
    private IntegerField x = new IntegerField("X");
    private IntegerField y = new IntegerField("Y");
    private Checkbox active = new Checkbox("Активирован");

    Binder<LayerImage> binder = new BeanValidationBinder<>(LayerImage.class);

    private LayerImage layerImage;

    Button save = new Button();
    Button delete = new Button();
    Button close = new Button();
    Button copy = new Button();

    public LayerImageForm(FolderService folderService){
        setWidth("400px");
        name.setWidth("300px");
        fileInfo.setWidth("300px");
        width.setWidth("150px");
        height.setWidth("150px");
        x.setWidth("150px");
        y.setWidth("150px");
        active.setWidth("300px");

        binder.bindInstanceFields(this);

        TreeComboBox<Folder> folder = new TreeComboBox<>(Folder::getName);
        folder.setLabel("Папка");
        folder.setWidth("300px");

        folder.setTreeData(new TreeData<Folder>().addItems(folderService.parent(),
                folderService::fetchChildren));

        folder.addValueChangeListener(event -> {
            ArrayList<FileInfo> arrayList = new ArrayList();
            event.getValue().getFileInfos().forEach(fileInfo1 -> {
                if (fileInfo1.getFile_type().equals("image")){
                    arrayList.add(fileInfo1);
                }
            });
            fileInfo.setItems(arrayList);
        });

        add(new VerticalLayout(name,
            folder,
            fileInfo,
            new HorizontalLayout(width,
            height),
            new HorizontalLayout(x,
            y),
                active,
                createButtonsLayout()));

    }

    private HorizontalLayout createButtonsLayout(){
        IronIcon iconDelete = IronIcons.DELETE.create();
        iconDelete.setColor("red");
        delete.setIcon(iconDelete);
        save.setIcon(IronIcons.SAVE.create());
        close.setIcon(IronIcons.CLOSE.create());
        copy.setIcon(IronIcons.CONTENT_COPY.create());

        save.addClickShortcut(Key.ENTER);
        close.addClickShortcut(Key.ESCAPE);

        Tooltip saveToolTip = getTooltip(save, "Сохранить");
        Tooltip deleteToolTip = getTooltip(delete, "Удалить");
        Tooltip closeToolTip = getTooltip(close, "Отменить");
        Tooltip copyToolTip = getTooltip(copy, "Копировать");

        save.addClickListener(event -> validateAndSave());
        delete.addClickListener(event -> fireEvent(new DeleteEvent(this, layerImage)));
        close.addClickListener(event -> fireEvent(new CloseEvent(this)));
        copy.addClickListener(event -> fireEvent(new CopyEvent(this, layerImage)));

        binder.addStatusChangeListener(e -> save.setEnabled(binder.isValid()));
        return new HorizontalLayout(save, copy, close, delete, saveToolTip, copyToolTip, closeToolTip, deleteToolTip);
    }

    private void validateAndSave(){
        try {
            binder.writeBean(layerImage);
            fireEvent(new SaveEvent(this, layerImage));
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }


    public void setLayerImage(LayerImage layerImage) {
        this.layerImage = layerImage;
        binder.readBean(layerImage);
    }

    // Events
    public static abstract class LayerImageFormEvent extends ComponentEvent<LayerImageForm> {
        private LayerImage layerImage;

        protected LayerImageFormEvent(LayerImageForm source, LayerImage layerImage) {
            super(source, false);
            this.layerImage = layerImage;
        }

        public LayerImage getLayerImage() {
            return layerImage;
        }
    }

    public static class SaveEvent extends LayerImageFormEvent {
        SaveEvent(LayerImageForm source, LayerImage layerImage) {
            super(source, layerImage);
        }
    }

    public static class DeleteEvent extends LayerImageFormEvent {
        DeleteEvent(LayerImageForm source, LayerImage layerImage) {
            super(source, layerImage);
        }

    }

    public static class CloseEvent extends LayerImageFormEvent {
        CloseEvent(LayerImageForm source) {
            super(source, null);
        }
    }

    public static class CopyEvent extends LayerImageFormEvent {
        CopyEvent(LayerImageForm source, LayerImage layerImage) {
            super(source, layerImage);
        }
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType,
                                                                  ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }
}


