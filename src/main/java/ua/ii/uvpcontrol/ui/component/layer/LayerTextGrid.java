package ua.ii.uvpcontrol.ui.component.layer;

import com.flowingcode.vaadin.addons.ironicons.IronIcons;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.grid.GridSortOrderBuilder;
import com.vaadin.flow.component.grid.contextmenu.GridContextMenu;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.hierarchy.AbstractBackEndHierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalQuery;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import org.claspina.confirmdialog.ButtonOption;
import org.claspina.confirmdialog.ConfirmDialog;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.TransactionSystemException;
import ua.ii.uvpcontrol.backend.data.layer.LayerGroupPanel;
import ua.ii.uvpcontrol.backend.data.layer.LayerGroupText;
import ua.ii.uvpcontrol.backend.data.layer.LayerPanel;
import ua.ii.uvpcontrol.backend.data.layer.LayerText;
import ua.ii.uvpcontrol.backend.data.station.Station;
import ua.ii.uvpcontrol.backend.service.exception.UniqueDataException;
import ua.ii.uvpcontrol.backend.service.layer.LayerGroupTextService;
import ua.ii.uvpcontrol.backend.service.layer.LayerTextService;

import java.time.LocalTime;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;

public class LayerTextGrid extends VerticalLayout {
    private final LayerTextService layerTextService;
    private final LayerGroupTextService layerGroupTextService;

    private final Grid<LayerText> grid = new Grid<>(LayerText.class);
    private final TreeGrid<LayerGroupText> gridTree = new TreeGrid<>();
    private final TextField filterText = new TextField();

    private final LayerTextForm form;

    public LayerTextGrid(LayerTextService layerTextService, LayerGroupTextService layerGroupTextService){
        this.layerTextService = layerTextService;
        this.layerGroupTextService = layerGroupTextService;

        addClassName("list-view");
        setSizeFull();
        Div div = new Div(gridTree);
        div.addClassName("content");
        div.setSizeFull();
        div.setWidth("400px");
        configureGrid();

        setPadding(true);
        setSpacing(false);

        form = new LayerTextForm();
        form.addListener(LayerTextForm.SaveEvent.class, this::saveLayerText);
        form.addListener(LayerTextForm.DeleteEvent.class, this::deleteLayerText);
        form.addListener(LayerTextForm.CloseEvent.class, e -> closeEditor());
        form.addListener(LayerTextForm.CopyEvent.class, this::copyLayerText);

        Div content = new Div(div, grid, form);
        content.addClassName("content");
        content.setSizeFull();

        add(getToolbar(), content);

        closeEditor();
    }

    private void copyLayerText(LayerTextForm.CopyEvent event){
        Dialog dialog = new Dialog();


        TreeGrid<LayerGroupText> tempTreeGrid = new TreeGrid<>();
        tempTreeGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        tempTreeGrid.setWidth("500px");
        tempTreeGrid.setHeight("400px");

        tempTreeGrid.addHierarchyColumn(LayerGroupText::getName).setHeader("Группы");

        HierarchicalDataProvider<LayerGroupText, Void> dataProvider =
                new AbstractBackEndHierarchicalDataProvider<>() {

                    @Override
                    public int getChildCount(HierarchicalQuery<LayerGroupText, Void> query) {
                        return (int) layerGroupTextService.getChildCount(query.getParent());
                    }

                    @Override
                    public boolean hasChildren(LayerGroupText item) {
                        return layerGroupTextService.hasChildren(item);
                    }

                    @Override
                    protected Stream<LayerGroupText> fetchChildrenFromBackEnd(
                            HierarchicalQuery<LayerGroupText, Void> query) {
                        return layerGroupTextService.fetchChildren(query.getParent()).stream().
                                sorted(Comparator.comparing(LayerGroupText::getName));
                    }
                };

        tempTreeGrid.setDataProvider(dataProvider);

        Button save = new Button(IronIcons.SAVE.create());
        Button cancel = new Button(IronIcons.CANCEL.create());

        cancel.addClickListener(eventClick -> dialog.close());
        save.addClickListener(buttonClickEvent -> ConfirmDialog
                .createQuestion()
                .withCaption("Сохранение")
                .withMessage("Сохранить изменения?")
                .withOkButton(() -> {
                        if (tempTreeGrid.asMultiSelect().getSelectedItems().size() > 0) {

                        tempTreeGrid.asMultiSelect().getSelectedItems().forEach(layerGroupText -> {
                            LayerText clone = new LayerText(event.getLayerText().getName() + " - " + (LocalTime.now()),
                                    event.getLayerText().getWidth(), event.getLayerText().getHeight(), event.getLayerText().getX(),
                                    event.getLayerText().getY(), event.getLayerText().getSizeFont(),
                                    event.getLayerText().getText(), event.getLayerText().getNameFont(),
                                    event.getLayerText().getStyleFont(), event.getLayerText().getOrientation(),
                                    event.getLayerText().getColor(), event.getLayerText().getTextAlignment(),
                                    event.getLayerText().getLocaleLanguage(), false,
                                    event.getLayerText().getType());

                            layerTextService.save(clone);
                            layerGroupText.getLayerTexts().add(clone);
                            layerGroupTextService.edit(layerGroupText);
                        });
                        gridTree.getDataProvider().refreshAll();
                        gridTree.select(tempTreeGrid.getSelectedItems().iterator().next());
                        grid.setItems(tempTreeGrid.getSelectedItems().iterator().next().getLayerTexts());
                        Notification.show("Сохранено!", 2000,
                                Notification.Position.MIDDLE);
                        dialog.close();
                    } else {
                        Notification.show("Выберить группу в которой нужно создать копию!", 2000,
                                Notification.Position.MIDDLE);
                    }
                }, ButtonOption.focus(), ButtonOption.caption("Да"))
                .withCancelButton(ButtonOption.caption("Нет"))
                .open());

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setWidth("550px");
        verticalLayout.setHeight("550px");
        verticalLayout.add(new H2("Клонирование"),tempTreeGrid, new HorizontalLayout(save, cancel));

        dialog.setWidth("600px");
        dialog.setHeight("600px");

        dialog.add(verticalLayout);
        dialog.open();
    }

    private void saveLayerText(LayerTextForm.SaveEvent event){
        ConfirmDialog
                .createQuestion()
                .withCaption("Сохранение")
                .withMessage("Сохранить изменения?")
                .withOkButton(() -> {
                    try {
                        layerTextService.save(event.getLayerText());
                        layerGroupTextService.findAll().forEach(layerGroupText -> {
                            if (layerGroupText.getName().equals(gridTree.asSingleSelect().getValue().getName())){
                                layerGroupText.getLayerTexts().add(event.getLayerText());
                                layerGroupTextService.edit(layerGroupText);
                                gridTree.getDataProvider().refreshAll();
                                gridTree.deselect(layerGroupText);
                                gridTree.select(layerGroupText);
                                grid.setItems(gridTree.asSingleSelect().getValue().getLayerTexts());
                            }
                        });
                        Notification.show("Сохранено!", 2000,
                                Notification.Position.MIDDLE);
                        closeEditor();
                    }  catch (DataIntegrityViolationException dataIntegrityViolationException){
                        new Notification(
                                "Ошибка: Слой с именем '" + event.getLayerText().getName() + "' существуе.\n" +
                                        "Дубликат имен запрещен.", 3000).open();
                        closeEditor();
                    }
                }, ButtonOption.focus(), ButtonOption.caption("Да"))
                .withCancelButton(ButtonOption.caption("Нет"))
                .open();

    }

    private void deleteLayerText(LayerTextForm.DeleteEvent event) {
        ConfirmDialog
                .createQuestion()
                .withCaption("Удаление")
                .withMessage("Вы уверенные что хотите удалить?\nОтменить действие будет не возможно")
                .withOkButton(() -> layerGroupTextService.findAll().forEach(layerGroupText -> {
                    if (layerGroupText.getName().equals(gridTree.asSingleSelect().getValue().getName())) {
                        try {
                            layerTextService.delete(grid.asSingleSelect().getValue());
                            layerGroupText.getLayerTexts().remove(grid.asSingleSelect().getValue());
                            layerGroupTextService.edit(layerGroupText);
                            gridTree.getDataProvider().refreshAll();
                            gridTree.deselect(layerGroupText);
                            gridTree.select(layerGroupText);
                            grid.setItems(gridTree.asSingleSelect().getValue().getLayerTexts());
                            new Notification(
                                    "Слой удален", 3000).open();
                        } catch (DataIntegrityViolationException d) {
                            if (d.getRootCause().getMessage().contains("layer_group_text_layer_texts")) {
                                layerGroupText.getLayerTexts().remove(grid.asSingleSelect().getValue());
                                layerGroupTextService.edit(layerGroupText);
                                layerTextService.delete(grid.asSingleSelect().getValue());
                                gridTree.getDataProvider().refreshAll();
                                gridTree.deselect(layerGroupText);
                                gridTree.select(layerGroupText);
                                grid.setItems(gridTree.asSingleSelect().getValue().getLayerTexts());

                                new Notification(
                                        "Слой удален", 3000).open();
                            } else {
                                new Notification(
                                        "Слой - " + grid.asSingleSelect().getValue().getName() + " используется, удалить не возможно",
                                        3000).open();
                            }
                        }
                    }
                }), ButtonOption.focus(), ButtonOption.caption("Да"))
                .withCancelButton(ButtonOption.caption("Нет"))
                .open();

    }

    private void configureGrid(){
        GridContextMenu<LayerGroupText> contextMenu = new GridContextMenu<>(gridTree);

        gridTree.setSizeFull();
        gridTree.setWidth("200px");
        gridTree.addHierarchyColumn(LayerGroupText::getName).setHeader("Группы");


        grid.addClassName("contact-grid");
        grid.setSizeFull();
        grid.setColumns("name", "width", "height", "x", "y", "text", "orientation", "active", "type");
        grid.getColumnByKey("name").setHeader("Имя").setResizable(true);
        grid.getColumnByKey("width").setHeader("Ширина");
        grid.getColumnByKey("height").setHeader("Высота");
        grid.getColumnByKey("x").setHeader("X");
        grid.getColumnByKey("y").setHeader("Y");
        grid.getColumnByKey("text").setHeader("Текст").setResizable(true);
        grid.getColumnByKey("orientation").setHeader("Ориентация");
        grid.getColumnByKey("type").setHeader("Тип");
        grid.removeColumnByKey("active");
        grid.addColumn(new ComponentRenderer<>(layerText -> {
            Checkbox checkbox = new Checkbox();
            checkbox.setValue(layerText.isActive());
            checkbox.addClickListener(checkboxClickEvent -> ConfirmDialog
                    .createQuestion()
                    .withCaption("Изменения статуса")
                    .withMessage("Применить изменение?")
                    .withOkButton(() -> {
                        layerText.setActive(checkbox.getValue());
                        layerTextService.save(layerText);
                        updateList();
                    }, ButtonOption.focus(), ButtonOption.caption("Да"))
                    .withCancelButton(() -> checkbox.setValue(layerText.isActive()), ButtonOption.caption("Нет"))
                    .open());
            return checkbox;
        })).setHeader("Активирован").setKey("active");

        grid.getColumns().forEach(col -> col.setAutoWidth(true));

        grid.addColumn(item -> "").setKey("rowIndex").setFlexGrow(0).setWidth("75px");

        grid.addAttachListener(event -> grid.getColumnByKey("rowIndex").getElement().executeJs(
                "this.renderer = function(root, column, rowData) {root.textContent = rowData.index + 1}"
        ));

        grid.setColumnOrder(
                grid.getColumnByKey("rowIndex"),
                grid.getColumnByKey("name"),
                grid.getColumnByKey("text"),
                grid.getColumnByKey("width"),
                grid.getColumnByKey("height"),
                grid.getColumnByKey("x"),
                grid.getColumnByKey("y"),
                grid.getColumnByKey("orientation"),
                grid.getColumnByKey("type"),
                grid.getColumnByKey("active"));

        List<GridSortOrder<LayerText>> sortByNumber = new GridSortOrderBuilder<LayerText>()
                .thenAsc(grid.getColumnByKey("name")).build();
        grid.sort(sortByNumber);

        grid.asSingleSelect().addValueChangeListener(event ->
                editLayerText(event.getValue()));

        gridTree.asSingleSelect().addValueChangeListener(gridFolderComponentValueChangeEvent -> {
            if(gridFolderComponentValueChangeEvent.getValue() != null) {
                grid.setItems(gridFolderComponentValueChangeEvent.getValue().getLayerTexts());
            }
        });



        HierarchicalDataProvider<LayerGroupText, Void> dataProvider =
                new AbstractBackEndHierarchicalDataProvider<>() {

                    @Override
                    public int getChildCount(HierarchicalQuery<LayerGroupText, Void> query) {
                        return (int) layerGroupTextService.getChildCount(query.getParent());
                    }

                    @Override
                    public boolean hasChildren(LayerGroupText item) {
                        return layerGroupTextService.hasChildren(item);
                    }

                    @Override
                    protected Stream<LayerGroupText> fetchChildrenFromBackEnd(
                            HierarchicalQuery<LayerGroupText, Void> query) {
                        return layerGroupTextService.fetchChildren(query.getParent()).stream();
                    }
                };

        gridTree.setDataProvider(dataProvider);

        contextMenu.addItem("Добавить", event -> {
            if (event.getItem().isPresent()) {
                event.getItem().ifPresent(layerGroupText -> createAddLayerGroupTextForm(layerGroupTextService,
                        layerGroupText, dataProvider).open());
            } else {
                createAddLayerGroupTextForm(layerGroupTextService, null, dataProvider).open();
            }
        });

        contextMenu.addItem("Удалить", event -> event.getItem().ifPresent(layerGroupText -> {
            try {

                if (layerGroupText.getLayerTexts().size() == 0) {
                    layerGroupTextService.delete(layerGroupText);
                    dataProvider.refreshAll();
                } else {
                    new Notification(
                            "Ошибка: Не возможно удалить группу '" + layerGroupText.getName() + "'.\n" +
                                    "В группе есть слои.", 3000).open();
                }
            } catch (DataIntegrityViolationException dataIntegrityViolationException){
                new Notification(
                        "Ошибка: Не возможно удалить группу '" + layerGroupText.getName() + "'.\n" +
                                "В группе есть вложенные группы.", 3000).open();
            }
        }));
    }

    public void editLayerText(LayerText layerText){
        if (layerText == null) {
            closeEditor();
        } else {
            form.setLayerText(layerText);
            form.setVisible(true);
            addClassName("editing");
        }
    }

    private void closeEditor(){
        form.setVisible(false);
        removeClassName("editing");
    }

    private HorizontalLayout getToolbar(){
        filterText.setPlaceholder("Поиск...");
        filterText.setClearButtonVisible(true);
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.addValueChangeListener(e -> updateList());

        Button addLayerTextButton = new Button(IronIcons.ADD.create());
        addLayerTextButton.setEnabled(false);
        gridTree.addSelectionListener(selectionEvent -> addLayerTextButton
                .setEnabled(selectionEvent.getFirstSelectedItem().isPresent()));

        addLayerTextButton.addClickListener(click -> addLayerText());

        HorizontalLayout toolbar = new HorizontalLayout(filterText, addLayerTextButton);
        toolbar.addClassName("toolbar");

        return toolbar;
    }

    void addLayerText(){
        grid.asSingleSelect().clear();
        editLayerText(new LayerText());
    }

    private void updateList(){
        grid.setItems(gridTree.asSingleSelect()
                .getValue()
                .getLayerTexts()
                .stream()
                .filter(layerText  -> layerText.getName()
                        .toLowerCase(Locale.ROOT)
                        .contains(filterText.getValue())));
    }

    private Dialog createAddLayerGroupTextForm(LayerGroupTextService layerGroupTextService, LayerGroupText parent,
                                             HierarchicalDataProvider<LayerGroupText, Void> dataProvider){
        Dialog dialog = new Dialog();
        dialog.setCloseOnEsc(false);
        dialog.setCloseOnOutsideClick(false);

        TextField textField = new TextField("Имя группы");

        Binder<LayerGroupText> binder = new Binder<>(LayerGroupText.class);

        binder.forField(textField)
                .withValidator(min -> min.length() >= 2, "Минимум 2 символа")
                .bind(LayerGroupText::getName, LayerGroupText::setName);

        dialog.add(textField);

        Button confirmButton = new Button("Добавить", event -> {
            try {
                LayerGroupText layerGroupText;
                if (parent != null) {
                    layerGroupText = new LayerGroupText(textField.getValue(), parent, new HashSet<>());
                } else {
                    layerGroupText = new LayerGroupText(textField.getValue(), null, new HashSet<>());
                }
                layerGroupTextService.save(layerGroupText);
                dataProvider.refreshAll();
                new Notification(
                        "Сохранено.", 3000).open();
                dialog.close();
            } catch (TransactionSystemException transactionSystemException){
                new Notification(
                        "Ошибка: Имя не может быть пустым!!", 3000).open();
            }  catch (UniqueDataException uniqueDataException){
                new Notification(
                        uniqueDataException.getMessage(), 3000).open();
            }

        });
        confirmButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        Button cancelButton = new Button("Отмена", event -> dialog.close());

        cancelButton.addThemeVariants(ButtonVariant.LUMO_ERROR);

        dialog.add(new Div( confirmButton, new Text("    "), cancelButton));

        return dialog;
    }

}
