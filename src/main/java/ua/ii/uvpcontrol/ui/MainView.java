package ua.ii.uvpcontrol.ui;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.TabVariant;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.VaadinServlet;
import org.springframework.security.access.annotation.Secured;
import ua.ii.uvpcontrol.app.security.SecurityUtils;
import ua.ii.uvpcontrol.backend.data.users.Role;
import ua.ii.uvpcontrol.ui.view.admin.ContentView;
import ua.ii.uvpcontrol.ui.view.admin.report.ReportView;
import ua.ii.uvpcontrol.ui.view.admin.station.StationsView;
import ua.ii.uvpcontrol.ui.view.admin.*;
import ua.ii.uvpcontrol.ui.view.admin.layer.LayerVideoView;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@Secured({Role.ADMIN, Role.MANAGER})
public class MainView extends AppLayout {

    private final Tabs menu;

    public MainView() {
        Image img = new Image("https://iicontrol.s3.eu-north-1.amazonaws.com/resource/logo.svg", "UVP Control Logo");
        img.setHeight("70px");
        this.setDrawerOpened(false);


        menu = createMenuTabs();
        VerticalLayout verticalLayout = new VerticalLayout(img);
        verticalLayout.setDefaultHorizontalComponentAlignment(FlexComponent.Alignment.START);
        HorizontalLayout header = new HorizontalLayout(verticalLayout);
        header.setDefaultVerticalComponentAlignment(
                FlexComponent.Alignment.CENTER);
        this.addToNavbar(header);
        this.addToNavbar(true, menu);

        getElement().addEventListener("search-focus", e -> {
            getElement().getClassList().add("hide-navbar");
        });

        getElement().addEventListener("search-blur", e -> {
            getElement().getClassList().remove("hide-navbar");
        });
    }

    @Override
    protected void afterNavigation() {
        super.afterNavigation();
        RouteConfiguration configuration = RouteConfiguration.forSessionScope();
        if (configuration.isRouteRegistered(this.getContent().getClass())) {
            String target = configuration.getUrl(this.getContent().getClass());
            Optional<Component> tabToSelect = menu.getChildren().filter(tab -> {
                Component child = tab.getChildren().findFirst().get();
                return child instanceof RouterLink && ((RouterLink) child).getHref().equals(target);
            }).findFirst();
            tabToSelect.ifPresent(tab -> menu.setSelectedTab((Tab) tab));
        } else {
            menu.setSelectedTab(null);
        }
    }

    private static Tabs createMenuTabs() {
        final Tabs tabs = new Tabs();
        tabs.setOrientation(Tabs.Orientation.HORIZONTAL);
        tabs.add(getAvailableTabs());
        return tabs;
    }

    private static Tab[] getAvailableTabs() {
        final List<Tab> tabs = new ArrayList<>(5);
        tabs.add(createTab(VaadinIcon.PLAY_CIRCLE, "Станції", StationsView.class));
        tabs.add(createTab(VaadinIcon.LAYOUT, "Шар", LayerVideoView.class));
        tabs.add(createTab(VaadinIcon.FOLDER, "Контент", ContentView.class));
        tabs.add(createTab(VaadinIcon.LIST_SELECT, "Ефір", EtherView.class));
        tabs.add(createTab(VaadinIcon.TABLE, "Звіт", ReportView.class));

        if (SecurityUtils.isAccessGranted(UsersView.class)){
            tabs.add(createTab(VaadinIcon.USER, "Користувачі", UsersView.class));
        }

        final String contextPath = VaadinServlet.getCurrent().getServletContext().getContextPath();
        final Tab logoutTab = createTab(createLogoutLink(contextPath));
        tabs.add(logoutTab);
        return tabs.toArray(new Tab[tabs.size()]);
    }

    private static Tab createTab(VaadinIcon icon, String title, Class<? extends Component> viewClass) {
        return createTab(populateLink(new RouterLink(null, viewClass), icon, title));
    }

    private static Tab createTab(Component content) {
        final Tab tab = new Tab();
        tab.addThemeVariants(TabVariant.LUMO_ICON_ON_TOP);
        tab.add(content);
        return tab;
    }

    private static Anchor createLogoutLink(String contextPath) {
        final Anchor a = populateLink(new Anchor(), VaadinIcon.SIGN_OUT, "Вийти");
        a.setHref(contextPath + "/logout");
        return a;
    }

    private static <T extends HasComponents> T populateLink(T a, VaadinIcon icon, String title) {
        a.add(icon.create());
        a.add(title);
        return a;
    }
}

/*
@CssImport("./styles/shared-styles.css")
public class MainLayout extends AppLayout {//AppLayout это макет Vaadin с заголовком и отзывчивым ящиком.
    public MainLayout(){
        createHeader();
        createDrawer();
    }

    private void createHeader(){

        Image img = new Image("img/logo.png", "II Control Logo");
        img.setHeight("74px");
        H1 logo = new H1("II Control");
        logo.addClassName("logo");
        //Создает новый Anchor( <a>тег), на который ссылается /logout.
        Anchor logout = new Anchor("logout", "Выход");

        //DrawerToggle - это кнопка меню, которая переключает видимость боковой панели.
        HorizontalLayout header = new HorizontalLayout(new DrawerToggle(), img, logo, logout);
        //Призывает header.expand(logo)к тому, чтобы логотип занимал все лишнее место в макете.
        //Это сдвигает кнопку выхода в крайнее правое положение.
        header.expand(logo);
        header.setDefaultVerticalComponentAlignment(
                FlexComponent.Alignment.CENTER);//Центрирует компоненты header по вертикальной оси.
        header.setWidth("100%");
        header.addClassName("header");

        addToNavbar(header);//Добавляет header макет на панель навигации макета приложения.
    }

    private void createDrawer(){
        //Создает RouterLink с текстом «Players» и PlayerView.class в качестве целевого представления
        Icon iconPlayer = VaadinIcon.START_COG.create();
        Icon iconSetting = VaadinIcon.OPTIONS.create();


        RouterLink listLink = new RouterLink(" Плееры", PlayerView.class);


        //Устанавливает setHighlightCondition (HighlightConditions.sameLocation ()),
        // чтобы не выделять ссылку для частичных совпадений маршрута.
        // (Технически каждый маршрут начинается с пустого маршрута,
        // поэтому без этого он всегда будет отображаться как активный,
        // даже если пользователь не находится в представлении)
        listLink.setHighlightCondition(HighlightConditions.sameLocation());

        //Переносит ссылку в VerticalLayout и добавляет ее в AppLayout ящик
        addToDrawer(new HorizontalLayout(iconPlayer, listLink),
                new VerticalLayout(new HorizontalLayout(iconSetting)));
    }

 */

