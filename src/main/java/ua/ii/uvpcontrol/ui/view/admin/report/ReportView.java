package ua.ii.uvpcontrol.ui.view.admin.report;

import ar.com.fdvs.dj.domain.*;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.constants.*;
import ar.com.fdvs.dj.domain.entities.columns.PropertyColumn;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.hierarchy.AbstractBackEndHierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalQuery;
import com.vaadin.flow.function.SerializableSupplier;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.StreamResource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.vaadin.reports.PrintPreviewReport;
import ua.ii.uvpcontrol.app.HasLogger;
import ua.ii.uvpcontrol.backend.data.station.Station;
import ua.ii.uvpcontrol.backend.data.users.Role;
import ua.ii.uvpcontrol.backend.data.users.User;
import ua.ii.uvpcontrol.backend.service.UserService;
import ua.ii.uvpcontrol.backend.service.station.StationService;
import ua.ii.uvpcontrol.ui.MainView;
import ua.ii.uvpcontrol.ui.component.RecursiveSelectTreeGrid;

import java.io.*;
import java.time.LocalDate;
import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

@Route(value = "report", layout = MainView.class)
@PageTitle("Звіт")
@Secured({Role.ADMIN, Role.MANAGER})
public class ReportView extends VerticalLayout implements HasLogger {
    private RecursiveSelectTreeGrid<Station> stationTreeGrid;

    private final ComboBox<String> reportTemplateComboBox = new ComboBox<>("Шаблон");
    private TextField nameFileTextField = new TextField("Назва ролика");
    private TextField nameAdvertiserTextField = new TextField("Рекламодавець");
    private TextField nameRKTextField = new TextField("Назва РК");
    private DatePicker startDatePicker = new DatePicker("Початок РК");
    private StationService stationService;
    private User currentUser;
    private Anchor pdf = new Anchor();
    private String STORAGE;

    public ReportView(StationService stationService, UserService userService, @Value("${storage}") String STORAGE){
        this.STORAGE = STORAGE;
        this.stationService = stationService;

        setSizeFull();

        reportTemplateComboBox.setItems("АЗК OKKO", "АЗК WOG", "ТРЦ SkyMall","ТРЦ Ekvator", "ТРЦ Gulliver", "ТРЦ Rayon", "ТРЦ Prospekt",
                "ТРЦ Riviera", "ТРЦ ZlataPlaza", "ТРЦ Bulvar", "visa tv", "Епіцентр К", "Нова Лінія");

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();

        currentUser = userService.findByEmailIgnoreCase(username);

        nameFileTextField.setWidth("420px");
        nameAdvertiserTextField.setWidth("200px");
        nameRKTextField.setWidth("200px");

        reportTemplateComboBox.setWidth("200px");
        startDatePicker.setWidth("200px");

        Tab reportTab = new Tab(new Icon(VaadinIcon.FILE_TABLE),new RouterLink("Звіт про вихід",
                ReportView.class));
        Tab reportEtherTab = new Tab(new Icon(VaadinIcon.TABLE),new RouterLink("Ефірна справка",
                ReportEtherView.class));
        Tabs mainTabs = new Tabs(reportTab, reportEtherTab);

        mainTabs.setSelectedTab(reportTab);

        createGridStation();

        nameFileTextField.setRequired(true);
        nameFileTextField.setErrorMessage("Введіть назву ролика!");
        nameFileTextField.setInvalid(true);

        reportTemplateComboBox.setRequired(true);
        reportTemplateComboBox.setErrorMessage("Виберіть шаблон!");
        reportTemplateComboBox.setInvalid(true);

        startDatePicker.setRequired(true);
        startDatePicker.setErrorMessage("Виберіть початкову дату!");
        startDatePicker.setInvalid(true);


        Button button = new Button("Отримати звіт");

        button.addClickListener(buttonClickEvent -> {

            if (stationTreeGrid.asMultiSelect().getValue().size() > 0) {
                if(!nameFileTextField.isEmpty()) {
                    if (!startDatePicker.isEmpty()) {

                        if (reportTemplateComboBox.getValue().toLowerCase().contains("wog")) {
                            List<ReportWog> reportWogs = stationSearchAZK();
                            reportWogs.sort(Comparator.comparing(ReportWog::getAddress));
                            reportWogs.forEach(reportWog -> reportWog.setCount(reportWogs.indexOf(reportWog) + 1));
                            SerializableSupplier<List<? extends ReportWog>> itemsSupplier = () -> reportWogs;
                            StreamResource streamResource = generateAZKWOGReport(reportWogs).getStreamResource(
                                    reportTemplateComboBox.getValue() + "_" + startDatePicker.getValue() + "." + PrintPreviewReport.Format.XLS.name().toLowerCase(), itemsSupplier, PrintPreviewReport.Format.XLS);

                            pdf.setHref(streamResource);
                            pdf.getElement().setAttribute("download", true);

                            com.vaadin.flow.component.page.Page page = UI.getCurrent().getPage();
                            page.executeJs("document.getElementById('" + "reportDownload" + "').click();");


                        } else if (reportTemplateComboBox.getValue().toLowerCase().contains("okko")) {
                            List<ReportWog> reportWogs = stationSearchAZK();
                            reportWogs.sort(Comparator.comparing(ReportWog::getAddress));
                            reportWogs.forEach(reportWog -> reportWog.setCount(reportWogs.indexOf(reportWog) + 1));
                            SerializableSupplier<List<? extends ReportWog>> itemsSupplier = () -> reportWogs;
                            StreamResource streamResource = generateAZKOKKOReport(reportWogs).getStreamResource(
                                    reportTemplateComboBox.getValue() + "_" + startDatePicker.getValue() + "." + PrintPreviewReport.Format.XLS.name().toLowerCase(), itemsSupplier, PrintPreviewReport.Format.XLS);

                            pdf.setHref(streamResource);
                            pdf.getElement().setAttribute("download", true);

                            com.vaadin.flow.component.page.Page page = UI.getCurrent().getPage();
                            page.executeJs("document.getElementById('" + "reportDownload" + "').click();");
                        } else if (reportTemplateComboBox.getValue().toLowerCase().contains("трц")) {
                            List<ReportTrc> reportTrcs = stationSearchTRC();
                            reportTrcs.sort(Comparator.comparing(ReportTrc::getAddress));
                            reportTrcs.forEach(reportTrc -> reportTrc.setCount(reportTrcs.indexOf(reportTrc) + 1));
                            SerializableSupplier<List<? extends ReportTrc>> itemsSupplier = () -> reportTrcs;
                            StreamResource streamResource = generateTRCReport(reportTrcs).getStreamResource(
                                    reportTemplateComboBox.getValue() + "_" + startDatePicker.getValue() + "." + PrintPreviewReport.Format.XLS.name().toLowerCase(), itemsSupplier, PrintPreviewReport.Format.XLS);

                            pdf.setHref(streamResource);
                            pdf.getElement().setAttribute("download", true);

                            com.vaadin.flow.component.page.Page page = UI.getCurrent().getPage();
                            page.executeJs("document.getElementById('" + "reportDownload" + "').click();");
                        }else if (reportTemplateComboBox.getValue().toLowerCase().contains("visa tv")) {
                            List<ReportTrc> reportTrcs = stationSearchTRC();
                            reportTrcs.sort(Comparator.comparing(ReportTrc::getAddress));
                            reportTrcs.forEach(reportTrc -> reportTrc.setCount(reportTrcs.indexOf(reportTrc) + 1));
                            SerializableSupplier<List<? extends ReportTrc>> itemsSupplier = () -> reportTrcs;
                            StreamResource streamResource = generateVisaTVReport(reportTrcs).getStreamResource(
                                    reportTemplateComboBox.getValue() + "_" + startDatePicker.getValue() + "." + PrintPreviewReport.Format.XLS.name().toLowerCase(), itemsSupplier, PrintPreviewReport.Format.XLS);

                            pdf.setHref(streamResource);
                            pdf.getElement().setAttribute("download", true);

                            com.vaadin.flow.component.page.Page page = UI.getCurrent().getPage();
                            page.executeJs("document.getElementById('" + "reportDownload" + "').click();");
                        } else if (reportTemplateComboBox.getValue().toLowerCase().contains("епіцентр к") | reportTemplateComboBox.getValue().toLowerCase().contains("нова лінія")) {
                        List<ReportTC> reportTcs = stationSearchTC();
                        reportTcs.sort(Comparator.comparing(ReportTC::getAddress));
                        reportTcs.forEach(reportTC -> reportTC.setCount(reportTcs.indexOf(reportTC) + 1));
                        SerializableSupplier<List<? extends ReportTC>> itemsSupplier = () -> reportTcs;
                        StreamResource streamResource = generateTCReport(reportTcs).getStreamResource(
                                reportTemplateComboBox.getValue() + "_" + startDatePicker.getValue() + "." + PrintPreviewReport.Format.XLS.name().toLowerCase(), itemsSupplier, PrintPreviewReport.Format.XLS);

                        pdf.setHref(streamResource);
                        pdf.getElement().setAttribute("download", true);

                        com.vaadin.flow.component.page.Page page = UI.getCurrent().getPage();
                        page.executeJs("document.getElementById('" + "reportDownload" + "').click();");
                        }
                    } else {
                        Notification notification = new Notification(
                                "Виберіть початкову дату!");
                        notification.setPosition(Notification.Position.MIDDLE);
                        notification.setDuration(5000);
                        notification.open();
                    }
                } else {
                    Notification notification = new Notification(
                            "Введіть назву ролика!");
                    notification.setPosition(Notification.Position.MIDDLE);
                    notification.setDuration(5000);
                    notification.open();
                }
            } else {
                Notification notification = new Notification(
                        "Виберіть стацію!");
                notification.setPosition(Notification.Position.MIDDLE);
                notification.setDuration(5000);
                notification.open();
            }
        });
        pdf.setId("reportDownload");
        VerticalLayout verticalLayout = new VerticalLayout(nameFileTextField, new HorizontalLayout(reportTemplateComboBox, startDatePicker),
                new HorizontalLayout(nameAdvertiserTextField, nameRKTextField), new HorizontalLayout(button, pdf));
        verticalLayout.setWidth("50%");
        verticalLayout.setHeight("100%");
        HorizontalLayout horizontalLayout = new HorizontalLayout(stationTreeGrid , verticalLayout);
        horizontalLayout.setSizeFull();
        add(mainTabs, horizontalLayout);
    }

    private PrintPreviewReport<ReportWog> generateAZKWOGReport(List<ReportWog> reportWogs){
        PrintPreviewReport<ReportWog> report = new PrintPreviewReport<>();
        reportWogs.sort(Comparator.comparing(ReportWog::getAddress));
        reportWogs.forEach(reportWog -> reportWog.setCount(reportWogs.indexOf(reportWog) + 1));
        Style styleHeader = new Style();
        styleHeader.setFont(styleHeader.getFont());
        styleHeader.getFont().setBold(true);

        Style styleText = new Style();
        styleText.setBorderBottom(Border.THIN());
        styleText.setBorderLeft(Border.THIN());
        styleText.setBorderRight(Border.THIN());
        styleText.setBorderTop(Border.THIN());
        styleText.getFont().setBold(false);

        Style styleC = new Style();
        styleC.setBorderBottom(Border.THIN());
        styleC.setBorderLeft(Border.THIN());
        styleC.setBorderRight(Border.THIN());
        styleC.setBorderTop(Border.THIN());
        styleC.setHorizontalAlign(HorizontalAlign.CENTER);
        styleC.getFont().setBold(false);

        Style style = new Style();
        style.setFont(style.getFont());
        style.getFont().setFontSize(11);
        style.setHorizontalAlign(HorizontalAlign.LEFT);
        style.setVerticalAlign(VerticalAlign.TOP);
        style.setHorizontalAlign(HorizontalAlign.LEFT);

        String rk = nameRKTextField.getValue().replace("\"", "&quot;");
        String advertiser = nameAdvertiserTextField.getValue().replace("\"", "&quot;");


        report.getReportBuilder()
                .setMargins(10, 20, 2, 2)
                .addAutoText("Підсумки планового оновлення ефіру мережі " + reportTemplateComboBox.getValue() + " станом на", AutoText.POSITION_HEADER, AutoText.ALIGMENT_LEFT, 390, styleHeader)
                .addAutoText(String.valueOf(startDatePicker.getValue()), AutoText.POSITION_HEADER, AutoText.ALIGNMENT_RIGHT, 100, styleHeader)
                .addAutoText("Рекламодавець: " + advertiser, AutoText.POSITION_HEADER, AutoText.ALIGMENT_LEFT, 200, styleHeader)
                .addAutoText("Назва РК: " + rk, AutoText.POSITION_HEADER, AutoText.ALIGNMENT_RIGHT, 200, styleHeader)
                .addColumn(ColumnBuilder.getNew()
                        .setColumnProperty("count", Integer.class)
                        .setTitle("№")
                        .setStyle(styleText).setWidth(10)
                        .build())
                .addColumn(ColumnBuilder.getNew()
                        .setColumnProperty("address", String.class)
                        .setTitle("Адреса")
                        .setStyle(styleText).setWidth(150)
                        .build())
                .addColumn(ColumnBuilder.getNew()
                        .setColumnProperty("tv", String.class)
                        .setTitle("TV")
                        .setStyle(styleC).setWidth(60)
                        .build())
                .addColumn(ColumnBuilder.getNew()
                        .setColumnProperty("kava", String.class)
                        .setTitle("KAVA")
                        .setStyle(styleC).setWidth(60)
                        .build())
                .addAutoText("", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150)
                .addAutoText(currentUser.getName(), AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style)
                .addAutoText("", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style)
                .addAutoText("", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style)
                .addAutoText("", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style)
                .addAutoText("Смоланов Д.С.", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style);

        report.getReportBuilder().setTemplateFile(STORAGE + "report/azk/azk.jrxml");
        report.getReportBuilder().setIgnorePagination(true);
        report.getReportBuilder().setReportName(reportTemplateComboBox.getValue());


        report.setItems(reportWogs);
        report.getReportBuilder().getColumn(0);

        return report;

    }
    private PrintPreviewReport<ReportWog> generateAZKOKKOReport(List<ReportWog> reportWogs){

        PrintPreviewReport<ReportWog> report = new PrintPreviewReport<>();
        reportWogs.sort(Comparator.comparing(ReportWog::getAddress));
        reportWogs.forEach(reportWog -> reportWog.setCount(reportWogs.indexOf(reportWog) + 1));
        Style styleHeader = new Style();
        styleHeader.setFont(styleHeader.getFont());
        styleHeader.getFont().setBold(true);

        Style styleText = new Style();
        styleText.setBorderBottom(Border.THIN());
        styleText.setBorderLeft(Border.THIN());
        styleText.setBorderRight(Border.THIN());
        styleText.setBorderTop(Border.THIN());
        styleText.getFont().setBold(false);

        Style styleC = new Style();
        styleC.setBorderBottom(Border.THIN());
        styleC.setBorderLeft(Border.THIN());
        styleC.setBorderRight(Border.THIN());
        styleC.setBorderTop(Border.THIN());
        styleC.setHorizontalAlign(HorizontalAlign.CENTER);
        styleC.getFont().setBold(false);

        Style style = new Style();
        style.setFont(style.getFont());
        style.getFont().setFontSize(11);
        style.setHorizontalAlign(HorizontalAlign.LEFT);
        style.setVerticalAlign(VerticalAlign.TOP);
        style.setHorizontalAlign(HorizontalAlign.LEFT);

        String rk = nameRKTextField.getValue().replace("\"", "&quot;");
        String advertiser = nameAdvertiserTextField.getValue().replace("\"", "&quot;");


        report.getReportBuilder()
                .setMargins(10, 20, 2, 2)
                .addAutoText("Підсумки планового оновлення ефіру мережі " + reportTemplateComboBox.getValue() + " станом на", AutoText.POSITION_HEADER, AutoText.ALIGMENT_LEFT, 390, styleHeader)
                .addAutoText(String.valueOf(startDatePicker.getValue()), AutoText.POSITION_HEADER, AutoText.ALIGNMENT_RIGHT, 100, styleHeader)
                .addAutoText("Рекламодавець: " + advertiser, AutoText.POSITION_HEADER, AutoText.ALIGMENT_LEFT, 200, styleHeader)
                .addAutoText("Назва РК: " + rk, AutoText.POSITION_HEADER, AutoText.ALIGNMENT_RIGHT, 200, styleHeader)
                .addColumn(ColumnBuilder.getNew()
                        .setColumnProperty("count", Integer.class)
                        .setTitle("№")
                        .setStyle(styleText).setWidth(10)
                        .build())
                .addColumn(ColumnBuilder.getNew()
                        .setColumnProperty("address", String.class)
                        .setTitle("Адреса")
                        .setStyle(styleText).setWidth(150)
                        .build())
                .addColumn(ColumnBuilder.getNew()
                        .setColumnProperty("tv", String.class)
                        .setTitle("INFO")
                        .setStyle(styleC).setWidth(60)
                        .build())
                .addColumn(ColumnBuilder.getNew()
                        .setColumnProperty("kava", String.class)
                        .setTitle("KAVA")
                        .setStyle(styleC).setWidth(60)
                        .build())
                .addAutoText("", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150)
                .addAutoText(currentUser.getName(), AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style)
                .addAutoText("", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style)
                .addAutoText("", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style)
                .addAutoText("", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style)
                .addAutoText("Смоланов Д.С.", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style);

        report.getReportBuilder().setTemplateFile(STORAGE + "report/azk/azk.jrxml");
        report.getReportBuilder().setIgnorePagination(true);
        report.getReportBuilder().setReportName(reportTemplateComboBox.getValue());
        

        report.setItems(reportWogs);
        report.getReportBuilder().getColumn(0);

        return report;

    }
    private PrintPreviewReport<ReportTrc> generateTRCReport(List<ReportTrc> reportTrcs){
        PrintPreviewReport<ReportTrc> report = new PrintPreviewReport<>();
        reportTrcs.sort(Comparator.comparing(ReportTrc::getAddress));
        reportTrcs.forEach(reportTrc -> reportTrc.setCount(reportTrcs.indexOf(reportTrc) + 1));
        Style styleHeader = new Style();
        styleHeader.setFont(styleHeader.getFont());
        styleHeader.getFont().setBold(true);

        Style styleText = new Style();
        styleText.setBorderBottom(Border.THIN());
        styleText.setBorderLeft(Border.THIN());
        styleText.setBorderRight(Border.THIN());
        styleText.setBorderTop(Border.THIN());
        styleText.getFont().setBold(false);

        Style styleC = new Style();
        styleC.setBorderBottom(Border.THIN());
        styleC.setBorderLeft(Border.THIN());
        styleC.setBorderRight(Border.THIN());
        styleC.setBorderTop(Border.THIN());
        styleC.setHorizontalAlign(HorizontalAlign.CENTER);
        styleC.getFont().setBold(false);

        Style style = new Style();
        style.setFont(style.getFont());
        style.getFont().setFontSize(11);
        style.setHorizontalAlign(HorizontalAlign.LEFT);
        style.setVerticalAlign(VerticalAlign.TOP);
        style.setHorizontalAlign(HorizontalAlign.LEFT);
        String rk = nameRKTextField.getValue().replace("\"", "&quot;");
        String advertiser = nameAdvertiserTextField.getValue().replace("\"", "&quot;");

        report.getReportBuilder()
                .setMargins(10, 20, 2, 2)
                .addAutoText("Підсумки планового оновлення ефіру мережі " + reportTemplateComboBox.getValue() + " станом на", AutoText.POSITION_HEADER, AutoText.ALIGMENT_LEFT, 390, styleHeader)
                .addAutoText(String.valueOf(startDatePicker.getValue()), AutoText.POSITION_HEADER, AutoText.ALIGNMENT_RIGHT, 100, styleHeader)
                .addAutoText("Рекламодавець: " + advertiser, AutoText.POSITION_HEADER, AutoText.ALIGMENT_LEFT, 200, styleHeader)
                .addAutoText("Назва РК: " + rk, AutoText.POSITION_HEADER, AutoText.ALIGNMENT_RIGHT, 200, styleHeader)
                .addColumn(ColumnBuilder.getNew()
                        .setColumnProperty("count", Integer.class)
                        .setTitle("№")
                        .setStyle(styleText).setWidth(10)
                        .build())
                .addColumn(ColumnBuilder.getNew()
                        .setColumnProperty("address", String.class)
                        .setTitle("Адреса")
                        .setStyle(styleText).setWidth(150)
                        .build())
                .addColumn(ColumnBuilder.getNew()
                        .setColumnProperty("tv", String.class)
                        .setTitle("Час оновлення")
                        .setStyle(styleC).setWidth(60)
                        .build())
                .addAutoText("", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150)
                .addAutoText(currentUser.getName(), AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style)
                .addAutoText("", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style)
                .addAutoText("", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style)
                .addAutoText("", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style)
                .addAutoText("Смоланов Д.С.", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style);

        report.getReportBuilder().setTemplateFile(STORAGE + "report/trc/trc.jrxml");
        report.getReportBuilder().setIgnorePagination(true);
        report.getReportBuilder().setReportName(reportTemplateComboBox.getValue());


        report.setItems(reportTrcs);
        report.getReportBuilder().getColumn(0);

        return report;

    }

    private PrintPreviewReport<ReportTrc> generateVisaTVReport(List<ReportTrc> reportTrcs){
        PrintPreviewReport<ReportTrc> report = new PrintPreviewReport<>();
        reportTrcs.sort(Comparator.comparing(ReportTrc::getAddress));
        reportTrcs.forEach(reportTrc -> reportTrc.setCount(reportTrcs.indexOf(reportTrc) + 1));
        Style styleHeader = new Style();
        styleHeader.setFont(styleHeader.getFont());
        styleHeader.getFont().setBold(true);

        Style styleText = new Style();
        styleText.setBorderBottom(Border.THIN());
        styleText.setBorderLeft(Border.THIN());
        styleText.setBorderRight(Border.THIN());
        styleText.setBorderTop(Border.THIN());
        styleText.getFont().setBold(false);

        Style styleC = new Style();
        styleC.setBorderBottom(Border.THIN());
        styleC.setBorderLeft(Border.THIN());
        styleC.setBorderRight(Border.THIN());
        styleC.setBorderTop(Border.THIN());
        styleC.setHorizontalAlign(HorizontalAlign.CENTER);
        styleC.getFont().setBold(false);

        Style style = new Style();
        style.setFont(style.getFont());
        style.getFont().setFontSize(11);
        style.setHorizontalAlign(HorizontalAlign.LEFT);
        style.setVerticalAlign(VerticalAlign.TOP);
        style.setHorizontalAlign(HorizontalAlign.LEFT);
        String rk = nameRKTextField.getValue().replace("\"", "&quot;");
        String advertiser = nameAdvertiserTextField.getValue().replace("\"", "&quot;");

        report.getReportBuilder()
                .setMargins(10, 20, 2, 2)
                .addAutoText("Підсумки планового оновлення ефіру мережі " + reportTemplateComboBox.getValue() + " станом на", AutoText.POSITION_HEADER, AutoText.ALIGMENT_LEFT, 390, styleHeader)
                .addAutoText(String.valueOf(startDatePicker.getValue()), AutoText.POSITION_HEADER, AutoText.ALIGNMENT_RIGHT, 100, styleHeader)
                .addAutoText("Рекламодавець: " + advertiser, AutoText.POSITION_HEADER, AutoText.ALIGMENT_LEFT, 200, styleHeader)
                .addAutoText("Назва РК: " + rk, AutoText.POSITION_HEADER, AutoText.ALIGNMENT_RIGHT, 200, styleHeader)
                .addColumn(ColumnBuilder.getNew()
                        .setColumnProperty("count", Integer.class)
                        .setTitle("№")
                        .setStyle(styleText).setWidth(10)
                        .build())
                .addColumn(ColumnBuilder.getNew()
                        .setColumnProperty("address", String.class)
                        .setTitle("Адреса")
                        .setStyle(styleText).setWidth(150)
                        .build())
                .addColumn(ColumnBuilder.getNew()
                        .setColumnProperty("tv", String.class)
                        .setTitle("Час оновлення")
                        .setStyle(styleC).setWidth(60)
                        .build())
                .addAutoText("", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150)
                .addAutoText(currentUser.getName(), AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style)
                .addAutoText("", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style)
                .addAutoText("", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style)
                .addAutoText("", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style)
                .addAutoText("Смоланов Д.С.", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style);

        report.getReportBuilder().setTemplateFile(STORAGE + "report/visatv/visatv.jrxml");
        report.getReportBuilder().setIgnorePagination(true);
        report.getReportBuilder().setReportName(reportTemplateComboBox.getValue());


        report.setItems(reportTrcs);
        report.getReportBuilder().getColumn(0);

        return report;

    }
    private PrintPreviewReport<ReportTC> generateTCReport(List<ReportTC> reportTCS){
        PrintPreviewReport<ReportTC> report = new PrintPreviewReport<>();
        reportTCS.sort(Comparator.comparing(ReportTC::getAddress));
        reportTCS.forEach(reportTC -> reportTC.setCount(reportTCS.indexOf(reportTC) + 1));
        Style styleHeader = new Style();
        styleHeader.setFont(styleHeader.getFont());
        styleHeader.getFont().setBold(true);

        Style styleText = new Style();
        styleText.setBorderBottom(Border.THIN());
        styleText.setBorderLeft(Border.THIN());
        styleText.setBorderRight(Border.THIN());
        styleText.setBorderTop(Border.THIN());
        styleText.getFont().setBold(false);

        Style styleC = new Style();
        styleC.setBorderBottom(Border.THIN());
        styleC.setBorderLeft(Border.THIN());
        styleC.setBorderRight(Border.THIN());
        styleC.setBorderTop(Border.THIN());
        styleC.setHorizontalAlign(HorizontalAlign.CENTER);
        styleC.getFont().setBold(false);

        Style style = new Style();
        style.setFont(style.getFont());
        style.getFont().setFontSize(11);
        style.setHorizontalAlign(HorizontalAlign.LEFT);
        style.setVerticalAlign(VerticalAlign.TOP);
        style.setHorizontalAlign(HorizontalAlign.LEFT);
        String rk = nameRKTextField.getValue().replace("\"", "&quot;");
        String advertiser = nameAdvertiserTextField.getValue().replace("\"", "&quot;");
        report.getReportBuilder()
                .setMargins(10, 10, 2, 2)
                .addAutoText("Підсумки планового оновлення ефіру в ТЦ мережі ТОВ " + "&quot;" + reportTemplateComboBox.getValue() + "&quot;" + " станом на", AutoText.POSITION_HEADER, AutoText.ALIGMENT_LEFT, 390, styleHeader)
                .addAutoText(String.valueOf(startDatePicker.getValue()), AutoText.POSITION_HEADER, AutoText.ALIGNMENT_RIGHT, 100, styleHeader)
                .addAutoText("Рекламодавець: " + advertiser, AutoText.POSITION_HEADER, AutoText.ALIGMENT_LEFT, 200, styleHeader)
                .addAutoText("Назва РК: " + rk, AutoText.POSITION_HEADER, AutoText.ALIGNMENT_RIGHT, 200, styleHeader)
                .addColumn(ColumnBuilder.getNew()
                        .setColumnProperty("count", Integer.class)
                        .setTitle("№")
                        .setStyle(styleText).setWidth(15)
                        .build())
                .addColumn(ColumnBuilder.getNew()
                        .setColumnProperty("address", String.class)
                        .setTitle("ТЦ")
                        .setStyle(styleText).setWidth(200)
                        .build())
                .addColumn(ColumnBuilder.getNew()
                        .setColumnProperty("tv", String.class)
                        .setTitle("Система TV-Radio")
                        .setStyle(styleC).setWidth(100)
                        .build())
                .addColumn(ColumnBuilder.getNew()
                        .setColumnProperty("panel", String.class)
                        .setTitle("Система INFO")
                        .setStyle(styleC).setWidth(100)
                        .build())
                .addColumn(ColumnBuilder.getNew()
                        .setColumnProperty("vj", String.class)
                        .setTitle("Система E-Poster")
                        .setStyle(styleC).setWidth(100)
                        .build())
                .addAutoText("", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150)
                .addAutoText(currentUser.getName(), AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style)
                .addAutoText("", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style)
                .addAutoText("", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style)
                .addAutoText("", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style)
                .addAutoText("Смоланов Д.С.", AutoText.POSITION_FOOTER, AutoText.ALIGNMENT_RIGHT, 150, style);

        report.getReportBuilder().setTemplateFile(STORAGE + "report/tc/tc.jrxml");
        report.getReportBuilder().setIgnorePagination(true);
        report.getReportBuilder().setReportName(reportTemplateComboBox.getValue());

        report.setItems(reportTCS);
        report.getReportBuilder().getColumn(0);

        return report;

    }

    private List<ReportTC> stationSearchTC(){
        Map<String, String> stations = new TreeMap<>();
        stationTreeGrid.asMultiSelect().getValue().forEach(station -> {
            if (station.getStationSetting() != null){
                Station parent = station.getParent();
                if (parent != null) {
                    if (parent.getParent() != null) {
                        Station parent2 = parent.getParent();
                        String parentPath = parent.getParent().getName() + "/" + parent.getName() + "/";
                        while (true) {
                            if (parent2.getParent() != null) {
                                parentPath = parent2.getParent().getName() + "/" + parentPath;
                                parent2 = parent2.getParent();
                            } else {
                                break;
                            }
                        }
                        parent.setParent(null);
                        parent.setName(parentPath);
                        parent.setEthers(new ArrayList<>());
                        parent.setStationSetting(null);

                    }
                    parent.setEthers(new ArrayList<>());
                    parent.setStationSetting(null);
                }
                if (!station.getAddress().isEmpty()) {
                    stations.put(parent.getName() + station.getName(), station.getAddress());
                }
            }
        });
        List<ReportTC> reportTCS = new ArrayList<>();
        stations.forEach((s, s2) -> {

            AtomicInteger i = new AtomicInteger();
            reportTCS.forEach(reportTC -> {
                if (reportTC.getAddress().contains(s2)){
                    i.set(reportTCS.indexOf(reportTC));
                }
            });

            if (i.get() == 0){
                if (reportTCS.size() > 0){
                    if ((reportTCS.get(0).getAddress().contains(s2))){
                        if (s.contains("TV")) {
                            reportTCS.get(i.get()).setTvPath(s);
                            reportTCS.get(i.get()).setAddress(s2);
                        } else {
                            reportTCS.get(i.get()).setVjPath(s);
                            reportTCS.get(i.get()).setAddress(s2);
                        }
                    } else {
                        ReportTC reportTC = new ReportTC();
                        if (s.contains("TV")){
                            reportTC.setTvPath(s);
                            reportTC.setAddress(s2);
                        } else {
                            reportTC.setVjPath(s);
                            reportTC.setAddress(s2);
                        }
                        reportTCS.add(reportTC);
                    }
                } else {
                    ReportTC reportTC = new ReportTC();
                    if (s.contains("TV")){
                        reportTC.setTvPath(s);
                        reportTC.setAddress(s2);
                    } else {
                        reportTC.setVjPath(s);
                        reportTC.setAddress(s2);
                    }
                    reportTCS.add(reportTC);
                }

            } else {
                if (s.contains("TV")){
                    reportTCS.get(i.get()).setTvPath(s);
                    reportTCS.get(i.get()).setAddress(s2);
                } else {
                    reportTCS.get(i.get()).setVjPath(s);
                    reportTCS.get(i.get()).setAddress(s2);
                }
            }
        });

        reportTCS.forEach(reportTC -> {
            if (reportTC.getTvPath() == null || reportTC.getTvPath().isEmpty()){
                reportTC.setTv("Система відсутня");
            } else {
                String date = "";
                for(int i = 0; i < 5; i++){
                    LocalDate localDate = startDatePicker.getValue().plusDays(i);
                    date = dateShow(STORAGE + "logs/" + reportTC.getTvPath() + "/log_" + localDate + ".txt");

                    if (!date.isEmpty()){
                        break;
                    }
                }

                if (date.isEmpty()){
                    reportTC.setTv("Не оновлено");
                } else {
                    reportTC.setTv(date);
                }

                date = "";
                for(int i = 0; i < 5; i++){
                    LocalDate localDate = startDatePicker.getValue().plusDays(i);
                    date = dateShowPanel(STORAGE + "logs/" + reportTC.getTvPath() + "/log_" + localDate + ".txt");

                    if (!date.isEmpty()){
                        break;
                    }
                }

                if (date.isEmpty()){
                    reportTC.setPanel("Не оновлено");
                } else {
                    reportTC.setPanel(date);
                }

            }
            if (reportTC.getVjPath() == null || reportTC.getVjPath().isEmpty()) {
                reportTC.setVj("Система відсутня");
            } else {
                String date = "";
                for(int i = 0; i < 5; i++){
                    LocalDate localDate = startDatePicker.getValue().plusDays(i);
                    date = dateShow(STORAGE + "logs/" + reportTC.getVjPath() + "/log_" + localDate + ".txt");

                    if (!date.isEmpty()){
                        break;
                    }
                }
                if (date.isEmpty()){
                    reportTC.setVj("Не оновлено");
                } else {
                    reportTC.setVj(date);
                }

            }
        });

        return reportTCS;
    }

    private List<ReportWog> stationSearchAZK(){
        Map<String, String> stations = new TreeMap<>();
        stationTreeGrid.asMultiSelect().getValue().forEach(station -> {
            if (station.getStationSetting() != null){
                Station parent = station.getParent();
                if (parent != null) {
                    if (parent.getParent() != null) {
                        Station parent2 = parent.getParent();
                        String parentPath = parent.getParent().getName() + "/" + parent.getName() + "/";
                        while (true) {
                            if (parent2.getParent() != null) {
                                parentPath = parent2.getParent().getName() + "/" + parentPath;
                                parent2 = parent2.getParent();
                            } else {
                                break;
                            }
                        }
                        parent.setParent(null);
                        parent.setName(parentPath);
                        parent.setEthers(new ArrayList<>());
                        parent.setStationSetting(null);

                    }
                    parent.setEthers(new ArrayList<>());
                    parent.setStationSetting(null);
                }
                if (!station.getAddress().isEmpty()) {
                    stations.put(parent.getName() + station.getName(), station.getAddress());
                }
            }
        });
        List<ReportWog> reporOKKO = new ArrayList<>();
        stations.forEach((s, s2) -> {

            AtomicInteger i = new AtomicInteger();
            reporOKKO.forEach(reportWOG1 -> {
                if (reportWOG1.getAddress().contains(s2)){
                    i.set(reporOKKO.indexOf(reportWOG1));
                }
            });

            if (i.get() == 0){
                ReportWog reportWOG = new ReportWog();
                if (s.contains("KAVA")){
                    reportWOG.setKavaPath(s);
                    reportWOG.setAddress(s2);
                } else {
                    reportWOG.setTvPath(s);
                    reportWOG.setAddress(s2);
                }
                reporOKKO.add(reportWOG);
            } else {
                if (s.contains("KAVA")){
                    reporOKKO.get(i.get()).setKavaPath(s);
                } else {
                    reporOKKO.get(i.get()).setTvPath(s);
                }
            }
        });

        reporOKKO.forEach(reportWog -> {
            if (reportWog.getTvPath() == null || reportWog.getTvPath().isEmpty()){
                reportWog.setTv("Система відсутня");
            } else {
                String date = "";
                for(int i = 0; i < 5; i++){
                    LocalDate localDate = startDatePicker.getValue().plusDays(i);
                    date = dateShow(STORAGE + "logs/" + reportWog.getTvPath() + "/log_" + localDate + ".txt");

                        if (!date.isEmpty()){
                            break;
                        }
                }

                if (date.isEmpty()){
                    reportWog.setTv("Не оновлено");
                } else {
                    reportWog.setTv(date);
                }

            }
            if (reportWog.getKavaPath() == null || reportWog.getKavaPath().isEmpty()) {
                reportWog.setKava("Система відсутня");
            } else {
                String date = "";
                for(int i = 0; i < 5; i++){
                    LocalDate localDate = startDatePicker.getValue().plusDays(i);
                    date = dateShow(STORAGE + "logs/" + reportWog.getKavaPath() + "/log_" + localDate + ".txt");

                    if (!date.isEmpty()){
                            break;
                        }
                }
                    if (date.isEmpty()){
                        reportWog.setKava("Не оновлено");
                    } else {
                        reportWog.setKava(date);
                    }

            }
        });

        return reporOKKO;
    }

    private List<ReportTrc> stationSearchTRC(){
        Map<String, String> stations = new TreeMap<>();
        stationTreeGrid.asMultiSelect().getValue().forEach(station -> {
            if (station.getStationSetting() != null){
                Station parent = station.getParent();
                if (parent != null) {
                    if (parent.getParent() != null) {
                        Station parent2 = parent.getParent();
                        String parentPath = parent.getParent().getName() + "/" + parent.getName() + "/";
                        while (true) {
                            if (parent2.getParent() != null) {
                                parentPath = parent2.getParent().getName() + "/" + parentPath;
                                parent2 = parent2.getParent();
                            } else {
                                break;
                            }
                        }
                        parent.setParent(null);
                        parent.setName(parentPath);
                        parent.setEthers(new ArrayList<>());
                        parent.setStationSetting(null);

                    }
                    parent.setEthers(new ArrayList<>());
                    parent.setStationSetting(null);
                }
                if (!station.getAddress().isEmpty()) {
                    stations.put(parent.getName() + station.getName(), station.getAddress());
                }
            }
        });
        List<ReportTrc> reporTrcs = new ArrayList<>();
        stations.forEach((s, s2) -> {
            ReportTrc reportTrc = new ReportTrc();
            reportTrc.setTvPath(s);
            reportTrc.setAddress(s2);
            reporTrcs.add(reportTrc);

        });

        reporTrcs.forEach(reportWog -> {
                String date = "";
                for(int i = 0; i < 5; i++){
                    LocalDate localDate = startDatePicker.getValue().plusDays(i);
                    date = dateShow(STORAGE + "logs/" + reportWog.getTvPath() + "/log_" + localDate + ".txt");

                    if (!date.isEmpty()){
                        break;
                    }
                }

                if (date.isEmpty()){
                    reportWog.setTv("Не оновлено");
                } else {
                    reportWog.setTv(date);
                }



        });

        return reporTrcs;
    }
    private void createGridStation(){
        stationTreeGrid = new RecursiveSelectTreeGrid<>();
        stationTreeGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        stationTreeGrid.setHeight("100%");
        stationTreeGrid.setWidth("50%");
        stationTreeGrid.addHierarchyColumn(Station::getName).setHeader("Станции").setResizable(true);

        HierarchicalDataProvider<Station, Void> dataProvider =
                new AbstractBackEndHierarchicalDataProvider<>() {

                    @Override
                    public int getChildCount(HierarchicalQuery<Station, Void> query) {
                        return (int) stationService.getChildCount(query.getParent());
                    }

                    @Override
                    public boolean hasChildren(Station item) {
                        return stationService.hasChildren(item);
                    }

                    @Override
                    protected Stream<Station> fetchChildrenFromBackEnd(
                            HierarchicalQuery<Station, Void> query) {
                        return stationService.fetchChildren(query.getParent()).stream().
                                sorted(Comparator.comparing(Station::getName));
                    }
                };

        stationTreeGrid.setDataProvider(dataProvider);
    }

    private String dateShow(String filePatch){
        File file = new File(filePatch);
        BufferedReader br = null;
        String date = "";
        try {
            br = new BufferedReader(new FileReader(file));

            String st;
            while ((st = br.readLine()) != null) {
                if (st.contains("Video File:")) {
                    if (st.contains(nameFileTextField.getValue())) {
                        String[] split = st.split(" Video ");
                        date = split[0];
                        if (!date.isEmpty()) {
                            break;
                        }
                    }
                }
            }
        }catch (FileNotFoundException e) {
            getLogger().error("" + e);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            }catch (IOException e) {
                getLogger().error("" + e);
            }
        }
        return date;
    }

    private String dateShowPanel(String filePatch){
        File file = new File(filePatch);
        BufferedReader br = null;
        String date = "";
        try {
            br = new BufferedReader(new FileReader(file));

            String st;
            while ((st = br.readLine()) != null) {
                if (st.contains("Panel File:")) {
                    if (st.contains(nameFileTextField.getValue())) {
                        String[] split = st.split(" Panel ");
                        date = split[0];
                        if (!date.isEmpty()) {
                            break;
                        }
                    }
                }
            }
        }catch (FileNotFoundException e) {
            getLogger().error("" + e);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            }catch (IOException e) {
                getLogger().error("" + e);
            }
        }
        return date;
    }


}
