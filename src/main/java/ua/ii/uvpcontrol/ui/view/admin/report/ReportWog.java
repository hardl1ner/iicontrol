package ua.ii.uvpcontrol.ui.view.admin.report;

import lombok.Data;

@Data
public class ReportWog {
        private String address;
        private String tvPath;
        private String kavaPath;
        private String tv;
        private String kava;
        private Integer count;

    }