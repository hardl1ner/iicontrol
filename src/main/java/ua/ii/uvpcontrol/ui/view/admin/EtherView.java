package ua.ii.uvpcontrol.ui.view.admin;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.security.access.annotation.Secured;
import ua.ii.uvpcontrol.backend.data.users.Role;
import ua.ii.uvpcontrol.backend.service.content.EtherService;
import ua.ii.uvpcontrol.backend.service.layer.LayerVideoService;
import ua.ii.uvpcontrol.backend.service.station.StationService;
import ua.ii.uvpcontrol.ui.MainView;
import ua.ii.uvpcontrol.ui.component.content.EtherGrid;


@Route(value = "ether", layout = MainView.class)
@PageTitle("Эфир")
@Secured({Role.ADMIN, Role.MANAGER})
public class EtherView extends VerticalLayout{

    public EtherView(EtherService etherService, StationService stationService, LayerVideoService layerVideoService){
        EtherGrid etherGrid = new EtherGrid(etherService, stationService, layerVideoService);
        setSizeFull();


        add(etherGrid);
    }

}
