package ua.ii.uvpcontrol.ui.view.admin.station;

import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import org.springframework.security.access.annotation.Secured;
import ua.ii.uvpcontrol.backend.data.users.Role;
import ua.ii.uvpcontrol.backend.service.station.StationSettingService;
import ua.ii.uvpcontrol.backend.service.station.StationService;
import ua.ii.uvpcontrol.ui.MainView;
import ua.ii.uvpcontrol.ui.component.station.StationGrid;


@Route(value = "station", layout = MainView.class)
@PageTitle("Станції")
@CssImport("./styles/shared-styles.css")
@Secured({Role.ADMIN, Role.MANAGER})
public class StationsView extends VerticalLayout {

    public StationsView(StationService stationService, StationSettingService stationSettingService){

        StationGrid stationGrid = new StationGrid(stationService, stationSettingService);

        Tab stationTab = new Tab(new Icon(VaadinIcon.LIST),new RouterLink("Список станцій",
                StationsView.class));
        Tab settingTab = new Tab(new Icon(VaadinIcon.WRENCH),new RouterLink("Налаштування станцій",
                StationsSettingView.class));
        Tabs mainTabs = new Tabs(stationTab, settingTab);

        mainTabs.setSelectedTab(stationTab);

        setSizeFull();

        add(mainTabs, stationGrid);
    }

}
