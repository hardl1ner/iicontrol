package ua.ii.uvpcontrol.ui.view.login;

import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.login.LoginI18n;
import com.vaadin.flow.component.login.LoginOverlay;
import com.vaadin.flow.router.*;
import ua.ii.uvpcontrol.app.security.SecurityUtils;
import ua.ii.uvpcontrol.ui.MainView;

@Route(value = "login")
@PageTitle("Авторизація | UVP Control")
@JsModule("./styles/shared-styles.js")
public class LoginView extends LoginOverlay implements BeforeEnterObserver, AfterNavigationObserver {

    public LoginView(){
        LoginI18n i18n = LoginI18n.createDefault();
        i18n.setHeader(new LoginI18n.Header());
        i18n.getHeader().setTitle("UVP Control Авторизація");
        i18n.setAdditionalInformation(null);
        i18n.setForm(new LoginI18n.Form());
        i18n.getForm().setSubmit("Увійти");
        i18n.getForm().setTitle("Увійти");
        i18n.getForm().setUsername("Почта");
        i18n.getForm().setPassword("Пароль");
        setI18n(i18n);
        setForgotPasswordButtonVisible(false);
        setAction("login");
    }

    @Override
    public void beforeEnter(BeforeEnterEvent event){
        if (SecurityUtils.isUserLoggedIn()){
            event.forwardTo("station");
        } else {
            setOpened(true);
        }
    }

    @Override
    public void afterNavigation(AfterNavigationEvent event){
        setError(
                event.getLocation().getQueryParameters().getParameters().containsKey(
                        "error"
                )
        );
    }
}
