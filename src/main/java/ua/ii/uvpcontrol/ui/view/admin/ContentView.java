package ua.ii.uvpcontrol.ui.view.admin;

import com.amazonaws.AmazonServiceException;
import com.brownie.videojs.VideoJS;
import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.drew.metadata.avi.AviDirectory;
import com.drew.metadata.file.FileTypeDirectory;
import com.drew.metadata.jpeg.JpegDirectory;
import com.drew.metadata.mov.QuickTimeDirectory;
import com.drew.metadata.mov.media.QuickTimeVideoDirectory;
import com.drew.metadata.mp4.Mp4Directory;
import com.drew.metadata.mp4.media.Mp4VideoDirectory;
import com.drew.metadata.png.PngDirectory;
import com.flowingcode.vaadin.addons.ironicons.EditorIcons;
import com.flowingcode.vaadin.addons.ironicons.IronIcons;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.grid.GridSortOrderBuilder;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.AllFinishedEvent;
import com.vaadin.flow.component.upload.FinishedEvent;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.hierarchy.AbstractBackEndHierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalQuery;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamResource;
import org.apache.commons.io.FileUtils;
import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.mp4.MP4Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.claspina.confirmdialog.ButtonOption;
import org.claspina.confirmdialog.ConfirmDialog;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.TransactionSystemException;
import org.xml.sax.SAXException;
import ua.ii.uvpcontrol.app.HasLogger;
import ua.ii.uvpcontrol.backend.data.content.FileInfo;
import ua.ii.uvpcontrol.backend.data.content.Folder;
import ua.ii.uvpcontrol.backend.data.layer.LayerImage;
import ua.ii.uvpcontrol.backend.data.users.Role;
import ua.ii.uvpcontrol.backend.service.content.EtherService;
import ua.ii.uvpcontrol.backend.service.content.FileInfoService;
import ua.ii.uvpcontrol.backend.service.content.FolderService;
import ua.ii.uvpcontrol.backend.service.exception.UniqueDataException;
import ua.ii.uvpcontrol.backend.service.layer.LayerGroupImageService;
import ua.ii.uvpcontrol.backend.service.layer.LayerImageService;
import ua.ii.uvpcontrol.backend.service.station.StationService;
import ua.ii.uvpcontrol.ui.MainView;
import ua.ii.uvpcontrol.ui.component.Notifications;
import ua.ii.uvpcontrol.ui.component.RecursiveSelectTreeGrid;
import ua.ii.uvpcontrol.ui.component.content.UploadS3;

import java.io.*;
import java.net.URL;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Stream;

@Route(value = "content", layout = MainView.class)
@PageTitle("Контент")
@Secured({Role.ADMIN, Role.MANAGER})
public class ContentView extends VerticalLayout implements HasLogger, Notifications {
    private FolderService folderService;
    private RecursiveSelectTreeGrid<Folder> grid;
    private Grid<FileInfo> fileInfoGrid;
    private FileInfoService fileInfoService;
    private LayerImageService layerImageService;
    private EtherService etherService;
    private StationService stationService;
    private LayerGroupImageService layerGroupImageService;
    private UploadS3 upload;
    private IntegerField secondField = new IntegerField();
    private String storage;
    private String objectKey;
    private Checkbox replacementFileCBox = new Checkbox("Заменить все совпадения");

    private FileInfo fileInfo = null;
    private final TextField filterText = new TextField();

    private final HorizontalLayout horizontalLayout = new HorizontalLayout();

    public ContentView(FolderService folderService, @Value("${storage}") String storage,
                       FileInfoService fileInfoService,
                       LayerImageService layerImageService, EtherService etherService,
                       StationService stationService, LayerGroupImageService layerGroupImageService) {
        this.folderService = folderService;
        this.fileInfoService = fileInfoService;
        this.layerImageService = layerImageService;
        this.etherService = etherService;
        this.stationService = stationService;
        this.layerGroupImageService = layerGroupImageService;
        this.storage = storage;

        upload = new UploadS3();
        upload.getUpload().setMaxFiles(0);
        upload.getUpload().setAcceptedFileTypes("video/mp4", "image/jpeg", "image/png", ".avi", ".mov", ".mkv", ".xml", ".jrxml");

        uploadFile();

        setSizeFull();
        horizontalLayout.setSizeFull();
        HorizontalLayout horizontalLayout1 = new HorizontalLayout(getToolbar());
        horizontalLayout1.setDefaultVerticalComponentAlignment(Alignment.END);
        add(horizontalLayout1, horizontalLayout);
        createLazyLoadingTreeGridUsage();
    }

    private void createLazyLoadingTreeGridUsage() {
        horizontalLayout.setPadding(true);
        horizontalLayout.setSpacing(false);
        grid = new RecursiveSelectTreeGrid<>();

        grid.setSizeFull();
        grid.setWidth("600px");
        grid.addHierarchyColumn(Folder::getName).setHeader("Папка").setResizable(true).setWidth("250px");
        grid.addColumn(Folder::getFileNumber).setHeader("кол-во").setKey("numbers");
        grid.setSelectionMode(Grid.SelectionMode.MULTI);

        Button addFolderButton = new Button(IronIcons.ADD.create());
        Button delFolderButton = new Button(IronIcons.DELETE.create());
        addFolderButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        delFolderButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
        delFolderButton.setEnabled(false);

        addFolderButton.addClickListener(buttonClickEvent -> {
            if (grid.asMultiSelect().getValue().size() > 0) {
                    createAddFolderForm(folderService, true, grid.getDataProvider()).open();

            } else {
                createAddFolderForm(folderService, false, grid.getDataProvider()).open();
            }
        });
        delFolderButton.addClickListener(buttonClickEvent -> grid.asMultiSelect().getValue().forEach(folder -> {
        try {

            if (folder.getFileInfos().size() == 0) {
                folderService.delete(folder);
                grid.getDataProvider().refreshAll();
            } else {
                error("Ошибка: Не возможно удалить папку '" + folder.getName() + "'.\n" +
                                "В папке есть файлы.");
            }
        } catch (DataIntegrityViolationException dataIntegrityViolationException){
            error(
                    "Ошибка: Не возможно удалить папку '" + folder.getName() + "'.\n" +
                            "В папке есть вложенные данные.");
        }
        }));

        HeaderRow headerRow = grid.prependHeaderRow();
        headerRow.getCell(grid.getColumnByKey("numbers")).setComponent(new HorizontalLayout(
                addFolderButton, delFolderButton));

        fileInfoGrid = new Grid<>(FileInfo.class);
        fileInfoGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        fileInfoGrid.removeColumnByKey("file_type");
        fileInfoGrid.removeColumnByKey("file_size");
        fileInfoGrid.setSizeFull();
        fileInfoGrid.removeColumnByKey("id");

        fileInfoGrid.addColumn(fileInfo1 -> {
            String s = new DecimalFormat("#0.00").format((float)fileInfo1.getFile_size() / (1024 * 1024));
            return s + " MB";
        }).setKey("file_size");
        fileInfoGrid.addColumn(item -> "").setKey("rowIndex").setWidth("80px");
        fileInfoGrid.addColumn(new ComponentRenderer<>(this::createButtonsLayout))
                .setHeader("").setKey("action").setWidth("100px").setAutoWidth(true);

        fileInfoGrid.addAttachListener(event -> fileInfoGrid.getColumnByKey("rowIndex").getElement().executeJs(
                "this.renderer = function(root, column, rowData) {root.textContent = rowData.index + 1}"
        ));

        fileInfoGrid.setColumnOrder(
                fileInfoGrid.getColumnByKey("rowIndex"),
                fileInfoGrid.getColumnByKey("file_name"),
                fileInfoGrid.getColumnByKey("file_time"),
                fileInfoGrid.getColumnByKey("file_width"),
                fileInfoGrid.getColumnByKey("file_height"),
                fileInfoGrid.getColumnByKey("file_size"),
                fileInfoGrid.getColumnByKey("file_path"),
                fileInfoGrid.getColumnByKey("uploadDate"),
                fileInfoGrid.getColumnByKey("action"));
        fileInfoGrid.getColumnByKey("file_name").setHeader("Имя файла").setWidth("300px").setResizable(true).setAutoWidth(true);
        fileInfoGrid.getColumnByKey("file_time").setHeader("сек").setAutoWidth(true);
        fileInfoGrid.getColumnByKey("file_width").setHeader("Ширина").setResizable(true).setAutoWidth(true);
        fileInfoGrid.getColumnByKey("file_height").setHeader("Высота").setResizable(true).setAutoWidth(true);
        fileInfoGrid.getColumnByKey("file_size").setHeader("Размер").setWidth("200px").setResizable(true).setAutoWidth(true);
        fileInfoGrid.getColumnByKey("file_path").setHeader("Путь").setWidth("150px").setResizable(true).setAutoWidth(true);
        fileInfoGrid.getColumnByKey("uploadDate").setHeader("Дата добавления").setAutoWidth(true);


        Button delFilesButton = new Button(IronIcons.DELETE.create());
        delFilesButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
        delFilesButton.setEnabled(false);
        delFilesButton.addClickListener(click -> delFiles());
        Button uploadButton = new Button(IronIcons.CLOUD_UPLOAD.create());
        uploadButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        uploadButton.setEnabled(false);
        uploadButton.addClickListener(click -> showUploadForm().open());

        HeaderRow headerFileRow = fileInfoGrid.prependHeaderRow();
        headerFileRow.getCell(fileInfoGrid.getColumnByKey("file_name")).setComponent(filterText);
        headerFileRow.getCell(fileInfoGrid.getColumnByKey("rowIndex")).setComponent(uploadButton);
        headerFileRow.getCell(fileInfoGrid.getColumnByKey("action")).setComponent(delFilesButton);

        List<GridSortOrder<FileInfo>> sortByNumber = new GridSortOrderBuilder<FileInfo>()
                .thenAsc(fileInfoGrid.getColumnByKey("file_name")).build();
        fileInfoGrid.sort(sortByNumber);
        //редактировать файл
        Binder<FileInfo> binder = new Binder<>(FileInfo.class);
        fileInfoGrid.getEditor().setBinder(binder);

        secondField.getElement()
                .addEventListener("keydown",
                        event -> fileInfoGrid.getEditor().cancel())
                .setFilter("event.key === 'Enter'");

        fileInfoGrid.getColumnByKey("file_time").setEditorComponent(secondField);

        fileInfoGrid.getEditor().addCloseListener(event -> {
            if (binder.getBean() != null) {
                FileInfo fileInfo = event.getItem();
                fileInfo.setFile_time((long) secondField.getValue());

                fileInfoService.save(fileInfo);

                grid.getDataProvider().refreshAll();
                Set<Folder> folders = grid.asMultiSelect().getValue();
                grid.deselectAll();
                folders.forEach(folder -> {
                    grid.select(folder);
                    fileInfoGrid.setItems(folder.getFileInfos());
                });


            }
        });

        fileInfoGrid.asMultiSelect().addSelectionListener(event -> delFilesButton.setEnabled(event.getValue().size() > 0));

        grid.asMultiSelect().addValueChangeListener(gridFolderComponentValueChangeEvent -> {

            if(gridFolderComponentValueChangeEvent.getValue().size() > 0) {
                ArrayList<FileInfo> fileInfos = new ArrayList<>();
                gridFolderComponentValueChangeEvent.getValue().forEach(folder -> fileInfos.addAll(folder.getFileInfos()));
                fileInfoGrid.setItems(fileInfos);
                upload.getUpload().setMaxFiles(1000);
                delFolderButton.setEnabled(true);
                uploadButton.setEnabled(true);
            } else {
                delFolderButton.setEnabled(false);
                uploadButton.setEnabled(false);
                fileInfoGrid.setItems(new ArrayList<>());
            }
        });

        HierarchicalDataProvider<Folder, Void> dataProvider =
                new AbstractBackEndHierarchicalDataProvider<>() {

                    @Override
                    public int getChildCount(HierarchicalQuery<Folder, Void> query) {
                        return (int) folderService.getChildCount(query.getParent());
                    }

                    @Override
                    public boolean hasChildren(Folder item) {
                        return folderService.hasChildren(item);
                    }

                    @Override
                    protected Stream<Folder> fetchChildrenFromBackEnd(
                            HierarchicalQuery<Folder, Void> query) {
                        return folderService.fetchChildren(query.getParent()).stream().
                                sorted(Comparator.comparing(Folder::getName));
                    }
                };

        grid.setDataProvider(dataProvider);

        horizontalLayout.add(grid, fileInfoGrid);
    }
    private void delFiles(){
        ConfirmDialog
                .createQuestion()
                .withCaption("Удаление")
                .withMessage("Файл(ы), будут удаленны навсегда с папки, с эфира из слоев. Вы уверенные что хотите удалить?")
                .withOkButton(() -> {
                    fileInfoGrid.asMultiSelect().getSelectedItems().forEach(fileInfo1 -> {
                        etherService.findAll().forEach(ether -> {
                            if (ether.getFile() != null) {
                                if (ether.getFile().getId().equals(fileInfo1.getId())) {
                                    stationService.searchEther(ether.getId()).forEach(station -> {
                                        station = stationService.findById(station.getId());
                                        station.getEthers().remove(ether);
                                        stationService.save(station);
                                    });

                                    etherService.delete(ether);
                                }
                            }
                        });

                        List<LayerImage> layerImages = layerImageService.searchFile(fileInfo1.getId());
                        if (layerImages.size() > 0) {
                            layerGroupImageService.findAll().forEach(layerGroupImage -> {
                                layerGroupImage.getLayerImages().removeAll(layerImages);
                                layerGroupImageService.save(layerGroupImage);
                            });

                            layerImages.forEach(layerImage -> layerImageService.delete(layerImage));
                        }

                    });
                    grid.asMultiSelect().getValue().forEach(folder -> {
                        ArrayList<FileInfo> fileInfos = new ArrayList<>(folder.getFileInfos());
                        folder.getFileInfos().removeAll(fileInfoGrid.asMultiSelect().getSelectedItems());
                        folderService.save(folder);
                        fileInfoGrid.asMultiSelect().getSelectedItems().forEach(fileInfo1 -> {
                            File fileDel = new File(storage + fileInfo1.getFile_path() + fileInfo1.getFile_name());
                            if (fileDel.exists()) {
                                if (fileDel.delete()) {
                                    Optional<FileInfo> matchingObject = fileInfos.stream().
                                            filter(f -> f.getId().equals(fileInfo1.getId())).
                                            findFirst();
                                    if (matchingObject.isPresent()) {
                                        fileInfoService.delete(fileInfo1);
                                       success(
                                                "Файл " + fileInfo1.getFile_name() + " удален");
                                    }

                                } else {
                                    error(
                                            "Файл " + fileInfo1.getFile_name() + " не удален");
                                }
                            } else {
                                Optional<FileInfo> matchingObject = fileInfos.stream().
                                        filter(f -> f.getId().equals(fileInfo1.getId())).
                                        findFirst();
                                if (matchingObject.isPresent()) {
                                    fileInfoService.delete(fileInfo1);
                                    success(
                                            "Файл " + fileInfo1.getFile_name() + " удален");
                                }

                            }

                        });
                    });
                    grid.getDataProvider().refreshAll();
                    grid.deselectAll();
                    fileInfoGrid.setItems(new ArrayList<>());

                }, ButtonOption.focus(), ButtonOption.caption("Да"))
                .withCancelButton(ButtonOption.caption("Нет"))
                .open();

    }
    private Dialog showUploadForm(){
        Dialog dialog = new Dialog();
        dialog.setHeight("500px");
        dialog.setWidth("700px");
        dialog.setCloseOnOutsideClick(false);

        H3 h3 = new H3("Добавление контента");
        h3.setWidth("400px");

        replacementFileCBox.setValue(false);

        upload.getUpload().setI18n(upload.getUpload().getI18n());

        Button cancelButton = new Button(IronIcons.CLOSE.create());
        cancelButton.addThemeVariants(ButtonVariant.LUMO_CONTRAST);
        cancelButton.addClickListener(click -> {
            dialog.close();
            grid.deselectAll();
            grid.getDataProvider().refreshAll();
            fileInfoGrid.setItems(new ArrayList<>());
        });

        VerticalLayout verticalLayout = new VerticalLayout(cancelButton);
        verticalLayout.setDefaultHorizontalComponentAlignment(Alignment.END);

        HorizontalLayout horizontalLayout = new HorizontalLayout(h3, verticalLayout);

        dialog.add(horizontalLayout, replacementFileCBox, upload);

        return dialog;
    }

    private Dialog createAddFolderForm(FolderService folderService, boolean isParent,
                                       HierarchicalDataProvider dataProvider){
        Dialog dialog = new Dialog();

        TextField textField = new TextField("Имя папки");
        textField.setAutofocus(true);

        Binder<Folder> binder = new Binder<>(Folder.class);

        binder.forField(textField)
                .withValidator(min -> min.length() >= 2, "Минимум 2 символа")
                .bind(Folder::getName, Folder::setName);

        dialog.add(textField);

        Button confirmButton = new Button(IronIcons.SAVE.create(), event -> {

            try {


                if (isParent) {
                    grid.asMultiSelect().getValue().forEach(folder -> {
                        Folder folder1 = new Folder(textField.getValue(), folder, new LinkedHashSet<>());
                        folderService.save(folder1);
                    });
                } else {
                    Folder folder1 = new Folder(textField.getValue(), null, new LinkedHashSet<>());
                    folderService.save(folder1);
                }

                dataProvider.refreshAll();
                success(
                        "Папка добавлена.");

                dialog.close();
            } catch (TransactionSystemException transactionSystemException){
                error(
                        "Ошибка: Имя не может быть пустым!");
            }  catch (UniqueDataException uniqueDataException){
                error(uniqueDataException.getMessage());
            }

        });
        confirmButton.addClickShortcut(Key.ENTER);
        confirmButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        Button cancelButton = new Button(IronIcons.CANCEL.create(), event -> dialog.close());
        cancelButton.addClickShortcut(Key.ESCAPE);
        cancelButton.addThemeVariants(ButtonVariant.LUMO_ERROR);

        dialog.add(new Div( confirmButton, new Text("    "), cancelButton));

        return dialog;
    }

    private void uploadFile() {

        upload.getUpload().addAllFinishedListener(( AllFinishedEvent finishedEvent ) -> {
            try {
                FileUtils.cleanDirectory(new File("uvp_temp"));
                
            } catch (IOException e) {
                getLogger().error("" + e);
            }
        });

        upload.getUpload().addFinishedListener(( FinishedEvent finishedEvent ) -> {
            upload.getBuffer().getFileData(finishedEvent.getFileName()).getFile().delete();
            upload.getBuffer().getFiles().remove(finishedEvent.getFileName());
            grid.getDataProvider().refreshAll();
        });

        upload.getUpload().addSucceededListener(event -> {
            new File("uvp_temp").mkdir();
            File tempFile = new File("uvp_temp/" + event.getFileName());
            InputStream is = upload.getBuffer().getInputStream(event.getFileName());

            try {
                FileUtils.copyInputStreamToFile(is, tempFile);
            } catch (IOException e) {
                getLogger().error("" + e);
            }
            try {
                is.close();
            } catch (IOException e) {
                getLogger().error("" + e);
            }


            grid.asMultiSelect().getValue().forEach(folder -> {
                if (folderService.getChildCount(folder) == 0){
                    try {

                        if (folder.getParent() != null) {
                            Folder folder2 = folder.getParent();
                            String path = folder.getParent().getName() + "/"
                                    + folder.getName() + "/";
                            while (true) {
                                if (folder2.getParent() != null) {
                                    path = folder2.getParent().getName() + "/" + path;
                                    folder2 = folder2.getParent();
                                } else {
                                    break;
                                }
                            }
                            objectKey = path + tempFile.getName();

                        } else {
                            objectKey = folder.getName() + "/" + tempFile.getName();
                        }


                        try {
                            Metadata metadata = ImageMetadataReader.readMetadata(tempFile);

                            FileTypeDirectory fileTypeDirectory = metadata.getFirstDirectoryOfType(FileTypeDirectory.class);

                            switch (fileTypeDirectory.getDescription(FileTypeDirectory.TAG_DETECTED_FILE_MIME_TYPE)) {
                                case "video/mp4" -> {
                                    //detecting the file type
                                    BodyContentHandler handler = new BodyContentHandler();
                                    org.apache.tika.metadata.Metadata metadata1 = new org.apache.tika.metadata.Metadata();
                                    FileInputStream inputStream = new FileInputStream(tempFile);
                                    ParseContext pcontext = new ParseContext();

                                    //Html parser
                                    MP4Parser MP4Parser = new MP4Parser();
                                    MP4Parser.parse(inputStream, handler, metadata1,pcontext);
                                    long duration = Math.round(Double.parseDouble(metadata1.get("xmpDM:duration")));

                                    Mp4VideoDirectory mp4VideoDirectory = metadata.getFirstDirectoryOfType(Mp4VideoDirectory.class);
                                    fileInfo = new FileInfo(tempFile.getName(),
                                            duration,
                                            tempFile.length(),
                                            Integer.parseInt(mp4VideoDirectory.getDescription(Mp4VideoDirectory.TAG_WIDTH)
                                                    .replace(" pixels", "")),
                                            Integer.parseInt(mp4VideoDirectory.getDescription(Mp4VideoDirectory.TAG_HEIGHT)
                                                    .replace(" pixels", "")),
                                            fileTypeDirectory.getDescription(FileTypeDirectory.TAG_DETECTED_FILE_MIME_TYPE)
                                                    .replace("/mp4", ""),
                                            objectKey.replace(tempFile.getName(), ""),
                                            LocalDate.now());
                                }
                                case "video/vnd.avi" -> {
                                    AviDirectory aviDirectory = metadata.getFirstDirectoryOfType(AviDirectory.class);
                                    fileInfo = new FileInfo(tempFile.getName(),
                                            toSecond(aviDirectory.getDescription(AviDirectory.TAG_DURATION)),
                                            tempFile.length(),
                                            Integer.parseInt(aviDirectory.getDescription(AviDirectory.TAG_WIDTH)
                                                    .replace(" pixels", "")),
                                            Integer.parseInt(aviDirectory.getDescription(AviDirectory.TAG_HEIGHT)
                                                    .replace(" pixels", "")),
                                            fileTypeDirectory.getDescription(FileTypeDirectory.TAG_DETECTED_FILE_MIME_TYPE)
                                                    .replace("/vnd.avi", ""),
                                            objectKey.replace(tempFile.getName(), ""),
                                            LocalDate.now());
                                }
                                case "video/quicktime" -> {
                                    QuickTimeDirectory quickTimeDirectory = metadata.getFirstDirectoryOfType(QuickTimeDirectory.class);
                                    QuickTimeVideoDirectory quickTimeVideoDirectory = metadata.getFirstDirectoryOfType(QuickTimeVideoDirectory.class);
                                    fileInfo = new FileInfo(tempFile.getName(),
                                            toSecond(quickTimeDirectory.getDescription(QuickTimeDirectory.TAG_DURATION_SECONDS)),
                                            tempFile.length(),
                                            Integer.parseInt(quickTimeVideoDirectory.getDescription(QuickTimeVideoDirectory.TAG_WIDTH)
                                                    .replace(" pixels", "")),
                                            Integer.parseInt(quickTimeVideoDirectory.getDescription(QuickTimeVideoDirectory.TAG_HEIGHT)
                                                    .replace(" pixels", "")),
                                            fileTypeDirectory.getDescription(FileTypeDirectory.TAG_DETECTED_FILE_MIME_TYPE)
                                                    .replace("/quicktime", ""),
                                            objectKey.replace(tempFile.getName(), ""),
                                            LocalDate.now());
                                }
                                case "image/png" -> {
                                    PngDirectory pngDirectory = metadata.getFirstDirectoryOfType(PngDirectory.class);
                                    fileInfo = new FileInfo(tempFile.getName(),
                                            0L,
                                            tempFile.length(),
                                            Integer.parseInt(pngDirectory.getDescription(PngDirectory.TAG_IMAGE_WIDTH)
                                                    .replace(" pixels", "")),
                                            Integer.parseInt(pngDirectory.getDescription(PngDirectory.TAG_IMAGE_HEIGHT)
                                                    .replace(" pixels", "")),
                                            fileTypeDirectory.getDescription(FileTypeDirectory.TAG_DETECTED_FILE_MIME_TYPE)
                                                    .replace("/png", ""),
                                            objectKey.replace(tempFile.getName(), ""),
                                            LocalDate.now());
                                }
                                case "image/jpeg" -> {
                                    JpegDirectory jpegDirectory = metadata.getFirstDirectoryOfType(JpegDirectory.class);
                                    fileInfo = new FileInfo(tempFile.getName(),
                                            0L,
                                            tempFile.length(),
                                            Integer.parseInt(jpegDirectory.getDescription(JpegDirectory.TAG_IMAGE_WIDTH)
                                                    .replace(" pixels", "")),
                                            Integer.parseInt(jpegDirectory.getDescription(JpegDirectory.TAG_IMAGE_HEIGHT)
                                                    .replace(" pixels", "")),
                                            fileTypeDirectory.getDescription(FileTypeDirectory.TAG_DETECTED_FILE_MIME_TYPE)
                                                    .replace("/jpeg", ""),
                                            objectKey.replace(tempFile.getName(), ""),
                                            LocalDate.now());
                                }
                            }
                        } catch (ImageProcessingException imageProcessingException) {
                            fileInfo = new FileInfo(tempFile.getName(),
                                    0L,
                                    tempFile.length(),
                                    0,
                                    0,
                                    "xml",
                                    objectKey.replace(tempFile.getName(), ""),
                                    LocalDate.now());
                        } catch (TikaException e) {
                            e.printStackTrace();
                        } catch (SAXException e) {
                            e.printStackTrace();
                        }

                        if (fileInfo != null) {
                            int temp = 0;

                            for (FileInfo fileInfo1 : folder.getFileInfos()) {
                                if (fileInfo1.getFile_name().equals(fileInfo.getFile_name())) {
                                    fileInfo.setId(fileInfo1.getId());
                                    temp++;
                                }
                            }

                            if (temp == 0) {
                                File dest = new File(storage + objectKey);
                                FileUtils.copyFile(tempFile, dest);
                                try {
                                    Thread.sleep(100);
                                } catch (InterruptedException e) {
                                    getLogger().error(e.toString());
                                }
                                fileInfoService.save(fileInfo);

                                folder = folderService.findById(folder.getId());
                                folder.getFileInfos().add(fileInfo);

                                folderService.save(folder);
                            } else {
                                if (replacementFileCBox.getValue()){
                                    File dest = new File(storage + objectKey);
                                    FileUtils.copyFile(tempFile, dest);
                                    try {
                                        Thread.sleep(100);
                                    } catch (InterruptedException e) {
                                        getLogger().error(e.toString());
                                    }
                                    fileInfoService.save(fileInfo);

                                    folder = folderService.findById(folder.getId());
                                    folder.getFileInfos().add(fileInfo);

                                    folderService.save(folder);
                                } else {
                                    error("Файл - " + fileInfo.getFile_name() + " уже существует в папке " + "\"" + folder + "\"");
                                }
                            }

                        } else {

                            error("Файл - " + fileInfo.getFile_name() + " не возможно добавить.");
                        }


                    } catch (AmazonServiceException | IOException ex) {
                        getLogger().error(ex.toString());

                    }
                }
            });
        });
    }


    private HorizontalLayout createButtonsLayout(FileInfo fileInfo){
        HorizontalLayout horizontalLayout = new HorizontalLayout();

        Button editButton = new Button(EditorIcons.MODE_EDIT.create());
        Button downloadButton = new Button(IronIcons.CLOUD_DOWNLOAD.create());
        Button viewButton = new Button(IronIcons.OPEN_IN_NEW.create());
        viewButton.addClickListener(e -> {
            if (fileInfo.getFile_type().equals("image")) {
                Dialog dialog = new Dialog();
                Button closeButton = new Button(IronIcons.CLOSE.create());
                closeButton.addClickListener(buttonClickEvent -> {
                    dialog.close();
                });
                dialog.setWidth("960px");
                dialog.setHeight("580px");
                StreamResource streamResource =  new StreamResource("bild", () -> {
                    // please change to an existing file...
                    File physicalFile = new File(storage + fileInfo.getFile_path() + fileInfo.getFile_name());
                    FileInputStream stream = null;
                    try {
                        stream = new FileInputStream(physicalFile);
                    } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                    }
                    return stream;
                });

                Image image = new Image(streamResource, fileInfo.getFile_name());
                image.setWidth("860px");
                image.setHeight("440px");
                VerticalLayout verticalLayout = new VerticalLayout(closeButton);
                verticalLayout.setDefaultHorizontalComponentAlignment(Alignment.END);
                dialog.add(verticalLayout, image);
                dialog.open();
            } else if (fileInfo.getFile_type().equals("video")) {
                Dialog dialog = new Dialog();
                Button closeButton = new Button(IronIcons.CLOSE.create());
                closeButton.addClickListener(buttonClickEvent -> {
                   dialog.close();
                });
                dialog.setWidth("960px");
                dialog.setHeight("580px");
                File mediaFile = new File(storage + fileInfo.getFile_path() + fileInfo.getFile_name());

                final VideoJS video = new VideoJS(UI.getCurrent().getSession(), mediaFile, null);
                video.setWidth("860px");
                video.setHeight("440px");
                VerticalLayout verticalLayout = new VerticalLayout(closeButton);
                verticalLayout.setDefaultHorizontalComponentAlignment(Alignment.END);
                dialog.add(verticalLayout, video);
                dialog.open();
            } else {
                downloadButton.click();
            }
        });
        downloadButton.addClickListener(e -> getUI().get().getPage().open("file/download/" + fileInfo.getId()));

        editButton.addClickListener(e-> {
                    fileInfoGrid.getEditor().editItem(fileInfo);
                    secondField.setValue(Math.toIntExact(fileInfo.getFile_time()));
                    secondField.focus();
        });

        if (fileInfo.getFile_type().equals("image")){
            horizontalLayout.add(viewButton, downloadButton, editButton);
        } else {
            if (fileInfo.getFile_type().equals("video")){
                horizontalLayout.add(viewButton, downloadButton);
            } else {
                horizontalLayout.add(viewButton);
            }
        }



        return horizontalLayout;
    }

    private HorizontalLayout getToolbar(){
        filterText.setPlaceholder("Поиск файлов...");
        filterText.setClearButtonVisible(true);
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.addValueChangeListener(e -> updateList());

        HorizontalLayout toolbar = new HorizontalLayout(filterText);
        toolbar.addClassName("toolbar");

        return toolbar;
    }

    private void updateList(){
        ArrayList<FileInfo> fileInfos = new ArrayList<>();
        grid.asMultiSelect().getValue().forEach(folder -> fileInfos.addAll(folder.getFileInfos()));
        fileInfoGrid.setItems(fileInfos.stream()
                .filter(fileInfo  -> fileInfo.getFile_name()
                        .toLowerCase(Locale.ROOT)
                        .contains(filterText.getValue())));
    }


    private long toSecond(String time) {
        String[] split = time.split(":");
        double hour = Double.parseDouble(split[0]);
        double minute = Double.parseDouble(split[1]);
        double second = Double.parseDouble(split[2]);

        return (long)((((hour * 60) + minute) * 60) + second);
    }

}

