package ua.ii.uvpcontrol.ui.view.admin.layer;

import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import org.springframework.security.access.annotation.Secured;
import ua.ii.uvpcontrol.backend.data.users.Role;
import ua.ii.uvpcontrol.backend.service.content.FolderService;
import ua.ii.uvpcontrol.backend.service.layer.LayerGroupPanelService;
import ua.ii.uvpcontrol.backend.service.layer.LayerPanelService;
import ua.ii.uvpcontrol.ui.MainView;
import ua.ii.uvpcontrol.ui.component.layer.LayerPanelGrid;

@Secured({Role.ADMIN, Role.MANAGER})
@Route(value = "layers/panel", layout = MainView.class)
@PageTitle("Шар панелі")
public class LayerPanelView extends VerticalLayout {
    public LayerPanelView(LayerPanelService layerPanelService, LayerGroupPanelService layerGroupPanelService,
                          FolderService folderService){
        LayerPanelGrid layerPanelGrid = new LayerPanelGrid(layerPanelService, layerGroupPanelService, folderService);
        Tab videoTab = new Tab(new Icon(VaadinIcon.MOVIE),new RouterLink("Відео",
                LayerVideoView.class));
        Tab panelTab = new Tab(new Icon(VaadinIcon.SPLIT_V),new RouterLink("Панелі",
                LayerPanelView.class));
        Tab imageTab = new Tab(new Icon(VaadinIcon.PICTURE),new RouterLink("Зображення",
                LayerImageView.class));
        Tab textTab = new Tab(new Icon(VaadinIcon.FONT),new RouterLink("Текст",
                LayerTextView.class));
        Tabs mainTabs = new Tabs(videoTab, panelTab, imageTab, textTab);
        mainTabs.setSelectedTab(panelTab);

        setSizeFull();


        add(mainTabs, layerPanelGrid);
    }
}
