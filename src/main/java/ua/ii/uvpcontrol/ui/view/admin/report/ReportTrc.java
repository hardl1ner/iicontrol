package ua.ii.uvpcontrol.ui.view.admin.report;

import lombok.Data;

@Data
public class ReportTrc {
        private String address;
        private String tvPath;
        private String tv;
        private Integer count;

    }