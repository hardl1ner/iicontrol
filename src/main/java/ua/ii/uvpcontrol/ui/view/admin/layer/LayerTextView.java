package ua.ii.uvpcontrol.ui.view.admin.layer;

import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import org.springframework.security.access.annotation.Secured;
import ua.ii.uvpcontrol.backend.data.users.Role;
import ua.ii.uvpcontrol.backend.service.layer.LayerGroupTextService;
import ua.ii.uvpcontrol.backend.service.layer.LayerTextService;
import ua.ii.uvpcontrol.ui.MainView;
import ua.ii.uvpcontrol.ui.component.layer.LayerTextGrid;

@Secured({Role.ADMIN, Role.MANAGER})
@Route(value = "layers/text", layout = MainView.class)
@PageTitle("Шар текст")
public class LayerTextView extends VerticalLayout {
    public LayerTextView(LayerTextService layerTextService, LayerGroupTextService layerGroupTextService){
        LayerTextGrid layerTextGrid = new LayerTextGrid(layerTextService, layerGroupTextService);
        Tab videoTab = new Tab(new Icon(VaadinIcon.MOVIE),new RouterLink("Відео",
                LayerVideoView.class));
        Tab panelTab = new Tab(new Icon(VaadinIcon.SPLIT_V),new RouterLink("Панелі",
                LayerPanelView.class));
        Tab imageTab = new Tab(new Icon(VaadinIcon.PICTURE),new RouterLink("Зображення",
                LayerImageView.class));
        Tab textTab = new Tab(new Icon(VaadinIcon.FONT),new RouterLink("Текст",
                LayerTextView.class));
        Tabs mainTabs = new Tabs(videoTab, panelTab, imageTab, textTab);
        mainTabs.setSelectedTab(textTab);

        setSizeFull();


        add(mainTabs, layerTextGrid);
    }
}
