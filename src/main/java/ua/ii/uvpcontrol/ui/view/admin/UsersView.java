package ua.ii.uvpcontrol.ui.view.admin;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import ua.ii.uvpcontrol.app.security.CurrentUser;
import ua.ii.uvpcontrol.backend.data.users.Role;
import ua.ii.uvpcontrol.backend.service.UserService;
import ua.ii.uvpcontrol.ui.MainView;
import ua.ii.uvpcontrol.ui.component.user.UserGrid;

@Route(value = "users", layout = MainView.class)
@PageTitle("Пользователи")
@Secured(Role.ADMIN)
public class UsersView extends VerticalLayout{

    public UsersView(UserService userService, CurrentUser currentUser, PasswordEncoder passwordEncoder){
        UserGrid userGrid = new UserGrid(userService, currentUser, passwordEncoder);
        setSizeFull();
        add(userGrid);
    }

}
