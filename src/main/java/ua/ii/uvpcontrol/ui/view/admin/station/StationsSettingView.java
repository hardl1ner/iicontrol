package ua.ii.uvpcontrol.ui.view.admin.station;

import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import org.springframework.security.access.annotation.Secured;
import ua.ii.uvpcontrol.backend.data.users.Role;
import ua.ii.uvpcontrol.backend.service.content.FolderService;
import ua.ii.uvpcontrol.backend.service.layer.LayerGroupImageService;
import ua.ii.uvpcontrol.backend.service.layer.LayerGroupPanelService;
import ua.ii.uvpcontrol.backend.service.layer.LayerGroupTextService;
import ua.ii.uvpcontrol.backend.service.layer.LayerGroupVideoService;
import ua.ii.uvpcontrol.backend.service.station.StationSettingService;
import ua.ii.uvpcontrol.ui.MainView;
import ua.ii.uvpcontrol.ui.component.station.StationSettingGrid;


@Route(value = "station/setting", layout = MainView.class)
@PageTitle("Налаштування станцій")
@CssImport("./styles/shared-styles.css")
@Secured({Role.ADMIN, Role.MANAGER})
public class StationsSettingView extends VerticalLayout {

    public StationsSettingView(StationSettingService stationSettingService,
                               LayerGroupImageService layerGroupImageService, LayerGroupPanelService layerGroupPanelService,
                               LayerGroupVideoService layerGroupVideoService, LayerGroupTextService layerGroupTextService,
                               FolderService folderService){

        StationSettingGrid stationSettingGrid = new StationSettingGrid(stationSettingService,
                layerGroupImageService, layerGroupPanelService, layerGroupVideoService, layerGroupTextService,
                folderService);

        Tab stationTab = new Tab(new Icon(VaadinIcon.LIST),new RouterLink("Список станцій",
                StationsView.class));
        Tab settingTab = new Tab(new Icon(VaadinIcon.WRENCH),new RouterLink("Налаштування станцій",
                StationsSettingView.class));
        Tabs mainTabs = new Tabs(stationTab, settingTab);

        mainTabs.setSelectedTab(settingTab);

        setSizeFull();

        add(mainTabs, stationSettingGrid);
    }

}
