package ua.ii.uvpcontrol.ui.view.admin.report;

import lombok.Data;

@Data
public class ReportTC {
        private String address;
        private String tvPath;
        private String vjPath;
        private String tv;
        private String vj;
        private String panel;
        private Integer count;

    }