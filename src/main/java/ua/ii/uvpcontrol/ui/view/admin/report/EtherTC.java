package ua.ii.uvpcontrol.ui.view.admin.report;

import lombok.Data;

@Data
public class EtherTC {
    private String stationName;
    private int a7 = 0;
    private int a8 = 0;
    private int a9 = 0;
    private int a10 = 0;
    private int a11= 0;
    private int a12= 0;
    private int a13= 0;
    private int a14= 0;
    private int a15= 0;
    private int a16= 0;
    private int a17= 0;
    private int a18= 0;
    private int a19= 0;
    private int a20= 0;
    private int a21= 0;
    private int a22= 0;
    private int count = 0;
}
