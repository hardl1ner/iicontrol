package ua.ii.uvpcontrol.ui.view.admin.report;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.hierarchy.AbstractBackEndHierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalQuery;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.StreamResource;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellBorder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.annotation.Secured;
import ua.ii.uvpcontrol.app.HasLogger;
import ua.ii.uvpcontrol.backend.data.station.Station;
import ua.ii.uvpcontrol.backend.data.users.Role;
import ua.ii.uvpcontrol.backend.service.station.StationService;
import ua.ii.uvpcontrol.ui.MainView;
import ua.ii.uvpcontrol.ui.component.RecursiveSelectTreeGrid;

import java.io.*;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;


@Route(value = "report_ether", layout = MainView.class)
@PageTitle("Ефірна справка")
@Secured({Role.ADMIN, Role.MANAGER})
public class ReportEtherView extends VerticalLayout implements HasLogger {
    private RecursiveSelectTreeGrid<Station> stationTreeGrid;
    private DatePicker start;
    private DatePicker end;
    private TextField nameTextField = new TextField("Назва ролика пошуку");
    private TextField nameDogovor = new TextField("Договір");
    private TextField nameRolika = new TextField("Назва ролика");
    private TextField nameRK = new TextField("Рекламодавець");
    private IntegerField durationVideo = new IntegerField("Тривалість ролика");
    private TextField nameDir = new TextField("Директор");
    private ComboBox<String> template = new ComboBox<>("Шаблон");
    private StationService stationService;
    private Button button = new Button("Отримати Звіт");
    private Anchor pdf = new Anchor();
    private String STORAGE;

    public ReportEtherView(StationService stationService, @Value("${storage}") String STORAGE){
        this.STORAGE = STORAGE;
        pdf.setId("reportDownload");
        setSizeFull();

        template.setItems("OKKO", "WOG", "ZlataPlaza", "Gulliver", "SkyMall", "Bulvar", "Riviera", "Prospekt", "Rayon", "visatv",
                "TC-TV", "TC-Panel", "TC-VJ");

        Tab reportTab = new Tab(new Icon(VaadinIcon.FILE_TABLE),new RouterLink("Звіт про вихід",
                ReportView.class));
        Tab reportEtherTab = new Tab(new Icon(VaadinIcon.TABLE),new RouterLink("Ефірна справка",
                ReportEtherView.class));
        Tabs mainTabs = new Tabs(reportTab, reportEtherTab);

        mainTabs.setSelectedTab(reportEtherTab);

        start = new DatePicker("Початок виходу");
        end = new DatePicker("Кінець виходу");
        start.setClearButtonVisible(true);
        end.setClearButtonVisible(true);
        Locale rus = new Locale("ru", "RU");
        start.setLocale(rus);
        end.setLocale(rus);

        nameTextField.setRequired(true);
        nameTextField.setErrorMessage("Введіть назву ролика!");
        nameTextField.setInvalid(true);

        start.setRequired(true);
        start.setErrorMessage("Виберіть початкову дату!");
        start.setInvalid(true);

        end.setRequired(true);
        end.setErrorMessage("Виберіть кінцеву дату!");
        end.setInvalid(true);

        button.addClickListener(buttonClickEvent -> {
           generateReport();
        });

        this.stationService = stationService;

        createGridStation();

        VerticalLayout verticalLayout = new VerticalLayout(new HorizontalLayout(nameTextField, nameRK),
                new HorizontalLayout(nameRolika, nameDogovor), new HorizontalLayout(nameDir, durationVideo),
                new HorizontalLayout(start, end),template, button, pdf);
        verticalLayout.setWidth("50%");
        verticalLayout.setHeight("100%");

        HorizontalLayout horizontalLayout = new HorizontalLayout(stationTreeGrid, verticalLayout);
        horizontalLayout.setSizeFull();

        add(mainTabs, horizontalLayout);
    }
    private Map<String, String> stationSearch(){
        Map<String, String> stations = new TreeMap<>();
        stationTreeGrid.asMultiSelect().getValue().forEach(station -> {
            if (station.getStationSetting() != null){
                Station parent = station.getParent();
                if (parent != null) {
                    if (parent.getParent() != null) {
                        Station parent2 = parent.getParent();
                        String parentPath = parent.getParent().getName() + "/" + parent.getName() + "/";
                        while (true) {
                            if (parent2.getParent() != null) {
                                parentPath = parent2.getParent().getName() + "/" + parentPath;
                                parent2 = parent2.getParent();
                            } else {
                                break;
                            }
                        }
                        parent.setParent(null);
                        parent.setName(parentPath);
                        parent.setEthers(new ArrayList<>());
                        parent.setStationSetting(null);

                    }
                    parent.setEthers(new ArrayList<>());
                    parent.setStationSetting(null);
                }
                if (!station.getAddress().isEmpty()) {
                    stations.put(station.getAddress(), parent.getName() + station.getName());
                }
            }
        });
        System.out.println(stations.size());
        return stations;
    }
    private void generateReport(){
        if (start.getValue() != null){
            if (end.getValue() != null){
                if (!nameTextField.getValue().isEmpty()){
                    if (stationTreeGrid.asMultiSelect().getValue().size() > 0){
                        if (!nameRolika.isEmpty()){
                            if (!nameDogovor.isEmpty()){
                                Map<String, Map<LocalDate, Integer>> reports = new TreeMap<>();
                                stationSearch().forEach((s, s2) -> reports.put(s,  report(s2)));
                                switch (template.getValue()) {
                                    case "TC-TV" -> {
                                        Map<String, Map<LocalDate, EtherTC>> reportsNl = new TreeMap<>();
                                        stationSearch().forEach((s, s2) -> reportsNl.put(s, reportTV(s2)));
                                        saveToNLExcel(reportsNl);
                                    }
                                    case "TC-VJ" -> {
                                        Map<String, Map<LocalDate, EtherTC>> reportsNl = new TreeMap<>();
                                        stationSearch().forEach((s, s2) -> reportsNl.put(s, reportTV(s2)));
                                        saveToNLVJExcel(reportsNl);
                                    }
                                    case "TC-Panel" -> {
                                        Map<String, Map<LocalDate, EtherTC>> reportsNlPanel = new TreeMap<>();
                                        stationSearch().forEach((s, s2) -> reportsNlPanel.put(s, reportPanel(s2)));
                                        saveToNLPanelExcel(reportsNlPanel);
                                    }
                                    case "WOG" -> saveToWOGExcel(reports);
                                    case "OKKO" -> saveToOkkoExcel(reports);
                                    case "SkyMall" -> saveToSkyMallExcel(reports);
                                    case "Gulliver" -> saveToGulliverExcel(reports);
                                    case "Prospekt" -> saveToProspektExcel(reports);
                                    case "ZlataPlaza" -> saveToZlataPlazaExcel(reports);
                                    case "Rayon" -> saveToRayonExcel(reports);
                                    case "Riviera" -> saveToRivieraExcel(reports);
                                    case "Bulvar" -> saveToBulvarExcel(reports);
                                    case "visatv" -> {
                                        reports.clear();
                                        stationSearch().forEach((s, s2) -> reports.put(s, reportVisaTV(s2)));
                                        saveToVisaTVExcel(reports);
                                    }
                                }
                            } else {
                                Notification.show("Введите договор!", 2000,
                                        Notification.Position.MIDDLE);
                            }
                        } else {
                            Notification.show("Введите имя ролика с договора!", 2000,
                                    Notification.Position.MIDDLE);
                        }
                    } else {
                        Notification.show("Выберить станцию!", 2000,
                                Notification.Position.MIDDLE);
                    }
                } else {
                    Notification.show("Введите имя ролика!", 2000,
                            Notification.Position.MIDDLE);
                }
            } else {
                Notification.show("Выберить конец компании!", 2000,
                            Notification.Position.MIDDLE);
            }
        } else {
            Notification.show("Выберить старт компании!", 2000,
                    Notification.Position.MIDDLE);
        }
    }
    private Map<LocalDate, Integer> report(String station){
        Map<LocalDate, Integer> tempMap = new TreeMap<>();
        LocalDate startDate = start.getValue();
        LocalDate endDate = end.getValue();
        while (startDate.isBefore(endDate)){
            tempMap.put(startDate,  countShow(STORAGE + "logs/" + station + "/log_" + startDate + ".txt"));

            startDate = startDate.plusDays(1);
        }
        tempMap.put(startDate,  countShow(STORAGE + "logs/" + station + "/log_" + startDate + ".txt"));
        return tempMap;
    }

    private Map<LocalDate, EtherTC> reportTV(String station){
        Map<LocalDate, EtherTC> tempMap = new TreeMap<>();
        LocalDate startDate = start.getValue();
        LocalDate endDate = end.getValue();
        while (startDate.isBefore(endDate)){
            tempMap.put(startDate,  countTC_TV(STORAGE + "logs/" + station + "/log_" + startDate + ".txt", station));

            startDate = startDate.plusDays(1);
        }
        tempMap.put(startDate,  countTC_TV(STORAGE + "logs/" + station + "/log_" + startDate + ".txt", station));
        return tempMap;
    }

    private Map<LocalDate, EtherTC> reportPanel(String station){
        Map<LocalDate, EtherTC> tempMap = new TreeMap<>();
        LocalDate startDate = start.getValue();
        LocalDate endDate = end.getValue();
        while (startDate.isBefore(endDate)){
            tempMap.put(startDate,  countTC_Panel(STORAGE + "logs/" + station + "/log_" + startDate + ".txt", station));

            startDate = startDate.plusDays(1);
        }
        tempMap.put(startDate,  countTC_Panel(STORAGE + "logs/" + station + "/log_" + startDate + ".txt", station));
        return tempMap;
    }

    private Map<LocalDate, Integer> reportVisaTV(String station){
        Map<LocalDate, Integer> tempMap = new TreeMap<>();
        LocalDate startDate = start.getValue();
        LocalDate endDate = end.getValue();
        while (startDate.isBefore(endDate)){
            if (!(startDate.getDayOfWeek().getValue() == (DayOfWeek.SATURDAY.getValue()))) {
                if (!(startDate.getDayOfWeek().getValue() == (DayOfWeek.SUNDAY.getValue()))) {
                    tempMap.put(startDate, countShowVisaTV(STORAGE + "logs/" + station + "/log_" + startDate + ".txt"));
                } else {
                    tempMap.put(startDate, 0);
                }
            } else {
                tempMap.put(startDate, 0);
            }

            startDate = startDate.plusDays(1);
        }
        if (!(startDate.getDayOfWeek().getValue() == (DayOfWeek.SATURDAY.getValue()))) {
            if (!(startDate.getDayOfWeek().getValue() == (DayOfWeek.SUNDAY.getValue()))) {
                tempMap.put(startDate, countShowVisaTV(STORAGE + "logs/" + station + "/log_" + startDate + ".txt"));
            }else {
                tempMap.put(startDate, 0);
            }
        } else {
            tempMap.put(startDate, 0);
        }
        return tempMap;
    }

    private void cellTHICK(CellStyle cellStyleBold){
        //border bold

        cellStyleBold.setBorderLeft(BorderStyle.THICK);
        cellStyleBold.setBorderRight(BorderStyle.THICK);
        cellStyleBold.setBorderTop(BorderStyle.THICK);
        cellStyleBold.setBorderBottom(BorderStyle.THICK);

        cellStyleBold.setBottomBorderColor(IndexedColors.BLACK1.getIndex());
        cellStyleBold.setLeftBorderColor(IndexedColors.BLACK1.getIndex());
        cellStyleBold.setRightBorderColor(IndexedColors.BLACK1.getIndex());
        cellStyleBold.setTopBorderColor(IndexedColors.BLACK1.getIndex());
    }

    private void cellTHIN(CellStyle cellStyle){
        cellStyle.setBorderLeft(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setBorderBottom(BorderStyle.THIN);

        cellStyle.setBottomBorderColor(IndexedColors.BLACK1.getIndex());
        cellStyle.setLeftBorderColor(IndexedColors.BLACK1.getIndex());
        cellStyle.setRightBorderColor(IndexedColors.BLACK1.getIndex());
        cellStyle.setTopBorderColor(IndexedColors.BLACK1.getIndex());
        cellStyle.setAlignment(HorizontalAlignment.CENTER);

    }
    private void saveToOkkoExcel(Map<String, Map<LocalDate, Integer>> reports){
        File file = new File(STORAGE + "report/azk/okko.xlsx");
        // Read XSL file
        try {
            FileInputStream inputStream = new FileInputStream(file);

            // Get the workbook instance for XLS file
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

            // Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            CellStyle cellStyle4 = workbook.createCellStyle();
            cellStyle4.setWrapText(true);

            // text center alignment
            cellStyle4.setAlignment(HorizontalAlignment.CENTER);
            Cell cell = sheet.getRow(2).createCell(2);
            cell.setCellValue(nameDogovor.getValue());
            cell.setCellStyle(cellStyle4);

            sheet.getRow(5).createCell(3).setCellValue(nameRK.getValue());
            sheet.getRow(6).createCell(3).setCellValue(nameRolika.getValue());
            sheet.getRow(7).createCell(3).setCellValue("з " + start.getValue() + " по " + end.getValue());


            //border bold
            CellStyle cellStyleBold = workbook.createCellStyle();
            cellTHICK(cellStyleBold);



            //border smoll
            CellStyle cellStyleSmall = workbook.createCellStyle();
            cellTHIN(cellStyleSmall);

            AtomicInteger i = new AtomicInteger(11);
            AtomicInteger countS = new AtomicInteger(1);

            CellStyle cellS = workbook.createCellStyle();
            cellS.setAlignment(HorizontalAlignment.LEFT);

            cellTHICK(cellS);

            reports.forEach((s, localDateIntegerMap) -> {
                System.out.println(s);

                //border bold
                CellStyle cellStyletationBold = workbook.createCellStyle();

                cellTHICK(cellStyletationBold);


                Cell cellStation = sheet.getRow(i.get()).createCell(2);
                cellStation.setCellStyle(cellS);
                cellStation.setCellValue(s);
                cellStation.getCellStyle().setAlignment(HorizontalAlignment.LEFT);
                Cell cellN = sheet.getRow(i.get()).createCell(1);
                cellN.setCellValue(countS.get());
                cellN.setCellStyle(cellStyletationBold);
                countS.getAndIncrement();

                AtomicInteger count = new AtomicInteger();

                XSSFSheet xssfSheet = workbook.cloneSheet(1, stationService.findByAddress(s).getName());
                xssfSheet.getRow(4).createCell(3).setCellValue(nameRK.getValue());
                xssfSheet.getRow(5).createCell(3).setCellValue(nameRolika.getValue());
                xssfSheet.getRow(7).createCell(1).setCellValue("Термін проведення рекламної кампанії: "
                        + "з " + start.getValue() + " по " + end.getValue());
                xssfSheet.getRow(8).createCell(1).setCellValue("Місце трансляціїї: " + s);
                xssfSheet.getRow(48).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
                xssfSheet.getRow(49).createCell(1).setCellValue("Дата: " + end.getValue());

                AtomicInteger countDay = new AtomicInteger(12);

                localDateIntegerMap.forEach((localDate, integer) -> {
                    Cell cell1 = xssfSheet.getRow(countDay.get()).createCell(1);
                    cell1.setCellValue(localDate.toString());
                    cell1.setCellStyle(cellStyleSmall);

                    Cell cell2 = xssfSheet.getRow(countDay.get()).createCell(2);
                    cell2.setCellValue(integer);
                    cell2.setCellStyle(cellStyleSmall);

                    Cell cell3 = xssfSheet.getRow(countDay.get()).createCell(3);
                    cell3.setCellValue(integer);
                    cell3.setCellStyle(cellStyleSmall);

                    count.set(count.get() + integer);

                    countDay.getAndIncrement();
                });

                // количество згидно медиа плану
                Cell cellMediaPlan = sheet.getRow(i.get()).createCell(3);
                long days = start.getValue().until(end.getValue(), ChronoUnit.DAYS) + 1;
                cellMediaPlan.setCellValue(days * 252);
                cellStyleBold.setAlignment(HorizontalAlignment.CENTER);
                cellMediaPlan.setCellStyle(cellStyleBold);

                CellStyle cellStyleCount = workbook.createCellStyle();
                cellTHICK(cellStyleCount);


                // фактическое количество
                cellStyleCount.setAlignment(HorizontalAlignment.CENTER);
                Cell cellCount = sheet.getRow(i.get()).createCell(4);
                cellCount.setCellValue(count.get());
                cellCount.setCellStyle(cellStyleCount);

                Cell cellPohibka = sheet.getRow(i.get()).createCell(5);
                cellPohibka.setCellFormula("E" + (i.get() + 1) + "-" + "D" + (i.get() + 1));
                cellPohibka.setCellStyle(cellStyleCount);

                Cell cellD = xssfSheet.getRow(44).createCell(9);
                cellD.setCellValue(durationVideo.getValue());
                Cell cellSum = xssfSheet.getRow(46).createCell(9);
                cellSum.setCellFormula("SUM(D13:D44)");
                Cell cellRound = xssfSheet.getRow(45).createCell(9);
                cellRound.setCellFormula("ROUND(J45*J47/60,2)");

                i.getAndIncrement();
            });
            workbook.removeSheetAt(1);
            sheet.createRow(93);
            sheet.createRow(94);
            sheet.getRow(93).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
            sheet.getRow(94).createCell(1).setCellValue("Дата: " + end.getValue());

            File fileSave = new File(STORAGE +"report/" + nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue() + ".xlsx");
            FileOutputStream out = new FileOutputStream(fileSave);
            workbook.write(out);
            // Write File
            StreamResource resource = new StreamResource(nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue()+ ".xlsx",
                    () -> {
                        try {
                            return new FileInputStream(fileSave);
                        } catch (IOException e) {
                            getLogger().error("" + e);
                            return null;
                        }
                    });

            pdf.setHref(resource);
            pdf.getElement().setAttribute("download", true);

            com.vaadin.flow.component.page.Page page = UI.getCurrent().getPage();
            page.executeJs("document.getElementById('" + "reportDownload" + "').click();");

            inputStream.close();
            //workbook.write(out);
            //out.close();
        } catch (FileNotFoundException e) {
            getLogger().error("" + e);
        } catch (IOException e) {
            getLogger().error("" + e);
        }

    }

    private void saveToNLVJExcel(Map<String, Map<LocalDate, EtherTC>> reports){
        File file = new File(STORAGE + "report/tc/nl-vj.xlsx");
        // Read XSL file
        try {
            FileInputStream inputStream = new FileInputStream(file);

            // Get the workbook instance for XLS file
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

            // Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            CellStyle cellStyle4 = workbook.createCellStyle();
            cellStyle4.setWrapText(true);

            // text center alignment
            cellStyle4.setAlignment(HorizontalAlignment.CENTER);
            Cell cell = sheet.getRow(2).createCell(2);
            cell.setCellValue(nameDogovor.getValue());
            cell.setCellStyle(cellStyle4);

            sheet.getRow(5).createCell(3).setCellValue(nameRK.getValue());
            sheet.getRow(6).createCell(3).setCellValue(nameRolika.getValue());
            sheet.getRow(7).createCell(3).setCellValue("з " + start.getValue() + " по " + end.getValue());


            //border bold
            CellStyle cellStyleBold = workbook.createCellStyle();

            cellTHICK(cellStyleBold);



            //border smoll
            CellStyle cellStyleSmall = workbook.createCellStyle();

            cellTHIN(cellStyleSmall);
            cellStyleSmall.setAlignment(HorizontalAlignment.CENTER);

            AtomicInteger i = new AtomicInteger(11);
            AtomicInteger countS = new AtomicInteger(1);

            CellStyle cellS = workbook.createCellStyle();
            cellS.setAlignment(HorizontalAlignment.LEFT);

            cellTHICK(cellS);

            reports.forEach((s, localDateIntegerMap) -> {
                System.out.println(s);

                //border bold
                CellStyle cellStyletationBold = workbook.createCellStyle();

                cellTHICK(cellStyletationBold);


                Cell cellStation = sheet.getRow(i.get()).createCell(2);
                cellStation.setCellStyle(cellS);
                cellStation.setCellValue(s);
                cellStation.getCellStyle().setAlignment(HorizontalAlignment.LEFT);
                Cell cellN = sheet.getRow(i.get()).createCell(1);
                cellN.setCellValue(countS.get());
                cellN.setCellStyle(cellStyletationBold);
                countS.getAndIncrement();

                AtomicInteger count = new AtomicInteger();

                XSSFSheet xssfSheet = workbook.cloneSheet(1, stationService.findByAddress(s).getName());
                xssfSheet.getRow(4).createCell(3).setCellValue(nameRK.getValue());
                xssfSheet.getRow(5).createCell(3).setCellValue(nameRolika.getValue());
                xssfSheet.getRow(7).createCell(1).setCellValue("Термін проведення рекламної кампанії: "
                        + "з " + start.getValue() + " по " + end.getValue());
                xssfSheet.getRow(8).createCell(1).setCellValue("Місце трансляціїї: " + s);
                xssfSheet.getRow(48).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
                xssfSheet.getRow(49).createCell(1).setCellValue("Дата: " + end.getValue());

                AtomicInteger countDay = new AtomicInteger(12);

                localDateIntegerMap.forEach((localDate, etherTC) -> {
                    Cell cell1 = xssfSheet.getRow(countDay.get()).createCell(1);
                    cell1.setCellValue(localDate.toString());
                    cell1.setCellStyle(cellStyleSmall);

                    Cell cell2 = xssfSheet.getRow(countDay.get()).createCell(2);
                    cell2.setCellValue(etherTC.getA7());
                    cell2.setCellStyle(cellStyleSmall);

                    Cell cell3 = xssfSheet.getRow(countDay.get()).createCell(3);
                    cell3.setCellValue(etherTC.getA8());
                    cell3.setCellStyle(cellStyleSmall);

                    Cell cell4 = xssfSheet.getRow(countDay.get()).createCell(4);
                    cell4.setCellValue(etherTC.getA9());
                    cell4.setCellStyle(cellStyleSmall);
                    Cell cell5 = xssfSheet.getRow(countDay.get()).createCell(5);
                    cell5.setCellValue(etherTC.getA10());
                    cell5.setCellStyle(cellStyleSmall);
                    Cell cell6 = xssfSheet.getRow(countDay.get()).createCell(6);
                    cell6.setCellValue(etherTC.getA11());
                    cell6.setCellStyle(cellStyleSmall);
                    Cell cell7 = xssfSheet.getRow(countDay.get()).createCell(7);
                    cell7.setCellValue(etherTC.getA12());
                    cell7.setCellStyle(cellStyleSmall);
                    Cell cell8 = xssfSheet.getRow(countDay.get()).createCell(8);
                    cell8.setCellValue(etherTC.getA13());
                    cell8.setCellStyle(cellStyleSmall);
                    Cell cell9 = xssfSheet.getRow(countDay.get()).createCell(9);
                    cell9.setCellValue(etherTC.getA14());
                    cell9.setCellStyle(cellStyleSmall);
                    Cell cell10 = xssfSheet.getRow(countDay.get()).createCell(10);
                    cell10.setCellValue(etherTC.getA15());
                    cell10.setCellStyle(cellStyleSmall);
                    Cell cell11 = xssfSheet.getRow(countDay.get()).createCell(11);
                    cell11.setCellValue(etherTC.getA16());
                    cell11.setCellStyle(cellStyleSmall);
                    Cell cell12 = xssfSheet.getRow(countDay.get()).createCell(12);
                    cell12.setCellValue(etherTC.getA17());
                    cell12.setCellStyle(cellStyleSmall);
                    Cell cell13 = xssfSheet.getRow(countDay.get()).createCell(13);
                    cell13.setCellValue(etherTC.getA18());
                    cell13.setCellStyle(cellStyleSmall);
                    Cell cell14 = xssfSheet.getRow(countDay.get()).createCell(14);
                    cell14.setCellValue(etherTC.getA19());
                    cell14.setCellStyle(cellStyleSmall);
                    Cell cell15 = xssfSheet.getRow(countDay.get()).createCell(15);
                    cell15.setCellValue(etherTC.getA20());
                    cell15.setCellStyle(cellStyleSmall);
                    Cell cell16 = xssfSheet.getRow(countDay.get()).createCell(16);
                    cell16.setCellValue(etherTC.getA21());
                    cell16.setCellStyle(cellStyleSmall);

                    Cell cell17 = xssfSheet.getRow(countDay.get()).createCell(17);
                    cell17.setCellValue(etherTC.getCount());
                    cell17.setCellStyle(cellStyleSmall);

                    count.set(count.get() + etherTC.getCount());

                    countDay.getAndIncrement();
                });

                // количество згидно медиа плану
                Cell cellMediaPlan = sheet.getRow(i.get()).createCell(3);
                long days = start.getValue().until(end.getValue(), ChronoUnit.DAYS) + 1;
                cellMediaPlan.setCellValue(days * (60 / durationVideo.getValue() * 13));
                cellStyleBold.setAlignment(HorizontalAlignment.CENTER);
                cellMediaPlan.setCellStyle(cellStyleBold);

                CellStyle cellStyleCount = workbook.createCellStyle();

                //border
                cellTHICK(cellStyleCount);

                // фактическое количество
                cellStyleCount.setAlignment(HorizontalAlignment.CENTER);
                Cell cellCount = sheet.getRow(i.get()).createCell(4);
                cellCount.setCellValue(count.get());
                cellCount.setCellStyle(cellStyleCount);

                Cell cellPohibka = sheet.getRow(i.get()).createCell(5);
                cellPohibka.setCellFormula("E" + (i.get() + 1) + "-" + "D" + (i.get() + 1));
                cellPohibka.setCellStyle(cellStyleCount);

                Cell cellD = xssfSheet.getRow(44).createCell(9);
                cellD.setCellValue(durationVideo.getValue());
                Cell cellSum = xssfSheet.getRow(46).createCell(9);
                cellSum.setCellFormula("SUM(R13:R44)");
                Cell cellRound = xssfSheet.getRow(45).createCell(9);
                cellRound.setCellFormula("ROUND(J45*J47/60,2)");

                i.getAndIncrement();
            });
            workbook.removeSheetAt(1);
            sheet.getRow(94).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
            sheet.getRow(95).createCell(1).setCellValue("Дата: " + end.getValue());

            File fileSave = new File(STORAGE +"report/" + nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue() + ".xlsx");
            FileOutputStream out = new FileOutputStream(fileSave);
            workbook.write(out);
            // Write File
            StreamResource resource = new StreamResource(nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue() + ".xlsx",
                    () -> {
                        try {
                            return new FileInputStream(fileSave);
                        } catch (IOException e) {
                            getLogger().error("" + e);
                            return null;
                        }
                    });

            pdf.setHref(resource);
            pdf.getElement().setAttribute("download", true);

            com.vaadin.flow.component.page.Page page = UI.getCurrent().getPage();
            page.executeJs("document.getElementById('" + "reportDownload" + "').click();");

            inputStream.close();
            //workbook.write(out);
            //out.close();
        } catch (FileNotFoundException e) {
            getLogger().error("" + e);
        } catch (IOException e) {
            getLogger().error("" + e);
        }

    }
    private void saveToNLExcel(Map<String, Map<LocalDate, EtherTC>> reports){
        File file = new File(STORAGE + "report/tc/nl-tv.xlsx");
        // Read XSL file
        try {
            FileInputStream inputStream = new FileInputStream(file);

            // Get the workbook instance for XLS file
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

            // Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            CellStyle cellStyle4 = workbook.createCellStyle();
            cellStyle4.setWrapText(true);

            // text center alignment
            cellStyle4.setAlignment(HorizontalAlignment.CENTER);
            Cell cell = sheet.getRow(2).createCell(2);
            cell.setCellValue(nameDogovor.getValue());
            cell.setCellStyle(cellStyle4);

            sheet.getRow(5).createCell(3).setCellValue(nameRK.getValue());
            sheet.getRow(6).createCell(3).setCellValue(nameRolika.getValue());
            sheet.getRow(7).createCell(3).setCellValue("з " + start.getValue() + " по " + end.getValue());


            //border bold
            CellStyle cellStyleBold = workbook.createCellStyle();

            cellTHICK(cellStyleBold);



            //border smoll
            CellStyle cellStyleSmall = workbook.createCellStyle();

            cellTHIN(cellStyleSmall);

            AtomicInteger i = new AtomicInteger(11);
            AtomicInteger countS = new AtomicInteger(1);

            CellStyle cellS = workbook.createCellStyle();
            cellS.setAlignment(HorizontalAlignment.LEFT);

            cellTHICK(cellS);

            reports.forEach((s, localDateIntegerMap) -> {
                System.out.println(s);

                //border bold
                CellStyle cellStyletationBold = workbook.createCellStyle();

                cellTHICK(cellStyletationBold);


                Cell cellStation = sheet.getRow(i.get()).createCell(2);
                cellStation.setCellStyle(cellS);
                cellStation.setCellValue(s);
                cellStation.getCellStyle().setAlignment(HorizontalAlignment.LEFT);
                Cell cellN = sheet.getRow(i.get()).createCell(1);
                cellN.setCellValue(countS.get());
                cellN.setCellStyle(cellStyletationBold);
                countS.getAndIncrement();

                AtomicInteger count = new AtomicInteger();

                XSSFSheet xssfSheet = workbook.cloneSheet(1, stationService.findByAddress(s).getName());
                xssfSheet.getRow(4).createCell(3).setCellValue(nameRK.getValue());
                xssfSheet.getRow(5).createCell(3).setCellValue(nameRolika.getValue());
                xssfSheet.getRow(7).createCell(1).setCellValue("Термін проведення рекламної кампанії: "
                        + "з " + start.getValue() + " по " + end.getValue());
                xssfSheet.getRow(8).createCell(1).setCellValue("Місце трансляціїї: " + s);
                xssfSheet.getRow(48).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
                xssfSheet.getRow(49).createCell(1).setCellValue("Дата: " + end.getValue());

                AtomicInteger countDay = new AtomicInteger(12);

                localDateIntegerMap.forEach((localDate, etherTC) -> {
                    Cell cell1 = xssfSheet.getRow(countDay.get()).createCell(1);
                    cell1.setCellValue(localDate.toString());
                    cell1.setCellStyle(cellStyleSmall);

                    Cell cell2 = xssfSheet.getRow(countDay.get()).createCell(2);
                    cell2.setCellValue(etherTC.getA7());
                    cell2.setCellStyle(cellStyleSmall);

                    Cell cell3 = xssfSheet.getRow(countDay.get()).createCell(3);
                    cell3.setCellValue(etherTC.getA8());
                    cell3.setCellStyle(cellStyleSmall);

                    Cell cell4 = xssfSheet.getRow(countDay.get()).createCell(4);
                    cell4.setCellValue(etherTC.getA9());
                    cell4.setCellStyle(cellStyleSmall);
                    Cell cell5 = xssfSheet.getRow(countDay.get()).createCell(5);
                    cell5.setCellValue(etherTC.getA10());
                    cell5.setCellStyle(cellStyleSmall);
                    Cell cell6 = xssfSheet.getRow(countDay.get()).createCell(6);
                    cell6.setCellValue(etherTC.getA11());
                    cell6.setCellStyle(cellStyleSmall);
                    Cell cell7 = xssfSheet.getRow(countDay.get()).createCell(7);
                    cell7.setCellValue(etherTC.getA12());
                    cell7.setCellStyle(cellStyleSmall);
                    Cell cell8 = xssfSheet.getRow(countDay.get()).createCell(8);
                    cell8.setCellValue(etherTC.getA13());
                    cell8.setCellStyle(cellStyleSmall);
                    Cell cell9 = xssfSheet.getRow(countDay.get()).createCell(9);
                    cell9.setCellValue(etherTC.getA14());
                    cell9.setCellStyle(cellStyleSmall);
                    Cell cell10 = xssfSheet.getRow(countDay.get()).createCell(10);
                    cell10.setCellValue(etherTC.getA15());
                    cell10.setCellStyle(cellStyleSmall);
                    Cell cell11 = xssfSheet.getRow(countDay.get()).createCell(11);
                    cell11.setCellValue(etherTC.getA16());
                    cell11.setCellStyle(cellStyleSmall);
                    Cell cell12 = xssfSheet.getRow(countDay.get()).createCell(12);
                    cell12.setCellValue(etherTC.getA17());
                    cell12.setCellStyle(cellStyleSmall);
                    Cell cell13 = xssfSheet.getRow(countDay.get()).createCell(13);
                    cell13.setCellValue(etherTC.getA18());
                    cell13.setCellStyle(cellStyleSmall);
                    Cell cell14 = xssfSheet.getRow(countDay.get()).createCell(14);
                    cell14.setCellValue(etherTC.getA19());
                    cell14.setCellStyle(cellStyleSmall);
                    Cell cell15 = xssfSheet.getRow(countDay.get()).createCell(15);
                    cell15.setCellValue(etherTC.getA20());
                    cell15.setCellStyle(cellStyleSmall);
                    Cell cell16 = xssfSheet.getRow(countDay.get()).createCell(16);
                    cell16.setCellValue(etherTC.getA21());
                    cell16.setCellStyle(cellStyleSmall);

                    Cell cell17 = xssfSheet.getRow(countDay.get()).createCell(17);
                    cell17.setCellValue(etherTC.getCount());
                    cell17.setCellStyle(cellStyleSmall);

                    count.set(count.get() + etherTC.getCount());

                    countDay.getAndIncrement();
                });

                // количество згидно медиа плану
                Cell cellMediaPlan = sheet.getRow(i.get()).createCell(3);
                long days = start.getValue().until(end.getValue(), ChronoUnit.DAYS) + 1;
                cellMediaPlan.setCellValue(days * (60 / durationVideo.getValue() * 13));
                cellStyleBold.setAlignment(HorizontalAlignment.CENTER);
                cellMediaPlan.setCellStyle(cellStyleBold);

                CellStyle cellStyleCount = workbook.createCellStyle();

                cellTHICK(cellStyleCount);

                // фактическое количество
                cellStyleCount.setAlignment(HorizontalAlignment.CENTER);
                Cell cellCount = sheet.getRow(i.get()).createCell(4);
                cellCount.setCellValue(count.get());
                cellCount.setCellStyle(cellStyleCount);

                Cell cellPohibka = sheet.getRow(i.get()).createCell(5);
                cellPohibka.setCellFormula("E" + (i.get() + 1) + "-" + "D" + (i.get() + 1));
                cellPohibka.setCellStyle(cellStyleCount);

                Cell cellD = xssfSheet.getRow(44).createCell(9);
                cellD.setCellValue(durationVideo.getValue());
                Cell cellSum = xssfSheet.getRow(46).createCell(9);
                cellSum.setCellFormula("SUM(R13:R44)");
                Cell cellRound = xssfSheet.getRow(45).createCell(9);
                cellRound.setCellFormula("ROUND(J45*J47/60,2)");

                i.getAndIncrement();
            });
            workbook.removeSheetAt(1);
            sheet.getRow(96).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
            sheet.getRow(97).createCell(1).setCellValue("Дата: " + end.getValue());

            File fileSave = new File(STORAGE +"report/" + nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue() + ".xlsx");
            FileOutputStream out = new FileOutputStream(fileSave);
            workbook.write(out);
            // Write File
            StreamResource resource = new StreamResource(nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue() + ".xlsx",
                    () -> {
                        try {
                            return new FileInputStream(fileSave);
                        } catch (IOException e) {
                            getLogger().error("" + e);
                            return null;
                        }
                    });

            pdf.setHref(resource);
            pdf.getElement().setAttribute("download", true);

            com.vaadin.flow.component.page.Page page = UI.getCurrent().getPage();
            page.executeJs("document.getElementById('" + "reportDownload" + "').click();");

            inputStream.close();
            //workbook.write(out);
            //out.close();
        } catch (FileNotFoundException e) {
            getLogger().error("" + e);
        } catch (IOException e) {
            getLogger().error("" + e);
        }

    }
    private void saveToNLPanelExcel(Map<String, Map<LocalDate, EtherTC>> reports){
        File file = new File(STORAGE + "report/tc/nl-panel.xlsx");
        // Read XSL file
        try {
            FileInputStream inputStream = new FileInputStream(file);

            // Get the workbook instance for XLS file
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

            // Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            CellStyle cellStyle4 = workbook.createCellStyle();
            cellStyle4.setWrapText(true);

            // text center alignment
            cellStyle4.setAlignment(HorizontalAlignment.CENTER);
            Cell cell = sheet.getRow(2).createCell(2);
            cell.setCellValue(nameDogovor.getValue());
            cell.setCellStyle(cellStyle4);

            sheet.getRow(5).createCell(3).setCellValue(nameRK.getValue());
            sheet.getRow(6).createCell(3).setCellValue(nameRolika.getValue());
            sheet.getRow(7).createCell(3).setCellValue("з " + start.getValue() + " по " + end.getValue());


            //border bold
            CellStyle cellStyleBold = workbook.createCellStyle();

            cellTHICK(cellStyleBold);



            //border smoll
            CellStyle cellStyleSmall = workbook.createCellStyle();

            cellTHIN(cellStyleSmall);

            AtomicInteger i = new AtomicInteger(11);
            AtomicInteger countS = new AtomicInteger(1);

            CellStyle cellS = workbook.createCellStyle();
            cellS.setAlignment(HorizontalAlignment.LEFT);

            cellTHICK(cellS);

            reports.forEach((s, localDateIntegerMap) -> {
                System.out.println(s);

                //border bold
                CellStyle cellStyletationBold = workbook.createCellStyle();

                cellTHICK(cellStyletationBold);


                Cell cellStation = sheet.getRow(i.get()).createCell(2);
                cellStation.setCellStyle(cellS);
                cellStation.setCellValue(s);
                cellStation.getCellStyle().setAlignment(HorizontalAlignment.LEFT);
                Cell cellN = sheet.getRow(i.get()).createCell(1);
                cellN.setCellValue(countS.get());
                cellN.setCellStyle(cellStyletationBold);
                countS.getAndIncrement();

                AtomicInteger count = new AtomicInteger();

                XSSFSheet xssfSheet = workbook.cloneSheet(1, stationService.findByAddress(s).getName());
                xssfSheet.getRow(4).createCell(3).setCellValue(nameRK.getValue());
                xssfSheet.getRow(5).createCell(3).setCellValue(nameRolika.getValue());
                xssfSheet.getRow(7).createCell(1).setCellValue("Термін проведення рекламної кампанії: "
                        + "з " + start.getValue() + " по " + end.getValue());
                xssfSheet.getRow(8).createCell(1).setCellValue("Місце трансляціїї: " + s);
                xssfSheet.getRow(48).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
                xssfSheet.getRow(49).createCell(1).setCellValue("Дата: " + end.getValue());

                AtomicInteger countDay = new AtomicInteger(12);

                localDateIntegerMap.forEach((localDate, etherTC) -> {
                    Cell cell1 = xssfSheet.getRow(countDay.get()).createCell(1);
                    cell1.setCellValue(localDate.toString());
                    cell1.setCellStyle(cellStyleSmall);

                    Cell cell2 = xssfSheet.getRow(countDay.get()).createCell(2);
                    cell2.setCellValue(etherTC.getA7());
                    cell2.setCellStyle(cellStyleSmall);

                    Cell cell3 = xssfSheet.getRow(countDay.get()).createCell(3);
                    cell3.setCellValue(etherTC.getA8());
                    cell3.setCellStyle(cellStyleSmall);

                    Cell cell4 = xssfSheet.getRow(countDay.get()).createCell(4);
                    cell4.setCellValue(etherTC.getA9());
                    cell4.setCellStyle(cellStyleSmall);
                    Cell cell5 = xssfSheet.getRow(countDay.get()).createCell(5);
                    cell5.setCellValue(etherTC.getA10());
                    cell5.setCellStyle(cellStyleSmall);
                    Cell cell6 = xssfSheet.getRow(countDay.get()).createCell(6);
                    cell6.setCellValue(etherTC.getA11());
                    cell6.setCellStyle(cellStyleSmall);
                    Cell cell7 = xssfSheet.getRow(countDay.get()).createCell(7);
                    cell7.setCellValue(etherTC.getA12());
                    cell7.setCellStyle(cellStyleSmall);
                    Cell cell8 = xssfSheet.getRow(countDay.get()).createCell(8);
                    cell8.setCellValue(etherTC.getA13());
                    cell8.setCellStyle(cellStyleSmall);
                    Cell cell9 = xssfSheet.getRow(countDay.get()).createCell(9);
                    cell9.setCellValue(etherTC.getA14());
                    cell9.setCellStyle(cellStyleSmall);
                    Cell cell10 = xssfSheet.getRow(countDay.get()).createCell(10);
                    cell10.setCellValue(etherTC.getA15());
                    cell10.setCellStyle(cellStyleSmall);
                    Cell cell11 = xssfSheet.getRow(countDay.get()).createCell(11);
                    cell11.setCellValue(etherTC.getA16());
                    cell11.setCellStyle(cellStyleSmall);
                    Cell cell12 = xssfSheet.getRow(countDay.get()).createCell(12);
                    cell12.setCellValue(etherTC.getA17());
                    cell12.setCellStyle(cellStyleSmall);
                    Cell cell13 = xssfSheet.getRow(countDay.get()).createCell(13);
                    cell13.setCellValue(etherTC.getA18());
                    cell13.setCellStyle(cellStyleSmall);
                    Cell cell14 = xssfSheet.getRow(countDay.get()).createCell(14);
                    cell14.setCellValue(etherTC.getA19());
                    cell14.setCellStyle(cellStyleSmall);
                    Cell cell15 = xssfSheet.getRow(countDay.get()).createCell(15);
                    cell15.setCellValue(etherTC.getA20());
                    cell15.setCellStyle(cellStyleSmall);
                    Cell cell16 = xssfSheet.getRow(countDay.get()).createCell(16);
                    cell16.setCellValue(etherTC.getA21());
                    cell16.setCellStyle(cellStyleSmall);

                    Cell cell17 = xssfSheet.getRow(countDay.get()).createCell(17);
                    cell17.setCellValue(etherTC.getCount());
                    cell17.setCellStyle(cellStyleSmall);

                    count.set(count.get() + etherTC.getCount());

                    countDay.getAndIncrement();
                });

                // количество згидно медиа плану
                Cell cellMediaPlan = sheet.getRow(i.get()).createCell(3);
                long days = start.getValue().until(end.getValue(), ChronoUnit.DAYS) + 1;
                cellMediaPlan.setCellValue(days * (8 * 13));
                cellStyleBold.setAlignment(HorizontalAlignment.CENTER);
                cellMediaPlan.setCellStyle(cellStyleBold);

                CellStyle cellStyleCount = workbook.createCellStyle();

                cellTHICK(cellStyleCount);

                // фактическое количество
                cellStyleCount.setAlignment(HorizontalAlignment.CENTER);
                Cell cellCount = sheet.getRow(i.get()).createCell(4);
                cellCount.setCellValue(count.get());
                cellCount.setCellStyle(cellStyleCount);

                Cell cellPohibka = sheet.getRow(i.get()).createCell(5);
                cellPohibka.setCellFormula("E" + (i.get() + 1) + "-" + "D" + (i.get() + 1));
                cellPohibka.setCellStyle(cellStyleCount);

                Cell cellD = xssfSheet.getRow(44).createCell(9);
                cellD.setCellValue(durationVideo.getValue());
                Cell cellSum = xssfSheet.getRow(46).createCell(9);
                cellSum.setCellFormula("SUM(R13:R44)");
                Cell cellRound = xssfSheet.getRow(45).createCell(9);
                cellRound.setCellFormula("ROUND(J45*J47/60,2)");

                i.getAndIncrement();
            });
            workbook.removeSheetAt(1);
            sheet.getRow(93).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
            sheet.getRow(94).createCell(1).setCellValue("Дата: " + end.getValue());

            File fileSave = new File(STORAGE +"report/" + nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue() + ".xlsx");
            FileOutputStream out = new FileOutputStream(fileSave);
            workbook.write(out);
            // Write File
            StreamResource resource = new StreamResource(nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue() + ".xlsx",
                    () -> {
                        try {
                            return new FileInputStream(fileSave);
                        } catch (IOException e) {
                            getLogger().error("" + e);
                            return null;
                        }
                    });

            pdf.setHref(resource);
            pdf.getElement().setAttribute("download", true);

            com.vaadin.flow.component.page.Page page = UI.getCurrent().getPage();
            page.executeJs("document.getElementById('" + "reportDownload" + "').click();");

            inputStream.close();
            //workbook.write(out);
            //out.close();
        } catch (FileNotFoundException e) {
            getLogger().error("" + e);
        } catch (IOException e) {
            getLogger().error("" + e);
        }

    }
    private void saveToVisaTVExcel(Map<String, Map<LocalDate, Integer>> reports){
        File file = new File(STORAGE + "report/visatv/visatv.xlsx");
        // Read XSL file
        try {
            FileInputStream inputStream = new FileInputStream(file);

            // Get the workbook instance for XLS file
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

            // Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            CellStyle cellStyle4 = workbook.createCellStyle();
            cellStyle4.setWrapText(true);

            // text center alignment
            cellStyle4.setAlignment(HorizontalAlignment.CENTER);
            Cell cell = sheet.getRow(2).createCell(2);
            cell.setCellValue(nameDogovor.getValue());
            cell.setCellStyle(cellStyle4);

            sheet.getRow(5).createCell(3).setCellValue(nameRK.getValue());
            sheet.getRow(6).createCell(3).setCellValue(nameRolika.getValue());
            sheet.getRow(7).createCell(3).setCellValue("з " + start.getValue() + " по " + end.getValue());


            //border bold
            CellStyle cellStyleBold = workbook.createCellStyle();

            cellTHICK(cellStyleBold);



            //border smoll
            CellStyle cellStyleSmall = workbook.createCellStyle();

            cellTHIN(cellStyleSmall);
            cellStyleSmall.setAlignment(HorizontalAlignment.CENTER);

            AtomicInteger i = new AtomicInteger(11);
            AtomicInteger countS = new AtomicInteger(1);

            CellStyle cellS = workbook.createCellStyle();
            cellS.setAlignment(HorizontalAlignment.LEFT);

            cellTHICK(cellS);

            reports.forEach((s, localDateIntegerMap) -> {
                System.out.println(s);

                //border bold
                CellStyle cellStyletationBold = workbook.createCellStyle();

                cellTHICK(cellStyletationBold);


                Cell cellStation = sheet.getRow(i.get()).createCell(2);
                cellStation.setCellStyle(cellS);
                cellStation.setCellValue(s);
                cellStation.getCellStyle().setAlignment(HorizontalAlignment.LEFT);
                Cell cellN = sheet.getRow(i.get()).createCell(1);
                cellN.setCellValue(countS.get());
                cellN.setCellStyle(cellStyletationBold);
                countS.getAndIncrement();

                AtomicInteger count = new AtomicInteger();

                XSSFSheet xssfSheet = workbook.cloneSheet(1, stationService.findByAddress(s).getName());
                xssfSheet.getRow(2).createCell(4).setCellValue(nameDogovor.getValue());
                xssfSheet.getRow(4).createCell(3).setCellValue(nameRK.getValue());
                xssfSheet.getRow(5).createCell(3).setCellValue(nameRolika.getValue());
                xssfSheet.getRow(7).createCell(1).setCellValue("Термін проведення рекламної кампанії: "
                        + "з " + start.getValue() + " по " + end.getValue());
                xssfSheet.getRow(8).createCell(1).setCellValue("Місце трансляціїї: " + s);
                xssfSheet.getRow(48).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
                xssfSheet.getRow(49).createCell(1).setCellValue("Дата: " + end.getValue());

                AtomicInteger countDay = new AtomicInteger(12);

                localDateIntegerMap.forEach((localDate, integer) -> {
                    Cell cell1 = xssfSheet.getRow(countDay.get()).createCell(1);
                    cell1.setCellValue(localDate.toString());
                    cell1.setCellStyle(cellStyleSmall);

                    Cell cell2 = xssfSheet.getRow(countDay.get()).createCell(2);
                    cell2.setCellValue(integer);
                    cell2.setCellStyle(cellStyleSmall);

                    Cell cell3 = xssfSheet.getRow(countDay.get()).createCell(3);
                    cell3.setCellValue(integer);
                    cell3.setCellStyle(cellStyleSmall);

                    count.set(count.get() + integer);

                    countDay.getAndIncrement();
                });

                // количество згидно медиа плану
                Cell cellMediaPlan = sheet.getRow(i.get()).createCell(3);
                long days = calcWeekDays(start.getValue(), end.getValue());

                cellMediaPlan.setCellValue(days * 96);
                cellStyleBold.setAlignment(HorizontalAlignment.CENTER);
                cellMediaPlan.setCellStyle(cellStyleBold);

                CellStyle cellStyleCount = workbook.createCellStyle();

                cellTHICK(cellStyleCount);

                // фактическое количество
                cellStyleCount.setAlignment(HorizontalAlignment.CENTER);
                Cell cellCount = sheet.getRow(i.get()).createCell(4);
                cellCount.setCellValue(count.get());
                cellCount.setCellStyle(cellStyleCount);

                Cell cellPohibka = sheet.getRow(i.get()).createCell(5);
                cellPohibka.setCellFormula("E" + (i.get() + 1) + "-" + "D" + (i.get() + 1));
                cellPohibka.setCellStyle(cellStyleCount);

                Cell cellD = xssfSheet.getRow(44).createCell(9);
                cellD.setCellValue(durationVideo.getValue());
                Cell cellSum = xssfSheet.getRow(46).createCell(9);
                cellSum.setCellFormula("SUM(D13:D44)");
                Cell cellRound = xssfSheet.getRow(45).createCell(9);
                cellRound.setCellFormula("ROUND(J45*J47/60,2)");

                i.getAndIncrement();
            });
            workbook.removeSheetAt(1);
            sheet.getRow(49).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
            sheet.getRow(50).createCell(1).setCellValue("Дата: " + end.getValue());

            File fileSave = new File(STORAGE +"report/" + nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue() + ".xlsx");
            FileOutputStream out = new FileOutputStream(fileSave);
            workbook.write(out);
            // Write File
            StreamResource resource = new StreamResource(nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue()+ ".xlsx",
                    () -> {
                        try {
                            return new FileInputStream(fileSave);
                        } catch (IOException e) {
                            getLogger().error("" + e);
                            return null;
                        }
                    });

            pdf.setHref(resource);
            pdf.getElement().setAttribute("download", true);

            com.vaadin.flow.component.page.Page page = UI.getCurrent().getPage();
            page.executeJs("document.getElementById('" + "reportDownload" + "').click();");

            inputStream.close();
            //workbook.write(out);
            //out.close();
        } catch (FileNotFoundException e) {
            getLogger().error("" + e);
        } catch (IOException e) {
            getLogger().error("" + e);
        }

    }

    long calcWeekDays(final LocalDate start, final LocalDate end) {
        final int startW = start.getDayOfWeek().getValue();
        final int endW = end.getDayOfWeek().getValue();

        final long days = ChronoUnit.DAYS.between(start, end);
        long result = days - 2*(days/7); //remove weekends

        if (days % 7 != 0) { //deal with the rest days
            if (startW == 7) {
                result -= 1;
            } else if (endW == 7) {  //they can't both be Sunday, otherwise rest would be zero
                result -= 1;
            } else if (endW < startW) { //another weekend is included
                result -= 2;
            }
        }

        return result;
    }
    private void saveToWOGExcel(Map<String, Map<LocalDate, Integer>> reports){
        File file = new File(STORAGE + "report/azk/wog.xlsx");
        // Read XSL file
        try {
            FileInputStream inputStream = new FileInputStream(file);

            // Get the workbook instance for XLS file
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

            // Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            CellStyle cellStyle4 = workbook.createCellStyle();
            cellStyle4.setWrapText(true);

            // text center alignment
            cellStyle4.setAlignment(HorizontalAlignment.CENTER);
            Cell cell = sheet.getRow(2).createCell(2);
            cell.setCellValue(nameDogovor.getValue());
            cell.setCellStyle(cellStyle4);

            sheet.getRow(5).createCell(3).setCellValue(nameRK.getValue());
            sheet.getRow(6).createCell(3).setCellValue(nameRolika.getValue());
            sheet.getRow(7).createCell(3).setCellValue("з " + start.getValue() + " по " + end.getValue());


            //border bold
            CellStyle cellStyleBold = workbook.createCellStyle();

            cellTHICK(cellStyleBold);

            //border smoll
            CellStyle cellStyleSmall = workbook.createCellStyle();

            cellTHIN(cellStyleSmall);

            AtomicInteger i = new AtomicInteger(11);
            AtomicInteger countS = new AtomicInteger(1);

            reports.forEach((s, localDateIntegerMap) -> {
                System.out.println(s);

                //border bold
                CellStyle cellStyletationBold = workbook.createCellStyle();

                cellTHICK(cellStyletationBold);


                Cell cellStation = sheet.getRow(i.get()).createCell(2);
                cellStation.setCellValue(s);
                cellStation.setCellStyle(cellStyletationBold);
                Cell cellN = sheet.getRow(i.get()).createCell(1);
                cellN.setCellValue(countS.get());
                cellN.setCellStyle(cellStyletationBold);
                countS.getAndIncrement();

                AtomicInteger count = new AtomicInteger();

                XSSFSheet xssfSheet = workbook.cloneSheet(1, stationService.findByAddress(s).getName());
                xssfSheet.getRow(4).createCell(3).setCellValue(nameRK.getValue());
                xssfSheet.getRow(5).createCell(3).setCellValue(nameRolika.getValue());
                xssfSheet.getRow(7).createCell(1).setCellValue("Термін проведення рекламної кампанії: "
                        + "з " + start.getValue() + " по " + end.getValue());
                xssfSheet.getRow(8).createCell(1).setCellValue("Місце трансляціїї: " + s);
                xssfSheet.getRow(48).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
                xssfSheet.getRow(49).createCell(1).setCellValue("Дата: " + end.getValue());

                AtomicInteger countDay = new AtomicInteger(12);

                localDateIntegerMap.forEach((localDate, integer) -> {
                    Cell cell1 = xssfSheet.getRow(countDay.get()).createCell(1);
                    cell1.setCellValue(localDate.toString());
                    cell1.setCellStyle(cellStyleSmall);

                    Cell cell2 = xssfSheet.getRow(countDay.get()).createCell(2);
                    cell2.setCellValue(integer);
                    cell2.setCellStyle(cellStyleSmall);

                    Cell cell3 = xssfSheet.getRow(countDay.get()).createCell(3);
                    cell3.setCellValue(integer);
                    cell3.setCellStyle(cellStyleSmall);

                    count.set(count.get() + integer);

                    countDay.getAndIncrement();
                });

                // количество згидно медиа плану
                Cell cellMediaPlan = sheet.getRow(i.get()).createCell(3);
                long days = start.getValue().until(end.getValue(), ChronoUnit.DAYS) + 1;
                cellMediaPlan.setCellValue(days * 252);
                cellStyleBold.setAlignment(HorizontalAlignment.CENTER);
                cellMediaPlan.setCellStyle(cellStyleBold);

                CellStyle cellStyleCount = workbook.createCellStyle();

                cellTHICK(cellStyleCount);

                // фактическое количество
                cellStyleCount.setAlignment(HorizontalAlignment.CENTER);
                Cell cellCount = sheet.getRow(i.get()).createCell(4);
                cellCount.setCellValue(count.get());
                cellCount.setCellStyle(cellStyleCount);

                Cell cellPohibka = sheet.getRow(i.get()).createCell(5);
                cellPohibka.setCellFormula("E" + (i.get() + 1) + "-" + "D" + (i.get() + 1));
                cellPohibka.setCellStyle(cellStyleCount);

                Cell cellD = xssfSheet.getRow(44).createCell(9);
                cellD.setCellValue(durationVideo.getValue());
                Cell cellSum = xssfSheet.getRow(46).createCell(9);
                cellSum.setCellFormula("SUM(D13:D43)");
                Cell cellRound = xssfSheet.getRow(45).createCell(9);
                cellRound.setCellFormula("ROUND(J45*J47/60,2)");

                i.getAndIncrement();
            });
            workbook.removeSheetAt(1);
            sheet.getRow(111).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
            sheet.getRow(112).createCell(1).setCellValue("Дата: " + end.getValue());

            File fileSave = new File(STORAGE +"report/" + nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue() + ".xlsx");
            FileOutputStream out = new FileOutputStream(fileSave);
            workbook.write(out);
            // Write File
            StreamResource resource = new StreamResource(nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue()+ ".xlsx",
                    () -> {
                        try {
                            return new FileInputStream(fileSave);
                        } catch (IOException e) {
                            getLogger().error("" + e);
                            return null;
                        }
                    });

            pdf.setHref(resource);
            pdf.getElement().setAttribute("download", true);

            com.vaadin.flow.component.page.Page page = UI.getCurrent().getPage();
            page.executeJs("document.getElementById('" + "reportDownload" + "').click();");

            inputStream.close();
            //workbook.write(out);
            //out.close();
        } catch (FileNotFoundException e) {
           getLogger().error("" + e);
        } catch (IOException e) {
            getLogger().error("" + e);
        }

    }
    private void saveToSkyMallExcel(Map<String, Map<LocalDate, Integer>> reports){
        File file = new File(STORAGE + "report/trc/skymall.xlsx");
        // Read XSL file
        try {
            FileInputStream inputStream = new FileInputStream(file);

            // Get the workbook instance for XLS file
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

            // Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            CellStyle cellStyle4 = workbook.createCellStyle();
            cellStyle4.setWrapText(true);

            // text center alignment
            cellStyle4.setAlignment(HorizontalAlignment.CENTER);
            Cell cell = sheet.getRow(2).createCell(2);
            cell.setCellValue(nameDogovor.getValue());
            cell.setCellStyle(cellStyle4);

            sheet.getRow(5).createCell(3).setCellValue(nameRK.getValue());
            sheet.getRow(6).createCell(3).setCellValue(nameRolika.getValue());
            sheet.getRow(7).createCell(3).setCellValue("з " + start.getValue() + " по " + end.getValue());


            //border bold
            CellStyle cellStyleBold = workbook.createCellStyle();

            cellTHICK(cellStyleBold);
            //border smoll
            CellStyle cellStyleSmall = workbook.createCellStyle();

            cellTHIN(cellStyleSmall);

            AtomicInteger i = new AtomicInteger(11);

            reports.forEach((s, localDateIntegerMap) -> {
                System.out.println(s);

                //border bold
                CellStyle cellStyletationBold = workbook.createCellStyle();

                cellTHICK(cellStyletationBold);


                Cell cellStation = sheet.getRow(i.get()).createCell(2);
                cellStation.setCellValue(s);
                cellStation.setCellStyle(cellStyletationBold);

                AtomicInteger count = new AtomicInteger();

                XSSFSheet xssfSheet = workbook.cloneSheet(1, stationService.findByAddress(s).getName());
                xssfSheet.getRow(4).createCell(3).setCellValue(nameRK.getValue());
                xssfSheet.getRow(5).createCell(3).setCellValue(nameRolika.getValue());
                xssfSheet.getRow(7).createCell(1).setCellValue("Термін проведення рекламної кампанії: "
                        + "з " + start.getValue() + " по " + end.getValue());
                xssfSheet.getRow(8).createCell(1).setCellValue("Місце трансляціїї: " + s);
                xssfSheet.getRow(48).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
                xssfSheet.getRow(49).createCell(1).setCellValue("Дата: " + end.getValue());

                AtomicInteger countDay = new AtomicInteger(12);

                localDateIntegerMap.forEach((localDate, integer) -> {
                    Cell cell1 = xssfSheet.getRow(countDay.get()).createCell(1);
                    cell1.setCellValue(localDate.toString());
                    cell1.setCellStyle(cellStyleSmall);

                    Cell cell2 = xssfSheet.getRow(countDay.get()).createCell(2);
                    cell2.setCellValue(integer);
                    cell2.setCellStyle(cellStyleSmall);

                    Cell cell3 = xssfSheet.getRow(countDay.get()).createCell(3);
                    cell3.setCellValue(integer);
                    cell3.setCellStyle(cellStyleSmall);

                    count.set(count.get() + integer);

                    countDay.getAndIncrement();
                });

                // количество згидно медиа плану
                Cell cellMediaPlan = sheet.getRow(i.get()).createCell(3);
                long days = start.getValue().until(end.getValue(), ChronoUnit.DAYS) + 1;
                Station station = stationService.findById(stationService.findByAddress(s).getId());
                if (station.getStationSetting().getMonday().getHour() == 3){
                    cellMediaPlan.setCellValue(days * 252);
                } else {
                    cellMediaPlan.setCellValue(days * 144);
                }
                cellStyleBold.setAlignment(HorizontalAlignment.CENTER);
                cellMediaPlan.setCellStyle(cellStyleBold);

                CellStyle cellStyleCount = workbook.createCellStyle();

                cellTHICK(cellStyleCount);

                // фактическое количество
                cellStyleCount.setAlignment(HorizontalAlignment.CENTER);
                Cell cellCount = sheet.getRow(i.get()).createCell(4);
                cellCount.setCellValue(count.get());
                cellCount.setCellStyle(cellStyleCount);

                Cell cellPohibka = sheet.getRow(i.get()).createCell(5);
                cellPohibka.setCellFormula("E" + (i.get() + 1) + "-" + "D" + (i.get() + 1));
                cellPohibka.setCellStyle(cellStyleCount);

                Cell cellD = xssfSheet.getRow(44).createCell(9);
                cellD.setCellValue(durationVideo.getValue());
                Cell cellSum = xssfSheet.getRow(46).createCell(9);
                cellSum.setCellFormula("SUM(D13:D43)");
                Cell cellRound = xssfSheet.getRow(45).createCell(9);
                cellRound.setCellFormula("ROUND(J45*J47/60,2)");

                i.getAndIncrement();
            });
            workbook.removeSheetAt(1);
            sheet.getRow(30).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
            sheet.getRow(31).createCell(1).setCellValue("Дата: " + end.getValue());
            File fileSave = new File(STORAGE +"report/" + nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue() + ".xlsx");
            FileOutputStream out = new FileOutputStream(fileSave);
            workbook.write(out);
            // Write File
            StreamResource resource = new StreamResource(nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue()+ ".xlsx",
                    () -> {
                        try {
                            return new FileInputStream(fileSave);
                        } catch (IOException e) {
                            getLogger().error("" + e);
                            return null;
                        }
                    });

            pdf.setHref(resource);
            pdf.getElement().setAttribute("download", true);

            com.vaadin.flow.component.page.Page page = UI.getCurrent().getPage();
            page.executeJs("document.getElementById('" + "reportDownload" + "').click();");

            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private void saveToZlataPlazaExcel(Map<String, Map<LocalDate, Integer>> reports){
        File file = new File(STORAGE + "report/trc/zlataplaza.xlsx");
        // Read XSL file
        try {
            FileInputStream inputStream = new FileInputStream(file);

            // Get the workbook instance for XLS file
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

            // Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            CellStyle cellStyle4 = workbook.createCellStyle();
            cellStyle4.setWrapText(true);

            // text center alignment
            cellStyle4.setAlignment(HorizontalAlignment.CENTER);
            Cell cell = sheet.getRow(2).createCell(2);
            cell.setCellValue(nameDogovor.getValue());
            cell.setCellStyle(cellStyle4);

            sheet.getRow(5).createCell(3).setCellValue(nameRK.getValue());
            sheet.getRow(6).createCell(3).setCellValue(nameRolika.getValue());
            sheet.getRow(7).createCell(3).setCellValue("з " + start.getValue() + " по " + end.getValue());


            //border bold
            CellStyle cellStyleBold = workbook.createCellStyle();

            cellTHICK(cellStyleBold);

            //border smoll
            CellStyle cellStyleSmall = workbook.createCellStyle();

            cellTHIN(cellStyleSmall);

            AtomicInteger i = new AtomicInteger(11);

            reports.forEach((s, localDateIntegerMap) -> {
                System.out.println(s);

                //border bold
                CellStyle cellStyletationBold = workbook.createCellStyle();

                cellTHICK(cellStyletationBold);


                Cell cellStation = sheet.getRow(i.get()).createCell(2);
                cellStation.setCellValue(s);
                cellStation.setCellStyle(cellStyletationBold);

                AtomicInteger count = new AtomicInteger();

                XSSFSheet xssfSheet = workbook.cloneSheet(1, stationService.findByAddress(s).getName());
                xssfSheet.getRow(4).createCell(3).setCellValue(nameRK.getValue());
                xssfSheet.getRow(5).createCell(3).setCellValue(nameRolika.getValue());
                xssfSheet.getRow(7).createCell(1).setCellValue("Термін проведення рекламної кампанії: "
                        + "з " + start.getValue() + " по " + end.getValue());
                xssfSheet.getRow(8).createCell(1).setCellValue("Місце трансляціїї: " + s);
                xssfSheet.getRow(48).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
                xssfSheet.getRow(49).createCell(1).setCellValue("Дата: " + end.getValue());

                AtomicInteger countDay = new AtomicInteger(12);

                localDateIntegerMap.forEach((localDate, integer) -> {
                    Cell cell1 = xssfSheet.getRow(countDay.get()).createCell(1);
                    cell1.setCellValue(localDate.toString());
                    cell1.setCellStyle(cellStyleSmall);

                    Cell cell2 = xssfSheet.getRow(countDay.get()).createCell(2);
                    cell2.setCellValue(integer);
                    cell2.setCellStyle(cellStyleSmall);

                    Cell cell3 = xssfSheet.getRow(countDay.get()).createCell(3);
                    cell3.setCellValue(integer);
                    cell3.setCellStyle(cellStyleSmall);

                    count.set(count.get() + integer);

                    countDay.getAndIncrement();
                });

                // количество згидно медиа плану
                Cell cellMediaPlan = sheet.getRow(i.get()).createCell(3);
                long days = start.getValue().until(end.getValue(), ChronoUnit.DAYS) + 1;
                Station station = stationService.findById(stationService.findByAddress(s).getId());
                if (station.getStationSetting().getMonday().getHour() == 3){
                    cellMediaPlan.setCellValue(days * 252);
                } else {
                    cellMediaPlan.setCellValue(days * 132);
                }
                cellStyleBold.setAlignment(HorizontalAlignment.CENTER);
                cellMediaPlan.setCellStyle(cellStyleBold);

                CellStyle cellStyleCount = workbook.createCellStyle();

                cellTHICK(cellStyleCount);

                // фактическое количество
                cellStyleCount.setAlignment(HorizontalAlignment.CENTER);
                Cell cellCount = sheet.getRow(i.get()).createCell(4);
                cellCount.setCellValue(count.get());
                cellCount.setCellStyle(cellStyleCount);

                Cell cellPohibka = sheet.getRow(i.get()).createCell(5);
                cellPohibka.setCellFormula("E" + (i.get() + 1) + "-" + "D" + (i.get() + 1));
                cellPohibka.setCellStyle(cellStyleCount);

                Cell cellD = xssfSheet.getRow(44).createCell(9);
                cellD.setCellValue(durationVideo.getValue());
                Cell cellSum = xssfSheet.getRow(46).createCell(9);
                cellSum.setCellFormula("SUM(D13:D43)");
                Cell cellRound = xssfSheet.getRow(45).createCell(9);
                cellRound.setCellFormula("ROUND(J45*J47/60,2)");

                i.getAndIncrement();
            });
            workbook.removeSheetAt(1);
            sheet.getRow(22).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
            sheet.getRow(23).createCell(1).setCellValue("Дата: " + end.getValue());
            File fileSave = new File(STORAGE +"report/" + nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue() + ".xlsx");
            FileOutputStream out = new FileOutputStream(fileSave);
            workbook.write(out);
            // Write File
            StreamResource resource = new StreamResource(nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue()+ ".xlsx",
                    () -> {
                        try {
                            return new FileInputStream(fileSave);
                        } catch (IOException e) {
                            getLogger().error("" + e);
                            return null;
                        }
                    });

            pdf.setHref(resource);
            pdf.getElement().setAttribute("download", true);

            com.vaadin.flow.component.page.Page page = UI.getCurrent().getPage();
            page.executeJs("document.getElementById('" + "reportDownload" + "').click();");

            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private void saveToGulliverExcel(Map<String, Map<LocalDate, Integer>> reports){
        File file = new File(STORAGE + "report/trc/gulliver.xlsx");
        // Read XSL file
        try {
            FileInputStream inputStream = new FileInputStream(file);

            // Get the workbook instance for XLS file
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

            // Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            CellStyle cellStyle4 = workbook.createCellStyle();
            cellStyle4.setWrapText(true);

            // text center alignment
            cellStyle4.setAlignment(HorizontalAlignment.CENTER);
            Cell cell = sheet.getRow(2).createCell(2);
            cell.setCellValue(nameDogovor.getValue());
            cell.setCellStyle(cellStyle4);

            sheet.getRow(5).createCell(3).setCellValue(nameRK.getValue());
            sheet.getRow(6).createCell(3).setCellValue(nameRolika.getValue());
            sheet.getRow(7).createCell(3).setCellValue("з " + start.getValue() + " по " + end.getValue());


            //border bold
            CellStyle cellStyleBold = workbook.createCellStyle();

            cellTHICK(cellStyleBold);

            //border smoll
            CellStyle cellStyleSmall = workbook.createCellStyle();

            cellTHIN(cellStyleSmall);
            cellStyleSmall.setAlignment(HorizontalAlignment.CENTER);

            AtomicInteger i = new AtomicInteger(11);

            reports.forEach((s, localDateIntegerMap) -> {
                System.out.println(s);

                //border bold
                CellStyle cellStyletationBold = workbook.createCellStyle();

                cellTHICK(cellStyletationBold);


                Cell cellStation = sheet.getRow(i.get()).createCell(2);
                cellStation.setCellValue(s);
                cellStation.setCellStyle(cellStyletationBold);

                AtomicInteger count = new AtomicInteger();

                XSSFSheet xssfSheet = workbook.cloneSheet(1, stationService.findByAddress(s).getName());
                xssfSheet.getRow(4).createCell(3).setCellValue(nameRK.getValue());
                xssfSheet.getRow(5).createCell(3).setCellValue(nameRolika.getValue());
                xssfSheet.getRow(7).createCell(1).setCellValue("Термін проведення рекламної кампанії: "
                        + "з " + start.getValue() + " по " + end.getValue());
                xssfSheet.getRow(8).createCell(1).setCellValue("Місце трансляціїї: " + s);
                xssfSheet.getRow(48).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
                xssfSheet.getRow(49).createCell(1).setCellValue("Дата: " + end.getValue());

                AtomicInteger countDay = new AtomicInteger(12);

                localDateIntegerMap.forEach((localDate, integer) -> {
                    Cell cell1 = xssfSheet.getRow(countDay.get()).createCell(1);
                    cell1.setCellValue(localDate.toString());
                    cell1.setCellStyle(cellStyleSmall);

                    Cell cell2 = xssfSheet.getRow(countDay.get()).createCell(2);
                    cell2.setCellValue(integer);
                    cell2.setCellStyle(cellStyleSmall);

                    Cell cell3 = xssfSheet.getRow(countDay.get()).createCell(3);
                    cell3.setCellValue(integer);
                    cell3.setCellStyle(cellStyleSmall);

                    count.set(count.get() + integer);

                    countDay.getAndIncrement();
                });

                // количество згидно медиа плану
                Cell cellMediaPlan = sheet.getRow(i.get()).createCell(3);
                long days = start.getValue().until(end.getValue(), ChronoUnit.DAYS) + 1;
                Station station = stationService.findById(stationService.findByAddress(s).getId());
                if (station.getStationSetting().getMonday().getHour() == 3){
                    cellMediaPlan.setCellValue(days * 252);
                } else {
                    cellMediaPlan.setCellValue(days * 144);
                }
                cellStyleBold.setAlignment(HorizontalAlignment.CENTER);
                cellMediaPlan.setCellStyle(cellStyleBold);

                CellStyle cellStyleCount = workbook.createCellStyle();

                //border
                cellTHICK(cellStyleCount);

                // фактическое количество
                cellStyleCount.setAlignment(HorizontalAlignment.CENTER);
                Cell cellCount = sheet.getRow(i.get()).createCell(4);
                cellCount.setCellValue(count.get());
                cellCount.setCellStyle(cellStyleCount);

                Cell cellPohibka = sheet.getRow(i.get()).createCell(5);
                cellPohibka.setCellFormula("E" + (i.get() + 1) + "-" + "D" + (i.get() + 1));
                cellPohibka.setCellStyle(cellStyleCount);

                Cell cellD = xssfSheet.getRow(44).createCell(9);
                cellD.setCellValue(durationVideo.getValue());
                Cell cellSum = xssfSheet.getRow(46).createCell(9);
                cellSum.setCellFormula("SUM(D13:D43)");
                Cell cellRound = xssfSheet.getRow(45).createCell(9);
                cellRound.setCellFormula("ROUND(J45*J47/60,2)");

                i.getAndIncrement();
            });
            workbook.removeSheetAt(1);
            sheet.getRow(39).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
            sheet.getRow(40).createCell(1).setCellValue("Дата: " + end.getValue());
            File fileSave = new File(STORAGE +"report/" + nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue() + ".xlsx");
            FileOutputStream out = new FileOutputStream(fileSave);
            workbook.write(out);
            // Write File
            StreamResource resource = new StreamResource(nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue()+ ".xlsx",
                    () -> {
                        try {
                            return new FileInputStream(fileSave);
                        } catch (IOException e) {
                            getLogger().error("" + e);
                            return null;
                        }
                    });

            pdf.setHref(resource);
            pdf.getElement().setAttribute("download", true);

            com.vaadin.flow.component.page.Page page = UI.getCurrent().getPage();
            page.executeJs("document.getElementById('" + "reportDownload" + "').click();");

            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private void saveToRayonExcel(Map<String, Map<LocalDate, Integer>> reports){
        File file = new File(STORAGE + "report/trc/rayon.xlsx");
        // Read XSL file
        try {
            FileInputStream inputStream = new FileInputStream(file);

            // Get the workbook instance for XLS file
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

            // Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            CellStyle cellStyle4 = workbook.createCellStyle();
            cellStyle4.setWrapText(true);

            // text center alignment
            cellStyle4.setAlignment(HorizontalAlignment.CENTER);
            Cell cell = sheet.getRow(2).createCell(2);
            cell.setCellValue(nameDogovor.getValue());
            cell.setCellStyle(cellStyle4);

            sheet.getRow(5).createCell(3).setCellValue(nameRK.getValue());
            sheet.getRow(6).createCell(3).setCellValue(nameRolika.getValue());
            sheet.getRow(7).createCell(3).setCellValue("з " + start.getValue() + " по " + end.getValue());


            //border bold
            CellStyle cellStyleBold = workbook.createCellStyle();

            cellTHICK(cellStyleBold);

            //border smoll
            CellStyle cellStyleSmall = workbook.createCellStyle();

            cellTHIN(cellStyleSmall);
            cellStyleSmall.setAlignment(HorizontalAlignment.CENTER);

            AtomicInteger i = new AtomicInteger(11);

            reports.forEach((s, localDateIntegerMap) -> {
                System.out.println(s);

                //border bold
                CellStyle cellStyletationBold = workbook.createCellStyle();

                cellTHICK(cellStyletationBold);


                Cell cellStation = sheet.getRow(i.get()).createCell(2);
                cellStation.setCellValue(s);
                cellStation.setCellStyle(cellStyletationBold);

                AtomicInteger count = new AtomicInteger();

                XSSFSheet xssfSheet = workbook.cloneSheet(1, stationService.findByAddress(s).getName());
                xssfSheet.getRow(4).createCell(3).setCellValue(nameRK.getValue());
                xssfSheet.getRow(5).createCell(3).setCellValue(nameRolika.getValue());
                xssfSheet.getRow(7).createCell(1).setCellValue("Термін проведення рекламної кампанії: "
                        + "з " + start.getValue() + " по " + end.getValue());
                xssfSheet.getRow(8).createCell(1).setCellValue("Місце трансляціїї: " + s);
                xssfSheet.getRow(48).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
                xssfSheet.getRow(49).createCell(1).setCellValue("Дата: " + end.getValue());

                AtomicInteger countDay = new AtomicInteger(12);

                localDateIntegerMap.forEach((localDate, integer) -> {
                    Cell cell1 = xssfSheet.getRow(countDay.get()).createCell(1);
                    cell1.setCellValue(localDate.toString());
                    cell1.setCellStyle(cellStyleSmall);

                    Cell cell2 = xssfSheet.getRow(countDay.get()).createCell(2);
                    cell2.setCellValue(integer);
                    cell2.setCellStyle(cellStyleSmall);

                    Cell cell3 = xssfSheet.getRow(countDay.get()).createCell(3);
                    cell3.setCellValue(integer);
                    cell3.setCellStyle(cellStyleSmall);

                    count.set(count.get() + integer);

                    countDay.getAndIncrement();
                });

                // количество згидно медиа плану
                Cell cellMediaPlan = sheet.getRow(i.get()).createCell(3);
                long days = start.getValue().until(end.getValue(), ChronoUnit.DAYS) + 1;
                Station station = stationService.findById(stationService.findByAddress(s).getId());
                if (station.getStationSetting().getMonday().getHour() == 3){
                    cellMediaPlan.setCellValue(days * 252);
                } else {
                    cellMediaPlan.setCellValue(days * 144);
                }
                cellStyleBold.setAlignment(HorizontalAlignment.CENTER);
                cellMediaPlan.setCellStyle(cellStyleBold);

                CellStyle cellStyleCount = workbook.createCellStyle();

                //border
                cellTHICK(cellStyleCount);

                // фактическое количество
                cellStyleCount.setAlignment(HorizontalAlignment.CENTER);
                Cell cellCount = sheet.getRow(i.get()).createCell(4);
                cellCount.setCellValue(count.get());
                cellCount.setCellStyle(cellStyleCount);

                Cell cellPohibka = sheet.getRow(i.get()).createCell(5);
                cellPohibka.setCellFormula("E" + (i.get() + 1) + "-" + "D" + (i.get() + 1));
                cellPohibka.setCellStyle(cellStyleCount);

                Cell cellD = xssfSheet.getRow(44).createCell(9);
                cellD.setCellValue(durationVideo.getValue());
                Cell cellSum = xssfSheet.getRow(46).createCell(9);
                cellSum.setCellFormula("SUM(D13:D43)");
                Cell cellRound = xssfSheet.getRow(45).createCell(9);
                cellRound.setCellFormula("ROUND(J45*J47/60,2)");

                i.getAndIncrement();
            });
            workbook.removeSheetAt(1);
            sheet.getRow(24).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
            sheet.getRow(25).createCell(1).setCellValue("Дата: " + end.getValue());
            File fileSave = new File(STORAGE +"report/" + nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue() + ".xlsx");
            FileOutputStream out = new FileOutputStream(fileSave);
            workbook.write(out);
            // Write File
            StreamResource resource = new StreamResource(nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue()+ ".xlsx",
                    () -> {
                        try {
                            return new FileInputStream(fileSave);
                        } catch (IOException e) {
                            getLogger().error("" + e);
                            return null;
                        }
                    });

            pdf.setHref(resource);
            pdf.getElement().setAttribute("download", true);

            com.vaadin.flow.component.page.Page page = UI.getCurrent().getPage();
            page.executeJs("document.getElementById('" + "reportDownload" + "').click();");

            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private void saveToProspektExcel(Map<String, Map<LocalDate, Integer>> reports){
        File file = new File(STORAGE + "report/trc/prospekt.xlsx");
        // Read XSL file
        try {
            FileInputStream inputStream = new FileInputStream(file);

            // Get the workbook instance for XLS file
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

            // Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            CellStyle cellStyle4 = workbook.createCellStyle();
            cellStyle4.setWrapText(true);

            // text center alignment
            cellStyle4.setAlignment(HorizontalAlignment.CENTER);
            Cell cell = sheet.getRow(2).createCell(2);
            cell.setCellValue(nameDogovor.getValue());
            cell.setCellStyle(cellStyle4);

            sheet.getRow(5).createCell(3).setCellValue(nameRK.getValue());
            sheet.getRow(6).createCell(3).setCellValue(nameRolika.getValue());
            sheet.getRow(7).createCell(3).setCellValue("з " + start.getValue() + " по " + end.getValue());


            //border bold
            CellStyle cellStyleBold = workbook.createCellStyle();

            cellTHICK(cellStyleBold);

            //border smoll
            CellStyle cellStyleSmall = workbook.createCellStyle();

            cellTHIN(cellStyleSmall);

            AtomicInteger i = new AtomicInteger(11);

            reports.forEach((s, localDateIntegerMap) -> {
                System.out.println(s);

                //border bold
                CellStyle cellStyletationBold = workbook.createCellStyle();

                cellTHICK(cellStyletationBold);


                Cell cellStation = sheet.getRow(i.get()).createCell(2);
                cellStation.setCellValue(s);
                cellStation.setCellStyle(cellStyletationBold);

                AtomicInteger count = new AtomicInteger();

                XSSFSheet xssfSheet = workbook.cloneSheet(1, stationService.findByAddress(s).getName());
                xssfSheet.getRow(4).createCell(3).setCellValue(nameRK.getValue());
                xssfSheet.getRow(5).createCell(3).setCellValue(nameRolika.getValue());
                xssfSheet.getRow(7).createCell(1).setCellValue("Термін проведення рекламної кампанії: "
                        + "з " + start.getValue() + " по " + end.getValue());
                xssfSheet.getRow(8).createCell(1).setCellValue("Місце трансляціїї: " + s);
                xssfSheet.getRow(48).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
                xssfSheet.getRow(49).createCell(1).setCellValue("Дата: " + end.getValue());

                AtomicInteger countDay = new AtomicInteger(12);

                localDateIntegerMap.forEach((localDate, integer) -> {
                    Cell cell1 = xssfSheet.getRow(countDay.get()).createCell(1);
                    cell1.setCellValue(localDate.toString());
                    cell1.setCellStyle(cellStyleSmall);

                    Cell cell2 = xssfSheet.getRow(countDay.get()).createCell(2);
                    cell2.setCellValue(integer);
                    cell2.setCellStyle(cellStyleSmall);

                    Cell cell3 = xssfSheet.getRow(countDay.get()).createCell(3);
                    cell3.setCellValue(integer);
                    cell3.setCellStyle(cellStyleSmall);

                    count.set(count.get() + integer);

                    countDay.getAndIncrement();
                });

                // количество згидно медиа плану
                Cell cellMediaPlan = sheet.getRow(i.get()).createCell(3);
                long days = start.getValue().until(end.getValue(), ChronoUnit.DAYS) + 1;
                Station station = stationService.findById(stationService.findByAddress(s).getId());
                if (station.getStationSetting().getMonday().getHour() == 3){
                    cellMediaPlan.setCellValue(days * 252);
                } else {
                    cellMediaPlan.setCellValue(days * 144);
                }
                cellStyleBold.setAlignment(HorizontalAlignment.CENTER);
                cellMediaPlan.setCellStyle(cellStyleBold);

                CellStyle cellStyleCount = workbook.createCellStyle();

                //border
                cellTHICK(cellStyleCount);

                // фактическое количество
                cellStyleCount.setAlignment(HorizontalAlignment.CENTER);
                Cell cellCount = sheet.getRow(i.get()).createCell(4);
                cellCount.setCellValue(count.get());
                cellCount.setCellStyle(cellStyleCount);

                Cell cellPohibka = sheet.getRow(i.get()).createCell(5);
                cellPohibka.setCellFormula("E" + (i.get() + 1) + "-" + "D" + (i.get() + 1));
                cellPohibka.setCellStyle(cellStyleCount);

                Cell cellD = xssfSheet.getRow(44).createCell(9);
                cellD.setCellValue(durationVideo.getValue());
                Cell cellSum = xssfSheet.getRow(46).createCell(9);
                cellSum.setCellFormula("SUM(D13:D43)");
                Cell cellRound = xssfSheet.getRow(45).createCell(9);
                cellRound.setCellFormula("ROUND(J45*J47/60,2)");

                i.getAndIncrement();
            });
            workbook.removeSheetAt(1);
            sheet.getRow(26).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
            sheet.getRow(27).createCell(1).setCellValue("Дата: " + end.getValue());
            File fileSave = new File(STORAGE +"report/" + nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue() + ".xlsx");
            FileOutputStream out = new FileOutputStream(fileSave);
            workbook.write(out);
            // Write File
            StreamResource resource = new StreamResource(nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue()+ ".xlsx",
                    () -> {
                        try {
                            return new FileInputStream(fileSave);
                        } catch (IOException e) {
                            getLogger().error("" + e);
                            return null;
                        }
                    });

            pdf.setHref(resource);
            pdf.getElement().setAttribute("download", true);

            com.vaadin.flow.component.page.Page page = UI.getCurrent().getPage();
            page.executeJs("document.getElementById('" + "reportDownload" + "').click();");

            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private void saveToBulvarExcel(Map<String, Map<LocalDate, Integer>> reports){
        File file = new File(STORAGE + "report/trc/bulvar.xlsx");
        // Read XSL file
        try {
            FileInputStream inputStream = new FileInputStream(file);

            // Get the workbook instance for XLS file
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

            // Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            CellStyle cellStyle4 = workbook.createCellStyle();
            cellStyle4.setWrapText(true);

            // text center alignment
            cellStyle4.setAlignment(HorizontalAlignment.CENTER);
            Cell cell = sheet.getRow(2).createCell(2);
            cell.setCellValue(nameDogovor.getValue());
            cell.setCellStyle(cellStyle4);

            sheet.getRow(5).createCell(3).setCellValue(nameRK.getValue());
            sheet.getRow(6).createCell(3).setCellValue(nameRolika.getValue());
            sheet.getRow(7).createCell(3).setCellValue("з " + start.getValue() + " по " + end.getValue());


            //border bold
            CellStyle cellStyleBold = workbook.createCellStyle();

            cellTHICK(cellStyleBold);

            //border smoll
            CellStyle cellStyleSmall = workbook.createCellStyle();

            cellTHIN(cellStyleSmall);

            AtomicInteger i = new AtomicInteger(11);

            reports.forEach((s, localDateIntegerMap) -> {
                System.out.println(s);

                //border bold
                CellStyle cellStyletationBold = workbook.createCellStyle();

                cellTHICK(cellStyletationBold);


                Cell cellStation = sheet.getRow(i.get()).createCell(2);
                cellStation.setCellValue(s);
                cellStation.setCellStyle(cellStyletationBold);

                AtomicInteger count = new AtomicInteger();

                XSSFSheet xssfSheet = workbook.cloneSheet(1, stationService.findByAddress(s).getName());
                xssfSheet.getRow(4).createCell(3).setCellValue(nameRK.getValue());
                xssfSheet.getRow(5).createCell(3).setCellValue(nameRolika.getValue());
                xssfSheet.getRow(7).createCell(1).setCellValue("Термін проведення рекламної кампанії: "
                        + "з " + start.getValue() + " по " + end.getValue());
                xssfSheet.getRow(8).createCell(1).setCellValue("Місце трансляціїї: " + s);
                xssfSheet.getRow(48).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
                xssfSheet.getRow(49).createCell(1).setCellValue("Дата: " + end.getValue());

                AtomicInteger countDay = new AtomicInteger(12);

                localDateIntegerMap.forEach((localDate, integer) -> {
                    Cell cell1 = xssfSheet.getRow(countDay.get()).createCell(1);
                    cell1.setCellValue(localDate.toString());
                    cell1.setCellStyle(cellStyleSmall);

                    Cell cell2 = xssfSheet.getRow(countDay.get()).createCell(2);
                    cell2.setCellValue(integer);
                    cell2.setCellStyle(cellStyleSmall);

                    Cell cell3 = xssfSheet.getRow(countDay.get()).createCell(3);
                    cell3.setCellValue(integer);
                    cell3.setCellStyle(cellStyleSmall);

                    count.set(count.get() + integer);

                    countDay.getAndIncrement();
                });

                // количество згидно медиа плану
                Cell cellMediaPlan = sheet.getRow(i.get()).createCell(3);
                long days = start.getValue().until(end.getValue(), ChronoUnit.DAYS) + 1;
                Station station = stationService.findById(stationService.findByAddress(s).getId());
                if (station.getStationSetting().getMonday().getHour() == 3){
                    cellMediaPlan.setCellValue(days * 252);
                } else {
                    cellMediaPlan.setCellValue(days * 156);
                }
                cellStyleBold.setAlignment(HorizontalAlignment.CENTER);
                cellMediaPlan.setCellStyle(cellStyleBold);

                CellStyle cellStyleCount = workbook.createCellStyle();

                //border
                cellTHICK(cellStyleCount);

                // фактическое количество
                cellStyleCount.setAlignment(HorizontalAlignment.CENTER);
                Cell cellCount = sheet.getRow(i.get()).createCell(4);
                cellCount.setCellValue(count.get());
                cellCount.setCellStyle(cellStyleCount);

                Cell cellPohibka = sheet.getRow(i.get()).createCell(5);
                cellPohibka.setCellFormula("E" + (i.get() + 1) + "-" + "D" + (i.get() + 1));
                cellPohibka.setCellStyle(cellStyleCount);

                Cell cellD = xssfSheet.getRow(44).createCell(9);
                cellD.setCellValue(durationVideo.getValue());
                Cell cellSum = xssfSheet.getRow(46).createCell(9);
                cellSum.setCellFormula("SUM(D13:D43)");
                Cell cellRound = xssfSheet.getRow(45).createCell(9);
                cellRound.setCellFormula("ROUND(J45*J47/60,2)");

                i.getAndIncrement();
            });
            workbook.removeSheetAt(1);
            sheet.getRow(29).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
            sheet.getRow(30).createCell(1).setCellValue("Дата: " + end.getValue());
            File fileSave = new File(STORAGE +"report/" + nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue() + ".xlsx");
            FileOutputStream out = new FileOutputStream(fileSave);
            workbook.write(out);
            // Write File
            StreamResource resource = new StreamResource(nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue()+ ".xlsx",
                    () -> {
                        try {
                            return new FileInputStream(fileSave);
                        } catch (IOException e) {
                            getLogger().error("" + e);
                            return null;
                        }
                    });

            pdf.setHref(resource);
            pdf.getElement().setAttribute("download", true);

            com.vaadin.flow.component.page.Page page = UI.getCurrent().getPage();
            page.executeJs("document.getElementById('" + "reportDownload" + "').click();");

            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private void saveToRivieraExcel(Map<String, Map<LocalDate, Integer>> reports){
        File file = new File(STORAGE + "report/trc/riviera.xlsx");
        // Read XSL file
        try {
            FileInputStream inputStream = new FileInputStream(file);

            // Get the workbook instance for XLS file
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

            // Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            CellStyle cellStyle4 = workbook.createCellStyle();
            cellStyle4.setWrapText(true);

            // text center alignment
            cellStyle4.setAlignment(HorizontalAlignment.CENTER);
            Cell cell = sheet.getRow(2).createCell(2);
            cell.setCellValue(nameDogovor.getValue());
            cell.setCellStyle(cellStyle4);

            sheet.getRow(5).createCell(3).setCellValue(nameRK.getValue());
            sheet.getRow(6).createCell(3).setCellValue(nameRolika.getValue());
            sheet.getRow(7).createCell(3).setCellValue("з " + start.getValue() + " по " + end.getValue());


            //border bold
            CellStyle cellStyleBold = workbook.createCellStyle();

            cellTHICK(cellStyleBold);

            //border smoll
            CellStyle cellStyleSmall = workbook.createCellStyle();

            cellTHIN(cellStyleSmall);

            AtomicInteger i = new AtomicInteger(11);

            reports.forEach((s, localDateIntegerMap) -> {
                System.out.println(s);

                //border bold
                CellStyle cellStyletationBold = workbook.createCellStyle();

                cellTHICK(cellStyletationBold);


                Cell cellStation = sheet.getRow(i.get()).createCell(2);
                cellStation.setCellValue(s);
                cellStation.setCellStyle(cellStyletationBold);

                AtomicInteger count = new AtomicInteger();

                XSSFSheet xssfSheet = workbook.cloneSheet(1, stationService.findByAddress(s).getName());
                xssfSheet.getRow(4).createCell(3).setCellValue(nameRK.getValue());
                xssfSheet.getRow(5).createCell(3).setCellValue(nameRolika.getValue());
                xssfSheet.getRow(7).createCell(1).setCellValue("Термін проведення рекламної кампанії: "
                        + "з " + start.getValue() + " по " + end.getValue());
                xssfSheet.getRow(8).createCell(1).setCellValue("Місце трансляціїї: " + s);
                xssfSheet.getRow(48).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
                xssfSheet.getRow(49).createCell(1).setCellValue("Дата: " + end.getValue());

                AtomicInteger countDay = new AtomicInteger(12);

                localDateIntegerMap.forEach((localDate, integer) -> {
                    Cell cell1 = xssfSheet.getRow(countDay.get()).createCell(1);
                    cell1.setCellValue(localDate.toString());
                    cell1.setCellStyle(cellStyleSmall);

                    Cell cell2 = xssfSheet.getRow(countDay.get()).createCell(2);
                    cell2.setCellValue(integer);
                    cell2.setCellStyle(cellStyleSmall);

                    Cell cell3 = xssfSheet.getRow(countDay.get()).createCell(3);
                    cell3.setCellValue(integer);
                    cell3.setCellStyle(cellStyleSmall);

                    count.set(count.get() + integer);

                    countDay.getAndIncrement();
                });

                // количество згидно медиа плану
                Cell cellMediaPlan = sheet.getRow(i.get()).createCell(3);
                long days = start.getValue().until(end.getValue(), ChronoUnit.DAYS) + 1;
                Station station = stationService.findById(stationService.findByAddress(s).getId());
                if (station.getStationSetting().getMonday().getHour() == 3){
                    cellMediaPlan.setCellValue(days * 252);
                } else {
                    cellMediaPlan.setCellValue(days * 144);
                }
                cellStyleBold.setAlignment(HorizontalAlignment.CENTER);
                cellMediaPlan.setCellStyle(cellStyleBold);

                CellStyle cellStyleCount = workbook.createCellStyle();
                cellTHICK(cellStyleCount);

                // фактическое количество
                cellStyleCount.setAlignment(HorizontalAlignment.CENTER);
                Cell cellCount = sheet.getRow(i.get()).createCell(4);
                cellCount.setCellValue(count.get());
                cellCount.setCellStyle(cellStyleCount);

                Cell cellPohibka = sheet.getRow(i.get()).createCell(5);
                cellPohibka.setCellFormula("E" + (i.get() + 1) + "-" + "D" + (i.get() + 1));
                cellPohibka.setCellStyle(cellStyleCount);

                Cell cellD = xssfSheet.getRow(44).createCell(9);
                cellD.setCellValue(durationVideo.getValue());
                Cell cellSum = xssfSheet.getRow(46).createCell(9);
                cellSum.setCellFormula("SUM(D13:D43)");
                Cell cellRound = xssfSheet.getRow(45).createCell(9);
                cellRound.setCellFormula("ROUND(J45*J47/60,2)");

                i.getAndIncrement();
            });
            workbook.removeSheetAt(1);
            sheet.getRow(26).createCell(1).setCellValue("Директор " + nameDir.getValue() + "  _______________ м.п.");
            sheet.getRow(27).createCell(1).setCellValue("Дата: " + end.getValue());
            File fileSave = new File(STORAGE +"report/" + nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue() + ".xlsx");
            FileOutputStream out = new FileOutputStream(fileSave);
            workbook.write(out);
            // Write File
            StreamResource resource = new StreamResource(nameTextField.getValue() + "_" + start.getValue() + "_" + end.getValue() + "_" + template.getValue() + ".xlsx",
                    () -> {
                        try {
                            return new FileInputStream(fileSave);
                        } catch (IOException e) {
                            getLogger().error("" + e);
                            return null;
                        }
                    });

            pdf.setHref(resource);
            pdf.getElement().setAttribute("download", true);

            com.vaadin.flow.component.page.Page page = UI.getCurrent().getPage();
            page.executeJs("document.getElementById('" + "reportDownload" + "').click();");

            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private  int countShow(String filePatch){
        File file = new File(filePatch);
        int count = 0;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));

            String st;
            while ((st = br.readLine()) != null) {
                if (st.contains(nameTextField.getValue())) {
                    count++;
                }
            }
        }catch (FileNotFoundException e) {
            getLogger().error("" + e);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            }catch (IOException e) {
                getLogger().error("" + e);
            }
        }
        return count;
    }

    private  EtherTC countTC_TV(String filePatch, String station){
        EtherTC etherTC = new EtherTC();
        etherTC.setStationName(station);

        File file = new File(filePatch);
        BufferedReader br = null;

        int count = 0;
        try {
            br = new BufferedReader(new FileReader(file));

            String st;
            while ((st = br.readLine()) != null) {
                if (st.contains("Video File:")) {
                    if (st.contains(nameTextField.getValue())) {
                        if (st.contains("07:")) {
                            int i = etherTC.getA7();
                            etherTC.setA7(++i);
                            count++;
                        } else if (st.contains("08:")) {
                            int i = etherTC.getA8();
                            etherTC.setA8(++i);
                            count++;
                        }else if (st.contains("09:")) {
                            int i = etherTC.getA9();
                            etherTC.setA9(++i);
                            count++;
                        }else if (st.contains("10:")) {
                            int i = etherTC.getA10();
                            etherTC.setA10(++i);
                            count++;
                        }else if (st.contains("11:")) {
                            int i = etherTC.getA11();
                            etherTC.setA11(++i);
                            count++;
                        }else if (st.contains("12:")) {
                            int i = etherTC.getA12();
                            etherTC.setA12(++i);
                            count++;
                        }else if (st.contains("13:")) {
                            int i = etherTC.getA13();
                            etherTC.setA13(++i);
                            count++;
                        }else if (st.contains("14:")) {
                            int i = etherTC.getA14();
                            etherTC.setA14(++i);
                            count++;
                        }else if (st.contains("15:")) {
                            int i = etherTC.getA15();
                            etherTC.setA15(++i);
                            count++;
                        }else if (st.contains("16:")) {
                            int i = etherTC.getA16();
                            etherTC.setA16(++i);
                            count++;
                        }else if (st.contains("17:")) {
                            int i = etherTC.getA17();
                            etherTC.setA17(++i);
                            count++;
                        }else if (st.contains("18:")) {
                            int i = etherTC.getA18();
                            etherTC.setA18(++i);
                            count++;
                        }else if (st.contains("19:")) {
                            int i = etherTC.getA19();
                            etherTC.setA19(++i);
                            count++;
                        }else if (st.contains("20:")) {
                            int i = etherTC.getA20();
                            etherTC.setA20(++i);
                            count++;
                        }else if (st.contains("21:")) {
                            int i = etherTC.getA21();
                            etherTC.setA21(++i);
                            count++;
                        }else if (st.contains("22:")) {
                            int i = etherTC.getA22();
                            etherTC.setA22(++i);
                            count++;
                        }
                    }
                }
            }

            etherTC.setCount(count);
        }catch (FileNotFoundException e) {
            getLogger().error("" + e);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            }catch (IOException e) {
                getLogger().error("" + e);
            }
        }
        return etherTC;
    }
    private  EtherTC countTC_Panel(String filePatch, String station){
        EtherTC etherTC = new EtherTC();
        etherTC.setStationName(station);

        File file = new File(filePatch);
        BufferedReader br = null;

        int count = 0;
        try {
            br = new BufferedReader(new FileReader(file));

            String st;
            while ((st = br.readLine()) != null) {
                if (st.contains("Panel File:")) {
                    if (st.contains(nameTextField.getValue())) {
                        if (st.contains("07:")) {
                            int i = etherTC.getA7();
                            etherTC.setA7(++i);
                            count++;
                        } else if (st.contains("08:")) {
                            int i = etherTC.getA8();
                            etherTC.setA8(++i);
                            count++;
                        }else if (st.contains("09:")) {
                            int i = etherTC.getA9();
                            etherTC.setA9(++i);
                            count++;
                        }else if (st.contains("10:")) {
                            int i = etherTC.getA10();
                            etherTC.setA10(++i);
                            count++;
                        }else if (st.contains("11:")) {
                            int i = etherTC.getA11();
                            etherTC.setA11(++i);
                            count++;
                        }else if (st.contains("12:")) {
                            int i = etherTC.getA12();
                            etherTC.setA12(++i);
                            count++;
                        }else if (st.contains("13:")) {
                            int i = etherTC.getA13();
                            etherTC.setA13(++i);
                            count++;
                        }else if (st.contains("14:")) {
                            int i = etherTC.getA14();
                            etherTC.setA14(++i);
                            count++;
                        }else if (st.contains("15:")) {
                            int i = etherTC.getA15();
                            etherTC.setA15(++i);
                            count++;
                        }else if (st.contains("16:")) {
                            int i = etherTC.getA16();
                            etherTC.setA16(++i);
                            count++;
                        }else if (st.contains("17:")) {
                            int i = etherTC.getA17();
                            etherTC.setA17(++i);
                            count++;
                        }else if (st.contains("18:")) {
                            int i = etherTC.getA18();
                            etherTC.setA18(++i);
                            count++;
                        }else if (st.contains("19:")) {
                            int i = etherTC.getA19();
                            etherTC.setA19(++i);
                            count++;
                        }else if (st.contains("20:")) {
                            int i = etherTC.getA20();
                            etherTC.setA20(++i);
                            count++;
                        }else if (st.contains("21:")) {
                            int i = etherTC.getA21();
                            etherTC.setA21(++i);
                            count++;
                        }else if (st.contains("22:")) {
                            int i = etherTC.getA22();
                            etherTC.setA22(++i);
                            count++;
                        }
                    }
                }
            }

            etherTC.setCount(count);
        }catch (FileNotFoundException e) {
            getLogger().error("" + e);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            }catch (IOException e) {
                getLogger().error("" + e);
            }
        }
        return etherTC;
    }

    private  int countShowVisaTV(String filePatch){
        File file = new File(filePatch);
        int count = 0;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));

            String st;
            while ((st = br.readLine()) != null) {
                if (st.contains(nameTextField.getValue())) {
                    String[] str = st.split(" Video ");

                    if (str.length > 1) {
                        //default, ISO_LOCAL_DATE
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                        System.out.println(str[0]);
                        LocalDateTime localDateTime = LocalDateTime.parse(str[0], formatter);
                        if (localDateTime.getHour() > 8) {
                            if (localDateTime.getHour() < 17) {
                                count++;
                            }
                        }
                    }
                }
            }
        }catch (FileNotFoundException e) {
            getLogger().error("" + e);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            }catch (IOException e) {
                getLogger().error("" + e);
            }
        }
        return count;
    }



    private void createGridStation(){
        stationTreeGrid = new RecursiveSelectTreeGrid<>();
        stationTreeGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        stationTreeGrid.setHeight("100%");
        stationTreeGrid.setWidth("50%");
        stationTreeGrid.addHierarchyColumn(Station::getName).setHeader("Станции").setResizable(true);

        HierarchicalDataProvider<Station, Void> dataProvider =
                new AbstractBackEndHierarchicalDataProvider<>() {

                    @Override
                    public int getChildCount(HierarchicalQuery<Station, Void> query) {
                        return (int) stationService.getChildCount(query.getParent());
                    }

                    @Override
                    public boolean hasChildren(Station item) {
                        return stationService.hasChildren(item);
                    }

                    @Override
                    protected Stream<Station> fetchChildrenFromBackEnd(
                            HierarchicalQuery<Station, Void> query) {
                        return stationService.fetchChildren(query.getParent()).stream().
                                sorted(Comparator.comparing(Station::getName));
                    }
                };

        stationTreeGrid.setDataProvider(dataProvider);
    }

}
